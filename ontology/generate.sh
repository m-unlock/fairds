#!/bin/bash
#============================================================================
#title          :FAIR Ontology used in unlock
#description    :FAIR ontology used within iRODS
#author         :Jasper Koehorst
#date           :2023
#version        :0.0.1
#============================================================================

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

git -C $DIR pull

# Get the code generator
EMPUSA=$DIR/EmpusaCodeGen.jar

if test -f "$EMPUSA"; then
    echo ""
else
    wget -nc http://download.systemsbiology.nl/empusa/EmpusaCodeGen.jar -O $DIR/EmpusaCodeGen.jar
    EMPUSA=$DIR/EmpusaCodeGen.jar
fi

java -jar $EMPUSA -i $DIR/FAIRDS.ttl -o $DIR/API -excludeBaseFiles
