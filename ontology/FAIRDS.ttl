@prefix : <http://fairbydesign.nl/> .
@prefix dc: <http://purl.org/dc/terms/> .
@prefix wv: <http://vocab.org/waiver/terms/norms> .
@prefix gen: <http://empusa.org/0.1/> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix xml: <http://www.w3.org/XML/1998/namespace> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix fair: <http://fairbydesign.nl/ontology/> .
@prefix jerm: <http://jermontology.org/ontology/JERMOntology#> .
@prefix ppeo: <http://purl.org/ppeo/PPEO.owl#> .
@prefix prov: <http://www.w3.org/ns/prov#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix schema: <http://schema.org/> .
@prefix dcmitype: <http://purl.org/dc/dcmitype/> .
@base <http://fairbydesign.nl/> .

<http://fairbydesign.nl/> rdf:type owl:Ontology .

#################################################################
#    Annotation properties
#################################################################

###  http://empusa.org/0.1/propertyDefinitions
gen:propertyDefinitions rdf:type owl:AnnotationProperty .


###  http://purl.org/dc/elements/1.1/description
<http://purl.org/dc/elements/1.1/description> rdf:type owl:AnnotationProperty .


###  http://purl.org/dc/elements/1.1/identifier
<http://purl.org/dc/elements/1.1/identifier> rdf:type owl:AnnotationProperty .


###  http://purl.org/ppeo/PPEO.owl#hasExactSynonym
ppeo:hasExactSynonym rdf:type owl:AnnotationProperty .


###  http://purl.org/ppeo/PPEO.owl#hasISASynonym
ppeo:hasISASynonym rdf:type owl:AnnotationProperty .


###  http://www.w3.org/2004/02/skos/core#definition
<http://www.w3.org/2004/02/skos/core#definition> rdf:type owl:AnnotationProperty .


###  http://www.w3.org/2004/02/skos/core#exactMatch
<http://www.w3.org/2004/02/skos/core#exactMatch> rdf:type owl:AnnotationProperty .


#################################################################
#    Classes
#################################################################

###  http://empusa.org/0.1/EnumeratedValues
gen:EnumeratedValues rdf:type owl:Class .


###  http://jermontology.org/ontology/JERMOntology#Assay
jerm:Assay rdf:type owl:Class ;
           rdfs:subClassOf jerm:process ;
           gen:propertyDefinitions """#* SOPs used to generate the assay 
mixs:sop IRI*;
#* Entity linking other parts
jerm:hasPart @jerm:Data_sample*;
#* Name of a given object
schema:name xsd:String*;
#* Assay type used
schema:additionalType IRI*"""@en ;
           rdfs:comment "Specific, individual experiments, measurements, or modelling tasks. This may be for example a rnaseq experiment, or a flux balance analysis." .


###  http://jermontology.org/ontology/JERMOntology#Asset
jerm:Asset rdf:type owl:Class ;
           rdfs:subClassOf jerm:Information_entity ;
           gen:propertyDefinitions """#* identifier
schema:identifier xsd:String;
#* Name of a given object
schema:name xsd:String?;
#* URL path of the file
schema:contentUrl IRI;
#* Base 64 checksum
fair:base64 xsd:String?;
#* SHA-256 checksum
schema:sha256 xsd:String?;
#* File size in (mega/kilo) bytes.
schema:contentSize xsd:Long?;""" ;
           rdfs:comment "all shareable entities are kinds of Assets" .


###  http://jermontology.org/ontology/JERMOntology#Data_sample
jerm:Data_sample rdf:type owl:Class ;
                 rdfs:subClassOf jerm:Asset ;
                 rdfs:comment "Examples include lists, tables, and databases. A dataset may be useful for direct machine processing." ,
                              "MIAPPE DATA FILE" ;
                 <http://www.w3.org/2004/02/skos/core#definition> "Data encoded in a defined structure." .


###  http://jermontology.org/ontology/JERMOntology#Information_entity
jerm:Information_entity rdf:type owl:Class ;
                        rdfs:subClassOf prov:Entity .


###  http://jermontology.org/ontology/JERMOntology#Investigation
jerm:Investigation rdf:type owl:Class ;
                   rdfs:subClassOf jerm:process ;
                   gen:propertyDefinitions """#* Entity linking other parts
jerm:hasPart @jerm:Study*;
#* Title of a given section
schema:title xsd:String;"""@en ;
                   rdfs:comment """A high level description of the overall area of research. This may be the overall aims of the project, as stated on your grant. If your project has several subprojects that do not share any data, you should define an investigation for each.

Example: Analysis of Central Carbon Metabolism of Sulfolobus solfataricus under varying temperatures"""@en ;
                   rdfs:label "Investigation"@en ;
                   <http://www.w3.org/2004/02/skos/core#exactMatch> jerm:Investigation .


###  http://jermontology.org/ontology/JERMOntology#Material_entity
jerm:Material_entity rdf:type owl:Class ;
                     gen:propertyDefinitions """#* Accountable person for the study performed
schema:accountablePerson @schema:Person?;
# Researcher that has access to this process
schema:contributor @schema:Person*;
#* Description of a given section
schema:description xsd:String;
#* Name of a given object
schema:name xsd:String;
#* identifier
schema:identifier xsd:String;
#* URL path of the file
schema:contentUrl IRI?;""" ;
                     rdfs:label "Material Entity" .


###  http://jermontology.org/ontology/JERMOntology#Project
jerm:Project rdf:type owl:Class ;
             rdfs:subClassOf jerm:Information_entity ;
             gen:propertyDefinitions """#* The person who  is the owner of a process (Principal Investigator)
schema:owns @schema:Person*;
# Main contact person for the investigation
base:contactPerson @schema:Person?;
# Researcher that has access to this process
base:researcher @schema:Person*;
#* Title of a given section
schema:title xsd:String;
#* Entity linking other parts
jerm:hasPart @jerm:Investigation*;
#* identifier
schema:identifier xsd:String;
#* Description of a given section
schema:description xsd:String;"""@en ;
             rdfs:comment "A research programme or a grant-funded programme funding the investigations." .


###  http://jermontology.org/ontology/JERMOntology#Sample
jerm:Sample rdf:type owl:Class ;
            rdfs:subClassOf jerm:Material_entity ;
            gen:propertyDefinitions """#* Entity linking other parts
jerm:hasPart @jerm:Assay*;"""@en ;
            rdfs:label "Sample" ;
            <http://www.w3.org/2004/02/skos/core#definition> "Information about the sample" .


###  http://jermontology.org/ontology/JERMOntology#Study
jerm:Study rdf:type owl:Class ;
           rdfs:subClassOf jerm:process ;
           gen:propertyDefinitions """#* Entity linking other parts
jerm:hasPart @ppeo:observation_unit*;
#* Title of a given section
schema:title xsd:String;"""@en ;
           rdfs:comment """A particular biological hypothesis, which you are planning to test in various ways, using various techniques, which could be experimental, informatics, modelling, or a mixture.

Comparison of S. solfataricus grown at 70 and 80 degrees"""@en ;
           rdfs:label "Study"@en .


###  http://jermontology.org/ontology/JERMOntology#process
jerm:process rdf:type owl:Class ;
             gen:propertyDefinitions """#* Researcher that has access to this process
schema:contributor @schema:Person*;
#* Description of a given section
schema:description xsd:String;
#* identifier
schema:identifier xsd:String;
#* Accountable person for the study performed
schema:accountablePerson @schema:Person?;
#* URL path of the file
schema:contentUrl IRI?;
#* Dataset associated with any of the ISA levels
schema:dataset @jerm:Data_sample*;"""@en ;
             rdfs:comment "experimental procedures" ;
             rdfs:label "Process" .


###  http://purl.org/ppeo/PPEO.owl#observation_unit
ppeo:observation_unit rdf:type owl:Class ;
                      rdfs:subClassOf jerm:process ;
                      gen:propertyDefinitions """#* Entity linking other parts
jerm:hasPart @jerm:Sample*;
#* Name of a given object
schema:name xsd:String;"""@en ;
                      rdfs:comment "Observation units are objects that are subject to instances of observation and measurement. An observation unit comprises one or more plants, and/or their environment. There can be pure environment observation units with no plants."@en ;
                      rdfs:label "Observation unit" ;
                      <http://www.w3.org/2004/02/skos/core#definition> "" .


###  http://schema.org/Organization
schema:Organization rdf:type owl:Class ;
                    gen:propertyDefinitions """#* The legal name of the organization
schema:legalName xsd:String?;
schema:sameAs IRI?;""" ;
                    rdfs:comment "The Organization class represents a kind of Agent corresponding to social instititutions such as companies, societies etc." ;
                    rdfs:label "schema:Organization" .


###  http://schema.org/Person
schema:Person rdf:type owl:Class ;
              gen:propertyDefinitions """#* The first name of the person
schema:givenName xsd:String;
#* The first name of the person
schema:familyName xsd:String;
#* mail address use  URI scheme (see RFC 2368). 
schema:email IRI;
#* The unique identifier to identify the scientist
base:orcid IRI?;
#* The job title of the person (for example, Financial Manager).
schema:jobTitle xsd:String?;
#* To which organization(s) does this person belongs to?
schema:memberOf @schema:Organization*;
#* To which department the person belongs to
schema:department xsd:String?;"""@en ;
              rdfs:comment "A person (alive, dead, undead, or fictional)." ;
              rdfs:label "schema:Person" ;
              <http://www.w3.org/2004/02/skos/core#definition> "Information related to a Person" .


###  http://www.w3.org/ns/prov#Entity
prov:Entity rdf:type owl:Class .


###  Generated by the OWL API (version 4.5.26.2023-07-17T20:34:13Z) https://github.com/owlcs/owlapi
