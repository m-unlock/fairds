# Remove fairds.jar
rm fairds.jar
# Description: Build the docker image locally and push it to the docker registry
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
# Remove all jar files from the build folder to ensure that the correct jar file is used
rm $DIR/../build/libs/*.jar
# Build the jar file
$DIR/../build.sh

# Obtain the branch name
gitcb=$(git rev-parse --abbrev-ref HEAD)
# Change main to latest
branch=$(git rev-parse --abbrev-ref HEAD)
branch=${gitcb/main/latest}

cp $DIR/../build/libs/fairds-*-"$gitcb".jar $DIR/fairds.jar
# cp $DIR/../src/main/resources/metadata.xlsx  $DIR/metadata.xlsx

# If branch is main
if [ "$branch" == "main" ]; then
    echo "Downloading metadata.xlsx"
    wget http://download.systemsbiology.nl/unlock/metadata.xlsx -O $DIR/metadata.xlsx
else
    echo "Downloading metadata-dev.xlsx"    
    wget http://download.systemsbiology.nl/unlock/metadata-dev.xlsx -O $DIR/metadata.xlsx
fi

docker login git@docker-registry.wur.nl  

docker pull docker-registry.wur.nl/m-unlock/docker/fairds:"$branch"

docker build --platform linux/amd64 -t docker-registry.wur.nl/m-unlock/docker/fairds:"$branch" .

docker push docker-registry.wur.nl/m-unlock/docker/fairds:"$branch"
