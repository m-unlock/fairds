# The build-stage image:
FROM condaforge/mambaforge AS build

# Install mamba
# RUN conda install -c conda-forge mamba && \
    # conda install libarchive==3.5.2 -c conda-forge
    # Was needed due to a bug in conda https://github.com/mamba-org/mamba/issues/1775


# Install the package as normal:
COPY environment.yml .
RUN mamba env create -f environment.yml

# Install conda-pack:
RUN mamba install -c conda-forge conda-pack

# Use conda-pack to create a standalone environment 
# in /venv:
RUN conda-pack -n start -o /tmp/env.tar && \
  mkdir /venv && cd /venv && tar xf /tmp/env.tar && \
  rm /tmp/env.tar

# We've put venv in same path it'll be in final image,
# so now fix up paths:
RUN /venv/bin/conda-unpack

# The runtime-stage image; we can use Debian as the
# base image since the Conda env also includes Python
# for us.
FROM debian:buster AS runtime

SHELL ["/bin/bash", "-c"]

# Copy /venv from the previous stage:
# Reset cache to avoid caching the venv
ARG CACHEBUST=1

COPY --from=build /venv /venv
COPY start.sh /start.sh

# Custom things here
COPY metadata.xlsx metadata.xlsx
COPY fairds.jar fairds.jar
# Add the metadata to the fairds_storage folder
RUN mkdir fairds_storage/ && mv metadata.xlsx ./fairds_storage/

ENTRYPOINT ["bash", "/start.sh"]
