package org.purl.ppeo.PPEO.owl.domain;

import java.lang.String;
import java.util.List;
import org.jermontology.ontology.JERMOntology.domain.Data_sample;
import org.jermontology.ontology.JERMOntology.domain.Sample;
import org.jermontology.ontology.JERMOntology.domain.process;
import org.schema.domain.Person;

/**
 * Code generated from http://purl.org/ppeo/PPEO.owl# ontology
 */
public interface observation_unit extends process {
  String getIdentifier();

  void setIdentifier(String val);

  void remContributor(Person val);

  List<? extends Person> getAllContributor();

  void addContributor(Person val);

  String getDescription();

  void setDescription(String val);

  String getName();

  void setName(String val);

  String getContentUrl();

  void setContentUrl(String val);

  void remDataset(Data_sample val);

  List<? extends Data_sample> getAllDataset();

  void addDataset(Data_sample val);

  Person getAccountablePerson();

  void setAccountablePerson(Person val);

  void remHasPart(Sample val);

  List<? extends Sample> getAllHasPart();

  void addHasPart(Sample val);
}
