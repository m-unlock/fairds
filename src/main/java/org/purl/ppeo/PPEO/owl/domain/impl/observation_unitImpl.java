package org.purl.ppeo.PPEO.owl.domain.impl;

import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.jermontology.ontology.JERMOntology.domain.Data_sample;
import org.jermontology.ontology.JERMOntology.domain.Sample;
import org.jermontology.ontology.JERMOntology.domain.impl.processImpl;
import org.purl.ppeo.PPEO.owl.domain.observation_unit;
import org.schema.domain.Person;

/**
 * Code generated from http://purl.org/ppeo/PPEO.owl# ontology
 */
public class observation_unitImpl extends processImpl implements observation_unit {
  public static final String TypeIRI = "http://purl.org/ppeo/PPEO.owl#observation_unit";

  protected observation_unitImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static observation_unit make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new observation_unitImpl(domain,resource);
      }
      else {
        toRet = domain.getObject(resource,observation_unit.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,observation_unit.class,false);
          if(toRet == null) {
            toRet = new observation_unitImpl(domain,resource);
          }
        }
        else if(!(toRet instanceof observation_unit)) {
          throw new RuntimeException("Instance of org.purl.ppeo.PPEO.owl.domain.impl.observation_unitImpl expected");
        }
      }
      return (observation_unit)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://schema.org/identifier");
    this.checkCardMin1("http://schema.org/description");
    this.checkCardMin1("http://schema.org/name");
  }

  public String getIdentifier() {
    return this.getStringLit("http://schema.org/identifier",false);
  }

  public void setIdentifier(String val) {
    this.setStringLit("http://schema.org/identifier",val);
  }

  public void remContributor(Person val) {
    this.remRef("http://schema.org/contributor",val,true);
  }

  public List<? extends Person> getAllContributor() {
    return this.getRefSet("http://schema.org/contributor",true,Person.class);
  }

  public void addContributor(Person val) {
    this.addRef("http://schema.org/contributor",val);
  }

  public String getDescription() {
    return this.getStringLit("http://schema.org/description",false);
  }

  public void setDescription(String val) {
    this.setStringLit("http://schema.org/description",val);
  }

  public String getName() {
    return this.getStringLit("http://schema.org/name",false);
  }

  public void setName(String val) {
    this.setStringLit("http://schema.org/name",val);
  }

  public String getContentUrl() {
    return this.getExternalRef("http://schema.org/contentUrl",true);
  }

  public void setContentUrl(String val) {
    this.setExternalRef("http://schema.org/contentUrl",val);
  }

  public void remDataset(Data_sample val) {
    this.remRef("http://schema.org/dataset",val,true);
  }

  public List<? extends Data_sample> getAllDataset() {
    return this.getRefSet("http://schema.org/dataset",true,Data_sample.class);
  }

  public void addDataset(Data_sample val) {
    this.addRef("http://schema.org/dataset",val);
  }

  public Person getAccountablePerson() {
    return this.getRef("http://schema.org/accountablePerson",true,Person.class);
  }

  public void setAccountablePerson(Person val) {
    this.setRef("http://schema.org/accountablePerson",val,Person.class);
  }

  public void remHasPart(Sample val) {
    this.remRef("http://jermontology.org/ontology/JERMOntology#hasPart",val,true);
  }

  public List<? extends Sample> getAllHasPart() {
    return this.getRefSet("http://jermontology.org/ontology/JERMOntology#hasPart",true,Sample.class);
  }

  public void addHasPart(Sample val) {
    this.addRef("http://jermontology.org/ontology/JERMOntology#hasPart",val);
  }
}
