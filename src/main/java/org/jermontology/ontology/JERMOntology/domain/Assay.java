package org.jermontology.ontology.JERMOntology.domain;

import java.lang.String;
import java.util.List;
import org.schema.domain.Person;

/**
 * Code generated from http://jermontology.org/ontology/JERMOntology# ontology
 */
public interface Assay extends process {
  String getIdentifier();

  void setIdentifier(String val);

  void remContributor(Person val);

  List<? extends Person> getAllContributor();

  void addContributor(Person val);

  String getDescription();

  void setDescription(String val);

  void remName(String val);

  List<? extends String> getAllName();

  void addName(String val);

  void remSop(String val);

  List<? extends String> getAllSop();

  void addSop(String val);

  String getContentUrl();

  void setContentUrl(String val);

  void remAdditionalType(String val);

  List<? extends String> getAllAdditionalType();

  void addAdditionalType(String val);

  void remDataset(Data_sample val);

  List<? extends Data_sample> getAllDataset();

  void addDataset(Data_sample val);

  Person getAccountablePerson();

  void setAccountablePerson(Person val);

  void remHasPart(Data_sample val);

  List<? extends Data_sample> getAllHasPart();

  void addHasPart(Data_sample val);
}
