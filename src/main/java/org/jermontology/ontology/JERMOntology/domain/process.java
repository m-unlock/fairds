package org.jermontology.ontology.JERMOntology.domain;

import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;
import org.schema.domain.Person;

/**
 * Code generated from http://jermontology.org/ontology/JERMOntology# ontology
 */
public interface process extends OWLThing {
  String getIdentifier();

  void setIdentifier(String val);

  void remContributor(Person val);

  List<? extends Person> getAllContributor();

  void addContributor(Person val);

  String getDescription();

  void setDescription(String val);

  String getContentUrl();

  void setContentUrl(String val);

  void remDataset(Data_sample val);

  List<? extends Data_sample> getAllDataset();

  void addDataset(Data_sample val);

  Person getAccountablePerson();

  void setAccountablePerson(Person val);
}
