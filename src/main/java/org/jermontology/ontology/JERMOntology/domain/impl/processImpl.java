package org.jermontology.ontology.JERMOntology.domain.impl;

import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThingImpl;
import org.apache.jena.rdf.model.Resource;
import org.jermontology.ontology.JERMOntology.domain.Data_sample;
import org.jermontology.ontology.JERMOntology.domain.process;
import org.schema.domain.Person;

/**
 * Code generated from http://jermontology.org/ontology/JERMOntology# ontology
 */
public class processImpl extends OWLThingImpl implements process {
  public static final String TypeIRI = "http://jermontology.org/ontology/JERMOntology#process";

  protected processImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static process make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new processImpl(domain,resource);
      }
      else {
        toRet = domain.getObject(resource,process.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,process.class,false);
          if(toRet == null) {
            toRet = new processImpl(domain,resource);
          }
        }
        else if(!(toRet instanceof process)) {
          throw new RuntimeException("Instance of org.jermontology.ontology.JERMOntology.domain.impl.processImpl expected");
        }
      }
      return (process)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://schema.org/identifier");
    this.checkCardMin1("http://schema.org/description");
  }

  public String getIdentifier() {
    return this.getStringLit("http://schema.org/identifier",false);
  }

  public void setIdentifier(String val) {
    this.setStringLit("http://schema.org/identifier",val);
  }

  public void remContributor(Person val) {
    this.remRef("http://schema.org/contributor",val,true);
  }

  public List<? extends Person> getAllContributor() {
    return this.getRefSet("http://schema.org/contributor",true,Person.class);
  }

  public void addContributor(Person val) {
    this.addRef("http://schema.org/contributor",val);
  }

  public String getDescription() {
    return this.getStringLit("http://schema.org/description",false);
  }

  public void setDescription(String val) {
    this.setStringLit("http://schema.org/description",val);
  }

  public String getContentUrl() {
    return this.getExternalRef("http://schema.org/contentUrl",true);
  }

  public void setContentUrl(String val) {
    this.setExternalRef("http://schema.org/contentUrl",val);
  }

  public void remDataset(Data_sample val) {
    this.remRef("http://schema.org/dataset",val,true);
  }

  public List<? extends Data_sample> getAllDataset() {
    return this.getRefSet("http://schema.org/dataset",true,Data_sample.class);
  }

  public void addDataset(Data_sample val) {
    this.addRef("http://schema.org/dataset",val);
  }

  public Person getAccountablePerson() {
    return this.getRef("http://schema.org/accountablePerson",true,Person.class);
  }

  public void setAccountablePerson(Person val) {
    this.setRef("http://schema.org/accountablePerson",val,Person.class);
  }
}
