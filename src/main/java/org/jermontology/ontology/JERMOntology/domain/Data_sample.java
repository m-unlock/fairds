package org.jermontology.ontology.JERMOntology.domain;

import java.lang.Long;
import java.lang.String;

/**
 * Code generated from http://jermontology.org/ontology/JERMOntology# ontology
 */
public interface Data_sample extends Asset {
  String getIdentifier();

  void setIdentifier(String val);

  String getName();

  void setName(String val);

  String getBase64();

  void setBase64(String val);

  String getContentUrl();

  void setContentUrl(String val);

  String getSha256();

  void setSha256(String val);

  Long getContentSize();

  void setContentSize(Long val);
}
