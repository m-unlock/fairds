package org.jermontology.ontology.JERMOntology.domain;

import java.lang.String;
import java.util.List;
import org.schema.domain.Person;

/**
 * Code generated from http://jermontology.org/ontology/JERMOntology# ontology
 */
public interface Project extends Information_entity {
  String getTitle();

  void setTitle(String val);

  String getIdentifier();

  void setIdentifier(String val);

  String getDescription();

  void setDescription(String val);

  Person getContactPerson();

  void setContactPerson(Person val);

  void remOwns(Person val);

  List<? extends Person> getAllOwns();

  void addOwns(Person val);

  void remResearcher(Person val);

  List<? extends Person> getAllResearcher();

  void addResearcher(Person val);

  void remHasPart(Investigation val);

  List<? extends Investigation> getAllHasPart();

  void addHasPart(Investigation val);
}
