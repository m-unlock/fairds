package org.jermontology.ontology.JERMOntology.domain.impl;

import java.lang.Long;
import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.jermontology.ontology.JERMOntology.domain.Data_sample;

/**
 * Code generated from http://jermontology.org/ontology/JERMOntology# ontology
 */
public class Data_sampleImpl extends AssetImpl implements Data_sample {
  public static final String TypeIRI = "http://jermontology.org/ontology/JERMOntology#Data_sample";

  protected Data_sampleImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Data_sample make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new Data_sampleImpl(domain,resource);
      }
      else {
        toRet = domain.getObject(resource,Data_sample.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Data_sample.class,false);
          if(toRet == null) {
            toRet = new Data_sampleImpl(domain,resource);
          }
        }
        else if(!(toRet instanceof Data_sample)) {
          throw new RuntimeException("Instance of org.jermontology.ontology.JERMOntology.domain.impl.Data_sampleImpl expected");
        }
      }
      return (Data_sample)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://schema.org/identifier");
    this.checkCardMin1("http://schema.org/contentUrl");
  }

  public String getIdentifier() {
    return this.getStringLit("http://schema.org/identifier",false);
  }

  public void setIdentifier(String val) {
    this.setStringLit("http://schema.org/identifier",val);
  }

  public String getName() {
    return this.getStringLit("http://schema.org/name",true);
  }

  public void setName(String val) {
    this.setStringLit("http://schema.org/name",val);
  }

  public String getBase64() {
    return this.getStringLit("http://fairbydesign.nl/ontology/base64",true);
  }

  public void setBase64(String val) {
    this.setStringLit("http://fairbydesign.nl/ontology/base64",val);
  }

  public String getContentUrl() {
    return this.getExternalRef("http://schema.org/contentUrl",false);
  }

  public void setContentUrl(String val) {
    this.setExternalRef("http://schema.org/contentUrl",val);
  }

  public String getSha256() {
    return this.getStringLit("http://schema.org/sha256",true);
  }

  public void setSha256(String val) {
    this.setStringLit("http://schema.org/sha256",val);
  }

  public Long getContentSize() {
    return this.getLongLit("http://schema.org/contentSize",true);
  }

  public void setContentSize(Long val) {
    this.setLongLit("http://schema.org/contentSize",val);
  }
}
