package org.jermontology.ontology.JERMOntology.domain.impl;

import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.jermontology.ontology.JERMOntology.domain.Assay;
import org.jermontology.ontology.JERMOntology.domain.Data_sample;
import org.schema.domain.Person;

/**
 * Code generated from http://jermontology.org/ontology/JERMOntology# ontology
 */
public class AssayImpl extends processImpl implements Assay {
  public static final String TypeIRI = "http://jermontology.org/ontology/JERMOntology#Assay";

  protected AssayImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Assay make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new AssayImpl(domain,resource);
      }
      else {
        toRet = domain.getObject(resource,Assay.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Assay.class,false);
          if(toRet == null) {
            toRet = new AssayImpl(domain,resource);
          }
        }
        else if(!(toRet instanceof Assay)) {
          throw new RuntimeException("Instance of org.jermontology.ontology.JERMOntology.domain.impl.AssayImpl expected");
        }
      }
      return (Assay)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://schema.org/identifier");
    this.checkCardMin1("http://schema.org/description");
  }

  public String getIdentifier() {
    return this.getStringLit("http://schema.org/identifier",false);
  }

  public void setIdentifier(String val) {
    this.setStringLit("http://schema.org/identifier",val);
  }

  public void remContributor(Person val) {
    this.remRef("http://schema.org/contributor",val,true);
  }

  public List<? extends Person> getAllContributor() {
    return this.getRefSet("http://schema.org/contributor",true,Person.class);
  }

  public void addContributor(Person val) {
    this.addRef("http://schema.org/contributor",val);
  }

  public String getDescription() {
    return this.getStringLit("http://schema.org/description",false);
  }

  public void setDescription(String val) {
    this.setStringLit("http://schema.org/description",val);
  }

  public void remName(String val) {
    this.remStringLit("http://schema.org/name",val,true);
  }

  public List<? extends String> getAllName() {
    return this.getStringLitSet("http://schema.org/name",true);
  }

  public void addName(String val) {
    this.addStringLit("http://schema.org/name",val);
  }

  public void remSop(String val) {
    this.remExternalRef("mixs:sop",val,true);
  }

  public List<? extends String> getAllSop() {
    return this.getExternalRefSet("mixs:sop",true);
  }

  public void addSop(String val) {
    this.addExternalRef("mixs:sop",val);
  }

  public String getContentUrl() {
    return this.getExternalRef("http://schema.org/contentUrl",true);
  }

  public void setContentUrl(String val) {
    this.setExternalRef("http://schema.org/contentUrl",val);
  }

  public void remAdditionalType(String val) {
    this.remExternalRef("http://schema.org/additionalType",val,true);
  }

  public List<? extends String> getAllAdditionalType() {
    return this.getExternalRefSet("http://schema.org/additionalType",true);
  }

  public void addAdditionalType(String val) {
    this.addExternalRef("http://schema.org/additionalType",val);
  }

  public void remDataset(Data_sample val) {
    this.remRef("http://schema.org/dataset",val,true);
  }

  public List<? extends Data_sample> getAllDataset() {
    return this.getRefSet("http://schema.org/dataset",true,Data_sample.class);
  }

  public void addDataset(Data_sample val) {
    this.addRef("http://schema.org/dataset",val);
  }

  public Person getAccountablePerson() {
    return this.getRef("http://schema.org/accountablePerson",true,Person.class);
  }

  public void setAccountablePerson(Person val) {
    this.setRef("http://schema.org/accountablePerson",val,Person.class);
  }

  public void remHasPart(Data_sample val) {
    this.remRef("http://jermontology.org/ontology/JERMOntology#hasPart",val,true);
  }

  public List<? extends Data_sample> getAllHasPart() {
    return this.getRefSet("http://jermontology.org/ontology/JERMOntology#hasPart",true,Data_sample.class);
  }

  public void addHasPart(Data_sample val) {
    this.addRef("http://jermontology.org/ontology/JERMOntology#hasPart",val);
  }
}
