package org.jermontology.ontology.JERMOntology.domain.impl;

import java.lang.String;
import java.util.List;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.jermontology.ontology.JERMOntology.domain.Data_sample;
import org.jermontology.ontology.JERMOntology.domain.Study;
import org.purl.ppeo.PPEO.owl.domain.observation_unit;
import org.schema.domain.Person;

/**
 * Code generated from http://jermontology.org/ontology/JERMOntology# ontology
 */
public class StudyImpl extends processImpl implements Study {
  public static final String TypeIRI = "http://jermontology.org/ontology/JERMOntology#Study";

  protected StudyImpl(Domain domain, Resource resource) {
    super(domain,resource);
  }

  public static Study make(Domain domain, Resource resource, boolean direct) {
    synchronized(domain) {
      Object toRet = null;
      if(direct) {
        toRet = new StudyImpl(domain,resource);
      }
      else {
        toRet = domain.getObject(resource,Study.class);
        if(toRet == null) {
          toRet = domain.getObjectFromResource(resource,Study.class,false);
          if(toRet == null) {
            toRet = new StudyImpl(domain,resource);
          }
        }
        else if(!(toRet instanceof Study)) {
          throw new RuntimeException("Instance of org.jermontology.ontology.JERMOntology.domain.impl.StudyImpl expected");
        }
      }
      return (Study)toRet;
    }
  }

  public void validate() {
    super.validate();
    this.checkCardMin1("http://schema.org/identifier");
    this.checkCardMin1("http://schema.org/title");
    this.checkCardMin1("http://schema.org/description");
  }

  public String getIdentifier() {
    return this.getStringLit("http://schema.org/identifier",false);
  }

  public void setIdentifier(String val) {
    this.setStringLit("http://schema.org/identifier",val);
  }

  public String getTitle() {
    return this.getStringLit("http://schema.org/title",false);
  }

  public void setTitle(String val) {
    this.setStringLit("http://schema.org/title",val);
  }

  public void remContributor(Person val) {
    this.remRef("http://schema.org/contributor",val,true);
  }

  public List<? extends Person> getAllContributor() {
    return this.getRefSet("http://schema.org/contributor",true,Person.class);
  }

  public void addContributor(Person val) {
    this.addRef("http://schema.org/contributor",val);
  }

  public String getDescription() {
    return this.getStringLit("http://schema.org/description",false);
  }

  public void setDescription(String val) {
    this.setStringLit("http://schema.org/description",val);
  }

  public String getContentUrl() {
    return this.getExternalRef("http://schema.org/contentUrl",true);
  }

  public void setContentUrl(String val) {
    this.setExternalRef("http://schema.org/contentUrl",val);
  }

  public void remDataset(Data_sample val) {
    this.remRef("http://schema.org/dataset",val,true);
  }

  public List<? extends Data_sample> getAllDataset() {
    return this.getRefSet("http://schema.org/dataset",true,Data_sample.class);
  }

  public void addDataset(Data_sample val) {
    this.addRef("http://schema.org/dataset",val);
  }

  public Person getAccountablePerson() {
    return this.getRef("http://schema.org/accountablePerson",true,Person.class);
  }

  public void setAccountablePerson(Person val) {
    this.setRef("http://schema.org/accountablePerson",val,Person.class);
  }

  public void remHasPart(observation_unit val) {
    this.remRef("http://jermontology.org/ontology/JERMOntology#hasPart",val,true);
  }

  public List<? extends observation_unit> getAllHasPart() {
    return this.getRefSet("http://jermontology.org/ontology/JERMOntology#hasPart",true,observation_unit.class);
  }

  public void addHasPart(observation_unit val) {
    this.addRef("http://jermontology.org/ontology/JERMOntology#hasPart",val);
  }
}
