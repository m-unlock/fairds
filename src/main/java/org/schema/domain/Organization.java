package org.schema.domain;

import java.lang.String;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;

/**
 * Code generated from http://schema.org/ ontology
 */
public interface Organization extends OWLThing {
  String getSameAs();

  void setSameAs(String val);

  String getLegalName();

  void setLegalName(String val);
}
