package nl.fairbydesign.views.ena;

import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.NativeLabel;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.progressbar.ProgressBar;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.MemoryBuffer;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.StreamResource;
import nl.fairbydesign.Application;
import nl.fairbydesign.backend.WebGeneric;
import nl.fairbydesign.backend.ena.submissionxml.ENAGenerator;
import nl.fairbydesign.views.main.MainView;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.commons.lang3.StringUtils;
import org.jermontology.ontology.JERMOntology.domain.Assay;
import org.jermontology.ontology.JERMOntology.domain.Investigation;
import org.jermontology.ontology.JERMOntology.domain.Study;
import org.purl.ppeo.PPEO.owl.domain.observation_unit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Collection;

import static com.vaadin.flow.component.notification.Notification.Position.MIDDLE;
import static com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment.CENTER;

@Route(value = "ena", layout = MainView.class)
@PageTitle("ENA Submission")
@CssImport("./styles/views/empty/empty-view.css")
public class ENAView extends Div implements BeforeEnterObserver {
    private static final Logger logger = LoggerFactory.getLogger(ENAView.class);
    private Domain domain;

    @Override // inform the user about an authentication error
    public void beforeEnter(BeforeEnterEvent beforeEnterEvent) {
        if (!Application.config.getMenus().isEna()) {
            beforeEnterEvent.forwardTo("");
        }
    }
    public ENAView() {
        MemoryBuffer buffer = new MemoryBuffer();
        Upload upload = new Upload(buffer);
        upload.setMaxFiles(1);
        upload.setDropLabel(new NativeLabel("Upload a project RDF file in .ttl format"));

        TextArea logArea = new TextArea("Description");
        TextArea stacktraceArea = new TextArea("Stacktrace");
        Button stacktraceButton = new Button("Stacktrace");
        stacktraceButton.setVisible(false);
        stacktraceButton.setEnabled(false);

        logArea.setWidthFull();
        logArea.setPlaceholder("Log will appear here ...");
        logArea.setVisible(false);

        stacktraceArea.setWidthFull();
        stacktraceArea.setVisible(false);

        ProgressBar progressBar = new ProgressBar();
        progressBar.setIndeterminate(true);
        progressBar.setVisible(false);

        // Grid
        Grid<Assay> grid = new Grid<>(Assay.class, false);
        grid.setSelectionMode(Grid.SelectionMode.MULTI);
        grid.addColumn(Assay::getIdentifier).setHeader("Identifier").setAutoWidth(true);
//        grid.addColumn(org.jermontology.ontology.JERMOntology.domain.Assay::getName).setHeader("Name").setAutoWidth(true);
//        grid.addColumn(org.jermontology.ontology.JERMOntology.domain.Assay::getDescription).setHeader("Description").setAutoWidth(true);
        grid.setVisible(false);
        // Combobox
        ComboBox<nl.fairbydesign.backend.data.objects.jerm.Study> comboBox = new ComboBox<>("Studies");
        Collection<nl.fairbydesign.backend.data.objects.jerm.Study> studies = new ArrayList<>();
        comboBox.setItems(studies);
        comboBox.setItemLabelGenerator(nl.fairbydesign.backend.data.objects.jerm.Study::getIdentifier);
        comboBox.setVisible(false);
        comboBox.addValueChangeListener(listener -> {
            grid.setVisible(true);
            Study study = listener.getValue().getRDFObject();
            ArrayList<Assay> assays = new ArrayList<>();
            for (observation_unit observation_unit : study.getAllHasPart()) {
                for (org.jermontology.ontology.JERMOntology.domain.Sample sample : observation_unit.getAllHasPart()) {
                    for (Assay assay : sample.getAllHasPart()) {
                        assays.add(assay);
                    }

                }
            }
            grid.setItems(assays);
        });

        Button gridButton = new Button("Generate");
        gridButton.setVisible(false);

        grid.addSelectionListener(selectionEvent -> {
            gridButton.setVisible(selectionEvent.getAllSelectedItems().size() != 0);
        });

        gridButton.addClickListener(clicked -> {
            // Process selected items...
            File zipFile = ENAGenerator.start(domain, comboBox.getValue(), grid.getSelectedItems());
            if (zipFile.exists()) {
                Button downloadExcelMetadataButton = new Button("Download");
                add(downloadExcelMetadataButton);
                downloadExcelMetadataButton.addClickListener(downloadEvent -> {
                    downloadFunction(zipFile);
                });
                add(downloadExcelMetadataButton);
            }
        });

        add(upload);
        add(logArea);
        add(stacktraceButton);
        add(stacktraceArea);
        add(comboBox);
        add(grid);
        add(gridButton);

        stacktraceButton.addClickListener(clicked -> {
           if (stacktraceArea.isVisible()) {
               stacktraceArea.setVisible(false);
               stacktraceArea.setEnabled(false);
           } else {
               stacktraceArea.setVisible(true);
               stacktraceArea.setEnabled(true);
           }
        });

        upload.addFileRejectedListener(event -> Notification.show(event.getErrorMessage()));

        upload.addSucceededListener(succeededEvent -> {
            logArea.setValue("File " + buffer.getFileName() + " uploaded...");
            // When upload is successful start the Excel validator!
            if (!buffer.getFileName().endsWith("ttl")) {
                Notification notification = new Notification();
                notification.addThemeVariants(NotificationVariant.LUMO_ERROR);
                Div text = new Div(new Text("File format not accepted. Only validated TURTLE files (.ttl)"));
                Button closeButton = new Button(new Icon("lumo", "cross"));
                closeButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY_INLINE);
                closeButton.getElement().setAttribute("aria-label", "Close");
                closeButton.addClickListener(event -> {
                    notification.close();
                    // Refresh current page
                    UI.getCurrent().getPage().reload();
                });

                HorizontalLayout layout = new HorizontalLayout(text, closeButton);
                layout.setAlignItems(CENTER);
                notification.setPosition(MIDDLE);
                notification.add(layout);
                notification.open();
                try {
                    buffer.getInputStream().close();
                } catch (IOException e) {
                    logger.error("Upload closing stream failed: " + e.getMessage());
                }
            } else {
                try {
                    logArea.setVisible(true);
                    progressBar.setVisible(true);
                    // Load TURTLE FILE as memory and generate files
                    // Storage in a specified location with ena subfolder for the turtle file
                    Path path = new File(Application.commandOptions.storage + "/ena/").toPath();
                    path.toFile().mkdirs();
                    path = Files.createTempFile(path,"ena_", ".ttl");
                    Files.copy(buffer.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
                    domain = new Domain("file://" + path.toFile().getAbsolutePath());
                    logArea.setValue(logArea.getValue() + "\nFinished loading " + domain.getRDFSimpleCon().getModel().size() + " statements");
                    ArrayList<String> investigations = new ArrayList<>();
                    domain.getRDFSimpleCon().runQuery("getInvestigations.txt", true).iterator().
                            forEachRemaining(resultLine -> investigations.add(resultLine.getIRI("iri")));

                    for (String investigationIRI : investigations) {
                        Investigation investigation = domain.make(Investigation.class, investigationIRI);
                        for (Study study : investigation.getAllHasPart()) {
                            // Populate dropdown menu
                            nl.fairbydesign.backend.data.objects.jerm.Study studyX = new nl.fairbydesign.backend.data.objects.jerm.Study();
                            studyX.setIdentifier(study.getIdentifier());
                            studyX.setRDFObject(study);
                            studies.add(studyX);
                        }
                    }
                    if (studies.size() == 0) {
                        logArea.setValue("No studies found in the input file...");
                    } else {
                        logArea.setValue("Please select a study from the dropdown menu");
                    }
                    comboBox.setVisible(true);
                } catch (Exception e) {
                    stacktraceButton.setEnabled(true);
                    stacktraceArea.setValue(e.getMessage() + "\n\n" + StringUtils.join(e.getStackTrace(),"\n"));
                }
            }
        });

        // Set footer
        Html footerContent = WebGeneric.getTextFromResource("views/footer.html");
        Div footer = new Div();
        footer.add(footerContent);
        add(footer);
    }

    private InputStream createResource(File excelFile) {
        try {
            InputStream targetStream = new FileInputStream(excelFile);
            return targetStream;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void downloadFunction(File excelFile) {
        Button button = new Button("Hidden Download");
        button.setVisible(false);

        button.addClickListener(clicked -> {
            logger.info("Export ENA CLICKED!");

            StreamResource streamResource = new StreamResource(excelFile.getName(), () -> {
                try {
                    return new FileInputStream(excelFile.getPath());
                } catch (FileNotFoundException e) {
                    System.err.println("File not found: " + excelFile.getPath());
                    this.getUI().ifPresent(ui -> ui.access(() -> Notification.show("Failed to download file", 3000, Notification.Position.MIDDLE)));
                }
                return null;
            });
            if (streamResource != null) {
                Anchor link = new Anchor(streamResource, "SOME CONTENT");
                link.getElement().setAttribute("download", true);
                add(link);
                // Trigger the anchor link programmatically
                UI.getCurrent().getPage().executeJs("arguments[0].click()", link.getElement());
            }
        });
        button.clickInClient();
        button.click();
    }
}
