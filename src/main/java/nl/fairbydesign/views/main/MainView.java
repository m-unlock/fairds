package nl.fairbydesign.views.main;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.applayout.DrawerToggle;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.tabs.TabsVariant;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.RouterLink;
import com.vaadin.flow.server.StreamResource;
import com.vaadin.flow.server.VaadinSession;
import nl.fairbydesign.Application;
import nl.fairbydesign.backend.config.Config;
import nl.fairbydesign.backend.config.Menus;
import nl.fairbydesign.backend.credentials.Credentials;
import nl.fairbydesign.views.about.AboutView;
import nl.fairbydesign.views.authentication.LoginView;
import nl.fairbydesign.views.authentication.LogoutView;
import nl.fairbydesign.views.browser.BrowserView;
import nl.fairbydesign.views.diskusage.DiskUsageView;
import nl.fairbydesign.views.help.TermsView;
import nl.fairbydesign.views.organise.OrganiseView;
import nl.fairbydesign.views.configurator.ConfiguratorView;
import nl.fairbydesign.views.unlock.archive.Archive;
import nl.fairbydesign.views.validation.ValidationView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Optional;

/**
 * The main view is a top-level placeholder for all other views.
 */
@JsModule("./styles/shared-styles.js")
@CssImport("./styles/views/main/main-view.css")
//@PWA(name = "FAIR Data Station", shortName = "FAIRDS")
//@Push
public class MainView extends AppLayout {
    // The menu buttons
    private final Tabs menu;
    // H1 header
    private H1 viewTitle;
    // Logger
    public static final Logger logger = LogManager.getLogger(MainView.class);

    /**
     * The main view of the interface
     */
    public MainView() {
        // Set the page
        setPrimarySection(Section.DRAWER);
        addToNavbar(true, createHeaderContent());
        menu = createMenu();
        addToDrawer(createDrawerContent(menu));
    }

    /**
     * Function to create header content
     * @return the layout of the header
     */
    private Component createHeaderContent() {
        HorizontalLayout layout = new HorizontalLayout();
        layout.setId("header");
        layout.getThemeList().set("dark", true);
        layout.setWidthFull();
        layout.setSpacing(false);
        layout.setAlignItems(FlexComponent.Alignment.CENTER);
        layout.add(new DrawerToggle());
        viewTitle = new H1();
        layout.add(viewTitle);

        Anchor authenticate;
        // Added user icon
        StreamResource imageResource = new StreamResource("user.svg", () -> getClass().getResourceAsStream("/META-INF/resources/images/user.svg"));
        layout.add(new Image(imageResource, "Avatar"));

        // Obtain credentials for authentication purposes
        Credentials credentials = (Credentials) VaadinSession.getCurrent().getAttribute("credentials");

        // Check authentication with irods instance when successful show username otherwise show login button
        if (credentials != null && credentials.isSuccess()) {
            authenticate = new Anchor("user", credentials.getUsername());
        } else {
            authenticate = new Anchor("", ""); // new Anchor("login", "Login");
        }
        // layout.add(UI.getCurrent().getSession().getSession().getId() + " ");

        layout.add(authenticate);
        return layout;
    }

    /**
     * @param menu the menu on the left side
     * @return update of the layout with unlock or generic logo
     */
    private Component createDrawerContent(Tabs menu) {
        VerticalLayout layout = new VerticalLayout();
        layout.setSizeFull();
        layout.setPadding(false);
        layout.setSpacing(false);
        layout.getThemeList().set("spacing-s", true);
        layout.setAlignItems(FlexComponent.Alignment.STRETCH);
        HorizontalLayout logoLayout = new HorizontalLayout();
        logoLayout.setId("logo");
        logoLayout.setAlignItems(FlexComponent.Alignment.CENTER);

        // Get url host path if unlock in name use unlock logo
        UI.getCurrent().getPage().executeJs("return window.location.href").then(String.class, location -> {
            if (location.contains("unlock")) {
                StreamResource imageResource = new StreamResource("unlock_logo.png", () -> getClass().getResourceAsStream("/META-INF/resources/images/unlock_logo.png"));
                logoLayout.add(new Image(imageResource, "Unlock logo"));
            } else if (location.contains("aspar")) {
                logger.debug("We are in ASPAR_KR mode.");
                StreamResource imageResource = new StreamResource("aspar_kr.png", () -> getClass().getResourceAsStream("/META-INF/resources/images/aspar_kr.png"));
                logoLayout.add(new Image(imageResource, "ASPAR logo"));
            } else {
                StreamResource imageResource = new StreamResource("FAIRDS_logo.png", () -> getClass().getResourceAsStream("/META-INF/resources/images/FAIRDS_logo.png"));
                Image image = new Image(imageResource, "Unlock logo");
                image.setMaxWidth(100, Unit.PERCENTAGE);
                logoLayout.add(image);
            }
        });

        layout.add(logoLayout, menu);
        return layout;
    }

    /**
     * @return the tabs created for the menu
     */
    private Tabs createMenu() {
        Tabs tabs = new Tabs();
        tabs.setOrientation(Tabs.Orientation.VERTICAL);
        tabs.addThemeVariants(TabsVariant.LUMO_MINIMAL);
        tabs.setId("tabs");
        tabs = createMenuItems(tabs);
        return tabs;
    }

    /**
     * This function creates the buttons on the left side
     *
     * @return the tabs for the menu in array format
     */
    private Tabs createMenuItems(Tabs tabs) {
        Menus menus = Application.config.getMenus();
        // About / frontpage view
        if (menus.isAbout()) tabs.add(createTab("About", AboutView.class));
        // The metadata template generator view
        if (menus.isConfigurator()) tabs.add(createTab("Metadata Configurator", ConfiguratorView.class));
        // The view to upload and validate the metadata + conversion to RDF
        if (menus.isValidator()) tabs.add(createTab("Validate Metadata", ValidationView.class));
        // The view to generate Excel files based on bioproject identifiers
        if (menus.isBioprojects()) tabs.add(createTab("BioProjects Export", nl.fairbydesign.views.bioprojects.BioProjectView.class));
        // The view to generate ENA compatible submission files
        if (menus.isEna())  tabs.add(createTab("ENA Submission (Beta)", nl.fairbydesign.views.ena.ENAView.class));
        // Overview of all the terms used in the metadata.xlsx file
        if (menus.isTerms()) tabs.add(createTab("Terms Overview", TermsView.class));
        // Retrieve the credentials object
        Credentials credentials = (Credentials) VaadinSession.getCurrent().getAttribute("credentials");
        // Check credentials for irods based modules
        if (credentials != null && credentials.isSuccess()) {
            // registerViewIfApplicable();
            // File browser for iRODS
            tabs.add(createTab("iRODS Browser", BrowserView.class));
            // Dashboard overview
            tabs.add(createTab("iRODS Usage", DiskUsageView.class));
            // Organise data in iRODS
             tabs.add(createTab("iRODS Organise", OrganiseView.class));
            // Logout view
            tabs.add(createTab("iRODS Logout", LogoutView.class));
        } else {
            // Login button for iRODS
            if (menus.isIrods()) tabs.add(createTab("iRODS Login", LoginView.class));
        }

        // For admin users
        if (credentials != null && credentials.isAdministrator()) {
            // File size search system to archive data
            tabs.add(createTab("Archive", Archive.class));
        }
        return tabs;
    }

    public static Tab createTab(String text, Class<? extends Component> navigationTarget) {
        final Tab tab = new Tab();
        tab.add(new RouterLink(text, navigationTarget));
        ComponentUtil.setData(tab, Class.class, navigationTarget);
        return tab;
    }

    @Override
    protected void afterNavigation() {
        super.afterNavigation();
        getTabForComponent(getContent()).ifPresent(menu::setSelectedTab);
        viewTitle.setText(getCurrentPageTitle());
    }

    private Optional<Tab> getTabForComponent(Component component) {
        return menu.getChildren()
                .filter(tab -> ComponentUtil.getData(tab, Class.class)
                        .equals(component.getClass()))
                .findFirst().map(Tab.class::cast);
    }

    private String getCurrentPageTitle() {
        return getContent().getClass().getAnnotation(PageTitle.class).value();
    }

}
