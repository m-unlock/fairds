package nl.fairbydesign.views.organise;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.NativeLabel;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.data.renderer.LitRenderer;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinSession;
import nl.fairbydesign.backend.credentials.Credentials;
import nl.fairbydesign.backend.data.objects.File;
import nl.fairbydesign.backend.irods.Data;
import nl.fairbydesign.backend.manager.Manager;
import nl.fairbydesign.backend.metadata.Metadata;
import nl.fairbydesign.views.main.MainView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.query.GenQueryBuilderException;

import java.util.ArrayList;
import java.util.List;

@Route(value = "organiser", layout = MainView.class)
@PageTitle("Metadata Organiser")
@CssImport("./styles/views/empty/empty-view.css")
public class OrganiseView extends Div {
    private final Logger logger = LogManager.getLogger(Metadata.class);

    private final Credentials credentials;

    public OrganiseView() {
        // Organise view

        // If logged in
        setId("master-detail-view");

        credentials = (Credentials) VaadinSession.getCurrent().getAttribute("credentials");

        if (credentials != null && credentials.isSuccess()) {
            // Layout for the overall page
            VerticalLayout verticalLayout = new VerticalLayout();
            verticalLayout.setId("organise-view");
            verticalLayout.setPadding(false);
            verticalLayout.setSpacing(false);
            verticalLayout.setMargin(false);
            verticalLayout.setSizeFull();

            // Grid layout for the metadata files
            Grid<File> grid = new Grid<>(File.class);
            grid.setSelectionMode(Grid.SelectionMode.MULTI);
            grid.setColumns("path", "size", "modificationDate");
            grid.setWidthFull();
            grid.setHeight(500, Unit.PIXELS);

            // Demo items
            grid.setItems(new File("path", 100, "date"));

            // Process button
            Button button = new Button("Process");
            button.setEnabled(false);

            // Log area for the processing of the files from the grid selection
            TextArea logArea = new TextArea("Progress Log");
            logArea.setWidthFull();
            logArea.setPlaceholder("Log will appear here ...");
            logArea.setValue("Log will appear here ..." + System.lineSeparator());
            logArea.setVisible(true);
            logArea.setReadOnly(true);
            logArea.setHeight(500, Unit.PIXELS);

//            logArea.setHeight(500, Unit.PIXELS);

            verticalLayout.add(button, grid, button, logArea);
            add(verticalLayout);

            grid.removeAllColumns();
            grid.addColumn(LitRenderer
                    .<File>of("<b>${item.path}</b>")
                    .withProperty("path", File::getPath)
            ).setHeader("Path");
            grid.addColumn(LitRenderer
                    .<File>of("<b>${item.size}</b>")
                    .withProperty("size", File::getSize)
            ).setHeader("Size");
            grid.addColumn(LitRenderer
                    .<File>of("<b>${item.moddate}</b>")
                    .withProperty("moddate", File::getCalendarDate)
            ).setHeader("Modification Date");


            // Query for all ttl files in the metadata folder
            // A thread to obtain the list of metadata files
            List<File> metadataFiles = new ArrayList<>();
            List<File> finalMetadataFiles = metadataFiles;
            grid.setItems(finalMetadataFiles);
            Thread t = new Thread(() -> {
                try {
                    Data.getMetadataFiles(credentials, finalMetadataFiles, grid, logArea);
                    grid.getUI().ifPresent(ui -> ui.access(() -> grid.getDataProvider().refreshAll()));
                } catch (GenQueryBuilderException | JargonException e) {
                    throw new RuntimeException(e);
                }
            });
            t.start();

//                Data.getMetadataFiles(credentials, metadataFiles);
            logger.info("Metadata files for the grid: {}", metadataFiles.size());


//                grid.removeAllColumns();
//                grid.setSizeFull();

            button.addSingleClickListener(event -> grid.getSelectedItems().forEach(file -> {
                Thread t2 = new Thread(() -> {
                    try {
                        logger.info("Processing file: {}", file.getPath());
                        logArea.getUI().ifPresent(ui -> ui.access(() -> logArea.setValue(logArea.getValue() + "Processing file: " + file.getPath() + System.lineSeparator())));
                        Manager manager = new Manager();
                        manager.process(credentials, file, logArea);
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                });
                t2.start();
            }));


            grid.addSelectionListener(event -> {
                // Show process button
                button.setEnabled(!grid.getSelectedItems().isEmpty());
            });
        } else {
            add(new NativeLabel("To use this you need to login first..."));
            Button button = new Button("Login", event -> UI.getCurrent().navigate("login"));
            add(button);
            button.click();
            button.clickInClient();
        }
    }
}
