package nl.fairbydesign.views.validation;

import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.NativeLabel;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.progressbar.ProgressBar;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.MultiFileMemoryBuffer;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.StreamResource;
import com.vaadin.flow.server.VaadinSession;
import nl.fairbydesign.Application;
import nl.fairbydesign.backend.WebGeneric;
import nl.fairbydesign.backend.credentials.Credentials;
import nl.fairbydesign.backend.parsers.ExcelValidator;
import nl.fairbydesign.views.main.MainView;
import nl.wur.ssb.RDFSimpleCon.RDFFormat;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;

import static com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment.CENTER;

@Route(value = "validate", layout = MainView.class)
@PageTitle("Ingestion engine")
@CssImport("./styles/views/empty/empty-view.css")
public class ValidationView extends Div implements BeforeEnterObserver {
    private static final Logger logger = LoggerFactory.getLogger(Credentials.class);

    @Override // inform the user about an authentication error
    public void beforeEnter(BeforeEnterEvent beforeEnterEvent) {
        if (!Application.config.getMenus().isValidator()) {
            beforeEnterEvent.forwardTo("");
        }
    }
    public ValidationView() {

        Credentials credentials = (Credentials) VaadinSession.getCurrent().getAttribute("credentials");

        MultiFileMemoryBuffer buffer = new MultiFileMemoryBuffer();
        Upload upload = new Upload(buffer);
        // upload.setMaxFiles(1);
        upload.setDropLabel(new NativeLabel("Upload a FAIR Data Station compatible excel file in .xlsx format"));
        
        Select<String> fileFormat = new Select<>();
        fileFormat.setLabel("(Optional) Select file format");
        ArrayList<String> formats = new ArrayList<>();
        for (RDFFormat value : RDFFormat.values()) {
            formats.add(value.toString());
        }
        fileFormat.setItems(formats);

        TextArea logArea = new TextArea("Description");
        TextArea stacktraceArea = new TextArea("Stacktrace");
        Button stacktraceButton = new Button("Stacktrace");
        stacktraceButton.setVisible(false);
        stacktraceButton.setEnabled(false);
        // TODO find xlsx file type
        // upload.setAcceptedFileTypes("xlsx");
        // upload.setMaxFileSize(300);

        logArea.setWidthFull();
        logArea.setPlaceholder("Log will appear here ...");
        logArea.setVisible(false);
        logArea.setReadOnly(true);
        logArea.setHeight(500, Unit.PIXELS);

        stacktraceArea.setWidthFull();
        stacktraceArea.setVisible(false);

        ProgressBar progressBar = new ProgressBar();
        progressBar.setIndeterminate(true);
        progressBar.setVisible(false);

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.setAlignItems(CENTER);
        verticalLayout.setSpacing(false);
        verticalLayout.setPadding(false);
        verticalLayout.setMaxHeight(100, Unit.PERCENTAGE);
        verticalLayout.add(progressBar, logArea, stacktraceButton, stacktraceArea);
        verticalLayout.setVisible(false);
        add(upload, verticalLayout); // fileFormat not sure if it is really needed, turtle by default

        stacktraceButton.addClickListener(clicked -> {
            if (stacktraceArea.isVisible()) {
                stacktraceArea.setVisible(false);
                stacktraceArea.setEnabled(false);
            } else {
                stacktraceArea.setVisible(true);
                stacktraceArea.setEnabled(true);
            }
        });

        upload.addFileRejectedListener(event -> Notification.show(event.getErrorMessage()));

        upload.addSucceededListener(event -> {
            String fileName = event.getFileName();
            InputStream inputStream = buffer.getInputStream(fileName);
            // Reset stacktrace due to multiple file upload
            stacktraceArea.getUI().ifPresent(ui -> ui.access(() -> stacktraceArea.setValue("")));

            verticalLayout.setVisible(true);
            // upload.setVisible(false);
            // fileFormat.setVisible(false);
            // Copy to close buffer
            try {
                java.nio.file.Files.copy(
                        inputStream,
                        new File(Application.commandOptions.storage +"/validation/" + fileName).toPath(),
                        StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {
                logger.error("Copy inputstream failed");
            }
            String excelFileName = fileName;

            Thread t = new Thread(() -> {
                // When upload is successful start the Excel validator!
                if (!fileName.endsWith("xlsx")) {
                    logArea.getUI().ifPresent(ui -> ui.access(() -> {
                        logArea.setVisible(true);
                        logArea.setValue("Input file does not appear to be a valid XLSX file");
                    }));
                } else {
                    logArea.getUI().ifPresent(ui -> ui.access(() -> logArea.setVisible(true)));
                    progressBar.getUI().ifPresent(ui -> ui.access(() -> {
                        progressBar.setVisible(true);
                        progressBar.setIndeterminate(true);
                    }));
                    ExcelValidator excelValidator = new ExcelValidator();
                    try {
                        InputStream fileInputStream = new FileInputStream(Application.commandOptions.storage +"/validation/" + excelFileName);
                        String rdfPath = excelValidator.validate(excelFileName, fileInputStream, credentials, logArea, fileFormat.getValue());
                        File rdfFile = new File(rdfPath);
                        if (excelValidator.getMessage().contains("Is this an excel file?")) {
                            logArea.getUI().ifPresent(ui -> ui.access(() -> logArea.setValue(excelValidator.getMessage() + "\n\nInput file does not appear to be a valid XLSX file")));
                        } else {
                            if (credentials != null && credentials.isSuccess()) {
                                logArea.getUI().ifPresent(ui -> ui.access(() -> logArea.setValue(excelValidator.getMessage() + "\n\nValidation appeared to be successful please notify the data steward of this project")));
                            } else {
                                logArea.getUI().ifPresent(ui -> ui.access(() -> logArea.setValue(excelValidator.getMessage() + "\n\nValidation appeared to be successful.")));
                                if (rdfFile.exists()) {
                                    // Provide download button
                                    Button buttonDownload = new Button("DOWNLOAD RDF");
                                    this.getUI().ifPresent(ui -> ui.access(() -> {
                                        add(buttonDownload);
                                        buttonDownload.setEnabled(true);
                                        buttonDownload.addClickListener(e -> downloadFunction(rdfFile));
                                    }));
                                }
                            }
                        }
                    } catch (Exception e) {
                        logArea.getUI().ifPresent(ui -> ui.access(() -> logArea.setValue(
                                "\n\n=================================================================\n\n" +
                                        "Your dataset did not pass the validation...\n\n" +
                                        "When it is unclear please provide the following message to your data steward\n\n" +
                                        excelValidator.getMessage() +
                                        "\n\nPlease validate your mappings in the excel file\n\n" +
                                        "\n\n=================================================================\n\n")));
                        stacktraceArea.getUI().ifPresent(ui -> ui.access(() -> stacktraceArea.setValue("Message & Stacktrace:\n\n" + e.getMessage() + "\n\n" + StringUtils.join(e.getStackTrace(), "\n"))));
                        stacktraceArea.getUI().ifPresent(ui -> ui.access(() -> stacktraceArea.setVisible(true)));
                        stacktraceArea.getUI().ifPresent(ui -> ui.access(() -> stacktraceArea.setEnabled(true)));
                        System.err.println(StringUtils.join(e.getStackTrace(), "\n"));

                    }
                }
                progressBar.getUI().ifPresent(ui -> ui.access(() -> progressBar.setVisible(false)));
            });
            t.start();
        });

        // Set footer
        Html footerContent = WebGeneric.getTextFromResource("views/footer.html");
        Div footer = new Div();
        footer.add(footerContent);
        add(footer);
    }

    private void downloadFunction(File rdfFile) {
        Button button = new Button("Hidden Download");
        button.setVisible(false);

        button.addClickListener(clicked -> {
            StreamResource streamResource = new StreamResource(rdfFile.getName(), () -> {
                try {
                    return new FileInputStream(rdfFile.getPath());
                } catch (FileNotFoundException e) {
                    Notification.show("Failed to download file", 3000, Notification.Position.MIDDLE);
                }
                return null;
            });
            if (streamResource != null) {
                Anchor link = new Anchor(streamResource, "SOME CONTENT");
                link.getElement().setAttribute("download", true);
                add(link);
                // Trigger the anchor link programmatically
                UI.getCurrent().getPage().executeJs("arguments[0].click()", link.getElement());
            }
        });
        button.clickInClient();
        button.click();
    }

}
