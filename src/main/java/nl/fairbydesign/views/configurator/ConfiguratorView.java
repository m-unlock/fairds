package nl.fairbydesign.views.configurator;

import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.accordion.Accordion;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.HeaderRow;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.BinderValidationStatus;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.data.validator.EmailValidator;
import com.vaadin.flow.data.validator.StringLengthValidator;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.StreamRegistration;
import com.vaadin.flow.server.StreamResource;
import nl.fairbydesign.Application;
import nl.fairbydesign.backend.WebGeneric;
import nl.fairbydesign.backend.data.objects.Member;
import nl.fairbydesign.backend.data.objects.jerm.Investigation;
import nl.fairbydesign.backend.data.objects.jerm.ObservationUnit;
import nl.fairbydesign.backend.data.objects.jerm.Study;
import nl.fairbydesign.backend.metadata.Metadata;
import nl.fairbydesign.backend.parsers.ExcelGenerator;
import nl.fairbydesign.views.main.MainView;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.vaadin.olli.FileDownloadWrapper;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import static com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment.CENTER;
import static nl.fairbydesign.Application.getMetadataSet;
import static nl.fairbydesign.backend.WebGeneric.getFileContentFromResources;
import static nl.fairbydesign.backend.parsers.ExcelGenerator.SELECT_A_PACKAGE;
import static nl.fairbydesign.backend.parsers.ExcelGenerator.makeSheetOnly;

/**
 * The view to create an Excel file to fill in project metadata
 */
@Route(value = "configurator", layout = MainView.class)
@PageTitle("Metadata Configurator")
@CssImport("./styles/views/empty/empty-view.css")
public class ConfiguratorView extends Div implements BeforeEnterObserver {

    private final ServerProperties serverProperties;

    // Check if the user is allowed to access this page
    @Override // inform the user about an authentication error
    public void beforeEnter(BeforeEnterEvent beforeEnterEvent) {
        if (!Application.config.getMenus().isConfigurator()) {
            beforeEnterEvent.forwardTo("");
        }
    }

    public static final Logger logger = LogManager.getLogger(ConfiguratorView.class);
    private final Button buttonDownload;

    // Create empty person grid list
    private final List<Member> memberList = new ArrayList<>();
    // public static final Logger logger = Logger.getLogger(AccordionForm.class);

    // Variables needed for the Excel sheet
    Binder<Investigation> investigationBinder = new Binder<>();
    Binder<Study> studyBinder = new Binder<>();
    Binder<ObservationUnit> observationUnitBinder = new Binder<>();

    // Grids needed for Sample / Assay objects
    ArrayList<Grid> gridList = new ArrayList<>();

    // Package names for investigation, study, 'observation unit', 'sample' and 'assay'
    // Create an array of 5 elements all with the same value
    ArrayList<String> packages = new ArrayList<>(Collections.nCopies(5, "Select a package"));

    /**
     * Creates the template generator page
     */
    public ConfiguratorView(ServerProperties serverProperties) {
        // Setup interface
        Accordion accordionIS = new Accordion();
        accordionIS.setWidth("80%");
        accordionIS.getElement().getStyle().set("border", "2px solid #990099");

        Accordion accordionOSA = new Accordion();
        accordionOSA.setWidth("80%");
        accordionOSA.getElement().getStyle().set("border", "2px solid #0080ff");
        accordionOSA.close();

        // Adding investigation information form
        VerticalLayout investigationInformation = new VerticalLayout();
        createInvestigationLayout(investigationInformation);
        createPackageSelectionGrids(investigationInformation, "Investigation", "help/investigation.txt");
        accordionIS.add("Investigation Information", investigationInformation);

        // study form
//        VerticalLayout studyInformation = new VerticalLayout();
//        createStudy(studyInformation);
//        accordionIS.add("Study Information", studyInformation);

        // Study form with package selection
        VerticalLayout studyInformation = new VerticalLayout();
        createStudy(studyInformation);
        createPackageSelectionGrids(studyInformation, "Study", "help/study.txt");
        accordionIS.add("Study Information", studyInformation);


        // Observation Unit form with package selection
        VerticalLayout observationUnitInformation = new VerticalLayout();
        // createObservationUnit(observationUnitInformation);
        createPackageSelectionGrids(observationUnitInformation, "ObservationUnit", "help/observationUnit.txt");
        accordionOSA.add("Observation Unit Information", observationUnitInformation);

        // Sample form with package selection
        VerticalLayout sampleInformation = new VerticalLayout();
        createPackageSelectionGrids(sampleInformation, "Sample", "help/sample.txt");
        accordionOSA.add("Sample Information", sampleInformation);

        // Assay form with + one more button
        VerticalLayout assayLayout = new VerticalLayout();
        createPackageSelectionGrids(assayLayout, "Assay", "help/assay/assay.txt");
        accordionOSA.add("Assay Information", assayLayout);

        // Generate and "download" button
        Button buttonGenerate = new Button("Generate workbook");
        Button buttonGenerate2 = new Button("Generate workbook");

        // Placeholder for the automatic download click
        buttonDownload = new Button("");

        // Hide download button
        buttonDownload.addClickListener(clicked -> {
            // Cannot do it directly so make it invisible via a listener
            buttonDownload.setVisible(false);
        });

        buttonGenerate2.addClickListener(e -> buttonGenerate.click());

        buttonGenerate.addClickListener(e -> {
            // Close both views to make it easier to open the correct panel
            accordionOSA.close();
            accordionIS.close();

            // Project project = new Project();
            Investigation investigation = new Investigation();
            Study study = new Study();

            ObservationUnit observationUnit = new ObservationUnit();
            String message = null;
            if (
                    investigationBinder.writeBeanIfValid(investigation) &&
                            studyBinder.writeBeanIfValid(study) &&
                            observationUnitBinder.writeBeanIfValid(observationUnit)
            ) {
                // Perform check if any of the assays has been enabled
                AtomicBoolean initialValidation = new AtomicBoolean(false);
                gridList.forEach(grid -> {
                    if (grid.isEnabled() && grid.getId().get().endsWith("Assay")) {
                        initialValidation.set(true);
                    } else {
                        // Assay selection check is disabled by true in the following line
                        initialValidation.set(true);
                    }
                });

                if (!initialValidation.get()) {
                    Notification.show("Please select at least one assay type for registration", 5000, Notification.Position.MIDDLE);
                }

                // Perform check if investigation contains at least 1 member
                if (memberList.isEmpty()) {
                    initialValidation.set(false);
                    accordionIS.open(1);
                    Notification notification = Notification.show("Please register at least one member under the investigation tab", 5000, Notification.Position.MIDDLE);
                    notification.addThemeVariants(NotificationVariant.LUMO_CONTRAST);
                }

                ArrayList<String> levels = new ArrayList<>(Arrays.asList("Investigation", "Study", "ObservationUnit", "Sample", "Assay"));
                ArrayList<String> missedPackages = new ArrayList<>();

                for (int i = 0; i < packages.size(); i++) {
                    if (packages.get(i).equalsIgnoreCase(SELECT_A_PACKAGE)) {
                        // Making it obligatory to select a package for Investigation or Study
                        if (i == 0 || i == 1)
                            initialValidation.set(false);
                        missedPackages.add(levels.get(i).toLowerCase());
                    }
                }

                if (!missedPackages.isEmpty()) {
                    Notification notification = Notification.show("Package(s) for:\n\n" + StringUtils.join(missedPackages, ", ") + "\n\n have not been selected, you can export the sheets for Observation Unit, Sample and Assay at a later stage... and merge them with the generated excel file", 10000, Notification.Position.BOTTOM_CENTER);
                    notification.addThemeVariants(NotificationVariant.LUMO_CONTRAST);
                    // Check for investigation and study
                    if (missedPackages.contains("investigation")) {
                        message = "Please select a package in the investigation section";
                        accordionIS.scrollIntoView();
                        accordionIS.open(0);
                    } else if (missedPackages.contains("study")) {
                        message = "Please select a package in the study section";
                        accordionIS.scrollIntoView();
                        accordionIS.open(1);
                    }
                }

                if (initialValidation.get()) {
                    HashMap<String, ArrayList<Metadata>> gridSelection = getHashMapFromGrid(gridList);
                    File excelFile = ExcelGenerator.generateForm(investigation, memberList, study, observationUnit, gridSelection, packages.get(1));
                    downloadFunction(excelFile);
                }
            } else {
                if (investigationBinder.validate().hasErrors()) {
                    message = "The investigation tab has not been filled in properly, please have a look and try again";
                    // Vaadin scroll to the beginning of the accordion
                    accordionIS.scrollIntoView();
                    accordionIS.open(0);
                } else if (studyBinder.validate().hasErrors()) {
                    message = "The study tab has not been filled in properly, please have a look and try again";
                    accordionIS.scrollIntoView();
                    accordionIS.open(1);
                } else if (observationUnitBinder.validate().hasErrors()) {
                    message = "The observation unit tab has not been filled in properly, please have a look and try again";
                    accordionOSA.scrollIntoView();
                    accordionOSA.open(0);
                }
            }

            if (message != null) {
                // TODO make a proper CSS file

                // @formatter:off
                    String styles = ".my-style { "
                            + "  color: red;"
                            + " }";
                    // @formatter:on

                /*
                 * The code below register the style file dynamically. Normally you
                 * use @StyleSheet annotation for the component class. This way is
                 * chosen just to show the style file source code.
                 */
                StreamRegistration resource = UI.getCurrent().getSession()
                        .getResourceRegistry()
                        .registerResource(new StreamResource("styles.css", () -> {
                            byte[] bytes = styles.getBytes(StandardCharsets.UTF_8);
                            return new ByteArrayInputStream(bytes);
                        }));
                UI.getCurrent().getPage().addStyleSheet(
                        "base://" + resource.getResourceUri().toString());

                Div content = new Div();
                content.addClassName("my-style");
                content.setText(message);

                Notification notification = new Notification(content);
                notification.setDuration(3000);
                notification.setPosition(Notification.Position.MIDDLE);
                notification.open();
            }
        });

        // Finalise accordion and align center
        Div htmlSection1 = new Div();
        htmlSection1.setWidth("80%");
        htmlSection1.add(getHelpText("help/configurator_section1.txt"));

        Div htmlSection2 = new Div();
        htmlSection2.setWidth("80%");
        htmlSection2.add(getHelpText("help/configurator_section2.txt"));

        Div htmlSection3 = new Div();
        htmlSection3.setWidth("80%");
        htmlSection3.add(getHelpText("help/configurator_section3.txt"));

        Div htmlSection4 = new Div();
        htmlSection4.setWidth("80%");
        htmlSection4.add(getHelpText("help/configurator_section4.txt"));

        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.add(htmlSection1, accordionIS, buttonGenerate2, htmlSection2, accordionOSA, htmlSection3, buttonGenerate, htmlSection4);
        verticalLayout.setWidthFull();
        verticalLayout.setDefaultHorizontalComponentAlignment(CENTER);

        // Set footer
        Html footerContent = WebGeneric.getTextFromResource("views/footer.html");
        Div footer = new Div();
        footer.add(footerContent);
        verticalLayout.add(footer);

        add(verticalLayout);
        // add(buttonDownload);
        this.serverProperties = serverProperties;
    }

    private void downloadFunction(File excelFile) {
        Button button = new Button("Hidden Download");
        button.setVisible(false);

        button.addClickListener(clicked -> {
            StreamResource streamResource = new StreamResource(excelFile.getName(), () -> {
                try {
                    return new FileInputStream(excelFile.getPath());
                } catch (FileNotFoundException e) {
                    Notification.show("Failed to download file", 3000, Notification.Position.MIDDLE);
                }
                return null;
            });
            if (streamResource != null) {
                Anchor link = new Anchor(streamResource, "SOME CONTENT");
                link.getElement().setAttribute("download", true);
                add(link);
                // Trigger the anchor link programmatically
                UI.getCurrent().getPage().executeJs("arguments[0].click()", link.getElement());
            }
        });
        button.clickInClient();
        button.click();
    }

    private void createPackageSelectionGrids(VerticalLayout packageLayout, String metadata, String help) {
        // assay layout is the overall placeholder with a button to add more assay layouts
        ArrayList<VerticalLayout> verticalLayouts = new ArrayList<>();

        // Button removeAssayButton = new Button("Remove assay");

        // Holder for a specific assay
        VerticalLayout verticalLayout = new VerticalLayout();
        // Add the help text for the investigation or study section only
        if (metadata.equalsIgnoreCase("Investigation") || metadata.equalsIgnoreCase("Study")) {
            verticalLayout.add(new Text("Below you can select the appropriate package for your " + metadata.toLowerCase() + " object\nVarious packages are available to select from"));
        }
        createPackageSelectionGrid(verticalLayout, metadata, help, true);
        verticalLayouts.add(verticalLayout);
        HorizontalLayout templateButtons = new HorizontalLayout();
        packageLayout.add(verticalLayouts.get(0));
        packageLayout.add(templateButtons);

        // Only for non Investigation and Study objects
        if (!metadata.equalsIgnoreCase("Investigation") && !metadata.equalsIgnoreCase("Study")) {
            Button addPackage = new Button("Add another package");
            templateButtons.add(addPackage);
            // Button to add another assay to the assay layout
            addPackage.addClickListener(clicked -> {
                // Add another assay!
                createPackageSelectionGrid(verticalLayout, metadata, help, true);
                verticalLayouts.add(verticalLayout);
                packageLayout.add(verticalLayout);
                // Remove the button and add it again to the bottom
                packageLayout.remove(templateButtons);
                packageLayout.add(templateButtons);
            });
        }
    }

    /**
     * Creating the package selection interface based on multiple environmental packages according to MIxS
     *
     * @param verticalLayout the verticalLayout to store all the wrappers with the grid and buttons in
     * @param metadata       the metadata object name
     * @param help           general help information
     * @param visible        make this visible or not
     */
    private void createPackageSelectionGrid(VerticalLayout verticalLayout, String metadata, String help, boolean visible) {
        VerticalLayout wrapper = new VerticalLayout();
        // Obligatory items
        ArrayList<Metadata> obligatoryList = new ArrayList<>();
        // All items using an atomic reference to be able to modify it within
        AtomicReference<ArrayList<Metadata>> items = new AtomicReference<>(new ArrayList<>());
        AtomicReference<ArrayList<Metadata>> otherItems = new AtomicReference<>(new ArrayList<>());

        // Setup grid
        Grid<Metadata> gridSelection = new Grid<>(Metadata.class);
        gridSelection.removeAllColumns();

        Grid.Column<Metadata> packageNameColumn = gridSelection.addColumn(Metadata::getPackageName).setHeader("Package");
        Grid.Column<Metadata> labelColumn = gridSelection.addColumn(Metadata::getLabel).setHeader("Label");
        Grid.Column<Metadata> syntaxColumn = gridSelection.addColumn(m -> m.getTerm().getSyntax()).setHeader("Syntax");
        Grid.Column<Metadata> exampleColumn = gridSelection.addColumn(m -> m.getTerm().getExample()).setHeader("Example");
        Grid.Column<Metadata> definitionColumn = gridSelection.addColumn(m -> m.getTerm().getDefinition()).setHeader("Definition");

        gridSelection.setSelectionMode(Grid.SelectionMode.MULTI);
        gridSelection.asMultiSelect().select(obligatoryList);
        gridSelection.setId(metadata);
        gridSelection.setItems(items.get());
        gridSelection.asMultiSelect().select(obligatoryList);
        gridSelection.setEnabled(visible);
        gridSelection.setVisible(visible);

        // Add filters
        AtomicReference<ListDataProvider<Metadata>> dataProvider = new AtomicReference<>(new ListDataProvider<>(items.get()));

        HeaderRow filterRow = gridSelection.appendHeaderRow();
        // Label filter
        TextField labelFieldFilter = new TextField();
        labelFieldFilter.addValueChangeListener(event -> dataProvider.get().addFilter(file -> StringUtils.containsIgnoreCase(file.getLabel(), labelFieldFilter.getValue())));
        labelFieldFilter.setValueChangeMode(ValueChangeMode.EAGER);
        filterRow.getCell(labelColumn).setComponent(labelFieldFilter);
        labelFieldFilter.setSizeFull();
        labelFieldFilter.setPlaceholder("Filter");

        // Package filter
        TextField packageFieldFilter = new TextField();
        packageFieldFilter.addValueChangeListener(event -> dataProvider.get().addFilter(file -> StringUtils.containsIgnoreCase(file.getPackageName(), packageFieldFilter.getValue())));
        packageFieldFilter.setValueChangeMode(ValueChangeMode.EAGER);
        filterRow.getCell(packageNameColumn).setComponent(packageFieldFilter);
        packageFieldFilter.setSizeFull();
        packageFieldFilter.setPlaceholder("Filter");

        // Package filter
        TextField exampleFieldFilter = new TextField();
        exampleFieldFilter.addValueChangeListener(event -> dataProvider.get().addFilter(file -> StringUtils.containsIgnoreCase(file.getTerm().getExample(), exampleFieldFilter.getValue())));
        exampleFieldFilter.setValueChangeMode(ValueChangeMode.EAGER);
        filterRow.getCell(exampleColumn).setComponent(exampleFieldFilter);
        exampleFieldFilter.setSizeFull();
        exampleFieldFilter.setPlaceholder("Filter");

        // Package filter
        TextField syntaxFieldFilter = new TextField();
        syntaxFieldFilter.addValueChangeListener(event -> dataProvider.get().addFilter(file -> StringUtils.containsIgnoreCase(file.getTerm().getSyntax(), syntaxFieldFilter.getValue())));
        syntaxFieldFilter.setValueChangeMode(ValueChangeMode.EAGER);
        filterRow.getCell(syntaxColumn).setComponent(syntaxFieldFilter);
        syntaxFieldFilter.setSizeFull();
        syntaxFieldFilter.setPlaceholder("Filter");

        // Package filter
        TextField definitionFieldFilter = new TextField();
        definitionFieldFilter.addValueChangeListener(event -> dataProvider.get().addFilter(file -> StringUtils.containsIgnoreCase(file.getTerm().getDefinition(), definitionFieldFilter.getValue())));
        definitionFieldFilter.setValueChangeMode(ValueChangeMode.EAGER);
        filterRow.getCell(definitionColumn).setComponent(definitionFieldFilter);
        definitionFieldFilter.setSizeFull();
        definitionFieldFilter.setPlaceholder("Filter");

        // When grid changes select obligatory list (when an obligatory element is unselected it is selected again)
        gridSelection.asMultiSelect().addValueChangeListener(event -> {
            // Check if the package was selected
            gridSelection.asMultiSelect().select(obligatoryList);
            logger.debug("Selected " + gridSelection.getSelectedItems().size() + " items");
        });

        TextField labelField = new TextField();

        labelField.setEnabled(visible);
        labelField.setVisible(visible);

        Button exportButton = new Button("Export", new Icon(VaadinIcon.FILE));
        exportButton.setEnabled(visible);
        exportButton.setVisible(visible);

        // Export this directly to a sheet...
        exportButton.addClickListener(buttonClickEvent -> exportSheet(gridSelection));


        Button removePackageSelectionButton = new Button("Remove", new Icon(VaadinIcon.TRASH));
        removePackageSelectionButton.addClickListener(buttonClickEvent -> {
            // Notification.show("Clicked on  button: " + removeMetadataSelectionButton.getId());
            verticalLayout.remove(wrapper);
        });

        ArrayList<String> skipFor = new ArrayList<>(Arrays.asList("investigation", "study"));
        if (skipFor.contains(metadata.toLowerCase())) {
            removePackageSelectionButton.setEnabled(false);
            removePackageSelectionButton.setVisible(false);
        }
        // List box for different environments
        ComboBox<String> listBox = new ComboBox<>();

        ArrayList<String> packagesViewList = new ArrayList<>();
        // Add select a package and core at the top
        packagesViewList.add(SELECT_A_PACKAGE);
        packagesViewList.add("default");
        // Add all packages from the metadata file as an ordered list according to the metadata Excel file
        ArrayList<String> subPackages = new ArrayList<>();

        // Add metadata fields from selected package
        for (Metadata m : getMetadataSet().get(metadata)) {
            // Skip core package as it is manually added
            if (m.getPackageName().equalsIgnoreCase("default")) continue;
            if (subPackages.contains(m.getPackageName())) continue;
            if (m.getPackageName().isEmpty()) continue;
            subPackages.add(m.getPackageName());
        }

        // Add all other packages in a sorted fashion
        packagesViewList.addAll(subPackages); // .stream().collect(Collectors.toList()));
        listBox.setItems(packagesViewList);
        listBox.setVisible(visible);
        listBox.setEnabled(visible);

        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.add(exportButton, removePackageSelectionButton, listBox); // labelField, addButton,

        // Listener for package selection
        listBox.addValueChangeListener(listener -> {
            // Ensure to erase when a new type is being selected
            obligatoryList.removeAll(obligatoryList);
            if (metadata.equalsIgnoreCase("Investigation")) {
                packages.set(0, listener.getValue());
            } else if (metadata.equalsIgnoreCase("Study")) {
                packages.set(1, listener.getValue());
            } else if (metadata.equalsIgnoreCase("ObservationUnit")) {
                packages.set(2, listener.getValue());
            } else if (metadata.equalsIgnoreCase("Sample")) {
                packages.set(3, listener.getValue());
            } else if (metadata.equalsIgnoreCase("Assay")) {
                packages.set(4, listener.getValue());
            } else {
                logger.error("Unknown metadata type: " + metadata);
            }
            // TODO add user and core previously selected elements
            // Remove all previous selections
            gridSelection.deselectAll();
            // Reset item list
            items.set(new ArrayList<>());
            // Only show items when a true package is selected
            // Process obligatory items first then the other to ensure that another item is not already present in the obligatory list
            if (listener.getValue().contains(SELECT_A_PACKAGE)) {
                // Clear the grid
            } else {
                // Add the metadata elements from selected package
                for (Metadata m : getMetadataSet().get(metadata)) {
                    // Add all elements matching the package, core or user-specified values
                    // logger.info("Package: " + m.getPackageName() + " " + listBox.getValue() + " " + m.getPackageName().matches(listBox.getValue()));
                    // General check for core and selected package
                    if (m.getPackageName().equalsIgnoreCase(listBox.getValue()) || m.getPackageName().equalsIgnoreCase("default")) {
                        items.get().add(m);
                        // Obligatory items to be added to list for selection
                        if (m.getRequirement().equals(Metadata.Requirement.MANDATORY)) {
                            obligatoryList.add(m);
                        } else if (m.getRequirement().equals(Metadata.Requirement.RECOMMENDED)) {
                            obligatoryList.add(m);
                        }
                    }
                }
                // Loop again but only check for the other items
                for (Metadata m : getMetadataSet().get(metadata)) {
                    // Add all elements matching the package, core or user specified values
                    logger.debug("Package: " + m.getPackageName() + " " + listBox.getValue() + " " + m.getPackageName().equalsIgnoreCase(listBox.getValue()));
                    // General check for core and selected package
                    if (m.getPackageName().equalsIgnoreCase(listBox.getValue()) || m.getPackageName().equalsIgnoreCase("default")) {
                        // Already processed and used as a filter here
                    } else {
                        // Check if the metadata entity is already in the other list using the label
                        boolean found = false;
                        // Check in obligatory list
                        for (Metadata metadata1 : obligatoryList) {
                            if (metadata1.getLabel().equalsIgnoreCase(m.getLabel())) {
                                found = true;
                                break;
                            }
                        }
                        if (!found) {
                            // Check if found in the other list when not found in obligatory list
                            for (Metadata metadata1 : otherItems.get()) {
                                if (metadata1.getLabel().equalsIgnoreCase(m.getLabel())) {
                                    found = true;
                                    break;
                                }
                            }
                        }
                        // Add to the other list if not found in the obligatory or other list
                        if (!found) {
                            Metadata newM = new Metadata(m);
                            // Set the package name to other
                            newM.setPackageName("other");
                            // Set the requirement to optional as all other fields are optional
                            newM.setRequirement("optional");
                            otherItems.get().add(newM);
                            logger.debug("Adding other " + m.getLabel() + " " + m.getPackageName() + " " + m.getRequirement());
                        } else {
                            logger.debug("Skipping other " + m.getLabel() + " " + m.getPackageName() + " " + m.getRequirement());
                        }
                    }
                }

                // TODO ... what is here...
                if (metadata.toLowerCase().matches(".*assay$")) {
                    for (Metadata m : getMetadataSet().get("Assay")) {
                        // Add all elements matching the package, core or user-specified values
                        if (m.getPackageName().equalsIgnoreCase(listBox.getValue()) || m.getPackageName().equalsIgnoreCase("default")) { // || m.getPackageName().matches("user")
                            items.get().add(m);
                            // Obligatory items to be added to list for selection
                            if (m.getRequirement().equals(Metadata.Requirement.MANDATORY)) {
                                obligatoryList.add(m);
                            } else if (m.getRequirement().equals(Metadata.Requirement.RECOMMENDED)) {
                                obligatoryList.add(m);
                            }
                        }
                    }
                }
            }

            // Check for duplicate items?
            ArrayList<Metadata> uniqueList = new ArrayList<>();
            for (Metadata metadata1 : items.get()) {
                boolean skip = false;
                for (Metadata metadata2 : uniqueList) {
                    if (metadata1.getLabel().equals(metadata2.getLabel())) {
                        skip = true;
                        break;
                    }
                }
                if (!skip) {
                    uniqueList.add(metadata1);
                }
            }

            // Remove all items
            items.get().removeAll(items.get());
            // Add the unique list TODO ensure that the obligatory items are not present twice in the other package
            items.get().addAll(uniqueList);
            items.get().addAll(otherItems.get());

            // Change label of grid based on package including its type
            gridSelection.setId(metadata + " - " + listener.getValue());
            wrapper.setId(metadata + " - " + listener.getValue());
            // Add the items and select the obligatory items
            dataProvider.set(new ListDataProvider<>(items.get()));
            // grid.setItems(metadataList);
            gridSelection.setItems(dataProvider.get());
            // gridSelection.setItems(items.get());
            // Select obligatory items
            gridSelection.asMultiSelect().select(obligatoryList);
        });

        // Set default value to core if there are no other packages present
        listBox.setValue(SELECT_A_PACKAGE);
        if (packagesViewList.size() == 2) {
            listBox.setValue("default");
        }

        // Help
        VerticalLayout header = new VerticalLayout();
        Html helpDetails = getHelpText(help);
        Div helpText = new Div();

        helpText.add(helpDetails);

        header.add(helpText, horizontalLayout, gridSelection);

        wrapper.add(header); // header text

        // List of grid objects according to the GridSelection object
        gridList.add(gridSelection);

        removePackageSelectionButton.setId(String.valueOf(gridList.size()));
        // Finalise the vertical layout with the wrapper
        verticalLayout.add(wrapper);
    }

    private HashMap<String, ArrayList<Metadata>> getHashMapFromGrid(ArrayList<Grid> gridList) {
        HashMap<String, ArrayList<Metadata>> hashMap = new HashMap<>();
        for (Grid grid : gridList) {
            if (grid.isEnabled()) {
                logger.info("Grid selection: " + grid.getId().get());
                String key = grid.getId().get();
                ArrayList<Metadata> selection = new ArrayList<>();
                for (Metadata gridSelection : (Iterable<Metadata>) grid.getSelectedItems()) {
                    selection.add(gridSelection);
                    logger.info(gridSelection.getLabel());
                }
                hashMap.put(key, selection);
            }
        }
        return hashMap;
    }

    private InputStream createResource(File excelFile) {
        try {
            return new FileInputStream(excelFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void createInvestigationLayout(VerticalLayout wrapper) {
        FormLayout layoutWithBinder = new FormLayout();
        VerticalLayout verticalLayout = new VerticalLayout();

        // Create the fields for the project section
        TextField investigationIdentifier = new TextField();
        investigationIdentifier.setPlaceholder(Application.config.getView().getValidation().getInvestigation_identifier());
        investigationIdentifier.setValueChangeMode(ValueChangeMode.EAGER);

        TextField investigationTitle = new TextField();
        investigationTitle.setPlaceholder(Application.config.getView().getValidation().getInvestigation_title());
        investigationTitle.setValueChangeMode(ValueChangeMode.EAGER);

        TextArea investigationDescription = new TextArea();
        investigationDescription.setWidthFull();
        investigationDescription.setValueChangeMode(ValueChangeMode.EAGER);
        investigationDescription.setPlaceholder(Application.config.getView().getValidation().getInvestigation_description());

        investigationBinder.forField(investigationIdentifier)
                .withValidator(new StringLengthValidator("Please add a investigation identifier (5-25 characters)", 4, 25))
                .bind(Investigation::getIdentifier, Investigation::setIdentifier);

        investigationBinder.forField(investigationTitle)
                .withValidator(new StringLengthValidator("Please add a investigation title (>10 characters)", 10, null))
                .bind(Investigation::getTitle, Investigation::setTitle);

        investigationBinder.forField(investigationDescription)
                .withValidator(new StringLengthValidator("Please add the investigation description (>50 characters)", 50, null))
                .bind(Investigation::getDescription, Investigation::setDescription);

//        layoutWithBinder.addFormItem(investigationIdentifier, "Investigation Name");
//        layoutWithBinder.addFormItem(investigationTitle, "Investigation Title");
//        layoutWithBinder.addFormItem(investigationDescription, "Investigation Description");

        investigationIdentifier.setWidthFull();
        investigationIdentifier.setLabel("Investigation identifier: This is a unique code that distinctly labels and tracks a specific scientific investigation");

        investigationTitle.setWidthFull();
        investigationTitle.setLabel("Investigation Title: Human-readable string summarising the investigation.");

        investigationDescription.setWidthFull();
        investigationDescription.setLabel("Investigation Description: Human-readable text describing the investigation in more detail.");

        layoutWithBinder.add(investigationIdentifier, 3);
        layoutWithBinder.add(investigationTitle, 3);
        layoutWithBinder.add(investigationDescription, 3);

        // Help
        // Html helpDetails = getHelpText("help/investigation.txt");
        verticalLayout.add(layoutWithBinder); // , helpDetails

        wrapper.add(verticalLayout);

        if (Application.debug) {
            investigationIdentifier.setValue("INV_ID");
            investigationDescription.setValue("INVESTIGATION DESCRIPTION THAT CONTAINS A LOT OF TEXT SO IT IS ACCEPTED BY THE VALIDATOR ETC ...");
            investigationTitle.setValue("INVESTIGATION TITLE INFORMATION");
        }

        // Force 3 column layout for the investigation section shows a bit worse so default is used.
        FormLayout layoutWithBinder2 = new FormLayout();

        Binder<Member> memberBinder = new Binder<>();

        // Create the fields for member section
        TextField firstName = new TextField();
        firstName.setValueChangeMode(ValueChangeMode.EAGER);

        TextField lastName = new TextField();
        lastName.setValueChangeMode(ValueChangeMode.EAGER);

        TextField email = new TextField();
        email.setValueChangeMode(ValueChangeMode.EAGER);

        TextField orcid = new TextField();
        orcid.setValueChangeMode(ValueChangeMode.EAGER);

        TextField organization = new TextField();
        organization.setValueChangeMode(ValueChangeMode.EAGER);

        TextField department = new TextField();
        department.setValueChangeMode(ValueChangeMode.EAGER);

        // Vaadin drop down menu
        Select<String> role = new Select<>();
        role.setItems("Contact person", "Project leader", "Investigator", "Researcher", "Technician");

        // Generate UUID (hidden) for editing a member.
        TextField identifier = new TextField();
        identifier.setValue(UUID.randomUUID().toString());

        // Create the buttons used on the member section
        Button save = new Button("Add");
        Button reset = new Button("Clear");
        Button delete = new Button("Remove Member");

        // Add all elements to the layout
        layoutWithBinder2.addFormItem(firstName, "First name");
        layoutWithBinder2.addFormItem(lastName, "Last name");
        layoutWithBinder2.addFormItem(email, "E-mail");
        layoutWithBinder2.addFormItem(orcid, "ORCID");
        layoutWithBinder2.addFormItem(organization, "Organization");
        layoutWithBinder2.addFormItem(department, "Department");
        layoutWithBinder2.addFormItem(role, "Role");

        // Button bar
        HorizontalLayout buttons = new HorizontalLayout();
        buttons.add(save, reset);
        save.getStyle().set("marginRight", "10px");

        // E-mail and phone have specific validators
        Binder.Binding<Member, String> emailBinding = memberBinder.forField(email)
                .withValidator(new EmailValidator("Incorrect email address"))
                .bind(Member::getEmail, Member::setEmail);

        // Set Required Fields
        firstName.setRequiredIndicatorVisible(true);
        lastName.setRequiredIndicatorVisible(true);
        email.setRequiredIndicatorVisible(true);
        organization.setRequiredIndicatorVisible(true);
        department.setRequiredIndicatorVisible(true);

        memberBinder.forField(firstName)
                .withValidator(new StringLengthValidator(
                        "Please add the first name", 1, null))
                .bind(Member::getFirstName, Member::setFirstName);
        memberBinder.forField(lastName)
                .withValidator(new StringLengthValidator(
                        "Please add the last name", 1, null))
                .bind(Member::getLastName, Member::setLastName);
        memberBinder.forField(organization)
                .withValidator(new StringLengthValidator(
                        "Please add the organization", 1, null))
                .bind(Member::getOrganization, Member::setOrganization);
        memberBinder.forField(department)
                .withValidator(new StringLengthValidator(
                        "Please add the department", 1, null))
                .bind(Member::getDepartment, Member::setDepartment);
        memberBinder.forField(orcid)
                .bind(Member::getORCID, Member::setORCID);
        memberBinder.forField(identifier)
                .bind(Member::getIdentifier, Member::setIdentifier);
        memberBinder.forField(role)
                .bind(Member::getRole, Member::setRole);

        // Grid section
        Grid<Member> grid = new Grid<>();
        grid.setItems(memberList);

        // Add Grid Columns
        grid.addColumn(Member::getFirstName).setHeader("First Name");
        grid.addColumn(Member::getLastName).setHeader("Last Name");
        grid.addColumn(Member::getEmail).setHeader("E-Mail");
        grid.addColumn(Member::getORCID).setHeader("ORCID");
        grid.addColumn(Member::getOrganization).setHeader("Organization");
        grid.addColumn(Member::getDepartment).setHeader("Department");
        grid.addColumn(Member::getRole).setHeader("Role");
        grid.setHeight(250, Unit.PIXELS);


        grid.asSingleSelect().addValueChangeListener(event -> {
            if (!grid.getSelectedItems().isEmpty()) {
                // Notification.show(message);
                memberBinder.readBean(grid.getSelectedItems().iterator().next());
            }
        });


        // Click listeners for the buttons
        save.addClickListener(event -> {
            Member memberBeingEdited = new Member();
            // The object that will be edited VALIDATE IF UUID ALREADY EXISTS...
            if (memberBinder.writeBeanIfValid(memberBeingEdited)) {
                logger.info(memberBeingEdited.getIdentifier());
                int index = -1;
                for (Member member : memberList) {
                    // There is an empty identifier?
                    if (!member.getIdentifier().isEmpty() && member.getIdentifier().equals(memberBeingEdited.getIdentifier())) {
                        // Delete member and add member being edited
                        index = memberList.indexOf(member);
                        break;
                    }
                }
                if (index != -1) {
                    memberList.remove(index);
                }

                memberList.add(memberBeingEdited);
                grid.getDataProvider().refreshAll();
                memberBinder.readBean(null);

            } else {
                BinderValidationStatus<Member> validate = memberBinder.validate();
//                String errorText = validate.getFieldValidationStatuses()
//                        .stream().filter(BindingValidationStatus::isError)
//                        .map(BindingValidationStatus::getMessage)
//                        .map(Optional::get).distinct()
//                        .collect(Collectors.joining(", "));
            }
        });
        reset.addClickListener(event -> {
            // clear fields by setting null
            memberBinder.readBean(null);
            identifier.setValue(UUID.randomUUID().toString());
            // infoLabel.setText("");
        });

        delete.addClickListener(event -> {
            // clear fields by setting null
            String tobeRemoved = grid.getSelectedItems().iterator().next().getIdentifier();
            int index = -1;
            for (Member member : memberList) {
                if (member.getIdentifier().equals(tobeRemoved)) {
                    // Delete selected member
                    index = memberList.indexOf(member);
                    break;
                }
            }
            if (index != -1) {
                memberList.remove(index);
            }
            grid.getDataProvider().refreshAll();
            memberBinder.readBean(null);
            identifier.setValue(UUID.randomUUID().toString());
        });

        // Help
        VerticalLayout verticalLayout2 = new VerticalLayout();
        // If anything other than unlock
        Html helpDetails2 = getHelpText("help/member.txt");
        verticalLayout2.add(layoutWithBinder2, helpDetails2);

        // Adding the message box, field layout, grid and delete button
        wrapper.add(verticalLayout2, buttons, grid, delete); // infoLabel

        if (Application.debug) {
            Member member = new Member();
            member.setDepartment("DEPARTMENT");
            member.setEmail("member@member.com");
            member.setFirstName("FIRSTNAME");
            member.setIdentifier(UUID.randomUUID().toString());
            member.setLastName("LASTNAME");
            member.setORCID("123456789");
            member.setOrganization("ORGANIZATION");
            member.setRole("A ROLE");
            memberList.add(member);
        }
    }

    private Html getHelpText(String help) {
        // HELP
        String content = getFileContentFromResources(help);
        Html text;
        if (content.contains("Reading failed")) {
            logger.error("Reading of help text from " + help + " failed. " + content);
            text = new Html("<span></span>");
        } else {
            String helpText = new Scanner(getFileContentFromResources(help)).useDelimiter("\\Z").next();
            text = new Html("<span>" + helpText + "</span>");
        }
        return text;
    }

    private void createStudy(VerticalLayout wrapper) {
        FormLayout layoutWithBinder = new FormLayout();
        VerticalLayout verticalLayout = new VerticalLayout();

        // Create the fields for the project section
        TextField studyIdentifier = new TextField();
        studyIdentifier.setPlaceholder(Application.config.getView().getValidation().getStudy_identifier());
        studyIdentifier.setValueChangeMode(ValueChangeMode.EAGER);

        TextField studyTitle = new TextField();
        studyTitle.setValueChangeMode(ValueChangeMode.EAGER);
        studyTitle.setPlaceholder(Application.config.getView().getValidation().getStudy_title());

        TextArea studyDescription = new TextArea();
        studyDescription.setWidthFull();
        studyDescription.setValueChangeMode(ValueChangeMode.EAGER);
        studyDescription.setPlaceholder(Application.config.getView().getValidation().getStudy_description());

        studyBinder.forField(studyIdentifier)
                .withValidator(new StringLengthValidator("Please add study identifier (5-25 characters)", 5, 25))
                .bind(Study::getIdentifier, Study::setIdentifier);

        studyBinder.forField(studyTitle)
                .withValidator(new StringLengthValidator("Please add a study title (>10 characters)", 10, null))
                .bind(Study::getTitle, Study::setTitle);

        studyBinder.forField(studyDescription)
                .withValidator(new StringLengthValidator("Please add the study description (>50 characters)", 50, null))
                .bind(Study::getDescription, Study::setDescription);


        studyIdentifier.setWidthFull();
        studyIdentifier.setLabel("Study identifier");

        studyTitle.setWidthFull();
        studyTitle.setLabel("Study title");

        studyDescription.setWidthFull();
        studyDescription.setLabel("Study description / Experimental design");

        layoutWithBinder.add(studyIdentifier, 3);
        layoutWithBinder.add(studyTitle, 3);
        layoutWithBinder.add(studyDescription, 3);

        // Help
        Html helpDetails = getHelpText("help/study.txt");
        verticalLayout.add(layoutWithBinder, helpDetails);

        wrapper.add(verticalLayout);

        if (Application.debug) {
            studyIdentifier.setValue("STUDY_IDENTIFIER");
            studyDescription.setValue("STUDY DESCRIPTION THAT CONTAINS A LOT OF TEXT SO IT IS ACCEPTED BY THE VALIDATOR ETC ...");
            studyTitle.setValue("STUDY TITLE INFORMATION");
        }
    }

    private void exportSheet(Grid<Metadata> metadataGrid) {
        // Create Excel sheet
//        HashMap<String, ArrayList<Metadata>> gridSelection = getHashMapFromGrid(gridList);

        File excelFile = new File(metadataGrid.getId().get() + ".xlsx");

        makeSheetOnly(excelFile, metadataGrid.getSelectedItems(), metadataGrid.getId().get());

        FileDownloadWrapper buttonWrapper = new FileDownloadWrapper(new StreamResource(excelFile.getName(), () -> createResource(excelFile)));
        buttonWrapper.wrapComponent(buttonDownload);
        add(buttonWrapper);
        buttonDownload.clickInClient();

        Button button = new Button("Hidden Download");
        button.setVisible(false);

        button.addClickListener(clicked -> {
            StreamResource streamResource = new StreamResource(excelFile.getName(), () -> {
                try {
                    return new FileInputStream(excelFile.getPath());
                } catch (FileNotFoundException e) {
                    Notification.show("Failed to download file", 3000, Notification.Position.MIDDLE);
                }
                return null;
            });
            Anchor link = new Anchor(streamResource, "SOME CONTENT");
            link.getElement().setAttribute("download", true);
            add(link);
            // Trigger the anchor link programmatically
            UI.getCurrent().getPage().executeJs("arguments[0].click()", link.getElement());
        });
        button.clickInClient();
        button.click();
    }
}
