package nl.fairbydesign.views.bioprojects;

import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.progressbar.ProgressBar;
import com.vaadin.flow.component.progressbar.ProgressBarVariant;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.router.*;
import com.vaadin.flow.server.SessionDestroyEvent;
import com.vaadin.flow.server.SessionDestroyListener;
import com.vaadin.flow.server.StreamResource;
import nl.fairbydesign.Application;
import nl.fairbydesign.backend.WebGeneric;
import nl.fairbydesign.backend.bioprojects.ebi.EBI;
import nl.fairbydesign.views.main.MainView;
import org.apache.commons.lang.StringUtils;
import org.apache.jena.atlas.lib.DateTimeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static com.vaadin.flow.component.Unit.PERCENTAGE;
import static com.vaadin.flow.component.Unit.PIXELS;
import static com.vaadin.flow.data.value.ValueChangeMode.EAGER;
import static nl.fairbydesign.backend.Generic.zipFiles;

@Route(value = "bioproject", layout = MainView.class)
@PageTitle("BioProject Metadata Generator")
@CssImport("./styles/views/empty/empty-view.css")
@PreserveOnRefresh
public class BioProjectView extends Div implements BeforeLeaveObserver, SessionDestroyListener, BeforeEnterObserver {
    private final ArrayList<String> logMessage = new ArrayList<>();
    private final File zipFile = new File(Application.commandOptions.storage + "/bioprojects/raw/" + UUID.randomUUID() + ".zip");
    private final HashSet<File> rawFiles = new HashSet<>();
    private String excelFilename;
    private final Logger logger = LogManager.getLogger(BioProjectView.class);
    private final TextArea stacktraceArea;

    private final HashSet<Thread> threads = new HashSet<>();

    @Override
    public void sessionDestroy(SessionDestroyEvent event) {
        logger.warn("Session destroyed");
        threads.forEach(Thread::interrupt);
    }

    @Override // inform the user about an authentication error
    public void beforeEnter(BeforeEnterEvent beforeEnterEvent) {
        if (!Application.config.getMenus().isBioprojects()) {
            beforeEnterEvent.forwardTo("");
        }
    }
    @Override
    public void beforeLeave(BeforeLeaveEvent event) {
        // Handle the before leave event here
        // You can perform any necessary cleanup or show a confirmation dialog
        logger.debug("Before leave event");
        threads.forEach(Thread::interrupt);
    }


    public BioProjectView() {
        int charLimit = 1000000;
        excelFilename = Application.commandOptions.storage +"/bioprojects/xlsx/" + UUID.randomUUID() + ".xlsx";
        // Create parent directory
        new File(excelFilename).getParentFile().mkdirs();
        // Set up text area
        TextArea textArea = new TextArea();
        textArea.setPlaceholder("Place the text containing bioproject identifiers here.\n" +
                "For example: PRJDB5113 you can even PRJEB1213 write in between PRJEB17083.\n" +
                "\nThe identifiers will be detected automatically.\n" +
                "\nYou can find the identifiers on the EBI website: https://www.ebi.ac.uk/ena/browser/home\n");
        textArea.setLabel("Bioproject identifiers");
        textArea.setMaxLength(charLimit);
        textArea.setValueChangeMode(EAGER);
        textArea.setWidthFull();
        textArea.setHeightFull();
        textArea.setMaxWidth(50, PERCENTAGE);
        textArea.setMaxHeight(500, PIXELS);

        stacktraceArea = new TextArea("Info");
        stacktraceArea.setWidthFull();
        stacktraceArea.setHeightFull();
        stacktraceArea.setMaxWidth(100, PERCENTAGE);
        stacktraceArea.setMaxHeight(500, PIXELS);
        stacktraceArea.setVisible(false);
        stacktraceArea.setEnabled(false);

        HashSet<String> bioprojects = new HashSet<>();
        textArea.addValueChangeListener(e -> {
            bioprojects.clear();
            String regex = "(PRJ[DEN][A-Z]\\d+)";
            Pattern r = Pattern.compile(regex);
            Matcher m = r.matcher(textArea.getValue());
            while (m.find()) {
                bioprojects.add(m.group());
            }
            e.getSource().setHelperText("Detected " + bioprojects.size() + " projects");
        });

        Button generateExcelMetadataButton = new Button("Generate");
        // Check box for individual files
        Select select = new Select();
        select.setItems("Combined excel files", "Individual excel files", "RDF data file", "Raw XML files");
        select.setValue("Combined excel files");

        // Checkbox individualFilesCheckbox = new Checkbox("Individual project files");
        // Add download button
        Button downloadExcelMetadataButton = new Button("Download");
        add(downloadExcelMetadataButton);
        downloadExcelMetadataButton.addClickListener(downloadEvent -> {
            downloadFunction(logMessage, new File(excelFilename));
        });
        downloadExcelMetadataButton.setEnabled(false);
        downloadExcelMetadataButton.setVisible(false);

        HorizontalLayout horizontalLayout = new HorizontalLayout();

        // Download
        horizontalLayout.add(generateExcelMetadataButton, downloadExcelMetadataButton, select); // , downloadExcelMetadataButton); // , buttonWrapper);

        HorizontalLayout topSection = new HorizontalLayout();
        VerticalLayout progressbarLayout = new VerticalLayout();
        topSection.add(textArea, progressbarLayout);
        add(topSection);
        add(horizontalLayout);
        // Add stacktrace area below everything else to make sure it is full width
        add(stacktraceArea);

        ProgressBar progressBar = new ProgressBar();
        progressBar.setVisible(false);
        ProgressBar memoryBar = new ProgressBar();
        memoryBar.setVisible(false);
        memoryBar.setMin(0);
        memoryBar.setMax(100);
        Div memoryBarLabel = new Div();
        Div progressBarLabel = new Div();
        progressbarLayout.add(progressBarLabel, progressBar, memoryBarLabel, memoryBar);

        // Set stacktrace layout full width below everything else

        // Set footer
        Html footerContent = WebGeneric.getTextFromResource("views/footer.html");
        Div footer = new Div();
        footer.add(footerContent);
        add(footer);

        generateExcelMetadataButton.addClickListener(clicked -> {
            // AtomicReference<String> logMessage = new AtomicReference<>("");
            // If bioprojects is empty use the demo example
            if (bioprojects.isEmpty())
                textArea.setValue(textArea.getPlaceholder());
            // Get the bioprojects from the text area
            ArrayList<String> xlsx_bioprojects = new ArrayList<>();
            String selectType = select.getValue().toString();

            // Start processing the bioprojects
            // EBI.message = DateTimeUtils.nowAsString() + " - Starting to generate metadata";
            logMessage.add(0, DateTimeUtils.nowAsString() + " - Starting to generate metadata");
            // Forces a refresh which includes a new UUID needed for the Excel file
            generateExcelMetadataButton.setEnabled(false);
            downloadExcelMetadataButton.setEnabled(true);
            // Create a separate thread to show the EBI.message content when it changes
            Thread message = new Thread(() -> {
                // Initialize the interface for the progress bar and information
                progressBar.getUI().ifPresent(ui -> ui.access(() -> progressBar.setMin(0)));
                progressBar.getUI().ifPresent(ui -> ui.access(() -> progressBar.setMax(bioprojects.size())));
                memoryBar.getUI().ifPresent(ui -> ui.access(() -> memoryBar.setMin(0)));
                memoryBar.getUI().ifPresent(ui -> ui.access(() -> memoryBar.setMax(100)));
                memoryBar.getUI().ifPresent(ui -> ui.access(() -> memoryBar.setVisible(true)));
                memoryBarLabel.getUI().ifPresent(ui -> ui.access(() -> memoryBarLabel.setVisible(true)));

                progressBarLabel.getUI().ifPresent(ui -> ui.access(() -> progressBarLabel.setText("Processing files (0/" + bioprojects.size() + ")")));
                progressBar.getUI().ifPresent(ui -> ui.access(() -> progressBar.setVisible(true)));
                progressBarLabel.getUI().ifPresent(ui -> ui.access(() -> progressBarLabel.setVisible(true)));
                stacktraceArea.getUI().ifPresent(ui -> ui.access(() -> stacktraceArea.setEnabled(true)));
                stacktraceArea.getUI().ifPresent(ui -> ui.access(() -> stacktraceArea.setVisible(true)));
                stacktraceArea.getUI().ifPresent(ui -> ui.access(() -> stacktraceArea.setValue(StringUtils.join(logMessage, "\n"))));

                while (true) {
                    // Obtain message from vaadin
                    stacktraceArea.getUI().ifPresent(ui -> ui.access(() -> stacktraceArea.setValue(StringUtils.join(logMessage, "\n"))));
                    try {
                        TimeUnit.MILLISECONDS.sleep(100);
                    } catch (InterruptedException e) {
                        logger.debug("Message thread stopped");
                    }
                }
            });
            // Start the logging thread to show the EBI.message
            message.start();
            threads.add(message);

            // Create a separate thread to process the bioprojects
            Thread t = new Thread(() -> {
                // Loop for the progress bar to fetch all data
                int progressBarValue = 0;
                EBI ebi = new EBI();
                logMessage.add(0, "Processing " + bioprojects.size() + " bioprojects");

                // A thread to retrieve and convert all bioproject files
                HashSet<String> bioprojectsKeep = new HashSet();
                for (String bioproject : bioprojects) {
                    progressBarValue++;
                    int finalProgressBarValue = progressBarValue;
                    progressBar.getUI().ifPresent(ui -> ui.access(() -> progressBar.setValue(finalProgressBarValue)));
                    progressBarLabel.getUI().ifPresent(ui -> ui.access(() -> progressBarLabel.setText("Processing project " + bioproject + " (" + finalProgressBarValue + "/" + bioprojects.size() + ")")));

                    // A thread to retrieve and convert all bioproject files
                    Thread bioprojectThread = new Thread(() -> {
                        EBI ebiThread = new EBI();
                        // To skip excel creation as this is the initial conversion
                        HashMap<String, ArrayList<File>> rawEnaFiles = ebiThread.fetch(logMessage, new HashSet<>(Set.of(bioproject)), false, true);
                        if (selectType.equalsIgnoreCase("Raw XML files")) {
                            rawEnaFiles.forEach((key, value) -> {
                                for (File file : value) {
                                    rawFiles.add(file);
                                }
                            });
                        } else {
                            // Convert to RDF?
                        }

                        logMessage.add(0, "Retrieved and converted " + bioproject);
                        bioprojectsKeep.add(bioproject);
                    });

                    bioprojectThread.start();

                    Thread memoryThread = new Thread(() -> {
                        while (true) {
                            long freeMemory = Application.getFreeMemory();
                            long percentageUsed = Application.getPercentageUsed();
                            long usedMemory = Application.getUsedMemory();
                            long maxMemory = Application.getMaxMemory();

                            String memoryMessage = "Memory usage: " + percentageUsed + "% (" + usedMemory + "MB/" + maxMemory + "MB)";

                            if (freeMemory < 100 && freeMemory > 0) {
                                logMessage.add(0, "Project " + bioproject + " stopped due to low memory (" + freeMemory + ") - " + memoryMessage);
                                bioprojectThread.interrupt();
                                return;
                            }

                            // Logging for memory usage
                            memoryBarLabel.getUI().ifPresent(ui -> ui.access(() -> memoryBarLabel.setText(memoryMessage)));
                            memoryBar.getUI().ifPresent(ui -> ui.access(() -> memoryBar.setVisible(true)));
                            memoryBar.getUI().ifPresent(ui -> ui.access(() -> memoryBar.setValue(percentageUsed)));
                            if (percentageUsed < 50) {
                                memoryBar.getUI().ifPresent(ui -> ui.access(() -> memoryBar.addThemeVariants(ProgressBarVariant.LUMO_CONTRAST)));
                            } else if (percentageUsed < 70) {
                                memoryBar.getUI().ifPresent(ui -> ui.access(() -> memoryBar.addThemeVariants(ProgressBarVariant.LUMO_SUCCESS)));
                            } else if (percentageUsed < 90) {
                                memoryBar.getUI().ifPresent(ui -> ui.access(() -> memoryBar.addThemeVariants(ProgressBarVariant.LUMO_ERROR)));
                            } else {
                                memoryBar.getUI().ifPresent(ui -> ui.access(() -> memoryBar.addThemeVariants(ProgressBarVariant.LUMO_ERROR)));
                            }

                            // If the bioproject thread is finished
                            if (!bioprojectThread.isAlive())
                                return;
                            // Sleep for 1 second
                            try {
                                TimeUnit.SECONDS.sleep(1);
                            } catch (InterruptedException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    });

                    // Wait for the bioproject thread to finish
                    try {
                        memoryThread.start();
                        bioprojectThread.join();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
                // Reset the progress bar
                progressBarValue = 0;

                logMessage.add(0, "Retrieved and converted " + bioprojectsKeep.size() + " bioprojects of which " + (bioprojects.size() - bioprojectsKeep.size()) + " failed");

                // For raw generation, we create a zip file with all xml files and directories
                if (selectType.equalsIgnoreCase("Raw XML files")) {
                    // Add all files from rawFiles
                    zipFiles(rawFiles, zipFile);
                }

                // For RDF and Excel generation
                for (String bioproject : bioprojectsKeep) {
                    // If checked, a new EBI object is created for each bioproject
                    if (selectType.equalsIgnoreCase("Individual excel files")) {
                        ebi = new EBI();
                    }
                    progressBarValue++;
                    int finalProgressBarValue = progressBarValue;
                    progressBar.getUI().ifPresent(ui -> ui.access(() -> progressBar.setValue(finalProgressBarValue)));
                    progressBarLabel.getUI().ifPresent(ui -> ui.access(() -> progressBarLabel.setText("Processing project " + bioproject + " (" + finalProgressBarValue + "/" + bioprojects.size() + ")")));

                    // Execute EBI retrieval and conversion one by one (progress bar)
                    try {
                        if (selectType.equalsIgnoreCase("Raw XML files")) {
                            // All raw xml files from a project
                            ebi.fetch(logMessage, new HashSet<>(Set.of(bioproject)), false, true);
                            logMessage.add(0, "Retrieved " + ebi.rawFiles.size() + " Raw files");
                        } else if (selectType.equalsIgnoreCase("RDF data file")) {
                            // All RDF files from a project
                            ebi.fetch(logMessage, new HashSet<>(Set.of(bioproject)), true);
                            logMessage.add(0, "Retrieved " + ebi.rdfFiles.size() + " RDF files");
                        } else {
                            ebi.fetch(logMessage, new HashSet<>(Set.of(bioproject)));
                        }
                        // If individual files are selected, a new EBI object is created for each bioproject
                        if (selectType.equalsIgnoreCase("Individual excel files")) {
                            excelFilename = Application.commandOptions.storage +"/bioprojects/xlsx/" + bioproject + ".xlsx";
                            ebi.xlsx.createExcelFile(logMessage, excelFilename);
                            xlsx_bioprojects.add(excelFilename);
                        }
                    } catch (Exception e) {
                        logMessage.add(0, e.getMessage());
                    }
                    // Update stacktrace area
                    try {
                        Thread.sleep(1);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }

                // Combine all RDF files
                if (selectType.equalsIgnoreCase("RDF data file")) {
                    logMessage.add(0, DateTimeUtils.nowAsString() + "- Combining RDF files...");
                    EBI rdfEbi = new EBI();
                    HashMap<String, ArrayList<File>> biordfFiles = rdfEbi.fetch(logMessage, bioprojects, true);
                    // Create a zip file with all RDF files
                    HashSet<File> rdfFiles = new HashSet<>();
                    for (String bioproject : biordfFiles.keySet()) {
                        biordfFiles.get(bioproject).forEach(rdfFiles::add);
                    }

                    logMessage.add(0, DateTimeUtils.nowAsString() + "- Creating zip file... with " + rdfFiles.size() + " files");
                    zipFiles(rdfFiles, zipFile);

                } else if (selectType.equalsIgnoreCase("Combined excel files")) {
                    logMessage.add(0, "Preparing excel file for download...");
                    ebi.xlsx.createExcelFile(logMessage, excelFilename);
                    logMessage.add(0, "Ready for download");
                }

                // Progress information invisible
                progressBar.getUI().ifPresent(ui -> ui.access(() -> progressBar.setVisible(false)));
                progressBarLabel.getUI().ifPresent(ui -> ui.access(() -> progressBarLabel.setVisible(false)));
                // Memory information invisible
                memoryBarLabel.getUI().ifPresent(ui -> ui.access(() -> memoryBarLabel.setVisible(false)));
                memoryBar.getUI().ifPresent(ui -> ui.access(() -> memoryBar.setVisible(false)));

                // Interrupt another thread?
                message.interrupt();
                // TODO check if this is user specific!! No cross contamination?
                if (selectType.equalsIgnoreCase("Raw XML files")) {
                    logMessage.add(0, "The following zip file was created: " + zipFile.getAbsolutePath());
                    excelFilename = zipFile.getAbsolutePath();
                } else if (selectType.equalsIgnoreCase("RDF data file")) {
                    logMessage.add(0, "Done..." + "The RDF file should be downloaded automatically.");
                    excelFilename = zipFile.getAbsolutePath();
                } else if (selectType.equalsIgnoreCase("Individual excel files")) {
                    logMessage.add(0, "The following files were created: " + StringUtils.join(xlsx_bioprojects, ", "));
                    // Zip them all into one zip file
                    try {
                        final FileOutputStream fos = new FileOutputStream(zipFile);
                        ZipOutputStream zipOut = new ZipOutputStream(fos);
                        for (String xlsx : xlsx_bioprojects) {
                            logMessage.add(0, "Adding file to zip: " + xlsx);
                            File fileToZip = new File(xlsx);
                            FileInputStream fis = new FileInputStream(fileToZip);
                            ZipEntry zipEntry = new ZipEntry(fileToZip.getName());
                            zipOut.putNextEntry(zipEntry);

                            byte[] bytes = new byte[1024];
                            int length;
                            while ((length = fis.read(bytes)) >= 0) {
                                zipOut.write(bytes, 0, length);
                            }
                            fis.close();
                        }
                        zipOut.close();
                        fos.close();
                        // Set filename to zip file
                        excelFilename = zipFile.getAbsolutePath(); // "bioprojects.zip";
                        logMessage.add(0, "The following zip file was created: " + zipFile.getName() + ".zip");
                    } catch (IOException e) {
                        logMessage.add(0, "Failed to zip files: " + e.getMessage());
                    }
                } else {
                    logMessage.add(0, "Done... The Excel file should be downloaded automatically.");
                    logMessage.add(0, "Retrieved and converted " + bioprojectsKeep.size() + " bioprojects of which " + (bioprojects.size() - bioprojectsKeep.size()) + " failed");
                }
                downloadExcelMetadataButton.getUI().ifPresent(ui -> ui.access(() -> downloadExcelMetadataButton.clickInClient()));
                downloadExcelMetadataButton.getUI().ifPresent(ui -> ui.access(() -> downloadExcelMetadataButton.setVisible(true)));
            });
            threads.add(t);
            // Start the thread to fetch the data from EBI and convert
            t.start();

            // Start a thread to check the state of the t thread to be able to send a message to the user if it fails...
            Thread tMessage = new Thread(() -> {
                while (true) {
                    try {
                        Thread.sleep(1000);
                        if (!t.isAlive()) {
                            // Check for errors?...
                            if (StringUtils.join(logMessage, "\n").contains("Done...")) {
                                logMessage.add(0, "Conversion finished successfully");
                            } else {
                                logMessage.add(0, "Please try again after removing the last project number (see this log for details)");
                                logMessage.add(0, "Thread is terminated, conversion failed");
                            }
                            break;
                        }
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            });
            threads.add(tMessage);
            tMessage.start();
        });
    }

    private void downloadFunction(ArrayList<String> logMessage, File excelFile) {
        logMessage.add(0, "Excel filename: " + excelFilename);
        Button button = new Button("Hidden Download");
        button.setVisible(false);

        button.addClickListener(clicked -> {
            StreamResource streamResource = new StreamResource(excelFile.getName(), () -> {
                try {
                    return new FileInputStream(excelFile.getPath());
                } catch (FileNotFoundException e) {
                    logMessage.add(0, "File not found: " + excelFile.getPath());
                    this.getUI().ifPresent(ui -> ui.access(() -> Notification.show("Failed to download file", 3000, Notification.Position.MIDDLE)));
                }
                return null;
            });
            if (streamResource != null) {
                Anchor link = new Anchor(streamResource, "SOME CONTENT");
                link.getElement().setAttribute("download", true);
                add(link);
                // Trigger the anchor link programmatically
                UI.getCurrent().getPage().executeJs("arguments[0].click()", link.getElement());
            }
        });
        button.clickInClient();
        button.click();
    }
}
