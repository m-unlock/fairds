package nl.fairbydesign.views.diskusage;


import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.NativeLabel;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.router.BeforeLeaveEvent;
import com.vaadin.flow.router.BeforeLeaveObserver;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinSession;
import nl.fairbydesign.backend.credentials.Credentials;
import nl.fairbydesign.backend.data.objects.DiskUsage;
import nl.fairbydesign.backend.data.objects.Process;
import nl.fairbydesign.backend.data.objects.ProcessLevel;
import nl.fairbydesign.backend.irods.Data;
import nl.fairbydesign.views.main.MainView;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.query.GenQueryBuilderException;
import org.irods.jargon.core.query.JargonQueryException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import java.io.File;
import java.util.ArrayList;

import static nl.fairbydesign.backend.irods.Data.getDiskUsage;
import static nl.fairbydesign.backend.irods.Data.getProgressLevels;

/**
 * Dashboard view for jobs to be processed and disk space usage
 */

@Route(value = "diskusage", layout = MainView.class)
@PageTitle("Disk Usage")
@CssImport("./styles/views/empty/empty-view.css")
public class DiskUsageView extends Div implements BeforeLeaveObserver {

    public static final Logger logger = LogManager.getLogger(DiskUsageView.class);
    private boolean threadStop = false;

    @Override
    public void beforeLeave(BeforeLeaveEvent event) {
        logger.error("Person leaves page!");
        threadStop = true;
    }

    public DiskUsageView() {
        setId("master-detail-view");

        Credentials credentials = (Credentials) VaadinSession.getCurrent().getAttribute("credentials");

        if (credentials != null && credentials.isSuccess()) {
            // Create a new thread to obtain the information
            ArrayList<DiskUsage> diskUsages = new ArrayList<>();
//            ArrayList<ProcessLevel> processLevels = new ArrayList<>();
            DiskUsage loading = new DiskUsage();
            loading.setGroup("Loading group");
            loading.setResource("Getting resource");
            loading.setStorageSize(0L);
            diskUsages.add(loading);
            // Disk usage overview
            Grid<DiskUsage> diskUsageGrid = new Grid<>(DiskUsage.class);
            // diskUsageGrid.setItems(diskUsages);
            ListDataProvider<DiskUsage> dataProvider = new ListDataProvider<>(diskUsages);
            diskUsageGrid.setItems(dataProvider);
            diskUsageGrid.removeAllColumns();
            diskUsageGrid.addColumn(DiskUsage::getGroup).setHeader("Group");
            diskUsageGrid.addColumn(DiskUsage::getResource).setHeader("Resource");
            diskUsageGrid.addColumn(DiskUsage::getStorageSizeFormat).setHeader("Storage size");
            add(diskUsageGrid);

            // processLevel grid for unlock
//            Grid<ProcessLevel> processLevelGrid = new Grid<>(ProcessLevel.class);
//            processLevelGrid.setItems(processLevels);
//            processLevelGrid.removeAllColumns();
//            processLevelGrid.addColumn(ProcessLevel::getName).setHeader("Name");
//            processLevelGrid.addColumn(ProcessLevel::getInvestigation).setHeader("Investigation");
//            processLevelGrid.addColumn(ProcessLevel::getStudy).setHeader("Study");
//            processLevelGrid.addColumn(ProcessLevel::getObservationUnit).setHeader("Observation unit");
//            processLevelGrid.addColumn(ProcessLevel::getSample).setHeader("Sample");
//            processLevelGrid.addColumn(ProcessLevel::getAssay).setHeader("Assay");
//            processLevelGrid.addColumn(ProcessLevel::getUnprocessed).setHeader("Unprocessed");
//            processLevelGrid.setVisible(false);
//            add(processLevelGrid);

            // Only visible for the unlock zone
//            if (credentials.getZone().equalsIgnoreCase("unlock")) {
//                processLevelGrid.setVisible(true);
//            }

            // Thread to collect information, TODO stop thread upon page disconnect...?
            Thread t = new Thread(() -> {
                // Get information
                logger.info("Obtaining usage information");
                // Drop down menu for projects?
                try {
                    String path = "/" + credentials.getZone() + "/home/";
                    // Obtain group names
                    // credentials.getAccessObjectFactory().getUserGroupAO(credentials.getIrodsAccount())..listUserGroupsForUserInZone(credentials.getIrodsAccount().getUserName(), credentials.getIrodsAccount().getZone());
                    for (File folder : credentials.getFileFactory().instanceIRODSFile(path).listFiles()) {
                        if (threadStop) {
                            logger.error("Thread stopped as user says bye...");
                            return;
                        }
                        logger.info("Processing for diskusage " + folder.getName());
                        DiskUsage diskUsage = new DiskUsage();
                        diskUsage.setGroup("Processing " + path);
                        // Show there is something happening
                        diskUsages.add(diskUsage);
                        // Refresh grid
                        diskUsageGrid.getUI().ifPresent(ui -> ui.access(() -> diskUsageGrid.getDataProvider().refreshAll()));
                        // Obtain disk usage from different archives
                        ArrayList<DiskUsage> diskUsageCollection = getDiskUsage(credentials, new File(path + "/" + folder.getName()));
                        // Remove placeholder
                        diskUsages.remove(diskUsage);
                        // Add all obtained storage information from specific path
                        diskUsages.addAll(diskUsageCollection);
                        // Refresh grid again to show new progress
                        diskUsageGrid.getUI().ifPresent(ui -> ui.access(() -> diskUsageGrid.getDataProvider().refreshAll()));
//                        if (credentials.getZone().equalsIgnoreCase("unlock")) {
//                            // Get progress for this folder
//                            processLevels.add(getProgressLevels(credentials, path + "/" + folder.getName()));
//                            // Refresh grid again to show new progress
//                            processLevelGrid.getUI().ifPresent(ui -> ui.access(() -> processLevelGrid.getDataProvider().refreshAll()));
//                        }
                    }
                    DiskUsage diskUsage = new DiskUsage();
                    diskUsage.setGroup("Finished");
                    diskUsages.add(diskUsage);
                    diskUsageGrid.getUI().ifPresent(ui -> ui.access(() -> diskUsageGrid.getDataProvider().refreshAll()));

                } catch (JargonException | JargonQueryException | GenQueryBuilderException e) {
                    throw new RuntimeException(e);
                }
            });
            t.start();
            // Remove first element when all folders are processed
            diskUsages.remove(0);
            diskUsageGrid.getUI().ifPresent(ui -> ui.access(() -> diskUsageGrid.getDataProvider().refreshAll()));

//            if (credentials.getZone().equalsIgnoreCase("unlock")) {
//                ArrayList<Process> processes = Data.getIS(credentials);
//                // Process overview
//                Grid<Process> grid = new Grid<>(Process.class);
//                grid.setSelectionMode(Grid.SelectionMode.SINGLE);
//                grid.setItems(processes);
//                grid.removeAllColumns();
//                grid.addColumn(Process::getInvestigationIdentifier).setHeader("Investigation");
//                grid.addColumn(Process::getStudyIdentifier).setHeader("Study");
//                grid.addColumn(Process::getWorkflow).setHeader("Workflow");
//                grid.addColumn(Process::getWaiting).setHeader("Waiting");
//                grid.addColumn(Process::getQueue).setHeader("Queue");
//                grid.addColumn(Process::getRunning).setHeader("Running");
//                grid.addColumn(Process::getFailed).setHeader("Failed");
//                grid.addColumn(Process::getFinished).setHeader("Finished");
//                grid.setMultiSort(true);
//                add(grid);
//            }

        } else {
            add(new NativeLabel("To use this you need to login first..."));
            Button button = new Button("Login", event -> UI.getCurrent().navigate("login"));
            add(button);
            button.click();
            button.clickInClient();
        }
    }
}
