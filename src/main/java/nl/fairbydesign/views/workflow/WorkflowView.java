package nl.fairbydesign.views.workflow;

import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.NativeLabel;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.progressbar.ProgressBar;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.MemoryBuffer;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import nl.fairbydesign.Application;
import nl.fairbydesign.backend.WebGeneric;
import nl.fairbydesign.views.main.MainView;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.commons.lang3.StringUtils;
import org.jermontology.ontology.JERMOntology.domain.Assay;
import org.jermontology.ontology.JERMOntology.domain.Investigation;
import org.jermontology.ontology.JERMOntology.domain.Sample;
import org.jermontology.ontology.JERMOntology.domain.Study;
import org.purl.ppeo.PPEO.owl.domain.observation_unit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;

import static com.vaadin.flow.component.notification.Notification.Position.MIDDLE;
import static com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment.CENTER;
import static nl.fairbydesign.backend.Generic.getValueOfPredicate;

@Route(value = "workflow", layout = MainView.class)
@PageTitle("Workflow generator")
@CssImport("./styles/views/empty/empty-view.css")
public class WorkflowView extends Div {
    private static final Logger log = LoggerFactory.getLogger(WorkflowView.class);
    private Domain domain;

    public WorkflowView() {
        MemoryBuffer buffer = new MemoryBuffer();
        Upload upload = new Upload(buffer);
        upload.setMaxFiles(1);
        upload.setDropLabel(new NativeLabel("Upload a project RDF file in .ttl format"));

        TextArea logArea = new TextArea("Description");
        TextArea stacktraceArea = new TextArea("Stacktrace");
        Button stacktraceButton = new Button("Stacktrace");
        stacktraceButton.setVisible(false);
        stacktraceButton.setEnabled(false);

        logArea.setWidthFull();
        logArea.setPlaceholder("Log will appear here ...");
        logArea.setVisible(false);

        stacktraceArea.setWidthFull();
        stacktraceArea.setVisible(false);

        ProgressBar progressBar = new ProgressBar();
        progressBar.setIndeterminate(true);
        progressBar.setVisible(false);

        // Grid
        ArrayList<nl.fairbydesign.backend.data.workflow.Assay> assayData = new ArrayList<>();
        Grid<nl.fairbydesign.backend.data.workflow.Assay> workflowGrid = new Grid<>(nl.fairbydesign.backend.data.workflow.Assay.class, false);
        ListDataProvider<nl.fairbydesign.backend.data.workflow.Assay> dataProvider = new ListDataProvider<>(assayData);
        workflowGrid.setItems(dataProvider);

        workflowGrid.setSelectionMode(Grid.SelectionMode.MULTI);
        workflowGrid.addColumn(nl.fairbydesign.backend.data.workflow.Assay::getIdentifier).setHeader("Identifier").setAutoWidth(true);
        workflowGrid.addColumn(nl.fairbydesign.backend.data.workflow.Assay::getaPackage).setHeader("Package").setAutoWidth(true);
        workflowGrid.addColumn(nl.fairbydesign.backend.data.workflow.Assay::getDescription).setHeader("Description").setAutoWidth(true);
        workflowGrid.addColumn(nl.fairbydesign.backend.data.workflow.Assay::getPlatform).setHeader("Platform").setAutoWidth(true);
        workflowGrid.addColumn(nl.fairbydesign.backend.data.workflow.Assay::getSelection).setHeader("Selection").setAutoWidth(true);
        workflowGrid.addColumn(nl.fairbydesign.backend.data.workflow.Assay::getSource).setHeader("Source").setAutoWidth(true);
        workflowGrid.addColumn(nl.fairbydesign.backend.data.workflow.Assay::getStrategy).setHeader("Strategy").setAutoWidth(true);
        workflowGrid.setVisible(true);

        add(upload);
        add(logArea);
        add(stacktraceButton);
        add(stacktraceArea);
        add(workflowGrid);

        stacktraceButton.addClickListener(clicked -> {
            if (stacktraceArea.isVisible()) {
                stacktraceArea.setVisible(false);
                stacktraceArea.setEnabled(false);
            } else {
                stacktraceArea.setVisible(true);
                stacktraceArea.setEnabled(true);
            }
        });

        upload.addFileRejectedListener(event -> Notification.show(event.getErrorMessage()));

        upload.addSucceededListener(succeededEvent -> {
            logArea.setValue("File " + buffer.getFileName() + " uploaded...");
            // When upload is successful start the Excel validator!
            if (!buffer.getFileName().endsWith("ttl")) {
                Notification notification = new Notification();
                notification.addThemeVariants(NotificationVariant.LUMO_ERROR);
                Div text = new Div(new Text("File format not accepted. Only validated TURTLE files (.ttl)"));
                Button closeButton = new Button(new Icon("lumo", "cross"));
                closeButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY_INLINE);
                closeButton.getElement().setAttribute("aria-label", "Close");
                closeButton.addClickListener(event -> {
                    notification.close();
                    // Refresh current page
                    UI.getCurrent().getPage().reload();
                });

                HorizontalLayout layout = new HorizontalLayout(text, closeButton);
                layout.setAlignItems(CENTER);
                notification.setPosition(MIDDLE);
                notification.add(layout);
                notification.open();
                try {
                    buffer.getInputStream().close();
                } catch (IOException e) {
                    log.error("Upload closing stream failed: " + e.getMessage());
                }
            } else {
                try {
                    logArea.setVisible(true);
                    progressBar.setVisible(true);
                    // Load TURTLE FILE as memory and generate files
                    // Storage in a specified location with ena subfolder for the turtle file
                    Path path = new File(Application.commandOptions.storage + "/ena/").toPath();
                    path.toFile().mkdirs();
                    path = Files.createTempFile(path,"ena_", ".ttl");
                    Files.copy(buffer.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
                    domain = new Domain("file://" + path.toFile().getAbsolutePath());
                    logArea.setValue(logArea.getValue() + "\nFinished loading " + domain.getRDFSimpleCon().getModel().size() + " statements");
                    ArrayList<String> investigations = new ArrayList<>();
                    domain.getRDFSimpleCon().runQuery("getInvestigations.txt", true).iterator().
                            forEachRemaining(resultLine -> investigations.add(resultLine.getIRI("iri")));

                    for (String investigationIRI : investigations) {
                        Investigation investigation = domain.make(Investigation.class, investigationIRI);
                        for (Study study : investigation.getAllHasPart()) {
                            for (observation_unit observationUnit : study.getAllHasPart()) {
                                for (Sample sample : observationUnit.getAllHasPart()) {
                                    for (Assay assay : sample.getAllHasPart()) {
                                        // List grid with assay selection and its types
                                        nl.fairbydesign.backend.data.workflow.Assay workflowAssay = new nl.fairbydesign.backend.data.workflow.Assay();
                                        // Set minimal information needed
                                        workflowAssay.setIdentifier(assay.getIdentifier());
                                        workflowAssay.setDescription(assay.getDescription());
                                        workflowAssay.setPlatform(getValueOfPredicate(domain, assay.getResource().getURI(), "base:platform"));
                                        workflowAssay.setSelection(getValueOfPredicate(domain, assay.getResource().getURI(), "base:library_selection"));
                                        workflowAssay.setSource(getValueOfPredicate(domain, assay.getResource().getURI(), "base:library_source"));
                                        workflowAssay.setStrategy(getValueOfPredicate(domain, assay.getResource().getURI(), "base:library_strategy"));
                                        workflowAssay.setType(StringUtils.join(assay.getAllAdditionalType(), " "));
                                        workflowAssay.setaPackage(getValueOfPredicate(domain,assay.getResource().getURI(), "http://fairbydesign.nl/ontology/packageName"));
                                        assayData.add(workflowAssay);
                                    }
                                }
                            }
                        }
                    }
                    // Refresh grid
                    workflowGrid.getUI().ifPresent(ui -> ui.access(() -> workflowGrid.getDataProvider().refreshAll()));
                } catch (Exception e) {
                    stacktraceButton.setEnabled(true);
                    stacktraceArea.setValue(e.getMessage() + "\n\n" + StringUtils.join(e.getStackTrace(),"\n"));
                }
            }
        });

        // Set footer
        Html footerContent = WebGeneric.getTextFromResource("views/footer.html");
        Div footer = new Div();
        footer.add(footerContent);
        add(footer);
    }
}
