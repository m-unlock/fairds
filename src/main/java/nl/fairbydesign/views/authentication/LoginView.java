package nl.fairbydesign.views.authentication;

import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.accordion.Accordion;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.login.LoginForm;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinService;
import com.vaadin.flow.server.VaadinSession;
import jakarta.servlet.http.Cookie;
import nl.fairbydesign.Application;
import nl.fairbydesign.backend.config.Config;
import nl.fairbydesign.backend.config.Irods;
import nl.fairbydesign.backend.credentials.Credentials;
import org.irods.jargon.core.exception.JargonException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.irods.jargon.core.connection.AuthScheme.*;
import static org.irods.jargon.core.connection.ClientServerNegotiationPolicy.SslNegotiationPolicy.*;

/**
 * Login and authentication system based on iRODS
 */
@Route("login")
@PageTitle("Login | iRODS / Yoda")
public class LoginView extends VerticalLayout implements BeforeEnterObserver {

    private final LoginForm login = new LoginForm();
    private static final Logger logger = LoggerFactory.getLogger(LoginView.class);

    // Before enter event
    public LoginView() {
        addClassName("login-view");
        setSizeFull();
        setAlignItems(Alignment.CENTER);
        setJustifyContentMode(JustifyContentMode.CENTER);

        login.setAction("login");

        Select<String> iRODS = new Select<>();
        iRODS.setLabel("iRODS instances");
        ArrayList<String> labels = new ArrayList<>();
        for (Irods irods : Application.config.getIrods()) {
            labels.add(irods.getLabel());
        }
        iRODS.setItems(labels);
        // Set the first in the array as default value
        iRODS.setValue(labels.get(0));

        add(iRODS);

        // irodsHost
        TextField hostField = new TextField("Host");
        // irodsZone
        TextField zoneField = new TextField("Zone");
        // irodsPort
        TextField portField = new TextField();
        portField.setPattern("[0-9]*");
        portField.setInvalid(true);
        portField.setMaxLength(4);
        portField.setLabel("Port");
        portField.setValue("1247");

        // irodsSSL
        Select<String> sslField = new Select<>();
        sslField.setLabel("SSL Negotiation Policy");
        sslField.setItems(CS_NEG_REQUIRE.name(), CS_NEG_DONT_CARE.name(), CS_NEG_REFUSE.name(), NO_NEGOTIATION.name(), CS_NEG_FAILURE.name());

        // irodsAuthScheme
        Select<String> authField = new Select<>();
        authField.setLabel("Authentication schema");
        authField.setItems(STANDARD.name(), GSI.name(), PAM.name(), KERBEROS.name());

        // Set the config values
        // IRODSConfig defaultConfig = Application.irodsConfig.get(labels[0]);
        Irods defaultConfig = Application.config.getIrods().get(0);
        hostField.setValue(defaultConfig.getHost());
        zoneField.setValue(defaultConfig.getZone());
        authField.setValue(defaultConfig.getAuthscheme());
        portField.setValue(String.valueOf(defaultConfig.getPort()));
        sslField.setValue(defaultConfig.getSsl());

        // Value change listener
        iRODS.addValueChangeListener(listener -> {
            String value = listener.getValue();
            Irods irodsConfig = Application.config.getIrods().stream().filter(irods -> irods.getLabel().equals(value)).findFirst().orElse(null);
            hostField.setValue(irodsConfig.getHost());
            zoneField.setValue(irodsConfig.getZone());
            authField.setValue(irodsConfig.getAuthscheme());
            portField.setValue(String.valueOf(irodsConfig.getPort()));
            sslField.setValue(irodsConfig.getSsl());
            // Notification.show("Switched to " + irodsConfig.getLabel(), 2000, Notification.Position.BOTTOM_START);
        });

        // The vaadin default login form
        LoginForm component = new LoginForm();

        // Dialog
        Dialog dialog = new Dialog();
        add(dialog);

        // Hides the forgot password button as it is not implemented
        component.setForgotPasswordButtonVisible(false);

        // Check cookie to change iRODS field
        Cookie[] cookies = VaadinService.getCurrentRequest().getCookies();
        for (Cookie cookie : cookies) {
            if (cookie.getName().equalsIgnoreCase("fairds_irods")) {
                String value = cookie.getValue();
                if (labels.contains(value)) {
                    iRODS.setValue(value);
                }
            }
        }

        // Upon login the session is coupled to the credentials
        component.addLoginListener(event -> {
            if (iRODS.isEmpty() && hostField.isEmpty()) {
                Notification.show("No iRODS environment selected", 1000, Notification.Position.MIDDLE);
            } else {
                // Set cookie for iRODS
                if (iRODS.getValue() != null)
                    addCookie("fairds_irods", iRODS.getValue());

                // Credentials object
                Credentials credentials = new Credentials();
                // Store it in this session
                VaadinSession.getCurrent().setAttribute("credentials", credentials);

                // Set credentials
                credentials.setUsername(event.getUsername());
                credentials.setPassword(event.getPassword());
                // Standard credential settings
                credentials.setHost(hostField.getValue());
                credentials.setPort(Integer.parseInt(portField.getValue()));
                credentials.setZone(zoneField.getValue());
                // Negotiation policies
                credentials.setSslNegotiationPolicy(sslField.getValue());
                // PAM / SSL authentication
                credentials.setAuthenticationScheme(authField.getValue());
                // Certificate when available
//            if (config.getIRODScertificateTrustStore() != null) {
//                credentials.setCertificateTrustStore(config.getIRODScertificateTrustStore());
//                credentials.setCertificateTrustStorePassword(config.getIRODScertificateTrustStorePassword());
//            }

                // Perform authentication
                boolean isAuthenticated = authenticate(credentials);

                // When authenticated navigate to frontpage
                if (isAuthenticated) {
                    UI.getCurrent().navigate("");
                } else {
                    component.setError(true);
                }
            }
        });

        // Forgot password module, still in progress not sure if needed with LDAP implementation on the way
        component.addForgotPasswordListener(event -> {
            // Clear dialog
            dialog.removeAll();
            // Add fields
            TextField labelField = new TextField();
            labelField.setLabel("Username");
            Button newPassword = new Button("New password please");
            dialog.add(new Text("Please fill in your username"), labelField, newPassword);
            dialog.setWidth("300px");
            dialog.setHeight("200px");
            dialog.open();
            // Clicked on forgot password what now?
            newPassword.addClickListener(buttonClickEvent -> {
                boolean status = false;
                if (labelField.getValue().length() > 0) {
                    // status = sentEmail(labelField.getValue());
                } else {
                    labelField.setInvalid(true);
                }
                dialog.close();
                if (status)
                    Notification.show("A new password request has been sent to you", 5000, Notification.Position.MIDDLE);
                else
                    Notification.show("Failed to sent a new password request please contact us", 5000, Notification.Position.MIDDLE);
            });
        });

        AtomicBoolean disabled = new AtomicBoolean(false);
        UI.getCurrent().getPage().executeJs("return window.location.href").then(String.class, location -> {
            if (location.contains("localhost")) {
                // Login enabled
            } else if (location.contains("m-unlock.nl")) {
                // Login enabled
            } else {
                disabled.set(true);
            }
        });

        Button anonymousLogin = new Button("Anonymous login");
        anonymousLogin.addClickListener(click -> {
            if (iRODS.isEmpty() && hostField.isEmpty()) {
                Notification.show("No iRODS environment selected", 1000, Notification.Position.MIDDLE);
            } else{
                Credentials credentials = new Credentials();
                VaadinSession.getCurrent().setAttribute("credentials", credentials);

                // Set credentials
                credentials.setUsername("anonymous");
                credentials.setPassword("");
                // Standard credential settings
                credentials.setHost(hostField.getValue());
                credentials.setPort(Integer.parseInt(portField.getValue()));
                credentials.setZone(zoneField.getValue());
                // Negotiation policies
                credentials.setSslNegotiationPolicy(sslField.getValue());
                // PAM / SSL authentication
                credentials.setAuthenticationScheme(authField.getValue());
                // Certificate when available
//            if (config.getIRODScertificateTrustStore() != null) {
//                credentials.setCertificateTrustStore(config.getIRODScertificateTrustStore());
//                credentials.setCertificateTrustStorePassword(config.getIRODScertificateTrustStorePassword());
//            }

                boolean isAuthenticated = authenticate(credentials);

                if (isAuthenticated) {
                    UI.getCurrent().navigate("");
                } else {
                    component.setError(true);
                }
            }
        });


        // Authentication is disabled when it is not localhost or not running from unlock domain
        if (disabled.get() && LoginView.class.getResource("LoginView.class").toString().contains("jar")) {
            add(new H1("FAIRDS Login disabled"));
        } else {
            // Add the login bells and whistles to the page
            add(component);
            Accordion accordion = new Accordion();
            VerticalLayout irodsSettings = new VerticalLayout();
            irodsSettings.add(hostField, zoneField, portField, sslField, authField);
            // Anonymous login added
            add(anonymousLogin);
            // Accordion for settings
            accordion.add("iRODS Settings", irodsSettings);
            accordion.close();
            add(accordion);
        }
    }

    /**
     * Performs the simple authentication in the backend and shows error if not successful.
     * @param credentials object with authentication information
     * @return if authentication is successful
     */
    private boolean authenticate(Credentials credentials) {
        try {
            credentials.authenticate();
            if (credentials.isSuccess()) {
                logger.info("Successful login for " + credentials.getIrodsAccount().getUserName());
                return true;
            } else {
                Notification.show("Authentication failed " + credentials.getMessage(), 5000, Notification.Position.MIDDLE);
                logger.error("Failed attempt for " + credentials.getIrodsAccount().getUserName());
                return false;
            }
        } catch (JargonException e) {
            Notification.show("Authentication failed " + credentials.getMessage(), 5000, Notification.Position.MIDDLE);
            logger.error("Failed attempt for " + credentials.getIrodsAccount().getUserName());
            return false;
        }
    }

    /**
     * add cookie implementation if needed
     * @param key for the cookie
     * @param value for the cookie
     */
    public static void addCookie(String key, String value) {
        try {
            Cookie myCookie = new Cookie(key, value);

            // Make cookie expire in 1 year
            myCookie.setMaxAge(60 * 60 * 24 * 7 * 52);

            // Set the cookie path.
            myCookie.setPath(VaadinService.getCurrentRequest().getContextPath());

            // Save cookie
            VaadinService.getCurrentResponse().addCookie(myCookie);
        } catch (IllegalArgumentException e) {
            logger.error("Failed to add cookie");
        }
    }

    @Override // inform the user about an authentication error
    public void beforeEnter(BeforeEnterEvent beforeEnterEvent) {
        if (beforeEnterEvent.getLocation()
                .getQueryParameters()
                .getParameters()
                .containsKey("error")) {
            login.setError(true);
        }
        if (!Application.config.getMenus().isIrods()) {
            beforeEnterEvent.forwardTo("");
        }
    }
}