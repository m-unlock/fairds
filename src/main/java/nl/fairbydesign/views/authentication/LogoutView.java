package nl.fairbydesign.views.authentication;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinRequest;
import com.vaadin.flow.server.VaadinSession;
import jakarta.servlet.http.Cookie;
import nl.fairbydesign.views.main.MainView;
import nl.fairbydesign.views.configurator.ConfiguratorView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;



@PageTitle("Logging out")
@Route(value = "logout", layout = MainView.class)
@CssImport("./styles/views/empty/empty-view.css")
public class LogoutView extends VerticalLayout {
    public static final Logger logger = LogManager.getLogger(ConfiguratorView.class);

    public LogoutView() {
        com.vaadin.flow.component.Html text = new com.vaadin.flow.component.Html("<span>You are now logged out </span>");
        add(text);

        VaadinSession.getCurrent().setAttribute("credentials", null);

        LoginView.addCookie("username", "");
        LoginView.addCookie("host", "");
        LoginView.addCookie("port", "");
        LoginView.addCookie("zone", "");
        LoginView.addCookie("authentication", "");

        // TODO empty credentials object
        // TODO remove cookies when available...
        Cookie[] cookies = VaadinRequest.getCurrent().getCookies();
        for (Cookie cookie : cookies) {
            logger.debug(">>> TODO remove vaadin cookies " + cookie.getName() + " " + cookie.getValue());
        }

        // TODO auto refresh or move to main after ... ?
        // Navigator navigator = new Navigator(this, this);
        UI.getCurrent().navigate("");
    }
}