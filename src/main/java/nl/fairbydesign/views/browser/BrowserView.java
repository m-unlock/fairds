package nl.fairbydesign.views.browser;


import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.NativeLabel;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.TabSheet;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.provider.Query;
import com.vaadin.flow.data.renderer.LitRenderer;
import com.vaadin.flow.router.*;
import com.vaadin.flow.server.StreamResource;
import com.vaadin.flow.server.VaadinSession;
import nl.fairbydesign.backend.Generic;
import nl.fairbydesign.backend.credentials.Credentials;
import nl.fairbydesign.backend.data.objects.AVU;
import nl.fairbydesign.backend.data.objects.Item;
import nl.fairbydesign.backend.irods.Browser;
import nl.fairbydesign.backend.irods.Data;
import nl.fairbydesign.backend.irods.ProgressListener;
import nl.fairbydesign.views.main.MainView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.pub.io.IRODSFile;
import org.irods.jargon.core.pub.io.IRODSFileInputStream;
import org.vaadin.firitin.components.button.VButton;
import org.vaadin.firitin.components.upload.UploadFileHandler;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.vaadin.flow.component.grid.GridVariant.LUMO_ROW_STRIPES;
import static com.vaadin.flow.component.notification.Notification.Position.MIDDLE;
import static com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment.CENTER;
import static nl.fairbydesign.backend.data.objects.Item.Type.FOLDER;
import static nl.fairbydesign.backend.irods.StreamCopyUtil.copyWithProgressCoordinated;

@Route(value = "browser", layout = MainView.class)
@PageTitle("Browser")
@CssImport("./styles/views/empty/empty-view.css")
public class BrowserView extends Div implements BeforeLeaveObserver, BeforeEnterObserver {

    private final Credentials credentials;
    private List<Item> filesAndFolders = new ArrayList<>();
    private TextField pathBar;
    private final Grid<Item> browserGrid = new Grid<>(Item.class);
    private Grid<AVU> avuGrid;
    public static final Logger logger = LogManager.getLogger(BrowserView.class);

    private final UploadFileHandler uploadFileHandler = new UploadFileHandler(
            (i, name, type) -> openDataFile(i, name, type))
            // make the file upload very compact by using
            // icon and disabling drag and drop area
            .withDragAndDrop(true)
            .withDropLabelIcon(VaadinIcon.DROP.create())
            .withUploadButton(new VButton(VaadinIcon.UPLOAD.create())
                    .withTooltip("Upload files here...")
            );

    public void openDataFile(InputStream content, String name, String type) {
        // Read the input stream and write to an output stream
        // inputStreamToiRODS(credentials, content, name, type);
        try {
            Data.uploadIrodsFile(credentials, content, pathBar.getValue(), name, type);
        } catch (JargonException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void beforeEnter(BeforeEnterEvent event) {
        logger.info("Person enters page!");
        // Process the path from url if present

    }

    @Override
    public void beforeLeave(BeforeLeaveEvent event) {
        logger.error("Person leaves page!");
    }

    public BrowserView() {
        setId("master-detail-view");

        credentials = (Credentials) VaadinSession.getCurrent().getAttribute("credentials");

        // Layout for the overall page
        VerticalLayout verticalLayout = new VerticalLayout();

        if (credentials != null && credentials.isSuccess()) {
            // Top bar containing the current path
            pathBar = new TextField();
            pathBar.setValue("/" + credentials.getIrodsAccount().getZone() + "/home/" + credentials.getIrodsAccount().getUserName());
            pathBar.setWidthFull();
            // Sets the filebrowser path if ?path= is not in the url
            if (getUI().isPresent()) {
                getUI().get().getPage().getHistory().replaceState(null, "/browser?path=" + pathBar.getValue());
            }

            // Horizontal layout for the top bar with download button
            HorizontalLayout topBar = new HorizontalLayout();
            topBar.setWidthFull();
            Button downloadButton = new Button(VaadinIcon.DOWNLOAD.create());
            Button deleteButton = new Button(VaadinIcon.TRASH.create());
            Button makeFolder = new Button(VaadinIcon.FOLDER_ADD.create());
            downloadButton.setEnabled(false);
            deleteButton.setEnabled(false);
            topBar.add(pathBar, downloadButton, deleteButton, makeFolder);

            // Browser grid containing files and folders
            browserGrid.setSelectionMode(Grid.SelectionMode.SINGLE);
            browserGrid.addThemeVariants(LUMO_ROW_STRIPES);

            // Bottom left grid containing the metadata grid in the metadata tab
            avuGrid = new Grid<>(AVU.class);

            // Set the columns for the file browser
//            browserGrid.setColumns("name", "modified", "size");
            browserGrid.getColumns().forEach(column -> column.setSortable(false));

            // Set fixed column width to speed up rendering
//            browserGrid.getColumnByKey("name").setWidth("60%");
//            browserGrid.getColumnByKey("modified").setWidth("30%");
//            browserGrid.getColumnByKey("size").setWidth("10%");
            browserGrid.removeAllColumns();
            browserGrid.addColumn(LitRenderer
                    .<Item>of("<b>${item.name}</b>")
                    .withProperty("name", Item::getName)
            ).setHeader("Name").setWidth("60%");

            browserGrid.addColumn(LitRenderer
                    .<Item>of("<b>${item.modified}</b>")
                    .withProperty("modified", Item::getModifiedString)
            ).setHeader("Modification date").setWidth("30%");

            browserGrid.addColumn(LitRenderer
                    .<Item>of("<b>${item.size}</b>")
                    .withProperty("size", Item::getSize)
            ).setHeader("Size").setWidth("10%");

            browserGrid.setWidthFull();

            // Set the columns for the metadata
            avuGrid.setColumns("attribute", "value", "unit");
            avuGrid.setWidthFull();

            // Layouts for metadata
            HorizontalLayout metadataHorizontalLayout = new HorizontalLayout();
            VerticalLayout verticalLayoutMetaDataFields = new VerticalLayout();
            TextField attribute = new TextField("Attribute");
            TextField value = new TextField("Value");
            TextField unit = new TextField("Unit");
            // Creating the buttons for metadata modifications
            Button buttonDeleteMetadata = new Button("Delete");
            Button buttonModifyMetadata = new Button("Modify");
            Button buttonAddMetadata = new Button("Add");
            // Disabled by default as no element is selected
            buttonDeleteMetadata.setEnabled(false);
            buttonModifyMetadata.setEnabled(false);

            HorizontalLayout metadataButtons = new HorizontalLayout();
            metadataButtons.add(buttonAddMetadata, buttonModifyMetadata, buttonDeleteMetadata);
            verticalLayoutMetaDataFields.add(attribute, value, unit, metadataButtons);
            metadataHorizontalLayout.add(avuGrid, verticalLayoutMetaDataFields);
            metadataHorizontalLayout.setWidthFull();
            avuGrid.setMaxWidth("50%");

            uploadFileHandler.allowMultiple();
            uploadFileHandler.setMaxConcurrentUploads(1);

            VerticalLayout uploadTab = new VerticalLayout();
            uploadTab.add(uploadFileHandler);

            // FAIRDS tab

            // Tab sheet
            TabSheet tabSheet = new TabSheet();
            tabSheet.add("Metadata", metadataHorizontalLayout);
            tabSheet.add("Upload", uploadTab);

            // Tabs
            add(verticalLayout);

            add(tabSheet);

            // * LISTENER SECTION

            uploadFileHandler.addUploadSucceededListener(event -> {
                String fileName = event.getFileName();
                Notification.show("File " + fileName + " uploaded!");
            });

            // Make folder button
            makeFolder.addClickListener(buttonClickEvent -> {
                // Create a folder
                Dialog dialog = new Dialog();
                TextField folderName = new TextField("Folder name");
                Button button = new Button("Create");
                // Listener for the create button
                button.addSingleClickListener(clicked -> {
                    // File factory
                    try {
                        credentials.getFileFactory().instanceIRODSFile(pathBar.getValue() + "/" + folderName.getValue()).mkdir();
                        // Refresh the grid
                        getContent(credentials, pathBar.getValue());
                        browserGrid.setItems(filesAndFolders);
                        dialog.close();
                    } catch (JargonException e) {
                        dialog.close();
                        Notification notification = new Notification();

                        Button closeButton = new Button(new Icon("lumo", "cross"));
                        closeButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY_INLINE);
                        closeButton.setAriaLabel("Close");
                        closeButton.addClickListener(event -> {
                            notification.close();
                        });
                        notification.addThemeVariants(NotificationVariant.LUMO_ERROR);
                        Div text = new Div(new Text("Failed to create folder"));
                        HorizontalLayout layout = new HorizontalLayout(text, closeButton);
                        layout.setAlignItems(FlexComponent.Alignment.CENTER);
                        notification.add(layout);
                        notification.open();
                    }
                });
                dialog.add(folderName, button);
                Button closeButton = new Button(new Icon("lumo", "cross"),
                        (e) -> dialog.close());
                dialog.getHeader().add(closeButton);
                dialog.open();
            });

            // Listener for double-click on file browser
            browserGrid.addItemDoubleClickListener(folderItemDoubleClickEvent -> {
                if (folderItemDoubleClickEvent.getItem().getType().equals(FOLDER)) {
                    pathBar.setValue(folderItemDoubleClickEvent.getItem().getPath());
                    // Time check
                    long startTime = System.currentTimeMillis();
                    getContent(credentials, pathBar.getValue());
                    long endTime = System.currentTimeMillis();
                    logger.info("Time taken to get content: " + (endTime - startTime) / 1000 + "s");
                    browserGrid.setItems(filesAndFolders);

                }
            });

            // Add ENTER listener when typing a path and hit enter
            pathBar.addKeyDownListener(Key.ENTER, e -> {
                if (pathBar.getValue().isEmpty()) {
                    pathBar.setValue("/");
                }
                getContent(credentials, pathBar.getValue());
                browserGrid.setItems(filesAndFolders);
            });

            // Add listener to the bar
            pathBar.addAttachListener(attachEvent -> {
                getContent(credentials, pathBar.getValue());
                browserGrid.setItems(filesAndFolders);
            });

            pathBar.addValueChangeListener(change -> {
                // Update the address bar in the browser
                getUI().get().getPage().getHistory().replaceState(null, "/browser?path=" + change.getValue());
            });

            // Listener when selecting a metadata item and deselecting an item
            avuGrid.addItemClickListener(avuItemClickEvent -> {
                if (avuGrid.getSelectedItems().isEmpty()) {
                    attribute.setValue("");
                    value.setValue("");
                    unit.setValue("");
                    buttonDeleteMetadata.setEnabled(false);
                    buttonModifyMetadata.setEnabled(false);
                } else {
                    AVU item = avuItemClickEvent.getItem();
                    attribute.setValue(item.getAttribute());
                    value.setValue(item.getValue());
                    unit.setValue(item.getUnit());
                    buttonDeleteMetadata.setEnabled(true);
                    buttonModifyMetadata.setEnabled(true);
                }
            });

            // Listener when clicking on the download button
            downloadButton.addSingleClickListener(downloadButtonClickEvent -> {
                Item item = browserGrid.getSelectedItems().iterator().next();

                // Create the download link (hidden from UI)
                StreamResource streamResource = new StreamResource(item.getName(), () -> {
                    // Ensure a fresh InputStream is created for each download request
                    return getStream(credentials, item.getPath());
                });

                Anchor downloadLink = new Anchor(streamResource, "");
                downloadLink.getElement().setAttribute("download", true);
                downloadLink.setId("download-link");
                downloadLink.getStyle().set("display", "none");  // Hide the download link
                add(downloadLink);

                // Create a button to trigger the download
                Button downloadButtonX = new Button("Download File", event ->
                        UI.getCurrent().getPage().executeJs("document.getElementById('download-link').click();")
                );
                downloadButtonX.setVisible(false);
                downloadButtonX.click();
                add(downloadButtonX);
            });

            // Listener when the add button is clicked
            buttonAddMetadata.addClickListener(buttonClickEvent -> {
                Set<Item> items = new HashSet<>();
                if (browserGrid.getSelectedItems().isEmpty()) {
                    // Check if the select column was used
                    for (Item item : browserGrid.getDataProvider().fetch(new Query<>()).collect(Collectors.toList())) {
                        if (item.isSelected()) {
                            items.add(item);
                        }
                    }
                    Notification.show("Multi select option used to add metadata");
                } else {
                    items = browserGrid.getSelectedItems();
                }
                for (Item item : items) {
                    Browser.addMetadata(credentials, item, attribute.getValue(), value.getValue(), unit.getValue());
                }
                avuGrid.getDataProvider().refreshAll();

                // Clear fields
                attribute.setValue("");
                value.setValue("");
                unit.setValue("");
                avuGrid.getDataProvider().refreshAll();
            });

            // Listener when the delete button is clicked
            buttonDeleteMetadata.addClickListener(buttonClickEvent -> {
                Item item = browserGrid.getSelectedItems().iterator().next();
                if (avuGrid.getSelectedItems().isEmpty()) {
                    Notification.show("No AVU element selected");
                } else {
                    Browser.deleteMetadata(credentials, item, avuGrid.getSelectedItems().iterator().next());
                }

                avuGrid.setItems(item.getAvus());
                // Clear fields
                attribute.setValue("");
                value.setValue("");
                unit.setValue("");
                avuGrid.getDataProvider().refreshAll();
            });

            // Listener when the modify button is clicked
            buttonModifyMetadata.addClickListener(buttonClickEvent -> {
                // Delete then add
                Item item = browserGrid.getSelectedItems().iterator().next();
                item = Browser.deleteMetadata(credentials, item, avuGrid.getSelectedItems().iterator().next());
                item = Browser.addMetadata(credentials, item, attribute.getValue(), value.getValue(), unit.getValue());
                avuGrid.setItems(item.getAvus());
                // When the add button is clicked the AVU is added...
                // Then the grid needs to be cleared and reloaded
                avuGrid.getDataProvider().refreshAll();
                // Clear fields
                attribute.setValue("");
                value.setValue("");
                unit.setValue("");
                avuGrid.getDataProvider().refreshAll();
            });

            verticalLayout.setMaxWidth("100%");
            // Top layout with path bar and grid
            verticalLayout.add(topBar, browserGrid);

            // Upload listener
//            upload.addSucceededListener(succeededEvent -> {
//                // TODO Need to find a way to check if the canWrite function is possible otherwise log gets overwhelmed with "errors"
//                boolean writable = true;
//                try {
//                    if (writable) {
//                        // To iRODS using current browser path
//                        File targetFile = new File(pathBar.getValue() + "/" + succeededEvent.getFileName());
//                        if (credentials.getFileFactory().instanceIRODSFile(targetFile.getAbsolutePath()).exists()) {
//                            throw new FileExistsException();
//                        }
//                        // Upload attempt
//                        try {
//                            // Check its magic number but can we reset the stream afterwards?
//                            byte[] magicNumber = new byte[4];
//                            buffer.getInputStream().read(magicNumber);
//                            // Target path
//                            String targetFilePath = targetFile.getAbsolutePath();
//                            // Change path if zip
//                            if (magicNumber[0] == 0x50 && magicNumber[1] == 0x4B && magicNumber[2] == 0x03 && magicNumber[3] == 0x04) {
//                                logger.debug("File is a zip file (or a folder)");
//                                targetFilePath = targetFile.getAbsolutePath() + ".zip";
//                            }
//                            // Create a DataObjectAO object to stream the data to
//                            DataObjectAO dataObjectAO = IRODSFileSystem.instance().getIRODSAccessObjectFactory().getDataObjectAO(credentials.getIrodsAccount());
//                            IRODSFileOutputStream irodsFileOutputStream = dataObjectAO.getIRODSFileFactory().instanceIRODSFileOutputStream(targetFilePath);
//                            // Stream to server to stream to irods including the magic number obtained
//                            irodsFileOutputStream.write(magicNumber);
//                            IOUtils.copy(buffer.getInputStream(), irodsFileOutputStream);
//                            // Closing the streams needed?
//                            buffer.getInputStream().close();
//                            irodsFileOutputStream.close();
//                            // dataObjectAO.closeSession();
//                            // Check if file is there
//                            IRODSFile irodsFile = IRODSFileSystem.instance().getIRODSFileFactory(credentials.getIrodsAccount()).instanceIRODSFile(targetFile.getAbsolutePath());
//                            if (irodsFile.exists()) {
//                                Notification notification = Notification.show("Upload successful of " + new File(targetFilePath).getName());
//                                notification.setPosition(MIDDLE);
//                                notification.addThemeVariants(NotificationVariant.LUMO_SUCCESS);
//                                notification.open();
//                            }
//                        } catch (JargonException e) {
//                            Notification notification = getNotification("Upload failed", NotificationVariant.LUMO_ERROR);
//                            notification.open();
//                        }
//                    } else {
//                        Notification.show("You have no write access to this folder");
//                    }
//                } catch (FileExistsException e) {
//                    Notification notification = getNotification("File already exists " + succeededEvent.getFileName(), NotificationVariant.LUMO_CONTRAST);
//                    notification.open();
//                    logger.info("File exists: " + succeededEvent.getFileName());
//                    if (e.getMessage() != null) {
//                        logger.error(e.getMessage());
//                    }
//                } catch (JargonException | IOException e) {
//                    Notification notification = getNotification("Upload failed " + succeededEvent.getFileName(), NotificationVariant.LUMO_ERROR);
//                    notification.open();
//                    logger.error("Upload failed: " + e.getMessage());
//                }
//            });

//            upload.addFailedListener(failed -> {
//                Notification notification = getNotification("Upload failed " + failed.getReason().getMessage(), NotificationVariant.LUMO_ERROR);
//                notification.open();
//            });

//            upload.addAllFinishedListener(listener -> {
//                // listener.unregisterListener(); // is this needed?
//                getContent(credentials, pathBar.getValue());
//                browserGrid.setItems(filesAndFolders);
//            });

            deleteButton.addSingleClickListener(deleteButtonClickEvent -> {
                // Delete a file or folder
                Item item = browserGrid.getSelectedItems().iterator().next();
                // Check if the item is a folder
                if (item.getType().equals(FOLDER)) {
                    Notification notification = getNotification("Deleting a folder is not supported", NotificationVariant.LUMO_ERROR);
                    notification.open();
                } else {
                    // Delete file
                    try {
                        IRODSFile irodsFile = credentials.getFileFactory().instanceIRODSFile(item.getPath());
                        irodsFile.delete();
                        // Refresh the grid
                        getContent(credentials, pathBar.getValue());
                        browserGrid.setItems(filesAndFolders);
                        Notification notification = getNotification("File deleted", NotificationVariant.LUMO_SUCCESS);
                        notification.open();
                    } catch (Exception e) {
                        Notification notification = getNotification("Failed to delete file", NotificationVariant.LUMO_ERROR);
                        notification.open();
                    }
                }
            });

            // Add info, almost working
            browserGrid.addItemClickListener(click -> {
                // Item deselected not much needed except reset the content?
                if (click.getItem() == null) {
                    downloadButton.setEnabled(false);
                    deleteButton.setEnabled(false);
                } else {
                    // Enable delete button when a file is selected
                    if (click.getItem().getType().equals(Item.Type.FILE)) {
                        downloadButton.setEnabled(true);
                        deleteButton.setEnabled(true);
                    } else {
                        downloadButton.setEnabled(false);
                        deleteButton.setEnabled(false);
                    }
                    // Get AVUs when AVU tab is active
                    if (tabSheet.getSelectedTab().getLabel().equalsIgnoreCase("Metadata")) {
                        Item item = Browser.getAVUs(credentials, click.getItem());
                        avuGrid.setItems(item.getAvus());
                    }
                }
            });
        } else {
            add(new NativeLabel("To use this you need to login first..."));
            Button button = new Button("Login", event -> UI.getCurrent().navigate("login"));
            add(button);
            button.click();
            button.clickInClient();
        }
    }

    private void inputStreamToiRODS(Credentials credentials, InputStream inputStream, String fileName, String mimeType) throws JargonException, IOException {
        logger.info("Copying " + fileName + " to iRODS using \"" + mimeType + "\" as mime type");
        // To iRODS using current browser path
        File targetFile = new File(pathBar.getValue() + "/" + fileName);
//        IRODSFile irodsFile = credentials.getFileFactory().instanceIRODSFile(targetFile.getAbsolutePath());
//        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(credentials.getFileFactory().instanceIRODSFileOutputStream(irodsFile, DataObjInp.OpenFlags.WRITE));
//
//        copyWithProgress(inputStream, bufferedOutputStream, new ProgressListener() {
//            @Override
//            public void update(long bytesRead) {
//                logger.info("Read: " + Generic.formatSize(bytesRead) + " from " + targetFile);
//            }
//        });

        copyWithProgressCoordinated(credentials, inputStream, targetFile.getAbsolutePath(), new ProgressListener() {
            long lastTotalBytesReadForProgress = 0;
            @Override
            public void update(long bytesRead) {
                if (bytesRead - lastTotalBytesReadForProgress >= 1024 * 1024 * 1.0) {
                    logger.info("Read: " + Generic.formatSize(bytesRead) + " from " + targetFile);
                    lastTotalBytesReadForProgress = bytesRead;
                }
            }
        });

        // Closing the streams needed?
        inputStream.close();
    }

    private Notification getNotification(String message, NotificationVariant notificationVariant) {
        // Red failed notification with closing function
        Notification notification = new Notification();
        notification.setDuration(5000);
        notification.addThemeVariants(notificationVariant);
        notification.setPosition(MIDDLE);

        Div text = new Div(new Text(message));

        Button closeButton = new Button(new Icon("lumo", "cross"));
        closeButton.addThemeVariants(ButtonVariant.LUMO_TERTIARY_INLINE);
        closeButton.getElement().setAttribute("aria-label", "Close");
        closeButton.addClickListener(event -> notification.close());

        HorizontalLayout layout = new HorizontalLayout(text, closeButton);
        layout.setAlignItems(CENTER);
        notification.add(layout);
        // Log
        if (notificationVariant.equals(NotificationVariant.LUMO_ERROR))
            logger.error(message);
        else if (notificationVariant.equals(NotificationVariant.LUMO_CONTRAST))
            logger.info(message);
        else if (notificationVariant.equals(NotificationVariant.LUMO_SUCCESS))
            logger.info(message);
        else if (notificationVariant.equals(NotificationVariant.LUMO_PRIMARY))
            logger.info(message);
        else logger.info(message);

        return notification;
    }

    private InputStream getStream(Credentials credentials, String file) {
        IRODSFileInputStream stream = null;
        try {
            stream = credentials.getFileFactory().instanceIRODSFileInputStream(file);
        } catch (JargonException e) {
            e.printStackTrace();
        }

        return stream;
    }

    public void getContent(Credentials credentials, String path) {
        // Temp disable grid?
//        browserGrid.getUI().ifPresent(ui -> ui.access(() -> browserGrid.setEnabled(false)));

        // Reset files and folders
        setFilesAndFolders(new ArrayList<>());
        // Add go to a parent option
        addFilesAndFolders(new Item("...", 0L, "", Item.Type.FOLDER, new File(path).getParent()));

        // New thread to populate file and folder information
//        Thread t = new Thread(() -> {
        IRODSFile irodsPath;
        try {
            irodsPath = credentials.getFileFactory().instanceIRODSFile(path);
            logger.debug("Path & iRODS Path: " + path + " " + irodsPath.getAbsolutePath());
        } catch (JargonException e) {
            // Stop thread
            return;
        }

        // Get list of files using irods query
        logger.debug("Getting files from iRODS");
        ArrayList<Item> items = Browser.getFiles(credentials, irodsPath.getAbsolutePath());
        filesAndFolders.addAll(items);

        // Get list of folders using irods query
        logger.debug("Getting folders from iRODS");
        ArrayList<Item> folders = Browser.getFolders(credentials, irodsPath.getAbsolutePath());
        logger.debug("Got folders from iRODS");
        filesAndFolders.addAll(folders);

        // Final refresh...
//            filesAndFolders.get(0).setName("...");

//            browserGrid.getUI().ifPresent(ui -> ui.access(() -> browserGrid.getDataProvider().refreshAll()));
//            browserGrid.getUI().ifPresent(ui -> ui.access(() -> browserGrid.setEnabled(true)));
//        });
//        t.start();
    }


    public void setFilesAndFolders(List<Item> filesAndFolders) {
        this.filesAndFolders = filesAndFolders;
    }

    public void addFilesAndFolders(Item item) {
        this.filesAndFolders.add(item);
    }

    // Overrides the default receiver to stream data to iRODS
//    MultiFileReceiver multiFileReceiver = new MultiFileReceiver() {
//        public OutputStream receiveUpload(String fileName, String mimeType) {
//            logger.info("Upload started using a custom receiver");
//            // Create an iRODS output stream
//            DataObjectAO dataObjectAO;
//            try {
//                File targetFile = new File(pathBar.getValue() + "/" + fileName);
//                logger.info("Target file: " + targetFile.getAbsolutePath());
//                dataObjectAO = IRODSFileSystem.instance().getIRODSAccessObjectFactory().getDataObjectAO(credentials.getIrodsAccount());
//                IRODSFileOutputStream irodsFileOutputStream = dataObjectAO.getIRODSFileFactory().instanceIRODSFileOutputStream(targetFile.getAbsolutePath());
//                // Stream to server to stream to irods including the magic number obtained
//                logger.info("Upload started using a custom receiver and returned a stream");
//                return irodsFileOutputStream;
//            } catch (JargonException e) {
//                throw new RuntimeException(e);
//            }
//
//            // return UploadFileHandler.this.receiveUpload(s, s1);
//        }
//    };
}
