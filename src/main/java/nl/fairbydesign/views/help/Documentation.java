package nl.fairbydesign.views.help;

import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import nl.fairbydesign.backend.WebGeneric;
import nl.fairbydesign.views.main.MainView;

@Route(value = "docs", layout = MainView.class)
@PageTitle("Docs")
@CssImport("./styles/views/about/about-view.css")
public class Documentation extends Div {

    public Documentation() {
        setId("about-view");
        Html docContent = WebGeneric.getTextFromResource("views/docs.html");
        Div docs = new Div();
        docs.setMaxWidth(50, Unit.PERCENTAGE);
        docs.add(docContent);
        VerticalLayout verticalLayout = new VerticalLayout();
        verticalLayout.add(docs);
        verticalLayout.setHorizontalComponentAlignment(FlexComponent.Alignment.CENTER, docs);
        add(verticalLayout);

        // Set footer
        Html footerContent = WebGeneric.getTextFromResource("views/footer.html");
        Div footer = new Div();
        footer.add(footerContent);
        add(footer);
    }
}
