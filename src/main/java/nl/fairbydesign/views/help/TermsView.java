package nl.fairbydesign.views.help;

import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.HeaderRow;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinSession;
import nl.fairbydesign.Application;
import nl.fairbydesign.backend.WebGeneric;
import nl.fairbydesign.backend.credentials.Credentials;
import nl.fairbydesign.backend.metadata.Metadata;
import nl.fairbydesign.views.main.MainView;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Route(value = "terms", layout = MainView.class)
@PageTitle("Terms")
@CssImport("./styles/views/empty/empty-view.css")
public class TermsView extends Div implements BeforeEnterObserver {

    @Override // inform the user about an authentication error
    public void beforeEnter(BeforeEnterEvent beforeEnterEvent) {
        if (!Application.config.getMenus().isTerms()) {
            beforeEnterEvent.forwardTo("");
        }
    }
    public TermsView() {
        // TODO make tab with multiple help elements
        Grid<Metadata> grid = new Grid<>(Metadata.class, false);
        Grid.Column<Metadata> sheetNameColumn = grid.addColumn(Metadata::getSheetName);
        Grid.Column<Metadata> packageNameColumn = grid.addColumn(Metadata::getPackageName);
        Grid.Column<Metadata> labelColumn = grid.addColumn(Metadata::getLabel);
        Grid.Column<Metadata> exampleColumn = grid.addColumn(m -> m.getTerm().getExample());
        Grid.Column<Metadata> descriptionColumn = grid.addColumn(m -> m.getTerm().getDefinition());

        List<Metadata> metadataList = new ArrayList<>();
        Application.getMetadataSet().forEach((s, metadatas) -> {
            metadatas.forEach(metadata -> {
                metadata.setSheetName(s);
                metadataList.add(metadata);
            });
        });

        ListDataProvider<Metadata> dataProvider = new ListDataProvider<>(metadataList);

        // grid.setItems(metadataList);
        grid.setItems(dataProvider);
        grid.getHeaderRows().clear();

        HeaderRow filterRow = grid.appendHeaderRow();

        // Sheet filter
        TextField sheetField = new TextField();
        sheetField.addValueChangeListener(event -> dataProvider.addFilter(file -> StringUtils.containsIgnoreCase(file.getSheetName(), sheetField.getValue())));
        sheetField.setValueChangeMode(ValueChangeMode.EAGER);
        filterRow.getCell(sheetNameColumn).setComponent(sheetField);
        sheetField.setSizeFull();
        sheetField.setPlaceholder("Filter sheet name");

        // Label filter
        TextField labelField = new TextField();
        labelField.addValueChangeListener(event -> dataProvider.addFilter(file -> StringUtils.containsIgnoreCase(file.getLabel(), labelField.getValue())));
        labelField.setValueChangeMode(ValueChangeMode.EAGER);
        filterRow.getCell(labelColumn).setComponent(labelField);
        labelField.setSizeFull();
        labelField.setPlaceholder("Filter field name");

        // Package filter
        TextField packageField = new TextField();
        packageField.addValueChangeListener(event -> dataProvider.addFilter(file -> StringUtils.containsIgnoreCase(file.getPackageName(), packageField.getValue())));
        packageField.setValueChangeMode(ValueChangeMode.EAGER);
        filterRow.getCell(packageNameColumn).setComponent(packageField);
        packageField.setSizeFull();
        packageField.setPlaceholder("Filter package name");

        // Example filter
        TextField exampleField = new TextField();
        exampleField.addValueChangeListener(event -> dataProvider.addFilter(file -> StringUtils.containsIgnoreCase(file.getTerm().getExample(), exampleField.getValue())));
        exampleField.setValueChangeMode(ValueChangeMode.EAGER);
        filterRow.getCell(exampleColumn).setComponent(exampleField);
        exampleField.setSizeFull();
        exampleField.setPlaceholder("Filter example");

        // descriptionField filter
        TextField descriptionField = new TextField();
        descriptionField.addValueChangeListener(event -> dataProvider.addFilter(file -> StringUtils.containsIgnoreCase(file.getTerm().getDefinition(), descriptionField.getValue())));
        descriptionField.setValueChangeMode(ValueChangeMode.EAGER);
        filterRow.getCell(descriptionColumn).setComponent(descriptionField);
        descriptionField.setSizeFull();
        descriptionField.setPlaceholder("Filter description");

        final VerticalLayout bottomPanel = new VerticalLayout();

        add(grid, bottomPanel);

        Html rawdocContent = WebGeneric.getTextFromResource("views/terms.html");

        grid.addItemClickListener(clicked -> {
            Metadata metadata = clicked.getItem();
            // Update the bottom part...
            //bottomPanel.removeAll();
            String htmlCode = rawdocContent.getInnerHtml();
            htmlCode = fixHtml(htmlCode,"LABEL", metadata.getLabel());
            htmlCode = fixHtml(htmlCode, "LABEL", metadata.getLabel());
            htmlCode = fixHtml(htmlCode, "REQUIREMENT", metadata.getRequirement().toString());
            htmlCode = fixHtml(htmlCode, "DEFINITION", metadata.getTerm().getDefinition());
            htmlCode = fixHtml(htmlCode, "EXAMPLE", metadata.getTerm().getExample());
            htmlCode = fixHtml(htmlCode, "UNITS", metadata.getTerm().getPreferredUnit());
            htmlCode = fixHtml(htmlCode, "SYNTAX", metadata.getTerm().getSyntax());
//            htmlCode = fixHtml(htmlCode, "PACKAGEID", metadata.getPackageIdentifier());
            htmlCode = fixHtml(htmlCode, "PACKAGENAME", metadata.getPackageName());
            htmlCode = fixHtml(htmlCode, "LINK", metadata.getTerm().getURL());

            Html docContent = new Html(htmlCode);
            Div docs = new Div();
            docs.setMaxWidth(80, Unit.PERCENTAGE);
            docs.add(docContent);
            bottomPanel.removeAll();
            bottomPanel.add(docs);
            bottomPanel.setHorizontalComponentAlignment(FlexComponent.Alignment.AUTO, docs);
            add(bottomPanel);

        });

        // Set footer
        Html footerContent = WebGeneric.getTextFromResource("views/footer.html");
        Div footer = new Div();
        footer.add(footerContent);
        add(footer);
    }

    private String fixHtml(String htmlCode, String label, String value) {
        if (value != null)
            return htmlCode.replaceAll(label, value);
        else {
            return htmlCode.replaceAll(".*"+label+".*", "");
        }
    }
}
