package nl.fairbydesign.views.about;

import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;
import nl.fairbydesign.backend.WebGeneric;
import nl.fairbydesign.views.main.MainView;

/**
 * Frontpage with different views depending on the resource this programming is running
 * Currently supports a default view, any url with unlock and localhost view.
 */
@Route(value = "", layout = MainView.class)
@PageTitle("FAIR Data Station")
@CssImport("./styles/views/about/about-view.css")
@RouteAlias(value = "", layout = MainView.class)

public class AboutView extends Div {
    public AboutView() {
        setId("about-view");
        // Obtain the URL of this page
        UI.getCurrent().getPage().executeJs("return window.location.href").then(String.class, location -> {
            Html content;
            // If it is from unlock, localhost or something else the view changes
            if (location.contains("unlock")) {
                content = WebGeneric.getTextFromResource("views/about/about_unlock.html");
            } else if (location.contains("localhost")) {
                content = WebGeneric.getTextFromResource("views/about/about_localhost.html");
            } else {
                content = WebGeneric.getTextFromResource("views/about/about_default.html");
            }
            Div div = new Div();
            div.add(content);
            add(div);
        });
        // Set footer with download and source code information
        Html footerContent = WebGeneric.getTextFromResource("views/footer.html");
        Div footer = new Div();
        footer.add(footerContent);
        add(footer);
    }
}