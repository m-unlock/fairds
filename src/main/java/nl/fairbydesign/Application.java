package nl.fairbydesign;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import com.vaadin.flow.component.page.AppShellConfigurator;
import com.vaadin.flow.component.page.Push;
import nl.fairbydesign.backend.CommandOptions;
import nl.fairbydesign.backend.config.Config;
import nl.fairbydesign.backend.metadata.Metadata;
import nl.fairbydesign.backend.metadata.MetadataParser;
import nl.fairbydesign.backend.metadata.Term;
import nl.fairbydesign.views.bioprojects.BioProjectView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;


@SpringBootApplication
@EnableAsync
@Push
public class Application implements AppShellConfigurator {


	// Generic logger
	public static final Logger logger = LogManager.getLogger(Application.class);

	// Debug variable
	public static boolean debug;

	public static HashMap<String, ArrayList<Metadata>> metadataSet = new HashMap<>();
	public static HashMap<String, Term> termLookup = new HashMap<>();

//	public static HashMap<String, IRODSConfig> irodsConfig;
	public static String version;
	private static long freeMemory = 0;
	private static long percentageUsed = 0;
	private static long maxMemory = 0;
	private static long usedMemory = 0;
	// Obtain default values which can be overwritten by command line arguments
	public static CommandOptions commandOptions = new CommandOptions();
	public static Config config = new Config().loadConfig();
	private static CompletableFuture<Void> memoryFuture;

	public Application() throws Exception {
    }

    public static void main(String[] args) throws InterruptedException {
		startMemoryAsync();

        version = BioProjectView.class.getPackage().getImplementationVersion();

		System.out.println("\n\n...BOOTING UP... ("+version+") for options check -help\n\n");

		// Obtain args
		commandOptions = new CommandOptions(args);

		logger.info("Starting application with options: ");
		logger.info("Storage: " + commandOptions.storage);
		logger.info("Metadata: " + commandOptions.metadata);
		logger.info("Port: " + commandOptions.port);
		logger.info("Debug: " + commandOptions.debug);

		// Set debug mode or not
		debug = commandOptions.debug;

		// Argument parser here for command line interface options
		MetadataParser.createMetaDataHashMap();

		// If metadata check is set, exit the application
		if (commandOptions.metadata_check) {
			logger.info("Metadata check completed, exiting...");
			System.exit(0);
		}

		// Create storage folder
		new File(Application.commandOptions.storage).mkdirs();

		// Create raw folder (for bioprojects zip etc...)
		new File(Application.commandOptions.storage + "/bioprojects/raw/").mkdirs();

		// Create the validation folder
		new File(Application.commandOptions.storage + "/validation/").mkdirs();

		// Start application
		logger.info("Starting application on port " + commandOptions.port);
		SpringApplication app = new SpringApplication(Application.class);
		// Sets the server port using the command line argument
		app.setDefaultProperties(Collections.singletonMap("server.port", commandOptions.port));
		app.run(args);
	}

	public static void startMemoryAsync() {
		memoryFuture = CompletableFuture.runAsync(() -> {
			while (!Thread.currentThread().isInterrupted()) {
				try {
					// Sleep for 1 second
					TimeUnit.SECONDS.sleep(1);
					// Obtain max memory in bytes
					maxMemory = Runtime.getRuntime().maxMemory() / 1024 / 1024;
					// Obtain used memory in bytes
					usedMemory = Runtime.getRuntime().totalMemory() / 1024 / 1024 - Runtime.getRuntime().freeMemory() / 1024 / 1024;
					// Obtain free memory in bytes
					freeMemory = maxMemory - usedMemory;
					// Obtain the percentage of used memory
					percentageUsed = (usedMemory * 100) / maxMemory;

					// Update progress bar or perform any UI updates
					logger.debug("Used: {}MB, Free: {}MB, Percentage: {}%", usedMemory, freeMemory, percentageUsed);

				} catch (InterruptedException e) {
					// Thread interrupted, exit gracefully
					Thread.currentThread().interrupt();
				}
			}
		});
	}

	public static void stopMemoryAsync() {
		if (memoryFuture != null) {
			memoryFuture.cancel(true); // Request cancellation
		}
	}


	public static HashMap<String, ArrayList<Metadata>> getMetadataSet() {
		if (metadataSet == null || metadataSet.isEmpty()) {
			logger.info("Initializing metadata set...");
			MetadataParser.createMetaDataHashMap();
		}
		return metadataSet;
	}

	public static long getFreeMemory() {
		return freeMemory;
	}

	public static long getPercentageUsed() {
		return percentageUsed;
	}

	public static long getMaxMemory() {
		return maxMemory;
	}

	public static long getUsedMemory() {
		return usedMemory;
	}
}


