package nl.fairbydesign.backend.irods;

public interface ProgressListener {
    void update(long totalBytesRead);
}
