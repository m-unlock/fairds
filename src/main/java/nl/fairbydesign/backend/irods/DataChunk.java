package nl.fairbydesign.backend.irods;

public class DataChunk {
    private final byte[] data;
    private final long offset;

    public DataChunk(byte[] data, long offset) {
        this.data = data;
        this.offset = offset;
    }

    public byte[] getData() {
        return data;
    }

    public long getOffset() {
        return offset;
    }
}
