package nl.fairbydesign.backend.irods;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.textfield.TextArea;
import nl.fairbydesign.backend.Generic;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.transfer.TransferStatus;
import org.irods.jargon.core.transfer.TransferStatus.TransferState;
import org.irods.jargon.core.transfer.TransferStatusCallbackListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class StatusCallbackListener implements TransferStatusCallbackListener {

    private static final Logger logger = LogManager.getLogger(StatusCallbackListener.class);

    private Optional<UI> ui;

    private int successCallbackCount = 0;
    private int errorCallbackCount = 0;
    private String lastSourcePath = "";
    private String lastTargetPath = "";
    private String lastResource = "";
    private CallbackResponse forceOption = CallbackResponse.NO_FOR_ALL;
    private long bytesReportedIntraFileCallbacks = 0L;
    private int numberIntraFileCallbacks = 0;
    private final List<TransferStatus> errorCallbacks = new ArrayList<>();
    private long transfered = 0L;
    private TextArea message;
    private String previousTargetPath = "";
    private long fileCounter = 0L;
    private long totalTransfered = 0L;

    public StatusCallbackListener(TextArea bulkLog, Optional<UI> ui) {
        this.message = bulkLog;
        this.ui = ui;
    }

    public StatusCallbackListener() { }

    public List<TransferStatus> getErrorCallbacks() {
        return errorCallbacks;
    }

    @Override
    public FileStatusCallbackResponse statusCallback(final TransferStatus transferStatus) throws JargonException {
        // TODO use generic get file size and check for differences
        long gbs = Math.round((double) transferStatus.getBytesTransfered() / (1024 * 1024));
        if (!previousTargetPath.equalsIgnoreCase(lastTargetPath)) {
            // Total file count
            fileCounter++;
            // Total transferred count
            totalTransfered+=transferStatus.getBytesTransfered();
            long totalSize = getTotalSize(transferStatus);
            String totalSizeFormat = Generic.formatSize(getTotalSize(transferStatus));
            String localMessage = "Transfer status: File " +fileCounter + " and transferred " + Generic.formatSize(totalTransfered) + " || " + transfered + "MB of " + totalSizeFormat + " " + String.format("%.2f", ((double) transfered / (double) totalSize) * 100.0) + "% || " + lastTargetPath;
            if (this.message != null) {
                ui.ifPresent(ui -> ui.access(() -> this.message.setValue(localMessage)));
            } else {
                System.err.println(localMessage);
            }
        } else if (transfered < gbs) {
            long totalSize = getTotalSize(transferStatus);
            String totalSizeFormat = Generic.formatSize(getTotalSize(transferStatus));
            String localMessage = "Transfer status: File " +fileCounter + " || " + transfered + "MB of " + totalSizeFormat + " " + String.format("%.2f", ((double) transfered / (double) totalSize) * 100.0) + "% || " + lastTargetPath;
            if (this.message != null) {
                ui.ifPresent(ui -> ui.access(() -> this.message.setValue(localMessage)));
            } else {
                System.err.println(localMessage);
            }
            transfered = gbs;
        } else if (transfered == 0) {
            long totalSize = getTotalSize(transferStatus);
            if (totalSize > 1 && transfered == 0) {
                String totalSizeFormat = Generic.formatSize(getTotalSize(transferStatus));
                String localMessage = "Transfer status: File " +fileCounter + " || " + transfered + "MB of " + totalSizeFormat + " " + String.format("%.2f", ((double) transfered / (double) totalSize) * 100.0) + "% || " + lastTargetPath;
                if (this.message != null) {
                    ui.ifPresent(ui -> ui.access(() -> this.message.setValue(localMessage)));
                } else {
                    System.err.println(localMessage);
                }
            }
        } else {
            // Not sure...
        }
        if (transferStatus.isIntraFileStatusReport()) {
            numberIntraFileCallbacks++;
            bytesReportedIntraFileCallbacks = transferStatus.getBytesTransfered();
        } else if (transferStatus.getTransferState() == TransferState.FAILURE) {
            logger.error("Transfer state to failure " + errorCallbackCount);
            errorCallbackCount++;
        } else if (transferStatus.getTransferState() == TransferState.IN_PROGRESS_START_FILE) {
            // ignored
        } else {
            successCallbackCount++;
            lastSourcePath = transferStatus.getSourceFileAbsolutePath();
            lastTargetPath = transferStatus.getTargetFileAbsolutePath();
            lastResource = transferStatus.getTargetResource();
        }

        if (transferStatus.getTransferException() != null) {
            errorCallbacks.add(transferStatus);
            logger.error("Transfer status exception: " + transferStatus.getTransferException().getMessage());
            throw new JargonException("Transfer stopped...");
        }
        return FileStatusCallbackResponse.CONTINUE;

    }

    private long getTotalSize(TransferStatus transferStatus) {
        long totalSize = transferStatus.getTotalSize();
        if (totalSize == 0)
            totalSize = 1;
        return totalSize;
    }

    public int getSuccessCallbackCount() {
        return successCallbackCount;
    }

    public void setSuccessCallbackCount(final int successCallbackCount) {
        this.successCallbackCount = successCallbackCount;
    }

    public int getErrorCallbackCount() {
        return errorCallbackCount;
    }

    public void setErrorCallbackCount(final int errorCallbackCount) {
        this.errorCallbackCount = errorCallbackCount;
    }

    public String getLastSourcePath() {
        return lastSourcePath;
    }

    public String getLastTargetPath() {
        return lastTargetPath;
    }

    public String getLastResource() {
        return lastResource;
    }

    @Override
    public void overallStatusCallback(final TransferStatus transferStatus) {
        if (transferStatus.getTransferState() == TransferState.FAILURE) {
            errorCallbackCount++;
        } else {
            successCallbackCount++;
        }
        lastSourcePath = transferStatus.getSourceFileAbsolutePath();
        lastTargetPath = transferStatus.getTargetFileAbsolutePath();
        lastResource = transferStatus.getTargetResource();
    }

    /**
     * @return the bytesReportedIntraFileCallbacks
     */
    public long getBytesReportedIntraFileCallbacks() {
        return bytesReportedIntraFileCallbacks;
    }

    /**
     * @return the numberIntraFileCallbacks
     */
    public int getNumberIntraFileCallbacks() {
        return numberIntraFileCallbacks;
    }

    @Override
    public CallbackResponse transferAsksWhetherToForceOperation(final String irodsAbsolutePath,
                                                                final boolean isCollection) {
        return forceOption;
    }

    public CallbackResponse getForceOption() {
        return forceOption;
    }

    public void setForceOption(final CallbackResponse forceOption) {
        this.forceOption = forceOption;
    }
}