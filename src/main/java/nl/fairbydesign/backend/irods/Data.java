package nl.fairbydesign.backend.irods;

import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.textfield.TextArea;
import nl.fairbydesign.backend.credentials.Credentials;
import nl.fairbydesign.backend.data.objects.DiskUsage;
import nl.fairbydesign.backend.data.objects.Process;
import nl.fairbydesign.backend.data.objects.ProcessLevel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.irods.jargon.core.checksum.ChecksumValue;
import org.irods.jargon.core.checksum.LocalChecksumComputerFactory;
import org.irods.jargon.core.checksum.LocalChecksumComputerFactoryImpl;
import org.irods.jargon.core.checksum.SHA256LocalChecksumComputerStrategy;
import org.irods.jargon.core.connection.IRODSAccount;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.protovalues.ChecksumEncodingEnum;
import org.irods.jargon.core.pub.*;
import org.irods.jargon.core.pub.domain.AvuData;
import org.irods.jargon.core.pub.io.*;
import org.irods.jargon.core.query.*;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import static org.irods.jargon.core.query.QueryConditionOperators.EQUAL;
import static org.irods.jargon.core.query.QueryConditionOperators.LIKE;
import static org.irods.jargon.core.query.RodsGenQueryEnum.*;

public class Data {
    public static final Logger logger = LogManager.getLogger(Data.class);


    public static void getMetadataFiles(Credentials credentials, List<nl.fairbydesign.backend.data.objects.File> metadataFiles, Grid<nl.fairbydesign.backend.data.objects.File> grid, TextArea logArea) throws GenQueryBuilderException, JargonException {
        // For each home folder
//        ArrayList<nl.fairbydesign.backend.data.objects.File> files = new ArrayList<>();
        for (File homeFolder : credentials.getFileFactory().instanceIRODSFile("/" + credentials.getZone() + "/home").listFiles()) {
            String search = homeFolder.getAbsolutePath() + "/metadata";
            logArea.getUI().ifPresent(ui -> ui.access(() -> logArea.setValue(logArea.getValue() + "Searching for " + search + System.lineSeparator())));
            if (homeFolder.isDirectory()) {
                IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);
                queryBuilder.addConditionAsGenQueryField(COL_COLL_NAME, EQUAL, search);
                queryBuilder.addConditionAsGenQueryField(COL_DATA_NAME, LIKE, "%.ttl");
                queryBuilder.addSelectAsGenQueryValue(COL_COLL_NAME);
                queryBuilder.addSelectAsGenQueryValue(COL_DATA_NAME);
                queryBuilder.addSelectAsGenQueryValue(COL_DATA_SIZE);
                queryBuilder.addSelectAsGenQueryValue(COL_D_MODIFY_TIME);
                IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(10000);
                IRODSGenQueryExecutor irodsGenQueryExecutor = credentials.getAccessObjectFactory().getIRODSGenQueryExecutor(credentials.getIrodsAccount());
                try {
                    IRODSQueryResultSet irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);
                    List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();
                    for (IRODSQueryResultRow irodsQueryResultRow : irodsQueryResultSetResults) {
                        String path = irodsQueryResultRow.getColumn(0) + "/" + irodsQueryResultRow.getColumn(1);
                        logger.info("Found " + path);
                        nl.fairbydesign.backend.data.objects.File file = new nl.fairbydesign.backend.data.objects.File();
                        file.setPath(path);
                        file.setSize(Long.parseLong(irodsQueryResultRow.getColumn(2)));
                        file.setModificationDate(irodsQueryResultRow.getColumn(3));
                        metadataFiles.add(file);
                        if (grid != null) {
                            grid.getUI().ifPresent(ui -> ui.access(() -> grid.setItems(metadataFiles)));
                            grid.getUI().ifPresent(ui -> ui.access(() -> grid.getDataProvider().refreshAll()));
                        }
                    }
                    logger.info("Found " + metadataFiles.size() + " metadata files");
                } catch (JargonException | JargonQueryException e) {
                    e.printStackTrace();
                }
            }
        }
        logger.info("Found in total " + metadataFiles.size() + " metadata files");
    }

    /**
     * Obtains Project, investigation and study information combined with yaml status and workflow
     *
     * @param credentials the credentials for the query
     */
    public static ArrayList<Process> getIS(Credentials credentials) {
        try {
            // Obtain all projects via metadata
            IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);
            // iquest --no-page "%s %s %s" "SELECT COLL_NAME, META_DATA_ATTR_VALUE, META_DATA_ATTR_UNITS WHERE
            // COLL_NAME NOT LIKE '/unlock/references%'
            // AND
            // COLL_NAME NOT LIKE '/unlock/trash%' AND
            // DATA_NAME LIKE '%.yaml'"
            queryBuilder.addConditionAsGenQueryField(COL_COLL_NAME, LIKE, "/unlock/home%");
            queryBuilder.addConditionAsGenQueryField(COL_DATA_NAME, LIKE, "%.yaml");

            queryBuilder.addSelectAsGenQueryValue(COL_COLL_NAME);
            queryBuilder.addSelectAsGenQueryValue(COL_META_DATA_ATTR_VALUE);
            queryBuilder.addSelectAsGenQueryValue(COL_META_DATA_ATTR_UNITS);

            // Set limit?
            IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(999999);

            IRODSFileSystem irodsFileSystem = new IRODSFileSystem();
            IRODSGenQueryExecutor irodsGenQueryExecutor = irodsFileSystem.getIRODSAccessObjectFactory().getIRODSGenQueryExecutor(credentials.getIrodsAccount());

            IRODSQueryResultSet irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);

            List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();

            // unlock [0]
            // home [1]
            // investigation [2]
            // study [3]

            HashMap<String, Process> processHashMap = new HashMap<>();

            // Zone / home
            String root = "/" + credentials.getZone() + "/home/";
            for (IRODSQueryResultRow irodsQueryResultRow : irodsQueryResultSetResults) {
                String workflow = irodsQueryResultRow.getColumn(1);
                String unit = irodsQueryResultRow.getColumn(2);
                File folder = new File(irodsQueryResultRow.getColumn(0));
                ArrayList<String> folders = new ArrayList<>();
                while (folder.getParentFile() != null) {
                    folders.add(0, folder.getName());
                    folder = folder.getParentFile();
                }
                String zone = folders.get(0);
                String home = folders.get(1);
                String investigation = folders.get(2);
                String study = folders.get(3);
                String path = "/" + zone + "/" + home + "/" + investigation + "/" + study;

                // For this path with this workflow
                String id = path + " " + workflow;
                Process process;

                if (processHashMap.containsKey(id)) {
                    process = processHashMap.get(id);
                } else {
                    process = new Process();
                    process.setInvestigationIdentifier(investigation);
                    process.setStudyIdentifier(study);
                    process.setPath(path);
                    process.setWorkflow(workflow);
                    processHashMap.put(id, process);
                }

                // Status
                if (unit.equals("waiting")) {
                    process.setWaiting(process.getWaiting() + 1);
                } else if (unit.equals("running")) {
                    process.setRunning(process.getRunning() + 1);
                } else if (unit.equals("finished")) {
                    process.setFinished(process.getFinished() + 1);
                } else if (unit.equals("failed")) {
                    logger.error(process.getPath() + " " + workflow + " " + unit);
                    process.setFailed(process.getFailed() + 1);
                } else if (unit.equals("queue")) {
                    process.setQueue(process.getQueue() + 1);
                } else {
                    logger.error("Status unknown" + unit);
                }
            }
            return new ArrayList<>(processHashMap.values());

        } catch (JargonException | JargonQueryException | GenQueryBuilderException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public static ProcessLevel getProgressLevels(Credentials credentials, String projectFolder) throws JargonException, GenQueryBuilderException, JargonQueryException {
        // Obtain all home folders
        File homeFolder = new File(projectFolder);
        ProcessLevel processLevel = new ProcessLevel();
        processLevel.setName(homeFolder.getName());
        // Obtain all job files via metadata
        IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);
        queryBuilder.addConditionAsGenQueryField(COL_COLL_NAME, LIKE, homeFolder.getAbsolutePath() + "/%");
        queryBuilder.addConditionAsGenQueryField(COL_DATA_NAME, LIKE, "%.yaml");
        // Change to status in the future once this is properly established
        queryBuilder.addConditionAsGenQueryField(COL_META_DATA_ATTR_NAME, LIKE, "%");
        queryBuilder.addSelectAsGenQueryValue(COL_META_DATA_ATTR_NAME);
        queryBuilder.addSelectAsGenQueryValue(COL_META_DATA_ATTR_VALUE);
        queryBuilder.addSelectAsGenQueryValue(COL_COLL_NAME);
        queryBuilder.addSelectAsGenQueryValue(COL_DATA_NAME);
        IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(999999);
        IRODSFileSystem irodsFileSystem = new IRODSFileSystem();
        IRODSGenQueryExecutor irodsGenQueryExecutor = irodsFileSystem.getIRODSAccessObjectFactory().getIRODSGenQueryExecutor(credentials.getIrodsAccount());
        IRODSQueryResultSet irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);
        List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();
        HashSet<String> unknownParents = new HashSet<>();
        for (IRODSQueryResultRow queryResultRow : irodsQueryResultSetResults) {
            // Status retrieval
            String metadataName = queryResultRow.getColumn(0);
            String metadataValue = queryResultRow.getColumn(1);
            String folder = queryResultRow.getColumn(2);
            String path = queryResultRow.getColumn(2) + "/" + queryResultRow.getColumn(3);
            // Parent name of the folder
            String parentType = folder.substring(folder.lastIndexOf("/") + 1).split("_")[0];
            // Can also use irods for this but that might be much slower to request
            if (parentType.equalsIgnoreCase("inv"))
                processLevel.setInvestigation(processLevel.getInvestigation() + 1);
            else if (parentType.equalsIgnoreCase("stu"))
                processLevel.setStudy(processLevel.getStudy() + 1);
            else if (parentType.equalsIgnoreCase("obs"))
                processLevel.setObservationUnit(processLevel.getObservationUnit() + 1);
            else if (parentType.equalsIgnoreCase("sam"))
                processLevel.setSample(processLevel.getSample() + 1);
            else if (parentType.equalsIgnoreCase("asy"))
                processLevel.setAssay(processLevel.getAssay() + 1);
            else if (parentType.equalsIgnoreCase("unprocessed"))
                processLevel.setUnprocessed(processLevel.getUnprocessed() + 1);
            else {
                if (unknownParents.contains(parentType))
                    continue;
                unknownParents.add(parentType);
                logger.warn("Unknown parent type: " + parentType);
            }
        }
        // Set to which level the yaml file is located at
        // Do a query to obtain all unprocessed folders or data folders
        queryBuilder = new IRODSGenQueryBuilder(true, null);
        queryBuilder.addConditionAsGenQueryField(COL_COLL_NAME, LIKE, homeFolder.getAbsolutePath() + "/%unprocessed");
        // Change to status in the future once this is properly established
        queryBuilder.addSelectAsGenQueryValue(COL_COLL_NAME);
        query = queryBuilder.exportIRODSQueryFromBuilder(999999);
        irodsFileSystem = new IRODSFileSystem();
        irodsGenQueryExecutor = irodsFileSystem.getIRODSAccessObjectFactory().getIRODSGenQueryExecutor(credentials.getIrodsAccount());
        irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);
        irodsQueryResultSetResults = irodsQueryResultSet.getResults();
        processLevel.setUnprocessed(irodsQueryResultSetResults.size());
        // Return processes
        return processLevel;
    }
//    public static Collection<Process> getJobInformation(IRODSAccount irodsAccount, Process process) throws GenQueryBuilderException, JargonException, JargonQueryException {
//
//        // Obtain all projects via metadata
//        IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);
//
//        queryBuilder.addConditionAsGenQueryField(COL_COLL_NAME, QueryConditionOperators.LIKE, process.getPath() + "%");
//        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_DATA_NAME, QueryConditionOperators.LIKE, "%.yaml");
//        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_META_DATA_ATTR_NAME, QueryConditionOperators.EQUAL, "cwl");
//
//        // Column name as default does a distinct?
//        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_META_DATA_ATTR_VALUE);
//        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_META_DATA_ATTR_UNITS);
//        queryBuilder.addSelectAsAgregateGenQueryValue(RodsGenQueryEnum.COL_D_DATA_ID, GenQueryField.SelectFieldTypes.COUNT);
//
//        // Set limit?
//        IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(1000);
//
//        IRODSFileSystem irodsFileSystem = new IRODSFileSystem();
//        IRODSGenQueryExecutor irodsGenQueryExecutor = irodsFileSystem.getIRODSAccessObjectFactory().getIRODSGenQueryExecutor(irodsAccount);
//
//        IRODSQueryResultSet irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);
//        List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();
//
//        HashMap<String, Process> processHashMap = new HashMap<>();
//
//        for (IRODSQueryResultRow irodsQueryResultRow : irodsQueryResultSetResults) {
//            String workflow = irodsQueryResultRow.getColumn(0).replaceAll(".*/", "");
//            if (!processHashMap.containsKey(workflow)) {
//                Process newProcess = new Process();
//                newProcess.setProjectIdentifier(process.getProjectIdentifier());
//                newProcess.setInvestigationIdentifier(process.getInvestigationIdentifier());
//                newProcess.setStudyIdentifier(process.getStudyIdentifier());
//                newProcess.setPath(process.getPath());
//                newProcess.setIdentifier(process.getIdentifier());
//                newProcess.setWorkflow(workflow);
//                processHashMap.put(workflow, newProcess);
//            }
//            // Get copy of process from specific workflow
//            Process processHash = processHashMap.get(workflow);
//            processHash.setWorkflow(workflow);
//
//            String unit = irodsQueryResultRow.getColumn(1);
//            int count = Integer.parseInt(irodsQueryResultRow.getColumn(2));
//
//            if (unit.equals("waiting")) {
//                processHash.setWaiting(count);
//            } else if (unit.equals("running")) {
//                processHash.setRunning(count);
//            } else if (unit.equals("finished")) {
//                processHash.setFinished(count);
//            } else if (unit.equals("failed")) {
//                logger.error(process.getPath() +" " + workflow +" " + unit + " " + count);
//                processHash.setFailed(count);
//            } else if (unit.equals("queue")) {
//                processHash.setQueue(count);
//            } else {
//                logger.error("Status unknown" + unit);
//            }
//        }
//        logger.info("Processes " + processHashMap.size());
//        return processHashMap.values();
//    }

    /**
     * @param credentials object
     * @param path        to obtain size information from
     * @return collection with storage information
     * @throws GenQueryBuilderException
     * @throws JargonException
     * @throws JargonQueryException
     */
    public static ArrayList<DiskUsage> getDiskUsage(Credentials credentials, File path) throws GenQueryBuilderException, JargonException, JargonQueryException {
        logger.debug("Obtaining usage for " + path);

        // Obtain all disk usage for each project
        IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);

        queryBuilder.addConditionAsGenQueryField(COL_COLL_NAME, QueryConditionOperators.LIKE, path + "%");

        // Column name as default does a distinct?
        queryBuilder.addSelectAsGenQueryValue(COL_R_RESC_NAME);
        queryBuilder.addSelectAsAgregateGenQueryValue(RodsGenQueryEnum.COL_DATA_SIZE, GenQueryField.SelectFieldTypes.SUM);

        // Set limit
        IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(1000);

        // Obtain file system from which we obtain access object
        IRODSGenQueryExecutor irodsGenQueryExecutor = credentials.getIrodsFileSystem().getIRODSAccessObjectFactory().getIRODSGenQueryExecutor(credentials.getIrodsAccount());

        IRODSQueryResultSet irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);
        List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();

        ArrayList<DiskUsage> diskUsages = new ArrayList<>();
        for (IRODSQueryResultRow irodsQueryResultRow : irodsQueryResultSetResults) {
            String resource = irodsQueryResultRow.getColumn(0);
            String size = irodsQueryResultRow.getColumn(1).trim().strip();
            DiskUsage diskUsage = new DiskUsage();
            diskUsage.setGroup(path.getName());
            diskUsage.setResource(resource);
            diskUsage.setStorageSize(Long.parseLong(size));
            diskUsages.add(diskUsage);
        }
        return diskUsages;
    }


    /**
     * Uploading a local file to iRODS
     *
     * @param irodsAccount the authentication object to access irods
     * @param localFile    a local file that will be uploaded to the irods location
     * @param remoteFile   the path to the destination
     * @throws JargonException
     * @throws FileNotFoundException
     */
    public static void uploadIrodsFile(IRODSAccount irodsAccount, File localFile, File remoteFile) throws JargonException, FileNotFoundException {
        uploadIrodsFile(irodsAccount, localFile, remoteFile, false);
    }

    public static void uploadIrodsFile(IRODSAccount irodsAccount, File localFile, File remoteFile, Boolean force) throws JargonException, FileNotFoundException {
        // Check if file is not an irods location
        IRODSFileFactory irodsFileFactory = IRODSFileSystem.instance().getIRODSAccessObjectFactory().getIRODSFileFactory(irodsAccount);
        IRODSFile irodsFileFinalDestination = irodsFileFactory.instanceIRODSFile(remoteFile.getAbsolutePath());

        ChecksumValue remoteChecksumValue;
        ChecksumValue localChecksumValue;

        if (irodsFileFinalDestination.exists()) {
            if (force) {
                logger.info("Remove remote file location " + remoteFile);
                irodsFileFinalDestination.delete();
            } else {
                // Get local HASH
                LocalChecksumComputerFactory factory = new LocalChecksumComputerFactoryImpl();
                SHA256LocalChecksumComputerStrategy localActual = (SHA256LocalChecksumComputerStrategy) factory.instance(ChecksumEncodingEnum.SHA256);
                localChecksumValue = localActual.computeChecksumValueForLocalFile(localFile.getAbsolutePath());
                // Get remote hash
                DataObjectChecksumUtilitiesAO dataObjectChecksumUtilitiesAO = IRODSFileSystem.instance().getIRODSAccessObjectFactory().getDataObjectChecksumUtilitiesAO(irodsAccount);
                remoteChecksumValue = dataObjectChecksumUtilitiesAO.computeChecksumOnDataObject(irodsFileFinalDestination);

                if (localChecksumValue.getBase64ChecksumValue().contains(remoteChecksumValue.getBase64ChecksumValue())) {
                    logger.info("Same checksum, not going to overwrite " + localFile.getName());
                    return;
                } else {
                    logger.info("Remove remote file location " + localFile + " " + localChecksumValue.getBase64ChecksumValue());
                    logger.info("Does not match checksum of " + irodsFileFinalDestination.getAbsolutePath() + " " + remoteChecksumValue.getBase64ChecksumValue());
                    irodsFileFinalDestination.delete();
                }
            }
        }

        if (!irodsFileFactory.instanceIRODSFile(remoteFile.getParentFile().getAbsolutePath()).exists()) {
            // Create collection in iRODS
            logger.info("Creating directory: " + remoteFile.getParentFile().getAbsolutePath());
            irodsFileFactory.instanceIRODSFile(remoteFile.getParentFile().getAbsolutePath()).mkdirs();
        }

        // Logger.getLogger("org.irods.jargon.core.transfer").setLevel(Level.OFF);
        DataTransferOperations dataTransferOperationsAO = IRODSFileSystem.instance().getIRODSAccessObjectFactory().getDataTransferOperations(irodsAccount);

        IRODSFile destFile = irodsFileFactory.instanceIRODSFile(remoteFile.getAbsolutePath());
//        dataTransferOperationsAO.putOperation(localFile, destFile, null, tcb);
        logger.info("Uploading " + localFile + " to " + remoteFile);
        dataTransferOperationsAO.putOperation(localFile, destFile, null, null);
        // TODO additional checksum test?

    }



    public static void downloadFile(Credentials credentials, String inputFile, String outputFile) throws JargonException, IOException {
        logger.debug("Checking " + inputFile + " for download");

        IRODSFile irodsFile = credentials.getFileFactory().instanceIRODSFile(inputFile);
        File localFile = new File(outputFile);
        if (!irodsFile.exists()) {
            logger.error("File " + irodsFile + " does not exist");
            return;
        }

        if (irodsFile.isDirectory()) {
            logger.error("File " + irodsFile + " is a directory");
            return;
        }

        if (localFile.exists()) {
            // Check file size
            long localSize = Files.size(Path.of(localFile.getPath()));
            long irodsSize = irodsFile.length();
            if (localSize == irodsSize) {
                logger.debug("File size the same...");
                return;
            } else {
                while (localFile.exists()) {
                    logger.debug("Deleting " + localFile);
                    localFile.delete();
                }
            }
        }
        logger.debug("Downloading " + irodsFile);

        DataTransferOperations dataTransferOperationsAO = IRODSFileSystem.instance().getIRODSAccessObjectFactory().getDataTransferOperations(credentials.getIrodsAccount());
        new File(outputFile).getParentFile().mkdirs();
        dataTransferOperationsAO.getOperation(irodsFile, new File(outputFile), null, null);
    }

    public static long getTrashInfo(IRODSAccount irodsAccount) throws JargonException, GenQueryBuilderException, JargonQueryException {
        // Get size and whatever is under
        long size = getFolderSize(new File("/" + irodsAccount.getZone() + "/trash"), irodsAccount);

        return size;
    }

    private static long getFolderSize(File trashFolder, IRODSAccount irodsAccount) throws GenQueryBuilderException, JargonException, JargonQueryException {
        logger.debug("Checking " + trashFolder.getAbsolutePath());

        IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);

        queryBuilder.addConditionAsGenQueryField(COL_COLL_NAME, QueryConditionOperators.LIKE, trashFolder.getAbsolutePath() + "%");
        queryBuilder.addSelectAsAgregateGenQueryValue(RodsGenQueryEnum.COL_DATA_SIZE, GenQueryField.SelectFieldTypes.SUM);

        // Set limit?
        IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(999999);
        IRODSGenQueryExecutor irodsGenQueryExecutor = IRODSFileSystem.instance().getIRODSAccessObjectFactory().getIRODSGenQueryExecutor(irodsAccount);
        IRODSQueryResultSet irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);
        List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();
        if (irodsQueryResultSetResults.size() > 1)
            throw new JargonException("To many items?");

        if (irodsQueryResultSetResults.get(0).getColumn(0).length() == 0) {
            return 0L;
        } else {
            return Long.parseLong(irodsQueryResultSetResults.get(0).getColumn(0));
        }
    }

    public static void deleteFile(String filePath, Credentials credentials) throws JargonException {
        IRODSFile irodsFile = credentials.getFileFactory().instanceIRODSFile(filePath);
        irodsFile.delete();
    }

    public static ArrayList<nl.fairbydesign.backend.data.objects.File> getFilesBySize(Credentials credentials, String megabytes) {

        try {
            // Get all files without the RESOURCE value
            // IRODSFileFactory fileFactory = IRODSFileSystem.instance().getIRODSFileFactory(irodsAccount);

            // Obtain all projects via metadata
            IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);
            queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_DATA_SIZE, QueryConditionOperators.NUMERIC_GREATER_THAN_OR_EQUAL_TO, Long.parseLong(megabytes) * 1024 * 1024);
            queryBuilder.addSelectAsGenQueryValue(COL_COLL_NAME);
            queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_DATA_NAME);
            queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_DATA_SIZE);
            queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_D_MODIFY_TIME);
            queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_D_RESC_NAME);

            // Set limit?
            IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(999999);

            // IRODSFileSystem irodsFileSystem = new IRODSFileSystem();
            IRODSGenQueryExecutor irodsGenQueryExecutor = credentials.getAccessObjectFactory().getIRODSGenQueryExecutor(credentials.getIrodsAccount());

            IRODSQueryResultSet irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);

            List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();
            HashMap<String, nl.fairbydesign.backend.data.objects.File> files = new HashMap<>();
            for (IRODSQueryResultRow irodsQueryResultRow : irodsQueryResultSetResults) {
                nl.fairbydesign.backend.data.objects.File file = new nl.fairbydesign.backend.data.objects.File();
                String path = irodsQueryResultRow.getColumn(0) + "/" + irodsQueryResultRow.getColumn(1);
                file.setPath(path);
                file.setSize(Long.parseLong(irodsQueryResultRow.getColumn(2)));
                file.setModificationDate(irodsQueryResultRow.getColumn(3));
                file.setStorage(irodsQueryResultRow.getColumn(4));
                files.put(path, file);
            }

            // Get all files with the resource value to be added to the object
            // Obtain all projects via metadata
            queryBuilder = new IRODSGenQueryBuilder(true, null);
            queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_DATA_SIZE, QueryConditionOperators.NUMERIC_GREATER_THAN_OR_EQUAL_TO, Long.parseLong(megabytes) * 1024 * 1024);
            queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_META_DATA_ATTR_NAME, QueryConditionOperators.EQUAL, "RESOURCE");
            queryBuilder.addSelectAsGenQueryValue(COL_COLL_NAME);
            queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_DATA_NAME);
            queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_DATA_SIZE);
            queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_META_DATA_ATTR_VALUE);

            // Set limit?
            query = queryBuilder.exportIRODSQueryFromBuilder(999999);

            // irodsFileSystem = new IRODSFileSystem();
            irodsGenQueryExecutor = credentials.getAccessObjectFactory().getIRODSGenQueryExecutor(credentials.getIrodsAccount());

            irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);

            irodsQueryResultSetResults = irodsQueryResultSet.getResults();

            for (IRODSQueryResultRow irodsQueryResultRow : irodsQueryResultSetResults) {
                String path = irodsQueryResultRow.getColumn(0) + "/" + irodsQueryResultRow.getColumn(1);
                nl.fairbydesign.backend.data.objects.File file = files.get(path);
                file.setResource(irodsQueryResultRow.getColumn(3));
                files.put(path, file);
            }
            return new ArrayList<>(files.values());
        } catch (JargonException | JargonQueryException | GenQueryBuilderException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    public static byte[] checkZip(InputStream inputStream) throws IOException {
        // Upload attempt
        // Check its magic number but can we reset the stream afterward?
        byte[] magicNumber = new byte[4];
        int numberOfBytesRead = inputStream.read(magicNumber);
        // Change path if zip
        if (numberOfBytesRead > -1 && magicNumber[0] == 0x50 && magicNumber[1] == 0x4B && magicNumber[2] == 0x03 && magicNumber[3] == 0x04) {
            logger.warn("File is a zip file (or a folder)");
            return magicNumber;
        }
        return null;
    }

    public static void addMetadataCollection(Credentials credentials, String name, String value, String unit, String collection) throws MalformedURLException {
        try {
            collection = new URL(collection).getPath();
            AvuData avuData = AvuData.instance(name, value, unit);
            CollectionAO collectionAO = credentials.getAccessObjectFactory().getCollectionAO(credentials.getIrodsAccount());
            collectionAO.deleteAVUMetadata(collection, avuData);
            collectionAO.addAVUMetadata(collection, avuData);
        } catch (JargonException e) {
            logger.error("Failed to add metadata to " + collection);
            throw new RuntimeException();
        }
    }

    public static HashMap<String, ArrayList<File>> SearchFile(Credentials credentials, String identifier, String root) throws GenQueryBuilderException, JargonException {
        IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);
        queryBuilder.addConditionAsGenQueryField(COL_COLL_NAME, QueryConditionOperators.LIKE, root + "%");
        queryBuilder.addConditionAsGenQueryField(COL_DATA_NAME, QueryConditionOperators.LIKE, identifier);
        queryBuilder.addSelectAsGenQueryValue(COL_COLL_NAME);
        queryBuilder.addSelectAsGenQueryValue(COL_DATA_NAME);

        IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(999999);
        IRODSGenQueryExecutor irodsGenQueryExecutor = credentials.getAccessObjectFactory().getIRODSGenQueryExecutor(credentials.getIrodsAccount());
        try {
            IRODSQueryResultSet irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);
            List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();
            HashMap<String, ArrayList<File>> files = new HashMap<>();
            for (IRODSQueryResultRow irodsQueryResultRow : irodsQueryResultSetResults) {
                if (!files.containsKey(irodsQueryResultRow.getColumn(1)))
                    files.put(irodsQueryResultRow.getColumn(1), new ArrayList<>());
                files.get(irodsQueryResultRow.getColumn(1)).add(new File(irodsQueryResultRow.getColumn(0) + "/" + irodsQueryResultRow.getColumn(1)));
            }
            return files;
        } catch (JargonException | JargonQueryException e) {
            throw new RuntimeException(e);
        }
    }

    public static void uploadIrodsFile(Credentials credentials, InputStream content, String value, String name, String type) throws JargonException, IOException {
        try {
            IRODSFileFactory irodsFileFactory = credentials.getFileFactory();
            IRODSFile irodsFile = irodsFileFactory.instanceIRODSFile(value);
            if (irodsFile.exists() && irodsFile.isFile()) {
                logger.warn("File exists, removing " + irodsFile.getAbsolutePath());
                irodsFile.delete();
            }
            // Write to local
            try (FileOutputStream irodsFileOutputStream = new FileOutputStream("bla.txt")) {
                byte[] buffer = new byte[1024];
                int bytesRead;
                long totalBytes = 0;
                while ((bytesRead = content.read(buffer)) != -1) {
                    irodsFileOutputStream.write(buffer, 0, bytesRead);
                    totalBytes += bytesRead;
                    // if 1 mb was written
                    if (totalBytes % 1024000 == 0) {
                        logger.info("Recorded " + totalBytes + " bytes");
                    }
                }
            }
        } catch (JargonException | IOException e) {
            throw new RuntimeException(e);
        }
        System.err.println("Finished writing to local file");

        // Second test.. upload the local file to java
//        credentials.getFileFactory().instanceIRODSFile("/unlock/home/jkoehorst/bla.txt").createNewFile();
        try (IRODSFileOutputStream irodsFileOutputStream = credentials.getIrodsFileSystem().getIRODSFileFactory(credentials.getIrodsAccount()).instanceIRODSFileOutputStream("/unlock/home/jkoehorst/bla2.txt")) {
            long totalBytes = 0;
            try (FileInputStream fileInputStream = new FileInputStream("bla.txt")) {
                byte[] buffer = new byte[1024];
                int bytesRead;
                while ((bytesRead = fileInputStream.read(buffer)) != -1) {
                    totalBytes += bytesRead;
                    irodsFileOutputStream.write(buffer, 0, bytesRead);
                    // if 1 mb was written
                    if (totalBytes % 1024000 == 0) {
                        logger.info("Uploaded " + totalBytes + " bytes");
                    }
                }
            }
        } catch (JargonException | IOException e) {
            throw new RuntimeException(e);
        }
        logger.debug("Finished writing to irods file");
    }
}