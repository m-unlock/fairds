package nl.fairbydesign.backend.irods;

import nl.fairbydesign.backend.credentials.Credentials;
import nl.fairbydesign.backend.data.objects.AVU;
import nl.fairbydesign.backend.data.objects.Item;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.pub.*;
import org.irods.jargon.core.pub.domain.AvuData;
import org.irods.jargon.core.pub.io.IRODSFile;
import org.irods.jargon.core.query.*;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static nl.fairbydesign.backend.Generic.humanReadableByteCountBin;
import static nl.fairbydesign.backend.data.objects.Item.Type.FILE;
import static nl.fairbydesign.backend.data.objects.Item.Type.FOLDER;
import static org.irods.jargon.core.query.QueryConditionOperators.EQUAL;
import static org.irods.jargon.core.query.QueryConditionOperators.LIKE;
import static org.irods.jargon.core.query.RodsGenQueryEnum.*;


/**
 * Functions used for browsing data files and collections in irods
 */
public class Browser {
    public static final Logger logger = LogManager.getLogger(Browser.class);


    /**
     * @param credentials object with credential information
     * @param item        object containing info of which AVU (metadata) information should be obtained from
     * @return the item with AVU information
     */
    public static Item getAVUs(Credentials credentials, Item item) {
        try {
            IRODSAccessObjectFactory accessObjectFactory = IRODSFileSystem.instance().getIRODSAccessObjectFactory();
            // Make a hashmap for each tab?

            IRODSFile irodsFile = credentials.getFileFactory().instanceIRODSFile(item.getPath());

            List<MetaDataAndDomainData> metaDataAndDomainDataList = new ArrayList<>();
            if (irodsFile.isDirectory()) {
                metaDataAndDomainDataList = accessObjectFactory.getCollectionAO(credentials.getIrodsAccount()).findMetadataValuesForCollection(item.getPath());
            } else if (irodsFile.isFile()) {
                metaDataAndDomainDataList = accessObjectFactory.getDataObjectAO(credentials.getIrodsAccount()).findMetadataValuesForDataObject(item.getPath());
            }
            item.removeAllAvus();
            for (MetaDataAndDomainData metaDataAndDomainData : metaDataAndDomainDataList) {
                AvuData avu = metaDataAndDomainData.asAvu();
                AVU localAVU = new AVU(avu.getAttribute(), avu.getValue(), avu.getUnit());
                logger.debug("AVU: " + avu.getAttribute() + " " + avu.getValue() + " " + avu.getUnit());
                item.addAvus(localAVU);
            }
            return item;
        } catch (JargonException | JargonQueryException e) {
            e.printStackTrace();
        }
        return item;
    }

    /**
     * @param credentials object with credential information
     * @param item        object containing file / folder informaiton
     * @param attribute   the metadata key
     * @param value       the metadata value
     * @param unit        the metadata unit
     * @return the item with AVU update info
     */
    public static Item addMetadata(Credentials credentials, Item item, String attribute, String value, String unit) {
        try {
            IRODSAccessObjectFactory accessObjectFactory = IRODSFileSystem.instance().getIRODSAccessObjectFactory();

            IRODSFile irodsFile = credentials.getFileFactory().instanceIRODSFile(item.getPath());
            AvuData avuData = new AvuData();
            avuData.setAttribute(attribute);
            avuData.setValue(value);
            avuData.setUnit(unit);
            if (irodsFile.isDirectory()) {
                CollectionAO collectionAO = accessObjectFactory.getCollectionAO(credentials.getIrodsAccount());
                collectionAO.addAVUMetadata(item.getPath(), avuData);
            } else {
                DataObjectAO dataObjectAO = accessObjectFactory.getDataObjectAO(credentials.getIrodsAccount());
                dataObjectAO.addAVUMetadata(item.getPath(), avuData);
            }
            return Browser.getAVUs(credentials, item);
        } catch (JargonException e) {
            e.printStackTrace();
        }
        return item;
    }

    /**
     * @param credentials object with credential information
     * @param item        object containing file / folder informaiton
     * @param avu         object to be removed
     * @return update of the item
     */
    public static Item deleteMetadata(Credentials credentials, Item item, AVU avu) {
        try {

            IRODSAccessObjectFactory accessObjectFactory = IRODSFileSystem.instance().getIRODSAccessObjectFactory();

            IRODSFile irodsFile = credentials.getFileFactory().instanceIRODSFile(item.getPath());
            AvuData avuData = new AvuData();
            avuData.setAttribute(avu.getAttribute());
            avuData.setValue(avu.getValue());
            avuData.setUnit(avu.getUnit());
            if (irodsFile.isDirectory()) {
                CollectionAO collectionAO = accessObjectFactory.getCollectionAO(credentials.getIrodsAccount());
                collectionAO.deleteAVUMetadata(item.getPath(), avuData);
            } else {
                DataObjectAO dataObjectAO = accessObjectFactory.getDataObjectAO(credentials.getIrodsAccount());
                dataObjectAO.deleteAVUMetadata(item.getPath(), avuData);
            }
            return Browser.getAVUs(credentials, item);
        } catch (JargonException e) {
            e.printStackTrace();
        }
        return item;
    }

    public static ArrayList<Item> getFiles(Credentials credentials, String irodsPath) {
        logger.debug("Getting files from " + irodsPath);
        // Perform an iRODS query to get all files in the current directory
        ArrayList<Item> files = new ArrayList<>();
        try {
            IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);
            queryBuilder.addSelectAsGenQueryValue(COL_COLL_NAME);
            queryBuilder.addSelectAsGenQueryValue(COL_DATA_NAME);
            queryBuilder.addSelectAsGenQueryValue(COL_DATA_SIZE);
            queryBuilder.addSelectAsGenQueryValue(COL_D_MODIFY_TIME);
            queryBuilder.addOrderByGenQueryField(COL_DATA_NAME, GenQueryOrderByField.OrderByType.ASC);
            // For files the collection of the file should be equal to the path that is given
            queryBuilder.addConditionAsGenQueryField(COL_COLL_NAME, EQUAL, irodsPath);
            IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(999999);
            IRODSFileSystem irodsFileSystem = new IRODSFileSystem();
            IRODSGenQueryExecutor irodsGenQueryExecutor = irodsFileSystem.getIRODSAccessObjectFactory().getIRODSGenQueryExecutor(credentials.getIrodsAccount());
            IRODSQueryResultSet irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);
            List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();
            for (IRODSQueryResultRow queryResultRow : irodsQueryResultSetResults) {
                logger.debug("Found file: " + queryResultRow.getColumn(1));
                String path = queryResultRow.getColumn(0);
                String name = queryResultRow.getColumn(1);
                long size = Long.parseLong(queryResultRow.getColumn(2));
                long modd = Long.parseLong(queryResultRow.getColumn(3));
                logger.debug("File: " + name + " " + size + " " + modd);
                Item item = new Item(name, modd, humanReadableByteCountBin(size), FILE, path + "/" + name);
                item.setTrash(true);
                item.setInfo(true);
                item.setDownload(true);
                files.add(item);
            }
        } catch (JargonQueryException | JargonException | GenQueryBuilderException e) {
            throw new RuntimeException(e);
        }
        logger.debug("Found " + files.size() + " files");
        return files;
    }

    public static HashMap<String, String> getFilesRecursively(Credentials credentials, IRODSFile irodsPath) {
        logger.info("Getting files from " + irodsPath.getAbsolutePath());
        // Perform an iRODS query to get all files in the current directory
        HashMap<String, String> files = new HashMap<>();
        try {
            IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);
            queryBuilder.addSelectAsGenQueryValue(COL_COLL_NAME);
            queryBuilder.addSelectAsGenQueryValue(COL_DATA_NAME);
            // List all files in the collection recursively
            queryBuilder.addConditionAsGenQueryField(COL_COLL_NAME, LIKE, irodsPath.getAbsolutePath() + "%");
            IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(999999);
            IRODSFileSystem irodsFileSystem = new IRODSFileSystem();
            IRODSGenQueryExecutor irodsGenQueryExecutor = irodsFileSystem.getIRODSAccessObjectFactory().getIRODSGenQueryExecutor(credentials.getIrodsAccount());
            IRODSQueryResultSet irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);
            List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();
            for (IRODSQueryResultRow queryResultRow : irodsQueryResultSetResults) {
                String path = queryResultRow.getColumn(0);
                String name = queryResultRow.getColumn(1);
                if (files.containsKey(name)) {
                    // Duplication detected... not sure which file belongs where?...
                    files.put("error", "duplicate files detected, identical filenames are not allowed to be in the fairds-data folder or any subfolder of the folder");
                } else {
                    files.put(name, path + "/" + name);
                }
            }
        } catch (JargonQueryException | JargonException | GenQueryBuilderException e) {
            throw new RuntimeException(e);
        }
        logger.debug("Found " + files.size() + " files");
        return files;
    }

    public static ArrayList<Item> getFolders(Credentials credentials, String irodsPath) {
        logger.debug("Getting folders from " + irodsPath);
        // Perform an iRODS query to get all files in the current directory
        ArrayList<Item> folders = new ArrayList<>();
        try {
            IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);
            queryBuilder.addSelectAsGenQueryValue(COL_COLL_NAME);
            queryBuilder.addSelectAsGenQueryValue(COL_COLL_MODIFY_TIME);
            // For a folder the parent collection (as it is a collection already) should be equal to the path that is given
            queryBuilder.addConditionAsGenQueryField(COL_COLL_PARENT_NAME, EQUAL, irodsPath);
            queryBuilder.addOrderByGenQueryField(COL_COLL_NAME, GenQueryOrderByField.OrderByType.ASC);
            IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(999999);
            IRODSFileSystem irodsFileSystem = new IRODSFileSystem();
            IRODSGenQueryExecutor irodsGenQueryExecutor = irodsFileSystem.getIRODSAccessObjectFactory().getIRODSGenQueryExecutor(credentials.getIrodsAccount());
            IRODSQueryResultSet irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);
            List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();
            for (IRODSQueryResultRow queryResultRow : irodsQueryResultSetResults) {
                // logger.debug("Found folder: " + queryResultRow.getColumn(0));
                String path = queryResultRow.getColumn(0);
                long modd = Long.parseLong(queryResultRow.getColumn(1));
                Item item = new Item(new File(path).getName(), modd, null, FOLDER, path);
                item.setTrash(true);
                item.setInfo(true);
                item.setDownload(true);
                folders.add(item);
            }
        } catch (JargonQueryException | JargonException | GenQueryBuilderException e) {
            throw new RuntimeException(e);
        }
        logger.debug("Found " + folders.size() + " folders");
        return folders;
    }
}