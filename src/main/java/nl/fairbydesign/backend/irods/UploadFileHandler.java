////
//// Source code recreated from a .class file by IntelliJ IDEA
//// (powered by FernFlower decompiler)
////
//
//package nl.fairbydesign.backend.irods;
//
//import com.vaadin.flow.component.UI;
//import com.vaadin.flow.component.upload.MultiFileReceiver;
//import com.vaadin.flow.component.upload.Receiver;
//import org.vaadin.firitin.components.upload.VUpload;
//
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.OutputStream;
//import java.io.PipedInputStream;
//import java.io.PipedOutputStream;
//
//public class UploadFileHandler extends VUpload {
//    MultiFileReceiver multiFileReceiver = new MultiFileReceiver() {
//        public OutputStream receiveUpload(String s, String s1) {
//            return UploadFileHandler.this.receiveUpload(s, s1);
//        }
//    };
//    Receiver receiver = new Receiver() {
//        public OutputStream receiveUpload(String s, String s1) {
//            return UploadFileHandler.this.receiveUpload(s, s1);
//        }
//    };
//    protected final FileHandler fileHandler;
//
//    public UploadFileHandler allowMultiple() {
//        this.setReceiver(this.multiFileReceiver);
//        this.setMaxFiles(Integer.MAX_VALUE);
//        return this;
//    }
//
//    public UploadFileHandler(FileHandler fileHandler) {
//        this.fileHandler = fileHandler;
//        this.setReceiver(this.receiver);
//    }
//
//    public OutputStream receiveUpload(String filename, String mimeType) {
//        try {
//            PipedInputStream in = new PipedInputStream();
//            PipedOutputStream out = new PipedOutputStream(in);
//            this.writeResponce(in, filename, mimeType);
//            return out;
//        } catch (IOException var5) {
//            throw new RuntimeException(var5);
//        }
//    }
//
//    protected void writeResponce(final PipedInputStream in, final String filename, final String mimeType) {
//        (new Thread() {
//            public void run() {
//                try {
//                    UploadFileHandler.this.fileHandler.handleFile(in, filename, mimeType);
//                    in.close();
//                } catch (Exception var2) {
//                    ((UI)UploadFileHandler.this.getUI().get()).access(() -> {
//                    });
//                }
//
//            }
//        }).start();
//    }
//
//    @FunctionalInterface
//    public interface FileHandler {
//        void handleFile(InputStream var1, String var2, String var3);
//    }
//}
