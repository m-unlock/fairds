package nl.fairbydesign.backend.irods;

import nl.fairbydesign.backend.credentials.Credentials;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.packinstr.DataObjInp;
import org.irods.jargon.core.pub.io.FileIOOperations;
import org.irods.jargon.core.pub.io.IRODSRandomAccessFile;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.atomic.AtomicLong;

public class StreamCopyUtil {
    public static final Logger logger = LogManager.getLogger(StreamCopyUtil.class);


    public static void copyWithProgress(InputStream input, BufferedOutputStream output, ProgressListener listener) throws IOException {
        long startTime = System.currentTimeMillis();
        byte[] buffer = new byte[8192]; // Buffer size is 100 KB
        int nRead;
        long totalBytesRead = 0;
        long lastTotalBytesReadForProgress = 0;

        while ((nRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, nRead);
            totalBytesRead += nRead;

            // Check if at least 5 MB has been read since the last update
            if (totalBytesRead - lastTotalBytesReadForProgress >= 1024 * 1024 * 1.0) {
                listener.update(totalBytesRead);
                lastTotalBytesReadForProgress = totalBytesRead;
            }
        }

        // Close the streams
        input.close();
        output.close();

        // Final update to ensure listener gets the complete file size at the end
        if (totalBytesRead != lastTotalBytesReadForProgress) {
            listener.update(totalBytesRead);
        }
        long endTime = System.currentTimeMillis();
        logger.info("Time elapsed: " + (endTime - startTime) / 1000.0 + " seconds");
    }

    public static void copyWithProgressCoordinated(Credentials credentials, InputStream fileInputStream, String path, ProgressListener listener) throws JargonException, IOException {
        long startTime = System.currentTimeMillis();
        // TODO limit the number of chunks to reduce memory size consumption
        // As long as there are chunks to read, read them

        Queue<DataChunk> chunks = new ConcurrentLinkedQueue<>();

        // Start a thread that keeps collecting chunks
        System.err.println("Buffer size: " + credentials.getAccessObjectFactory().getJargonProperties().getPutBufferSize());
        byte[] buffer = new byte[credentials.getAccessObjectFactory().getJargonProperties().getPutBufferSize()];
        Thread chunkCollector = getChunkCollector(fileInputStream, chunks, 100, buffer);
        logger.info("Started chunk collector");

        // In parallel with the chunk collector, write the chunks to the file
        ForkJoinPool customThreadPool = new ForkJoinPool(25);
        try {
            // Tracker of total bytes written
            AtomicLong totalBytesWritten = new AtomicLong();
            customThreadPool.submit(() -> {
                        try {
                            IRODSRandomAccessFile irodsRandomAccessFile = credentials.getFileFactory().instanceIRODSRandomAccessFile(path, DataObjInp.OpenFlags.READ_WRITE, true);

                            // As long as there are chunks to write, write them
                            logger.info("Writing chunks " + chunks.size() + " chunks left " + chunkCollector.isAlive() + " collector alive");
                            while (chunkCollector.isAlive() || !chunks.isEmpty()) {
                                logger.debug("Chunks left: " + chunks.size());
                                DataChunk chunk = chunks.poll();
                                assert chunk != null;
                                try {
                                    synchronized (irodsRandomAccessFile) {
                                        irodsRandomAccessFile.seek(chunk.getOffset(), FileIOOperations.SeekWhenceType.SEEK_START);
                                        irodsRandomAccessFile.write(chunk.getData(), 0, chunk.getData().length);
                                        totalBytesWritten.addAndGet(chunk.getData().length);
                                        listener.update(totalBytesWritten.get());
                                    }
                                } catch (IOException e) {
                                    // Handle exception
                                }
                            }
                            irodsRandomAccessFile.close();
                        } catch (JargonException | IOException e) {
                            throw new RuntimeException(e);
                        }
                    }
            ).get(); // This will wait for all tasks to complete
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
        logger.info("Finished writing chunks?");
        // iRODS get checksum
        String checksum = credentials.getAccessObjectFactory().getDataObjectAO(credentials.getIrodsAccount()).computeMD5ChecksumOnDataObject(credentials.getFileFactory().instanceIRODSFile(path));
        System.err.println("Checksum chunks irods: " + checksum);
        long endTime = System.currentTimeMillis();
        logger.info("Time elapsed: " + (endTime - startTime) / 1000.0 + " seconds");
    }

    @NotNull
    private static Thread getChunkCollector(InputStream fileInputStream, Queue<DataChunk> chunks, int chunkSize, byte[] buffer) {
        Thread chunkCollector = new Thread(() -> {
            int bytesRead;
            long currentOffset = 0; // Starting offset
            long counter = 0;
            while (true) {
                try {
                    // If the number of chunks is too large, wait and write them to the file
                    if (counter % 100 == 0) {
                        logger.debug("Chunks added " + counter);
                    }
                    while (chunks.size() > chunkSize) {
                        Thread.sleep(100);
                    }
                    // Read the next chunk
                    bytesRead = fileInputStream.read(buffer);
                    if (bytesRead == -1) {
                        logger.info("No more bytes to read");
                        // No more chunks to read
                        break;
                    }
                    byte[] dataChunk = new byte[bytesRead];
                    counter++;
                    System.arraycopy(buffer, 0, dataChunk, 0, bytesRead);
                    chunks.add(new DataChunk(dataChunk, currentOffset));
                    currentOffset += bytesRead; // increment by the actual number of bytes read
                } catch (IOException | InterruptedException e) {
                    // Handle exceptions
                }
            }
        });
        // Start the thread that collects the chunks
        chunkCollector.start();
        return chunkCollector;
    }
}