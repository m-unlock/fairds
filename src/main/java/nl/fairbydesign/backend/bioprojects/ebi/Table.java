package nl.fairbydesign.backend.bioprojects.ebi;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class Table {

    public static HashMap<String, Map<String, String>> readTsvFile(File tsvFile) throws IOException {
        HashMap<String, Map<String, String>> data = new HashMap<>();
        try (BufferedReader br = new BufferedReader(new FileReader(tsvFile))) {
            String line;
            String[] headers = null;

            while ((line = br.readLine()) != null) {
                String[] values = line.split("\t");

                if (headers == null) {
                    headers = values;
                } else {
                    Map<String, String> row = new HashMap<>();

                    for (int i = 0; i < headers.length; i++) {
                        if (i < values.length) {
                            row.put(headers[i], values[i]);
                        } else {
                            row.put(headers[i], "");
                        }
                    }
                    String accession = row.get("run_accession");
                    data.put(accession, row);
                }
            }
        }
        return data;
    }
}
