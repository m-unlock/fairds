package nl.fairbydesign.backend.bioprojects.ebi;

import com.github.jsonldjava.core.JsonLdOptions;
import com.github.jsonldjava.core.JsonLdProcessor;
import com.github.jsonldjava.utils.JsonUtils;
import com.vaadin.flow.server.VaadinSession;
import nl.fairbydesign.Application;
import org.apache.jena.atlas.lib.DateTimeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.util.FileUtils;
import org.eclipse.rdf4j.model.*;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.query.QueryLanguage;
import org.eclipse.rdf4j.query.Update;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.sail.memory.MemoryStore;
import org.json.JSONObject;
import org.json.XML;

import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.util.*;

import static nl.fairbydesign.backend.Generic.humanReadableByteCountBin;
import static org.apache.jena.reasoner.rulesys.builtins.BaseBuiltin.BASE_URI;

public class EBI {
    public final Logger logger = LogManager.getLogger(EBI.class);
    public HashMap<String, ArrayList<File>> rdfFiles = new HashMap<>();
    public HashMap<String, ArrayList<File>> rawFiles = new HashMap<>();

    public XLSX xlsx = new XLSX();

    // TODO this variable should not be static!
    // public static String message = "";

    public EBI(VaadinSession session) {
        logger.debug("EBI TEST MESSAGE: " + session.getSession().getAttribute("message"));
        setSession(session);
    }

    public EBI() {

    }

    public void fetch(ArrayList<String> logMessage, HashSet<String> bioprojects) {
        this.fetch(logMessage, bioprojects, false, false);
    }

    public HashMap<String, ArrayList<File>> fetch(ArrayList<String> logMessage, HashSet<String> bioprojects, boolean rdf) {
        return this.fetch(logMessage, bioprojects, rdf, false);
    }

    public HashMap<String, ArrayList<File>> fetch(ArrayList<String> logMessage, HashSet<String> bioprojects, boolean rdf, boolean raw) {
        // Folder structure
        String rootFolder = Application.commandOptions.storage +"/bioprojects/ebi/";
        new File(rootFolder).mkdirs();

        // Get bioprojects
        for (String bioproject : bioprojects) {
            logging(logMessage, "Processing bioproject " + bioproject);

            // Create bioproject file container
            ArrayList<File> files = new ArrayList<>();

            // Create bioproject folder
            String bioprojectFolder = rootFolder + bioproject + "/";
            new File(bioprojectFolder).mkdirs();

            // Get study
            logging(logMessage, "Getting study metadata");
            String url = "https://www.ebi.ac.uk/ena/browser/api/xml/" + bioproject + "?download=true";
            File outputFile = new File(bioprojectFolder + bioproject + "_study.xml"); // Specify the output file name
            files.add(retrieve(logMessage, outputFile, url));

            // Get sample
            logging(logMessage, "Getting sample metadata");
            url = "https://www.ebi.ac.uk/ena/browser/api/xml/search?query=study_accession=%22" + bioproject + "%22&result=sample&fields=sample_accession,sample_description,study_accession&limit=0&includeLinks=false";
            outputFile = new File(bioprojectFolder + bioproject + "_sample.xml"); // Specify the output file name
            files.add(retrieve(logMessage, outputFile, url));

            // Get read experiment
            logging(logMessage, "Getting read experiment metadata");
            url = "https://www.ebi.ac.uk/ena/browser/api/xml/search?query=study_accession=%22" + bioproject + "%22&result=read_experiment&fields=run_accession,experiment_title,study_accession&limit=0";
            outputFile = new File(bioprojectFolder + bioproject + "_read_experiment.xml"); // Specify the output file name
            files.add(retrieve(logMessage, outputFile, url));

            // Get raw reads
            logging(logMessage, "Getting raw reads metadata");
            url = "https://www.ebi.ac.uk/ena/browser/api/xml/search?query=study_accession=%22" + bioproject + "%22&result=read_run&fields=run_accession,experiment_title,study_accession&limit=0";
            outputFile = new File(bioprojectFolder + bioproject + "_read_run.xml"); // Specify the output file name
            files.add(retrieve(logMessage, outputFile, url));

            // Get the links
            logging(logMessage, "Getting links metadata");
            url = "https://www.ebi.ac.uk/ena/portal/api/filereport?accession=" + bioproject + "&result=read_run&fields=study_accession,sample_accession,experiment_accession,run_accession,tax_id,scientific_name,fastq_ftp,submitted_ftp,sra_ftp&format=json&download=true&limit=0";
            outputFile = new File(bioprojectFolder + bioproject + "_links.json"); // Specify the output file name
            files.add(retrieve(logMessage, outputFile, url));

            // Check if any of the added files is null
            if (files.contains(null)) {
                logging(logMessage, "Failed to download and convert " + url + " skipping bioproject " + bioproject);
                continue;
            }

            if (raw) {
                rawFiles.put(bioproject, files);
            } else if (rdf) {
                // On the fly conversion to excel for validation
                convertToRDF(logMessage, bioproject, files);
            } else {
                // Convert to RDF and then convert to excel?...
                convertToRDF(logMessage, bioproject, files);
                xlsx.convertToExcel(logMessage, rdfFiles.get(bioproject));
                logging(logMessage, "Finished converting bioproject " + bioproject + " from EBI");
                files.clear();
            }
        }

        if (raw) {
            return rawFiles;
        } else if (rdf) {
            return rdfFiles;
        } else {
            return null;
        }
    }

    private void convertToRDF(ArrayList<String> logMessage, String bioproject, ArrayList<File> files) {
        rdfFiles.put(bioproject, new ArrayList<>());
        files.forEach(enaFile -> {
            File rdfFile = new File(enaFile.getAbsolutePath().replace(".xml", ".ttl").replace(".json", ".ttl"));
            if (!rdfFile.exists()) {
                try {
                    rdfFile = convertToRDF(logMessage, enaFile);
                    rdfFiles.get(bioproject).add(rdfFile);
                    logging(logMessage, "Finished converting " + enaFile.getAbsolutePath() + " to RDF");
                } catch (Exception e) {
                    logging(logMessage, "Failed to convert " + enaFile.getAbsolutePath() + " to RDF. " + e.getMessage());
                }
            } else {
                rdfFiles.get(bioproject).add(rdfFile);
            }
        });
    }

    // There is an issue with ENA XML files missing the last line
//    private void enaFix(ArrayList<String> logMessage, String bioproject, File enaFile) {
//        // Read the first line of ena
//        try (BufferedReader br = new BufferedReader(new FileReader(enaFile))) {
//            String line;
//            StringBuilder sb = new StringBuilder();
//            while ((line = br.readLine()) != null) {
//                sb.append(line);
//            }
//            String content = sb.toString();
//            if (!content.endsWith("</ROOT>")) {
//                content += "</ROOT>";
//                PrintWriter out = new PrintWriter(enaFile);
//                out.println(content);
//                out.close();
//                logging(logMessage, "Fixed " + enaFile.getAbsolutePath() + " by adding the last line");
//                convertToRDF(logMessage, bioproject, new ArrayList<>(Collections.singletonList(enaFile)));
//            }
//        } catch (IOException e) {
//            logging(logMessage, "Failed to fix " + enaFile.getAbsolutePath() + " by adding the last line");
//        }
//    }

    private void logging(ArrayList<String> logMessage, String message) {
        logger.debug(message);
//        EBI.message += "\n" + DateTimeUtils.nowAsString() + " - " + message;
        // logMessage.add(0, DateTimeUtils.nowAsString() + " - " + message);
        logging(logMessage, message, false);
    }

    public void logging(ArrayList<String> logMessage, String message, Boolean overrideLast) {
        logger.debug(message);
        if (overrideLast && !logMessage.isEmpty()) {
            logMessage.remove(0);
        }
        logMessage.add(0, DateTimeUtils.nowAsString() + " - " + message);
    }

    public File retrieve(ArrayList<String> logMessage, File outputFile, String url) {
        int attempt = 0;
        File rdfFile = null;
        while (attempt < 3) {
            attempt++;
            // Attempt on download and conversion
            try {
                // TODO check if this needs to be implemented
                if (outputFile.exists() && outputFile.length() < -40) {
                    logging(logMessage, "File too small: " + outputFile.getName() + " (" + humanReadableByteCountBin(outputFile.length()) + ")");
                    outputFile.delete();
                    wget(logMessage, url, outputFile);
                } else if (!outputFile.exists()) {
                    wget(logMessage, url, outputFile);
                }
            } catch (Exception e) {
                logger.debug(e.getMessage());
                logging(logMessage, "Failed to download " + url + " on attempt " + attempt);
                // e.printStackTrace();
                // Delete the file if it exists and making sure it is deleted
                while (outputFile.exists()) {
                    outputFile.delete();
                }
            }
        }
        return outputFile;
    }

    public void wget(ArrayList<String> logMessage, String url, File outputFile) {
        logging(logMessage, "Opening the data stream to " + url + " and saving to " + outputFile.getName() + " might take a moment... Please be patient", true);
        try (InputStream inputStream = new URL(url).openStream();
             BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
             FileOutputStream fileOutputStream = new FileOutputStream(outputFile)) {

            byte[] buffer = new byte[1024];
            int bytesRead;
            // Keep trach of the size downloaded
            long size = 0;
            while ((bytesRead = bufferedInputStream.read(buffer, 0, buffer.length)) != -1) {
                size += bytesRead;
                fileOutputStream.write(buffer, 0, bytesRead);
                logging(logMessage, "Downloaded " + humanReadableByteCountBin(size) + " of " + url, true);
            }
            // Check file size of the downloaded file
            if (outputFile.length() < 40) {
                logging(logMessage, "File too small: " + outputFile.getName() + " (" + humanReadableByteCountBin(outputFile.length()) + ")", true);
                outputFile.delete();
            } else {
                logging(logMessage, "File downloaded successfully to " + outputFile.getName(), true);
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    private File convertToRDF(ArrayList<String> logMessage, File inputFile) throws IOException {
        String fileExtension = FileUtils.getFileExtension(inputFile);
        File jsonLDFile = new File(inputFile.getAbsolutePath().replace(fileExtension, "jsonld"));
        if (!jsonLDFile.exists()) {
            String jsonString = null;
            if (fileExtension.equalsIgnoreCase("json")) {
                // Read input file to string
                jsonString = new String(Files.readAllBytes(inputFile.toPath()));
                logger.debug("\n Finished loading JSON file: " + inputFile.getName() + " (" + Files.size(inputFile.toPath()) + ")");
            } else if (fileExtension.equalsIgnoreCase("xml")) {
                // Read input file to string
                if (inputFile.length() > 1000000000) {
                    logMessage.add(0, "File too large: " + inputFile.getName() + " (" + Files.size(inputFile.toPath()) + ")");
                    return null;
                }
                String xmlString = new String(Files.readAllBytes(inputFile.toPath()));
                // Convert input XML to JSON
                JSONObject json = XML.toJSONObject(xmlString);
                jsonString = json.toString(4);
                logger.debug("Finished loading XML file: " + inputFile.getName() + " (" + Files.size(inputFile.toPath()) + ")");
            } else {
                logger.error("File extension not supported: " + fileExtension);
            }
            // Read the JSON file
            Map<String, Object> jsonMap = null;
            ArrayList<Map<String, Object>> jsonArray = null;
            try {
                jsonMap = (Map<String, Object>) JsonUtils.fromString(jsonString);
            } catch (ClassCastException e) {
                jsonArray = (ArrayList<Map<String, Object>>) JsonUtils.fromString(jsonString);
            }

            // Convert JSON to JSON-LD with a default context
            logger.debug("Converting JSON to JSON-LD");
            Map<String, Object> context = new HashMap<>();
            context.put("@vocab", "http://fairbydesign.nl/ontology/");
            JsonLdOptions options = new JsonLdOptions();
            options.setExpandContext(context);
            // Array or object
            List<Object> jsonLd;
            if (jsonMap != null) {
                jsonLd = JsonLdProcessor.expand(jsonMap, options);
            } else {
                jsonLd = JsonLdProcessor.expand(jsonArray, options);
            }

            // Write the resulting JSON-LD to a file
            logger.debug("Writing JSON-LD to file");
            PrintWriter out = new PrintWriter(jsonLDFile);
            out.println(JsonUtils.toPrettyString(jsonLd));
            out.close();
        }

        // Map to store blank node IDs and UUIDs
        Map<String, String> blankNodeToUUID = new HashMap<>();

        // Create a disk store
        Repository repository = new SailRepository(new MemoryStore()); // (new NativeStore());
        repository.init();

        // Using jena as intermediate
        // Load the json-ld file into RDF4J
        Model model;
        try {
            logMessage.add(0, DateTimeUtils.nowAsString() + " - Loading JSON-LD file: " + jsonLDFile.getName() + " (" + humanReadableByteCountBin(jsonLDFile.length()) + ")");
            model = Rio.parse(new FileInputStream(jsonLDFile), BASE_URI, RDFFormat.JSONLD);
//            File tempDir = Files.createTempDirectory("jena").toFile();
//            Domain domain = new Domain("file://" + tempDir.getAbsolutePath() + "/");
//            File turtleFile = new File(jsonLDFile.getAbsolutePath().replace(".jsonld", ".ttl"));
//            domain.getRDFSimpleCon().getModel().read(jsonLDFile.getAbsolutePath(), RDFFormat.JSONLD.getName());
//            domain.save(turtleFile.getAbsolutePath());
//            domain.closeAndDelete();
//            model = Rio.parse(new FileInputStream(turtleFile), BASE_URI, RDFFormat.TURTLE);
//            turtleFile.delete();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        // Collect all statements with a blank node
        for (Statement statement : model.getStatements(null, null, null)) {
            if (statement.getSubject().isBNode() && statement.getObject().isBNode()) {
                // Check if the subject is in the blankNodeToUUID map
                if (!blankNodeToUUID.containsKey(statement.getSubject().stringValue())) {
                    // If not, add it
                    String uuid = "urn:uuid:" + UUID.randomUUID();
                    blankNodeToUUID.put(statement.getSubject().stringValue(), uuid);
                }
                // Check if the object is in the blankNodeToUUID map
                if (!blankNodeToUUID.containsKey(statement.getObject().stringValue())) {
                    // If not, add it
                    String uuid = "urn:uuid:" + UUID.randomUUID();
                    blankNodeToUUID.put(statement.getObject().stringValue(), uuid);
                }
                // Create a new statement
                IRI subject = SimpleValueFactory.getInstance().createIRI(blankNodeToUUID.get(statement.getSubject().stringValue()));
                IRI predicate = statement.getPredicate();
                Value object = SimpleValueFactory.getInstance().createIRI(blankNodeToUUID.get(statement.getObject().stringValue()));
                Statement newStatement = SimpleValueFactory.getInstance().createStatement(subject, predicate, object);
                repository.getConnection().add(newStatement);
            } else if (statement.getSubject().isBNode()) {
                // Check if the subject is in the blankNodeToUUID map
                if (!blankNodeToUUID.containsKey(statement.getSubject().stringValue())) {
                    // If not, add it
                    String uuid = "urn:uuid:" + UUID.randomUUID();
                    blankNodeToUUID.put(statement.getSubject().stringValue(), uuid);
                }
                // Create a new statement
                IRI subject = SimpleValueFactory.getInstance().createIRI(blankNodeToUUID.get(statement.getSubject().stringValue()));
                IRI predicate = statement.getPredicate();
                Value object = statement.getObject();
                Statement newStatement = SimpleValueFactory.getInstance().createStatement(subject, predicate, object);
                repository.getConnection().add(newStatement);
            } else if (statement.getObject().isBNode()) {
                // Check if the subject is in the blankNodeToUUID map
                if (!blankNodeToUUID.containsKey(statement.getObject().stringValue())) {
                    // If not, add it
                    String uuid = "urn:uuid:" + UUID.randomUUID();
                    blankNodeToUUID.put(statement.getObject().stringValue(), uuid);
                }
                // Create a new statement
                Resource subject = statement.getSubject();
                IRI predicate = statement.getPredicate();
                Value object = SimpleValueFactory.getInstance().createIRI(blankNodeToUUID.get(statement.getObject().stringValue()));
                Statement newStatement = SimpleValueFactory.getInstance().createStatement(subject, predicate, object);
                repository.getConnection().add(newStatement);
            } else {
                repository.getConnection().add(statement);
            }
        }

        // Change all predicates to lowercase
        for (Statement statement : repository.getConnection().getStatements(null, null, null, true)) {
            Resource subject = statement.getSubject();
            IRI predicate = SimpleValueFactory.getInstance().createIRI(statement.getPredicate().stringValue().toLowerCase());
            Value object = statement.getObject();
            Statement newStatement = SimpleValueFactory.getInstance().createStatement(subject, predicate, object);
            repository.getConnection().remove(statement);
            repository.getConnection().add(newStatement);
        }

        // INSERT QUERY for RDF TYPE
        String queryString = "INSERT {\n" +
                "    ?o a ?type .\n" +
                "} WHERE {\n" +
                "    ?s ?p ?o .\n" +
                "    FILTER(STRSTARTS(STR(?o), \"urn:\"))\n" +
                "    BIND(REPLACE(STR(?p), \"http://fairbydesign.nl/ontology/\", \"\") AS ?y)\n" +
                "    BIND(REPLACE(LCASE(STR(?y)), \"(\\\\b[a-z](?!\\\\s))\", UCASE(SUBSTR(?y, 1, 1)) ) as ?title_case)\n" +
                "    BIND(IRI(CONCAT(\"http://fairbydesign.nl/ontology/\", ?title_case)) AS ?type)\n" +
                "}";

        Update update = repository.getConnection().prepareUpdate(QueryLanguage.SPARQL, queryString);
        update.execute();

        // Save repository to file
        File rdfFile = new File(jsonLDFile.getAbsolutePath().replace(".jsonld", ".ttl"));
        // Set prefix for rdf4j repository
        repository.getConnection().setNamespace("fbd", "http://fairbydesign.nl/ontology/");
        Rio.write(repository.getConnection().getStatements(null, null, null, true), new PrintWriter(rdfFile), RDFFormat.TURTLE);
        return rdfFile;
    }

    public void setSession(VaadinSession session) {
    }
}