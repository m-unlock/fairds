package nl.fairbydesign.backend.bioprojects.ebi;

import nl.fairbydesign.Application;
import nl.fairbydesign.backend.metadata.Metadata;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.jena.atlas.lib.DateTimeUtils;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFHandlerException;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.eclipse.rdf4j.rio.RDFParser;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.helpers.StatementCollector;
import org.eclipse.rdf4j.sail.memory.MemoryStore;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import static nl.fairbydesign.backend.bioprojects.ebi.Table.readTsvFile;
import static nl.fairbydesign.backend.metadata.Metadata.Requirement.MANDATORY;
import static nl.fairbydesign.backend.metadata.Metadata.Requirement.RECOMMENDED;

public class XLSX {
    public final Logger logger = LogManager.getLogger(XLSX.class);
    private final SailRepository repository = new SailRepository(new MemoryStore());
    private final XSSFWorkbook workbook = new XSSFWorkbook();
    public CellStyle headerStyle;
    public final int defaultCellWidth = 25;
    public CellStyle obligatoryStyle;
    private ArrayList<String> headerList = new ArrayList<>();
    private ArrayList<Sheet> assaySheets = new ArrayList<>();
    private String study_accession;
    private HashMap<String, Map<String, String>> checksumMap = new HashMap<>();

    public XLSX() {

        // Create an RDF4J Repository
        repository.init();

        // Setup excel formatting
        makeHeaderCellStyle();
        makeObligatoryHeaderCellStyle();

        // Create investigation
        createInvestigation();
    }
    public void convertToExcel(ArrayList<String> logMessage, ArrayList<File> rdfFiles) {
        // Load bioproject data into repository
        try (RepositoryConnection connection = repository.getConnection()) {
            logger.debug("Loading RDF files into repository (size "+connection.size()+") for bioProject: " + rdfFiles.get(0).getParentFile().getName());
            // Iterate over the files and load them into the repository
            for (File rdfFile : rdfFiles) {

                // Create an RDF Parser
                RDFParser parser = Rio.createParser(RDFFormat.TURTLE);

                // Set up a StatementCollector to collect parsed statements
                StatementCollector collector = new StatementCollector();
                parser.setRDFHandler(collector);

                // Parse the file
                FileInputStream fis = new FileInputStream(rdfFile);
                // Parse the file and collect the statements
                parser.parse(fis, "");

                // Get the collected statements as a model
                Collection<Statement> statements = collector.getStatements();

                // Add the model to the repository
                connection.add(statements);
            }
            logger.debug("Finished loading RDF files into repository (size "+connection.size()+") for bioProject: " + rdfFiles.get(0).getParentFile().getName());
        } catch (IOException | RDFParseException | RDFHandlerException e) {
            e.printStackTrace();
        }

        // Create a connection to the repository
        RepositoryConnection connection = repository.getConnection();

        // Create study
        logging(logMessage, "Creating study");
        createStudy(logMessage, connection);
        logger.debug("Finished creating study");

        // Create investigation
        logger.debug("Creating investigation");
        createObservationUnit(connection);
        logger.debug("Finished creating investigation");

        // Create samples
        logger.debug("Creating samples");
        createSamples(connection);
        logger.debug("Finished creating samples");

        // Create assay
        logger.debug("Creating assay");
        createAssay(logMessage, connection);
        logger.debug("Finished creating assay");

        // Shut down the repository
        logger.debug("Shutting down repository with " + repository.getConnection().size() + " statements");
        repository.getConnection().clear();
        // repository.shutDown();
        logger.debug("Finished... shutting down repository");
    }

    void logging(ArrayList<String> logMessage, String message) {
        logger.info(message);
        logMessage.add(0,DateTimeUtils.nowAsString() + " - " + message);
    }

    private void createObservationUnit(RepositoryConnection connection) {
        Sheet obsSheet = createSheet("ObservationUnit");
        // TODO...
        headerList.add("study identifier");

        int rowIndex = obsSheet.getLastRowNum() + 1;

        // Obtain all samples from the repository using a sparql query
        TupleQuery query = connection.prepareTupleQuery("""
                PREFIX fair: <http://fairbydesign.nl/ontology/>
                select distinct ?sample where {
                	?sample a <http://fairbydesign.nl/ontology/Sample> .
                }""");
        // Execute query
        try (TupleQueryResult result = query.evaluate()) {
            for (BindingSet bindings : result) {
                Row obsRow = obsSheet.createRow(rowIndex);
                rowIndex++;

                // Obtain the sample
                String obsUnit = bindings.getBinding("sample").getValue().stringValue();

                // For each sample we create an identical observation unit with O_ as we do not now if there are multiple samples from the same observation unit
                String obs_accession = "O_" + getValueFrom(connection, obsUnit, "http://fairbydesign.nl/ontology/accession");
                obsRow.createCell(headerList.indexOf("observation unit identifier")).setCellValue(obs_accession);

                String obs_description = getValueFrom(connection, obsUnit, "http://fairbydesign.nl/ontology/description");
                if (obs_description != null && obs_description.length() >= 25) {
                    // pass
                } else if (obs_description == null) {
                    obs_description = "No description available and has been corrected to be longer than 25 characters.";
                } else {
                    obs_description = obs_description + " this description was not of sufficient length and has been amended to be longer than 25 characters.";
                }

                obsRow.createCell(headerList.indexOf("observation unit description")).setCellValue(obs_description);

                String obs_title = getValueFrom(connection, obsUnit, "http://fairbydesign.nl/ontology/title");
                obsRow.createCell(headerList.indexOf("observation unit name")).setCellValue(obs_title);

                // Set study accession from field
                obsRow.createCell(headerList.indexOf("study identifier")).setCellValue(study_accession);
            }
        }
    }

    private void createAssay(ArrayList<String> logMessage, RepositoryConnection connection) {
        // Be more specific with Assay type
        ArrayList<Sheet> assaySheets = createAssaySheets(connection);
        // Obtain all samples from the repository using a sparql query
        TupleQuery query = connection.prepareTupleQuery("""
                PREFIX fair: <http://fairbydesign.nl/ontology/>
                select * where {
                    ?run a fair:Run
                }
                """);
        // Execute query
        try (TupleQueryResult result = query.evaluate()) {
            for (BindingSet bindings : result) {
                // Obtain the assay
                String assay = bindings.getBinding("run").getValue().stringValue();

                // Get the correct sheet and obtain experiment information
                Sheet assaySheet = getAssaySheet(connection, assaySheets, assay);
                // Create a new row
                Row assayRow = assaySheet.createRow(assaySheet.getLastRowNum() + 1);
                //
                String assay_accession = getValueFrom(connection, assay, "http://fairbydesign.nl/ontology/accession");

                assayRow.createCell(headerList.indexOf("assay identifier")).setCellValue(assay_accession);

                // Connect to the sample
                // From sample accession get the experiment identifier
                String sample_accession = getObjectFromOtherObject(connection, assay_accession, "http://fairbydesign.nl/ontology/run_accession", "http://fairbydesign.nl/ontology/sample_accession");
                assayRow.createCell(headerList.indexOf("sample identifier")).setCellValue(sample_accession);

                // Sheet specific metadata
                if (assaySheet.getSheetName().equalsIgnoreCase("Assay - Nanopore")) {
                    assayRow.createCell(headerList.indexOf("sequencing kit")).setCellValue("unknown");
                    assayRow.createCell(headerList.indexOf("flowcell")).setCellValue("unknown");
                }

                // Depending on if it is PAIRED or not... generat fastq files
                // Check fastq_ftp
                String fastq_ftp = getObjectFromOtherObject(connection, assay_accession, "http://fairbydesign.nl/ontology/run_accession", "http://fairbydesign.nl/ontology/fastq_ftp");
                String[] fastq_ftps = {};
                String[] fastq_md5s = {};
                String[] fastq_bytes = {};
                // If string is empty set to null
                if (fastq_ftp == null || fastq_ftp.isEmpty()) {
                    logger.error("No fastq file location found for assay " + assay_accession);
                } else {
                    fastq_ftps = fastq_ftp.split(";");
                    fastq_md5s = checksumMap.get(assay_accession).get("fastq_md5").split(";");
                    fastq_bytes = checksumMap.get(assay_accession).get("fastq_bytes").split(";");
                    // Check if the order of fastq_ftp is the same as in the lookup file
                    if (!fastq_ftps[0].equalsIgnoreCase(checksumMap.get(assay_accession).get("fastq_ftp").split(";")[0])) {
                        logging(logMessage, "The order of the fastq files is not the same as in the lookup file. Inverting the array");
                        // Invert the array
                        fastq_md5s = checksumMap.get(assay_accession).get("fastq_md5").split(";");
                        ArrayUtils.reverse(fastq_md5s);
                    }
                    if (headerList.contains("demultiplexed forward file")) {
                        // Make sure to create primer columns
                        assayRow.createCell(headerList.indexOf("forward primer")).setCellValue("AAA");
                        assayRow.createCell(headerList.indexOf("reverse primer")).setCellValue("AAA");
                        assayRow.createCell(headerList.indexOf("primer names")).setCellValue("No name provided");
                        assayRow.createCell(headerList.indexOf("target subfragment")).setCellValue("VX-VY");
                        if (fastq_ftps.length == 1) {
                            assayRow.createCell(headerList.indexOf("demultiplexed forward file")).setCellValue(fastq_ftps[0]);
                            logger.warn(fastq_ftps.length + " fastq file location(s) found for assay " + assay_accession + " was expecting 2.");
                        } else if (fastq_ftps.length == 2) {
                            assayRow.createCell(headerList.indexOf("demultiplexed forward file")).setCellValue(fastq_ftps[0]);
                            assayRow.createCell(headerList.indexOf("demultiplexed reverse file")).setCellValue(fastq_ftps[1]);
                            // Set checksum
                            createCell(assayRow, "demultiplexed forward file md5", fastq_md5s[0]);
                            createCell(assayRow, "demultiplexed reverse file md5", fastq_md5s[1]);
                            // Set bytes
                            createCell(assayRow, "demultiplexed forward file bytes", fastq_bytes[0]);
                            createCell(assayRow, "demultiplexed reverse file bytes", fastq_bytes[1]);
                        } else if (fastq_ftps.length == 3) {
                            // Search for _1.fastq.gz and _2.fastq.gz
                            String[] forward = Arrays.stream(fastq_ftps).filter(s -> s.contains("_1.fastq.gz")).toArray(String[]::new);
                            String[] reverse = Arrays.stream(fastq_ftps).filter(s -> s.contains("_2.fastq.gz")).toArray(String[]::new);
                            if (forward.length == 1 && reverse.length == 1) {
                                logger.warn("Verify following code...");
                                createCell(assayRow, "demultiplexed forward file", forward[0]);
                                createCell(assayRow, "demultiplexed reverse file", reverse[0]);
                                // Set checksum
                                createCell(assayRow, "demultiplexed forward file md5", fastq_md5s[0]);
                                createCell(assayRow, "demultiplexed reverse file md5", fastq_md5s[1]);
                                // Set bytes
                                createCell(assayRow, "demultiplexed forward file bytes", fastq_bytes[0]);
                                createCell(assayRow, "demultiplexed reverse file bytes", fastq_bytes[1]);
                            } else {
                                logger.warn("Only " + fastq_ftps.length + " fastq file location(s) found for assay " + assay_accession + " was expecting 2.");
                            }
                        } else {
                            logger.warn(fastq_ftps.length + " fastq file location(s) found for assay " + assay_accession + " was expecting 2.");
                        }
                    } else if (headerList.contains("forward file")) {
                        if (fastq_ftps.length == 2) {
                            // Set file
                            createCell(assayRow, "forward file", fastq_ftps[0]);
                            createCell(assayRow, "reverse file", fastq_ftps[1]);
                            // Set checksum
                            createCell(assayRow, "forward file md5", fastq_md5s[0]);
                            createCell(assayRow, "reverse file md5", fastq_md5s[1]);
                            // Set bytes
                            createCell(assayRow, "forward file bytes", fastq_bytes[0]);
                            createCell(assayRow, "reverse file bytes", fastq_bytes[1]);
                        } else if (fastq_ftps.length == 1) {
                            logger.warn("Only " + fastq_ftps.length + " fastq file location(s) found for assay " + assay_accession + " was expecting 2.");
                            createCell(assayRow, "forward file", fastq_ftps[0]);
                            createCell(assayRow, "forward file md5", fastq_md5s[0]);
                            createCell(assayRow, "forward file bytes", fastq_bytes[0]);
                        } else if (fastq_ftps.length == 3) {
                            // Search for _1.fastq.gz and _2.fastq.gz
                            String[] forward = Arrays.stream(fastq_ftps).filter(s -> s.contains("_1.fastq.gz")).toArray(String[]::new);
                            String[] reverse = Arrays.stream(fastq_ftps).filter(s -> s.contains("_2.fastq.gz")).toArray(String[]::new);
                            if (forward.length == 1 && reverse.length == 1) {
                                // Set file
                                createCell(assayRow, "forward file", fastq_ftps[0]);
                                createCell(assayRow, "reverse file", fastq_ftps[1]);
                                // Set checksum
                                createCell(assayRow, "forward file md5", fastq_md5s[0]);
                                createCell(assayRow, "reverse file md5", fastq_md5s[1]);
                                // Set bytes
                                createCell(assayRow, "forward file bytes", fastq_bytes[0]);
                                createCell(assayRow, "reverse file bytes", fastq_bytes[1]);
                            } else {
                                logger.warn("Only " + fastq_ftps.length + " fastq file location(s) found for assay " + assay_accession + " was expecting 2.");
                            }
                        } else {
                            logger.warn("Only " + fastq_ftps.length + " fastq file location(s) found for assay " + assay_accession + " was expecting 2.");
                        }
                    } else if (headerList.contains("file")) {
                        if (fastq_ftps.length == 1) {
                            createCell(assayRow, "file", fastq_ftps[0]);
                            createCell(assayRow, "file md5", fastq_md5s[0]);
                            createCell(assayRow, "file bytes", fastq_bytes[0]);
                        } else {
                            logger.warn(fastq_ftps.length + " fastq file locations found for assay " + assay_accession + " was expecting 2.");
                        }
                    } else {
                        // Check if this results into a file string
                        if (fastq_ftps.length == 2) {
                            createCell(assayRow, "forward file", fastq_ftps[0]);
                            createCell(assayRow, "reverse file", fastq_ftps[1]);
                            // Set checksum
                            createCell(assayRow, "forward file md5", fastq_md5s[0]);
                            createCell(assayRow, "reverse file md5", fastq_md5s[1]);
                            // Set bytes
                            createCell(assayRow, "forward file bytes", fastq_bytes[0]);
                            createCell(assayRow, "reverse file bytes", fastq_bytes[1]);
                        } else if (fastq_ftps.length == 1) {
                            createCell(assayRow, "file", fastq_ftps[0]);
                            createCell(assayRow, "file md5", fastq_md5s[0]);
                            createCell(assayRow, "file bytes", fastq_bytes[0]);
                        } else if (fastq_ftps.length == 0) {
                            logger.warn("No fastq file locations found for assay " + assay_accession);
                        } else {
                            logger.warn(fastq_ftps.length + " fastq file locations found for assay " + assay_accession + " was expecting 1 or 2.");
                        }
                    }
                }

                // Assay description
                String assay_description = getValueFrom(connection, assay, "http://fairbydesign.nl/ontology/description");
                // If no description is found, try the title
                if (assay_description == null)
                    assay_description = getValueFrom(connection, assay, "http://fairbydesign.nl/ontology/title");
                // If description is found and length is above 25 all is well
                if (assay_description != null && assay_description.length() >= 25) {
                    // pass
                } else if (assay_description == null) {
                    assay_description = "No description available and has been corrected to be longer than 25 characters.";
                } else {
                    assay_description = assay_description + " this description was not of sufficient length and has been amended to be longer than 25 characters.";
                }
                assayRow.createCell(headerList.indexOf("assay description")).setCellValue(assay_description);

                // Check if there is an alias
                String alias = getValueFrom(connection, assay, "http://fairbydesign.nl/ontology/alias");
                if (alias != null) {
                    updateHeaderRow(assaySheet, "alias");
                    assayRow.createCell(headerList.indexOf("alias")).setCellValue(alias);
                }

                // Add all sample attributes
                String sample_attribute_holder = getValueFrom(connection, assay, "http://fairbydesign.nl/ontology/run_attributes");
                HashSet<String> run_attributes = getValuesFrom(connection, sample_attribute_holder, "http://fairbydesign.nl/ontology/run_attribute");
                for (String run_attribute : run_attributes) {
                    // Obtain the attribute object
                    // String run_attribute = getValueFrom(connection, runAttribute, "http://fairbydesign.nl/ontology/run_attribute");
                    // Obtain the attribute tag
                    String sample_attribute_tag = getValueFrom(connection, run_attribute, "http://fairbydesign.nl/ontology/tag");
                    // Cleaning the tag a little bit
                    sample_attribute_tag = sample_attribute_tag.replace("_", " ").toLowerCase();

                    // Obtain the attribute value
                    String sample_attribute_value = getValueFrom(connection, run_attribute, "http://fairbydesign.nl/ontology/value");
                    if (sample_attribute_value.length() == 0) continue;

                    // Add the attribute to the sample metadata
                    // sampleMetadata.get(sample).put(sample_attribute_tag, sample_attribute_value);
                    updateHeaderRow(assaySheet, sample_attribute_tag);
                    assayRow.createCell(headerList.indexOf(sample_attribute_tag)).setCellValue(sample_attribute_value);

                    // If run attribute tag is ENA-FIRST-PUBLIC then add it to the assay date
                    if (sample_attribute_tag.equalsIgnoreCase("ENA-FIRST-PUBLIC")) {
                        assayRow.createCell(headerList.indexOf("assay date")).setCellValue(sample_attribute_value);
                    }
                }

                // Experiment information
                String experiment = getExperiment(connection, assay);

                // Design information
                String design = getValueFrom(connection, experiment, "http://fairbydesign.nl/ontology/design");

                String library_descriptor = getValueFrom(connection, design, "http://fairbydesign.nl/ontology/library_descriptor");
                String library_selection = getValueFrom(connection, library_descriptor, "http://fairbydesign.nl/ontology/library_selection");
                String library_source = getValueFrom(connection, library_descriptor, "http://fairbydesign.nl/ontology/library_source");
                String library_strategy = getValueFrom(connection, library_descriptor, "http://fairbydesign.nl/ontology/library_strategy");

                createCell(assayRow, "library selection", library_selection);
                createCell(assayRow, "library source", library_source);
                createCell(assayRow, "library strategy", library_strategy);

                String platform = getValueFrom(connection, experiment, "http://fairbydesign.nl/ontology/platform");
                // Ensure platform is there as it is not always?...
                if (!headerList.contains("platform")) {
                    updateHeaderRow(assaySheet, "platform");
                }

                if (platform != null) {
                    assayRow.createCell(headerList.indexOf("platform")).setCellValue(platform.replace("http://fairbydesign.nl/ontology/", "").toUpperCase());
                    // Get instrument model?

                    // Execute a small query to get the instrument model
                    String queryString = "PREFIX fair: <http://fairbydesign.nl/ontology/>\n" +
                            "select distinct ?strategy ?type ?platform_p ?instrument_model where {\n" +
                            "    VALUES ?experiment { <XXX> } \n" +
                            "    ?experiment a fair:Experiment .\n" +
                            "    ?experiment fair:platform ?platform .\n" +
                            "    ?platform ?platform_p ?platform_o .\n" +
                            "    ?platform_o fair:instrument_model ?instrument_model .\n" +
                            "}";
                    queryString = queryString.replace("XXX", experiment);

                    TupleQueryResult tupleQueryResult = connection.prepareTupleQuery(queryString).evaluate();
                    while (tupleQueryResult.hasNext()) {
                        BindingSet bindingSet = tupleQueryResult.next();
//                        String strategy = bindingSet.getValue("strategy").stringValue();
//                        String type = bindingSet.getValue("type").stringValue();
                        String platform_p = bindingSet.getValue("platform_p").stringValue();
                        platform_p = platform_p.replace("http://fairbydesign.nl/ontology/", "").toUpperCase().replace("_", " ");
                        createCell(assayRow, "platform", platform_p);
                        String instrument_model = bindingSet.getValue("instrument_model").stringValue();
                        createCell(assayRow, "instrument model", instrument_model);
                    }
                }

                // Add additional assay attributes
                if (headerList.contains("protocol")) {
                    Cell cell = assayRow.getCell(headerList.indexOf("protocol"));
                    if (cell == null) {
                        assayRow.createCell(headerList.indexOf("protocol")).setCellValue("Protocol is not available");
                    }
                }
                if (headerList.contains("facility")) {
                    Cell cell = assayRow.getCell(headerList.indexOf("facility"));
                    if (cell == null) {
                        assayRow.createCell(headerList.indexOf("facility")).setCellValue("Sequence facility is not available");
                    }
                }
            }
        }
    }

    public void wget(String url, File outputFile) {
        try (InputStream inputStream = new URL(url).openStream();
             BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
             FileOutputStream fileOutputStream = new FileOutputStream(outputFile)) {

            byte[] buffer = new byte[1024];
            int bytesRead;
            while ((bytesRead = bufferedInputStream.read(buffer, 0, buffer.length)) != -1) {
                fileOutputStream.write(buffer, 0, bytesRead);
            }

            // logging("File downloaded successfully to " + outputFile.getName());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void createCell(Row assayRow, String column, String value) {
        if (!headerList.contains(column)) {
            updateHeaderRow(assayRow.getSheet(), column);
        }
        assayRow.createCell(headerList.indexOf(column)).setCellValue(value);
    }

    private String getExperiment(RepositoryConnection connection, String assay) {
        // Execute query
        String query = """
                PREFIX fair: <http://fairbydesign.nl/ontology/>
                select distinct ?experiment ?accession
                where {
                    VALUES ?run { <XXX> }
                    ?run a fair:Run .
                    ?run <http://fairbydesign.nl/ontology/experiment_ref> ?exp_ref .
                    ?exp_ref <http://fairbydesign.nl/ontology/accession> ?accession .
                    ?experiment <http://fairbydesign.nl/ontology/accession> ?accession .
                    ?experiment a <http://fairbydesign.nl/ontology/Experiment> .
                }""".replaceFirst("XXX", assay);
        TupleQueryResult result = connection.prepareTupleQuery(query).evaluate();
        HashMap<String, String> experiments = new HashMap<>();
        while (result.hasNext()) {
            BindingSet bindings = result.next();
            String experiment = bindings.getBinding("experiment").getValue().stringValue();
            String experimentAccession = bindings.getBinding("accession").getValue().stringValue();
            experiments.put(experimentAccession, experiment);
        }
        if (experiments.size() == 1)
            // When they all have the same accession number they also should have the same information
            return experiments.values().iterator().next();
        else {
            for (String experiment : experiments.keySet()) {
                logger.info("Multiple experiments found for assay " + assay + " " + experiment + " " + experiments.get(experiment));
            }
            throw new RuntimeException("Multiple experiments with different experiment identifiers found for assay " + assay);
        }
    }

    private Sheet getAssaySheet(RepositoryConnection connection, ArrayList<Sheet> assaySheets, String assay) {
        // Obtain experiment for this run
        String sparql_query = """
                PREFIX fair: <http://fairbydesign.nl/ontology/>
                select distinct ?strategy ?type ?platform_p
                where {
                    VALUES ?run { <XXX> }
                    ?run a fair:Run .
                    ?run <http://fairbydesign.nl/ontology/experiment_ref> ?exp_ref .
                    ?exp_ref <http://fairbydesign.nl/ontology/accession> ?accession .
                    ?experiment ?link ?accession .
                    ?experiment a <http://fairbydesign.nl/ontology/Experiment> .
                    ?experiment a fair:Experiment .
                    ?experiment fair:platform ?platform .
                    ?platform ?platform_p ?platform_o .
                    ?experiment fair:design ?design .
                    ?design fair:library_descriptor ?libd .
                    ?libd <http://fairbydesign.nl/ontology/library_strategy> ?strategy.
                    ?libd <http://fairbydesign.nl/ontology/library_layout> ?layout .
                    ?layout ?type ?z .
                    FILTER(strstarts(STR(?type), "http://fairbydesign.nl/ontology/"))
                    FILTER(strstarts(STR(?platform_p), "http://fairbydesign.nl/ontology/"))
                }""".replaceFirst("XXX", assay);

        String sheetName = null;
        TupleQuery query = connection.prepareTupleQuery(sparql_query);
        // Execute query
        TupleQueryResult tupleQueryResult = query.evaluate();
        if (!tupleQueryResult.hasNext()) {
            logger.error("No assay information found for assay " + assay);
        }
        for (BindingSet bindings : tupleQueryResult) {
            // Strategy
            String strategy = bindings.getBinding("strategy").getValue().stringValue();
            // String type = bindings.getBinding("type").getValue().stringValue();
            String platform_p = bindings.getBinding("platform_p").getValue().stringValue();
            if (strategy.equalsIgnoreCase("amplicon")) {
                sheetName = "Assay - Amplicon demultiplexed";
                break;
            }
            if (platform_p.toLowerCase().endsWith("illumina")) {
                sheetName = "Assay - Illumina";
                break;
            }
            if (platform_p.toLowerCase().endsWith("oxford_nanopore")) {
                sheetName = "Assay - Nanopore";
                break;
            }
            if (platform_p.toLowerCase().endsWith("pacbio") || platform_p.toLowerCase().endsWith("pacbio_smrt")) {
                sheetName = "Assay - PacBio SMRT";
                break;
            }
            if (platform_p.toLowerCase().endsWith("ion_torrent")) {
                sheetName = "Assay - Ion Torrent";
                break;
            }
            if (platform_p.toLowerCase().endsWith("ls454")) {
                sheetName = "Assay - LS454";
                break;
            }
            // If we get here we have an unknown platform
            sheetName = "Assay - " + platform_p.replace("http://fairbydesign.nl/ontology/","").toUpperCase();
            logger.error("Unknown platform: " + platform_p);
        }

        // Check if sheet exists
        for (Sheet sheet : assaySheets) {
            if (sheet.getSheetName().equalsIgnoreCase(sheetName)) {
                // Get header row
                Row headerRow = sheet.getRow(0);
                // Reset & Get header list todo hashmap lookup?
                headerList = new ArrayList<>();
                for (Cell cell : headerRow) {
                    headerList.add(cell.getStringCellValue());
                }
                return sheet;
            }
        }
        throw new RuntimeException("Sheet not found: " + sheetName);
//        // Create sheet if it does not exist
//        XSSFSheet sheet = workbook.createSheet(sheetName);
//        // Create header row
//        Row headerRow = sheet.createRow(0);
//        // Create header list
//        headerList = getObligatoryColumns(sheet);
//        // Create header cells
//
//        assaySheets.add(sheet);
//        logger.error("Sheet not found: " + sheetName);
//        return sheet;
    }

    private ArrayList<Sheet> createAssaySheets(RepositoryConnection connection) {
        // List of sheets depending on the repository
        assaySheets = new ArrayList<>();
        HashSet<String> sheetNames = new HashSet<>();

        // Run a generic query to obtain possible sheets
        TupleQuery query = connection.prepareTupleQuery("""
                PREFIX fair: <http://fairbydesign.nl/ontology/>
                select distinct ?strategy ?type ?platform_p ?instrument_model where {
                    ?experiment a fair:Experiment .
                    ?experiment fair:platform ?platform .
                    ?platform ?platform_p ?platform_o .
                    ?experiment fair:design ?design .
                    ?design fair:library_descriptor ?libd .
                    ?libd <http://fairbydesign.nl/ontology/library_strategy> ?strategy.
                    ?libd <http://fairbydesign.nl/ontology/library_layout> ?layout .
                    ?layout ?type ?z .
                    FILTER(strstarts(STR(?type), "http://fairbydesign.nl/ontology/"))
                    FILTER(strstarts(STR(?platform_p), "http://fairbydesign.nl/ontology/"))
                }""");

        // Execute query
        for (BindingSet bindings : query.evaluate()) {
            // Strategy
            String strategy = bindings.getBinding("strategy").getValue().stringValue();
            // String type = bindings.getBinding("type").getValue().stringValue();
            String platform_p = bindings.getBinding("platform_p").getValue().stringValue();
            if (strategy.equalsIgnoreCase("amplicon")) {
                String sheetName = "Assay - Amplicon demultiplexed";
                if (sheetNames.contains(sheetName))
                    continue;
                assaySheets.add(createSheet(sheetName));
                sheetNames.add(sheetName);
            } else if (platform_p.toLowerCase().endsWith("illumina")) {
                String sheetName = "Assay - Illumina";
                if (sheetNames.contains(sheetName))
                    continue;
                assaySheets.add(createSheet(sheetName));
                sheetNames.add(sheetName);
            } else if (platform_p.toLowerCase().endsWith("oxford_nanopore")) {
                String sheetName = "Assay - Nanopore";
                if (sheetNames.contains(sheetName))
                    continue;
                assaySheets.add(createSheet(sheetName));
                sheetNames.add(sheetName);
            } else if (platform_p.toLowerCase().endsWith("pacbio") || platform_p.toLowerCase().endsWith("pacbio_smrt")) {
                String sheetName = "Assay - Pacbio SMRT";
                if (sheetNames.contains(sheetName))
                    continue;
                assaySheets.add(createSheet(sheetName));
                sheetNames.add(sheetName);
            } else if (platform_p.toLowerCase().endsWith("ion_torrent")) {
                String sheetName = "Assay - Ion Torrent";
                if (sheetNames.contains(sheetName))
                    continue;
                assaySheets.add(createSheet(sheetName));
                sheetNames.add(sheetName);
            } else if (platform_p.toLowerCase().endsWith("ls454")) {
                String sheetName = "Assay - LS454";
                if (sheetNames.contains(sheetName))
                    continue;
                assaySheets.add(createSheet(sheetName));
                sheetNames.add(sheetName);
            } else {
                String sheetName = "Assay - " + platform_p.replace("http://fairbydesign.nl/ontology/","").toUpperCase();
                if (sheetNames.contains(sheetName))
                    continue;
                assaySheets.add(createSheet(sheetName));
                sheetNames.add(sheetName);
            }
        }

        // When all else fails?...
        // assaySheets.add(createSheet("Assay"));
        logger.info("Created "+assaySheets.size()+" assay sheet(s)");
        return assaySheets;
    }

    private void createSamples(RepositoryConnection connection) {
        // HashMap to store the sample metadata in
        HashMap<String, HashMap<String, String>> sampleMetadata = new HashMap<>();

        Sheet sampleSheet = createSheet("Sample");


        // TODO...
        // headerList.add("Observation unit identifier");

        int rowIndex = sampleSheet.getLastRowNum() + 1;

        // Obtain all samples from the repository using a sparql query
        TupleQuery query = connection.prepareTupleQuery("""
                PREFIX fair: <http://fairbydesign.nl/ontology/>
                select distinct ?sample where {
                	?sample a <http://fairbydesign.nl/ontology/Sample> .
                }""");
        // Execute query
        try (TupleQueryResult result = query.evaluate()) {
            for (BindingSet bindings : result) {
                Row sampleRow = sampleSheet.createRow(rowIndex);
                rowIndex++;

                // Obtain the sample
                String sample = bindings.getBinding("sample").getValue().stringValue();
                sampleMetadata.put(sample, new HashMap<>());
                // For each sample obtain various information like "sample identifier"	"sample description"	"sample name"	"ncbi taxonomy id"	"scientific name"
                String sample_accession = getValueFrom(connection, sample, "http://fairbydesign.nl/ontology/accession");
                sampleRow.createCell(headerList.indexOf("sample identifier")).setCellValue(sample_accession);

                // From sample accession create an O_ accession as Experiment is part of the Assay
                String obs_accession = "O_" + sample_accession; // getObjectFromOtherObject(connection, sample_accession, "http://fairbydesign.nl/ontology/sample_accession", "http://fairbydesign.nl/ontology/experiment_accession");
                sampleRow.createCell(headerList.indexOf("observation unit identifier")).setCellValue(obs_accession);

                String sample_description = getValueFrom(connection, sample, "http://fairbydesign.nl/ontology/description");
                if (sample_description != null && sample_description.length() >= 25) {
                    // pass
                } else if (sample_description == null) {
                    sample_description = "No description available and has been corrected to be longer than 25 characters.";
                } else {
                    sample_description = sample_description + " this description was not of sufficient length and has been amended to be longer than 25 characters.";
                }
                sampleRow.createCell(headerList.indexOf("sample description")).setCellValue(sample_description);

                String sample_title = getValueFrom(connection, sample, "http://fairbydesign.nl/ontology/title");
                if (sample_title == null) {
                    sampleRow.createCell(headerList.indexOf("sample name")).setCellValue("No sample title was provided");
                } else if (sample_title.length() < 10) {
                    sampleRow.createCell(headerList.indexOf("sample name")).setCellValue(sample_title + " this title was not of sufficient length and has been amended to be longer than 10 characters.");
                } else {
                    sampleRow.createCell(headerList.indexOf("sample name")).setCellValue(sample_title);
                }

                // Obtain taxonomy information
                String taxonomy = getValueFrom(connection, sample, "http://fairbydesign.nl/ontology/sample_name");
                String scientific_name  = getValueFrom(connection, taxonomy, "http://fairbydesign.nl/ontology/scientific_name");
                sampleRow.createCell(headerList.indexOf("scientific name")).setCellValue(scientific_name);

                String taxon_id  = getValueFrom(connection, taxonomy, "http://fairbydesign.nl/ontology/taxon_id");
                sampleRow.createCell(headerList.indexOf("ncbi taxonomy id")).setCellValue(taxon_id);
                // sampleMetadata.get(sample).put("ncbi taxonomy id", taxon_id);
                // sampleMetadata.get(sample).put("scientific name", scientific_name);

                // Check if there is an alias
                String alias = getValueFrom(connection, sample, "http://fairbydesign.nl/ontology/alias");
                if (alias != null) {
                    updateHeaderRow(sampleSheet, "alias");
                    sampleRow.createCell(headerList.indexOf("alias")).setCellValue(alias);
                    // sampleMetadata.get(sample).put("alias", alias);
                }
                // Add all sample attributes
                String sample_attribute_holder = getValueFrom(connection, sample, "http://fairbydesign.nl/ontology/sample_attributes");
                HashSet<String> sample_attributes = getValuesFrom(connection, sample_attribute_holder, "http://fairbydesign.nl/ontology/sample_attribute");
                for (String sampleAttribute : sample_attributes) {
                    // Obtain the attribute object
                    // String sample_attribute = getValueFrom(connection, sampleAttribute, "http://fairbydesign.nl/ontology/sample_attribute");
                    // Obtain the attribute tag
                    String sample_attribute_tag = getValueFrom(connection, sampleAttribute, "http://fairbydesign.nl/ontology/tag");
                    // Obtain the attribute value
                    String sample_attribute_value = getValueFrom(connection, sampleAttribute, "http://fairbydesign.nl/ontology/value");
                    // Add the attribute to the sample metadata
                    updateHeaderRow(sampleSheet, sample_attribute_tag);
                    sampleRow.createCell(headerList.indexOf(sample_attribute_tag)).setCellValue(sample_attribute_value);
                }

                // Finalize the sample metadata
                // Default values for some attributes
                if (headerList.contains("biosafety level")) {
                    Cell cell = sampleRow.getCell(headerList.indexOf("biosafety level"));
                    if (cell == null) {
                        sampleRow.createCell(headerList.indexOf("biosafety level")).setCellValue("unknown");
                    }
                }
            }
        }
    }

    private String getObjectFromOtherObject(RepositoryConnection connection, String accession, String url1, String url2) {
        String query = "select distinct ?accession where { ?subject <" + url1 + "> \"" + accession + "\" . ?subject <" + url2 + "> ?accession . " + "}";
        TupleQuery tupleQuery = connection.prepareTupleQuery(query);
        HashSet<String> values = new HashSet<>();
        try (TupleQueryResult result = tupleQuery.evaluate()) {
            for (BindingSet bindings : result) {
                String value = bindings.getBinding("accession").getValue().stringValue();
                values.add(value);
            }
        }
        if (values.size() == 1) {
            return values.iterator().next();
        } else {
            logger.error("There are " + values.size() + " values for accession " + accession + " and url " + url1);
            for (String value : values) {
                logger.error("Value: " + value);
            }
        }
        return null;
    }


    private HashSet<String> getValuesFrom(RepositoryConnection connection, String subject, String predicate) {
        HashSet<String> values = new HashSet<>();
        connection.getStatements(connection.getValueFactory().createIRI(subject), connection.getValueFactory().createIRI(predicate), null).forEach(statement -> {
            values.add(statement.getObject().stringValue());
        });
        return values;
    }

    private String getValueFrom(RepositoryConnection connection, String subject, String predicate) {
        final String[] value = {null};
        connection.getStatements(connection.getValueFactory().createIRI(subject), connection.getValueFactory().createIRI(predicate), null).forEach(statement -> {
            value[0] = statement.getObject().stringValue();
        });
        return value[0];
    }

    public void createExcelFile(ArrayList<String> logMessage, String excelFileName) {
        if (excelFileName == null) {
            logging(logMessage, "No excel file name provided");
            return;
        }

        // Write Excel file
        try {
            FileOutputStream fileOut = new FileOutputStream(excelFileName);
            workbook.write(fileOut);
            workbook.close();
            fileOut.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void createStudy(ArrayList<String> logMessage, RepositoryConnection connection) {
        // Core study headers
        Sheet studySheet = createSheet("Study");

        // Obtain all samples from the repository using a sparql query
        TupleQuery query = connection.prepareTupleQuery("""
                PREFIX fair: <http://fairbydesign.nl/ontology/>
                select distinct ?project where {
                	?project a <http://fairbydesign.nl/ontology/Project> .
                }""");

        // Execute query
        try (TupleQueryResult result = query.evaluate()) {
            for (BindingSet bindings : result) {
                Row studyRow = studySheet.createRow(studySheet.getLastRowNum() + 1);
                // Obtain the sample
                String project = bindings.getBinding("project").getValue().stringValue();
                // For each study/project create the basic information
                study_accession = getValueFrom(connection, project, "http://fairbydesign.nl/ontology/accession");

                // Get checksum and file information
                logging(logMessage, "Getting checksum and file information for " + study_accession);
                String url = "https://www.ebi.ac.uk/ena/portal/api/filereport?accession=" + study_accession + "&result=read_run&fields=run_accession,fastq_ftp,fastq_md5,fastq_bytes";
                // Download checksum file
                File checksumFile = new File(Application.commandOptions.storage + "/bioprojects/ebi/" + study_accession + "/" + study_accession + "_checksum.tsv");
                if (!checksumFile.exists())
                    wget(url, checksumFile);
                checksumMap = readTsvFile(checksumFile);
                // study identifier
                studyRow.createCell(headerList.indexOf("study identifier")).setCellValue(study_accession);
                // study title / study description
                String study_description = getValueFrom(connection, project, "http://fairbydesign.nl/ontology/description");
                if (study_description.length() > 50)
                    studyRow.createCell(headerList.indexOf("study description")).setCellValue(study_description);
                else
                    studyRow.createCell(headerList.indexOf("study description")).setCellValue(study_description + " (description is too short) so please fill in additional information by replacing this placeholder to ensure it passess the automatic validation.");

                String study_title = getValueFrom(connection, project, "http://fairbydesign.nl/ontology/title");
                studyRow.createCell(headerList.indexOf("study title")).setCellValue(study_title);
                // Add investigation template
                studyRow.createCell(headerList.indexOf("investigation identifier")).setCellValue("INVESTIGATION");
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void createInvestigation() {
        // Core study headers
        Sheet investigationSheet = createSheet("Investigation");

        // Execute query
        Row invRow = investigationSheet.createRow(investigationSheet.getLastRowNum() + 1);

        // Template for investigation
        invRow.createCell(headerList.indexOf("investigation identifier")).setCellValue("INVESTIGATION");
        invRow.createCell(headerList.indexOf("investigation description")).setCellValue("A description of the investigation and its goals and objectives");
        invRow.createCell(headerList.indexOf("investigation title")).setCellValue("Investigation title");
        invRow.createCell(headerList.indexOf("firstname")).setCellValue("Firstname");
        invRow.createCell(headerList.indexOf("lastname")).setCellValue("Lastname");
        invRow.createCell(headerList.indexOf("email address")).setCellValue("first.last@email.com");
        invRow.createCell(headerList.indexOf("organization")).setCellValue("Organization");
        invRow.createCell(headerList.indexOf("department")).setCellValue("Department");
//        invRow.createCell(headerList.indexOf("project identifier")).setCellValue("PROJECT");
    }

    /**
     * To create the header identical in all sheets
     */
    public void makeHeaderCellStyle() {
        headerStyle = workbook.createCellStyle();
        headerStyle.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
        headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        headerStyle.setAlignment(HorizontalAlignment.CENTER);
        headerStyle.setBorderBottom(BorderStyle.DOUBLE);

        XSSFFont font = workbook.createFont();
        font.setFontName("Arial");
        font.setFontHeightInPoints((short) 12);
        font.setBold(true);
        headerStyle.setFont(font);
    }

    /**
     * Header for obligatory headers
     */
    public void makeObligatoryHeaderCellStyle() {
        obligatoryStyle = workbook.createCellStyle();
        obligatoryStyle.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
        obligatoryStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        obligatoryStyle.setAlignment(HorizontalAlignment.CENTER);
        obligatoryStyle.setBorderBottom(BorderStyle.DOUBLE);

        XSSFFont font = workbook.createFont();
        font.setFontName("Arial");
        font.setFontHeightInPoints((short) 12);
        font.setBold(true);
        font.setColor(IndexedColors.RED.getIndex());
        obligatoryStyle.setFont(font);
    }
//    private void createProject() {
//        // Core study headers
//        Sheet projectSheet = createSheet("Project");
//
//        // Execute query
//        Row projectRow = projectSheet.createRow(projectSheet.getLastRowNum() + 1);
//
//        // Template for investigation
//        projectRow.createCell(headerList.indexOf("project identifier")).setCellValue("PROJECT");
//        projectRow.createCell(headerList.indexOf("project description")).setCellValue("A description of the overall project and its goals and objectives");
//        projectRow.createCell(headerList.indexOf("project title")).setCellValue("Project title");
//    }



    Sheet createSheet(String sheetName) {
        // Create sheet if it does not exist (important for assay sheets)
        if (workbook.getSheetIndex(sheetName) != -1) {
            logger.debug("Sheet already exists obtaining headers from sheet");
            XSSFSheet sheet = workbook.getSheet(sheetName);
            // Reset headerList
            headerList.clear();
            // Obtain the headers from the sheet
            sheet.getRow(0).cellIterator().forEachRemaining(cell -> headerList.add(cell.getStringCellValue()));
            logger.debug(StringUtils.join(headerList, ","));
            return sheet;
        }
        Sheet sheet = workbook.createSheet(sheetName);
        sheet.setDefaultColumnWidth(defaultCellWidth);

        headerList = getObligatoryColumns(sheet);
        updateHeaderRow(sheet);
        return sheet;
    }

    ArrayList<String> updateHeaderRow(Sheet assaySheet, String variable) {
        if (!headerList.contains(variable)) {
            headerList.add(variable);
            updateHeaderRow(assaySheet);
        }
        return headerList;
    }

    void updateHeaderRow(Sheet sheet) {
        // Create header row?
        Row sheetRow = sheet.createRow(0);

        // Creating the headers
        HashSet<String> finishedHeaderElements = new HashSet<>();
        int index = 0;
        for (int i = 0; i < headerList.size(); i++) {
            if (finishedHeaderElements.contains(headerList.get(i))) continue;
            sheetRow.createCell(index).setCellValue(headerList.get(i));
            finishedHeaderElements.add(headerList.get(i));
            index++;
        }

        // Header style
        sheetRow.cellIterator().forEachRemaining(cell -> cell.setCellStyle(headerStyle));
    }

    static ArrayList<String> getObligatoryColumns(Sheet sheet) {
        // Default is to include also the existing columns unless strict is used
        return getObligatoryColumns(sheet, false);
    }

    static ArrayList<String> getObligatoryColumns(Sheet sheet, boolean strict) {
        // Use the metadata object for the right obligatory headers?
        ArrayList<String> headerList = new ArrayList<>();
        HashMap<String, ArrayList<Metadata>> metadataSet = Application.getMetadataSet();
        for (String key : metadataSet.keySet()) {
            if (sheet.getSheetName().startsWith(key)) {
                ArrayList<Metadata> metadataArrayList = Application.getMetadataSet().get(key);
                for (Metadata metadata : metadataArrayList) {
                    if (metadata.getPackageName().equalsIgnoreCase("default")) {
                        if (metadata.getRequirement().equals(MANDATORY) || metadata.getRequirement().equals(RECOMMENDED)) {
                            headerList.add(metadata.getLabel());
                        }
                    } else if (sheet.getSheetName().toLowerCase().endsWith(metadata.getPackageName().toLowerCase())) {
                        if (metadata.getRequirement().equals(MANDATORY) || metadata.getRequirement().equals(RECOMMENDED)) {
                            headerList.add(metadata.getLabel());
                        }
                    }
                }
            }
        }

        if (!strict) {
            // Expansion of headerList with already existing columns
            if (sheet.getLastRowNum() != -1)
                sheet.getRow(0).cellIterator().forEachRemaining(cell -> headerList.add(cell.getStringCellValue()));
        }
        return headerList;
    }
}
