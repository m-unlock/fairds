package nl.fairbydesign.backend.bioprojects.ebi.objects.esearch;

public class Root{
    public String analysis_accession;
    public String sample_accession;
    public String secondary_sample_accession;
    public String study_accession;
    public String secondary_study_accession;
    public String sample_alias;
}