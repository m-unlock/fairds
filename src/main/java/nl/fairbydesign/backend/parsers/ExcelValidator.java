package nl.fairbydesign.backend.parsers;

import com.google.common.io.Resources;
import com.vaadin.flow.component.textfield.TextArea;
import nl.fairbydesign.Application;
import nl.fairbydesign.backend.Generic;
import nl.fairbydesign.backend.WebGeneric;
import nl.fairbydesign.backend.credentials.Credentials;
import nl.fairbydesign.backend.data.objects.metadata.geo.Coordinate;
import nl.fairbydesign.backend.data.objects.metadata.ontologies.Ontology;
import nl.fairbydesign.backend.metadata.Metadata;
import nl.fairbydesign.backend.metadata.Term;
import nl.fairbydesign.backend.wrappers.queries.Query;
import nl.wur.ssb.RDFSimpleCon.RDFFormat;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFactory;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.NodeIterator;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.impl.PropertyImpl;
import org.apache.jena.vocabulary.RDF;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.vocabulary.GEO;
import org.eclipse.rdf4j.rio.RDFParser;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.helpers.StatementCollector;
import org.irods.jargon.core.exception.DuplicateDataException;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.pub.CollectionAO;
import org.irods.jargon.core.pub.DataObjectAO;
import org.irods.jargon.core.pub.IRODSFileSystem;
import org.jermontology.ontology.JERMOntology.domain.Assay;
import org.jermontology.ontology.JERMOntology.domain.Data_sample;
import org.jermontology.ontology.JERMOntology.domain.Investigation;
import org.jermontology.ontology.JERMOntology.domain.Sample;
import org.jermontology.ontology.JERMOntology.domain.Study;
import org.jetbrains.annotations.NotNull;
import org.purl.ppeo.PPEO.owl.domain.observation_unit;
import org.rdfhdt.hdt.exceptions.IllegalFormatException;
import org.schema.domain.Organization;
import org.schema.domain.Person;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URL;
import java.text.Normalizer;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static nl.fairbydesign.Application.getMetadataSet;
import static nl.fairbydesign.backend.WebGeneric.getStringValueCell;
import static nl.fairbydesign.backend.irods.Data.uploadIrodsFile;
import static nl.fairbydesign.backend.metadata.Metadata.Requirement.MANDATORY;
import static nl.fairbydesign.backend.metadata.Metadata.Requirement.OPTIONAL;

/**
 * Main class for validating the Excel file and transforming it into RDF
 *
 * This class performs parsing and validation per sheet of the Excel file in a row wise manner. The class is used by
 * instantiating it and then using the validate() method.
 */
public class ExcelValidator {
    //TODO CHECK whether the sheet has only a single: project/study/investigation.

    public static final String OBSERVATION_UNIT_IDENTIFIER = "observation unit identifier";
    public static final String SAMPLE_IDENTIFIER = "sample identifier";
    public static final String ASSAY_IDENTIFIER = "assay identifier";
    public static final String SAME_AS = "same as";
    public static final String STUDY_IDENTIFIER = "study identifier";
    public static final String INVESTIGATION_IDENTIFIER = "investigation identifier";
    public static final String FIRSTNAME = "firstname";
    public static final String LASTNAME = "lastname";
    public static final String EMAIL = "email address";
    public static final String ORCID = "orcid";
    public static final String DEPARTMENT = "department";
    public static final String ORGANIZATION = "organization";

    // RDF triple store
    private Domain domain;
    // logger
    public static final Logger logger = LogManager.getLogger(ExcelValidator.class);
    // RDF prefix
    private String PREFIX;
    // Message to be shown to the user
    private String message = "";
    private final HashMap<File, Domain> ontologies = new HashMap<>();
    private final Ontology ontology = new Ontology();
    private HashMap<String, ArrayList<Metadata>> metadataset;
    private boolean ontology_warning;

    // String variables

    /**
     * @param fileName    of the input excel file
     * @param excelFile   input stream to be processed
     * @param credentials with credentials to upload after validation succeeds when logged in (not obligatory)
     * @param logArea
     * @return the RDF object
     * @throws Exception when validation fails, the message can be expanded upon failure to show a clearer message to
     *                   the user
     */
    public String validate(String fileName, InputStream excelFile, Credentials credentials, TextArea logArea, String rdfFormat) throws Exception {
        this.setMessage("");
        // Opening the workbook stream
        XSSFWorkbook workbook;
        try {
            workbook = new XSSFWorkbook(excelFile);
        } catch (Exception e) {
            logger.error("Not a valid excel file " + e.getMessage());
            return "Is this an excel file?";
        }
        return validate(credentials, workbook, logArea, fileName, rdfFormat);
    }

    public String validate(Credentials credentials, XSSFWorkbook workbook, TextArea logArea, String fileName, String rdfFormat) throws Exception {
        this.setMessage("");
        logger.info("Starting validation of " + fileName);

        metadataset = getMetadataSet();

        if (logArea != null)
            logArea.getUI().ifPresent(ui -> ui.access(() -> logArea.setValue("Obtaining information from reference file")));

        logger.debug("Obtaining information from reference file");

        if (logArea != null)
            logArea.getUI().ifPresent(ui -> ui.access(() -> logArea.setValue("Validation starting")));

        logger.debug("Validation starting");

        // Creation of an RDF memory store
        domain = new Domain("");

        // Add generic prefixes to the RDF file for more compressed TURTLE format
        setPrefixes();

        /**
         * The order of creation Project > Investigation > Study > Observation Unit > Sample > Assay is flipped
         * around due to cardinality restrictions by the API
         * An investigation needs to have 1 or more studies
         * A study needs to have 1 or more observation units
         * etc ...
         */

        PREFIX = "http://fairbydesign.nl";

        // Base prefix added
        domain.getRDFSimpleCon().setNsPrefix("base", PREFIX + "/ontology");

        // Parse the investigation information
        if (logArea != null)
            logArea.getUI().ifPresent(ui -> ui.access(() -> logArea.setValue("Processing investigations")));

        for (Sheet sheet : workbook) {
            if (sheet.getSheetName().toLowerCase().startsWith("investigation")) {
                if (logArea != null)
                    logArea.getUI().ifPresent(ui -> ui.access(() -> logArea.setValue("Processing " + sheet.getSheetName())));
                ArrayList<Metadata> metadataArrayList = getPackage(sheet.getSheetName());
                investigationCreation(sheet, metadataArrayList, logArea);
            }
        }

        // Parse the study information
        if (logArea != null)
            logArea.getUI().ifPresent(ui -> ui.access(() -> logArea.setValue("Processing studies")));

        for (Sheet sheet : workbook) {
            if (sheet.getSheetName().toLowerCase().startsWith("study")) {
                if (logArea != null)
                    logArea.getUI().ifPresent(ui -> ui.access(() -> logArea.setValue("Processing " + sheet.getSheetName())));
                ArrayList<Metadata> metadataArrayList = getPackage(sheet.getSheetName());
                studyCreation(sheet, metadataArrayList, logArea);
                // studyCreation(workbook.getSheet("Study"), getMetadataSet().get("Study"), logArea);
            }
        }

        for (Sheet sheet : workbook) {
            if (sheet.getSheetName().toLowerCase().startsWith("observationunit")) {
                if (logArea != null)
                    logArea.getUI().ifPresent(ui -> ui.access(() -> logArea.setValue("Processing " + sheet.getSheetName())));
                ArrayList<Metadata> metadataArrayList = getPackage(sheet.getSheetName());
                observationUnitCreation(sheet.getSheetName(), workbook.getSheet(sheet.getSheetName()), metadataArrayList, logArea);
            }
        }

        for (Sheet sheet : workbook) {
            if (sheet.getSheetName().toLowerCase().startsWith("sample")) {
                ArrayList<Metadata> metadataArrayList = getPackage(sheet.getSheetName());
                sampleCreation(sheet.getSheetName(), workbook.getSheet(sheet.getSheetName()), metadataArrayList, logArea);
            }
        }

        for (Sheet sheet : workbook) {
            if (sheet.getSheetName().toLowerCase().startsWith("assay")) {
                ArrayList<Metadata> metadataArrayList = getPackage(sheet.getSheetName());
                assayCreation(sheet.getSheetName(), workbook.getSheet(sheet.getSheetName()), metadataArrayList, logArea);
            }
        }

        convertCoordinates();
        makeDeriveColumn();

        if (logArea != null)
            logArea.getUI().ifPresent(ui -> ui.access(() -> logArea.setValue("Validating...")));

        // Validate the RDF file
        validateHelper();

        // Check RDFFormat, and save the file
        String rdfFile = getRDFFile(domain, fileName, rdfFormat);

        // Validate RDF file
        if (logArea != null)
            logArea.getUI().ifPresent(ui -> ui.access(() -> logArea.setValue("Validating RDF file")));

        logging("Validating RDF file: " + rdfFile);
        validateRDF(rdfFile);

        // Setting up folders and files in irods
        if (credentials != null) {
            logger.info("User logged in, setting up irods landingzone");
            irodsSetup(fileName, workbook, credentials);
            domain.closeAndDelete();
            return rdfFile;
        }

        logging("Validation successful, user not logged in.");
        logging("Result file not uploaded to the data storage facility");
        domain.closeAndDelete();
        return rdfFile;
    }

    @NotNull
    private static String getRDFFile(Domain domain, String fileName, String rdfFormat) throws Exception {
        // Remove all non alphanumeric characters from the file name
        if (rdfFormat == null) {
            logger.error("RDF format not set, defaulting to TURTLE");
            rdfFormat = "turtle";
        } else {
            rdfFormat = rdfFormat.replaceAll("[^a-zA-Z0-9]", "").toLowerCase();
            logger.info("RDF format set to " + rdfFormat);
        }
        String extension;
        String rdfFile;
        if (rdfFormat.equalsIgnoreCase("turtle")) {
            extension = ".ttl";
            rdfFile = Application.commandOptions.storage + "/validation/" + fileName.replace(".xlsx", "") + extension;
            new File(rdfFile).getParentFile().mkdirs();
            domain.save(rdfFile, RDFFormat.TURTLE);
        } else if (rdfFormat.equalsIgnoreCase("rdf") || rdfFormat.equalsIgnoreCase("xml") || rdfFormat.equalsIgnoreCase("rdfxmlabbrev")) {
            extension = ".rdf";
            rdfFile = Application.commandOptions.storage + "/validation/" + fileName.replace(".xlsx", "") + extension;
            new File(rdfFile).getParentFile().mkdirs();
            domain.save(rdfFile, RDFFormat.RDF_XML);
        } else if (rdfFormat.equalsIgnoreCase("ntriple")) {
            extension = ".nt";
            rdfFile = Application.commandOptions.storage + "/validation/" + fileName.replace(".xlsx", "") + extension;
            new File(rdfFile).getParentFile().mkdirs();
            domain.save(rdfFile, RDFFormat.N_TRIPLE);
        } else if (rdfFormat.equalsIgnoreCase("jsonld")) {
            extension = ".jsonld";
            rdfFile = Application.commandOptions.storage + "/validation/" + fileName.replace(".xlsx", "") + extension;
            new File(rdfFile).getParentFile().mkdirs();
            domain.save(rdfFile, RDFFormat.JSONLD);
        } else if (rdfFormat.equalsIgnoreCase("n3")) {
            extension = ".n3";
            rdfFile = Application.commandOptions.storage + "/validation/" + fileName.replace(".xlsx", "") + extension;
            new File(rdfFile).getParentFile().mkdirs();
            domain.save(rdfFile, RDFFormat.N3);
        } else {
            logger.error("Unknown RDF format ("+rdfFormat+"), defaulting to TURTLE");
            extension = ".ttl";
            rdfFile = Application.commandOptions.storage + "/validation/" + fileName.replace(".xlsx", "") + extension;
            new File(rdfFile).getParentFile().mkdirs();
            domain.save(rdfFile, RDFFormat.TURTLE);
        }
        return rdfFile;
    }


    /**
     * Check the coordinates and convert them to geoSPARQL format.
     * <p>
     *  This method adds the geometry object and sf:point to all objects in the database that have both a
     * latitude and longitude. When an invalid coordinate is found, the validation is halted, and an error message is
     * printed.
     * <p>
     * Jasper Koehorst helped with polishing the output.
     * @throws Exception when the query to the ttl version of the Excel file fails.
     */
    private void convertCoordinates() throws Exception {
        // Find all things that have lat long attached to them.
        //TODO extract some methods.
        Query whatThingsHaveGeoInformation = new Query(this.domain);
        URL f = Resources.getResource("queries/getGeoLatLong.txt");
        whatThingsHaveGeoInformation.setQueryData(f);
        ResultSet result = whatThingsHaveGeoInformation.runSelect();
        result = ResultSetFactory.copyResults(result);

        if (result.hasNext()) {
            logging("Found Lat long values to convert to the GeoSPARQL format.");
            Model model = domain.getRDFSimpleCon().getModel();
            model.setNsPrefix("geo", "http://www.opengis.net/ont/geosparql#");
            model.setNsPrefix("sf", "http://www.opengis.net/ont/sf#");
            Map<String, ArrayList<Double>> objectsToProcess = new HashMap<>();

            // We cannot modify the things we loop over, so we copy it...
            while (result.hasNext()) {
                QuerySolution currentResult = result.next();
                String subject = String.valueOf(currentResult.get("sub"));
                try {
                    Double lat = (Double.valueOf(currentResult.getLiteral("lat").getValue().toString()));
                    Double lon = (Double.valueOf(currentResult.getLiteral("long").getValue().toString()));
                    ArrayList<Double> latlongList = new ArrayList<>();
                    latlongList.add(lat);
                    latlongList.add(lon);
                    objectsToProcess.put(subject, latlongList);
                } catch (NumberFormatException e) {
                    if (!e.getMessage().contains("not provided") && !e.getMessage().contains("not collected") && !e.getMessage().contains("restricted access")) {
                        logging("Invalid coordinate for " + subject + " " + e);
                    }
                }
            }

            for (Map.Entry<String, ArrayList<Double>> entry : objectsToProcess.entrySet())  {
                String currentObject = entry.getKey();
                ArrayList<Double> latlong = entry.getValue();
                try {
                    double lat = latlong.get(0);
                    double lon = latlong.get(1);

                    // Current coordinate
                    Coordinate currentCoord = new Coordinate(lon, lat);
                    logging("Found coordinate " + currentCoord + " for " + currentObject);

                    // Here we add the object to the geometry
                    String geomURI = currentObject + "/" + "geometry";
                    Resource geoURI = model.createResource(geomURI);
                    domain.getRDFSimpleCon().add(currentObject, "geo:hasGeometry", geoURI);

                    // Create a property for the GeoSPARQL geometry
                    PropertyImpl asWKT = new PropertyImpl(GEO.AS_WKT.toString());

                    // Create a GeoSPARQL point literal
                    String pointWKT = currentCoord.get_geosparql_point();

                    Literal pointLiteral = domain.getRDFSimpleCon().getModel().createTypedLiteral(pointWKT,
                            "http://www.opengis.net/ont/geosparql#wktLiteral");

                    // Add the class type Point
                    domain.getRDFSimpleCon().getModel().add(geoURI,
                            RDF.type,
                            domain.getRDFSimpleCon().getModel().createResource("http://www.opengis.net/ont/sf#Point"));

                    // Add the point to the model
                    domain.getRDFSimpleCon().getModel().add(geoURI, asWKT, pointLiteral);

                } catch (Exception e) {
                    logging("There was an error adding the coordinate for "
                            + currentObject + "\n" + e);

                    throw new IllegalFormatException("Invalid coordinate: "
                            + e
                            + " for object "
                            + "\n"
                            + currentObject);
                }
            }
        }
    }


    /**
     * Check whether the derives_from field is set up correctly and sets the link in the RDF.
     * <p>
     * Samples can derive assays and other samples. For the derivation to be alright, the following conditions must
     * occur. 1) The top level sample, and the assay/sample that is derived must be connected to the same observation
     * unit. 2) They must be connected by a derives_from relation.
     *
     * @throws Exception
     */
    private void makeDeriveColumn() throws Exception {
        //TODO bindDerive should check, if it fails, an error must bubble up. This error should only bubble up after all
        // things have been checked?


        // This query finds out what derives from what, and what observational units its connected to.
        String query =  "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \n"
                + "prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> \n"
                + "prefix owl: <http://www.w3.org/2002/07/owl#> \n"
                + "prefix fair:     <http://fairbydesign.nl/ontology/> \n"
                + "prefix schema:   <http://schema.org/> \n"
                + "prefix jerm:     <http://jermontology.org/ontology/JERMOntology#> \n"
                + "SELECT * WHERE {"
                + "?lower_level_uri fair:derived_from ?top_level_identifier ."
                + "?top_level_uri schema:identifier ?top_level_identifier ."
                + " }";
        Query WhatDerivesFromWhat = new Query(this.domain);
        WhatDerivesFromWhat.setQueryData(query);
        ResultSet result = WhatDerivesFromWhat.runSelect();
        result = ResultSetFactory.copyResults(result);
        // System.out.println("Samples in the dataset");
        // System.out.println("Object - derives from -> Sample");
        while ( result.hasNext() ) {
            QuerySolution qs = result.next();
            String topLevelUri = String.valueOf(qs.get("?top_level_uri"));
            String lowerLevelUri = String.valueOf(qs.get("?lower_level_uri"));
            try {
                bindDeriveFrom(topLevelUri, lowerLevelUri);
            } catch (Exception e) {
                String ErrString = "There are samples where the derivation cannot be matched with the " +
                        "observational unit! These are:\n";
                logging(ErrString
                        + e);
                throw new Exception(ErrString + e);
            }
        }
    }

    /**
     * Binds the derive field for a single pair of objects.
     * @param o1 Uri pointing to top level object.
     * @param o2 Uri pointing to lower level object.
     * @throws Exception If o1 and o2 do not share an observational unit.
     */
    private void bindDeriveFrom(String o1, String o2) throws Exception {
        boolean shareObservationalUnit = doTwoObjectsShareObservationUnit(o1, o2);
        if (shareObservationalUnit) {
            // o2 is lower, o1 is upper.
            logging("Binding objects " + o1 + " and " + o2);
            Model model = this.domain.getRDFSimpleCon().getModel();
            Resource upperUri = model.createResource(o1);
            Resource lowerUri = model.createResource(o2);
            Property derives = model.createProperty("http://fairbydesign.nl/ontology/derives");
            model.add(upperUri, derives, lowerUri);
        } else {
            throw new Exception("Object " + o1
                    + " was used to derive " + o2 + " but they share no observational unit");
        }
    }

    /**
     * Determine whether two objects share a common observational unit
     * @param o1 Object one, the URI as a string.
     * @param o2 Object two, the URI as a string.
     * @return True if the two objects share an observational unit. False otherwise.
     */
    private boolean doTwoObjectsShareObservationUnit(String o1, String o2) {
        String doTwoObjectsShareOU =  "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \n"
                + "prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> \n"
                + "prefix ppeo:     <http://purl.org/ppeo/PPEO.owl#> \n"
                + "prefix owl: <http://www.w3.org/2002/07/owl#> \n"
                + "prefix fair:     <http://fairbydesign.nl/ontology/> \n"
                + "prefix jerm:     <http://jermontology.org/ontology/JERMOntology#> \n"
                + "ASK"
                + "WHERE {"
                + "?observation_unit jerm:hasPart <" + o1 + ">."
                + "?observation_unit jerm:hasPart <" + o2 + ">."
                + "}";
        Query question = new Query(this.domain);
        question.setQueryData(doTwoObjectsShareOU);
        return question.runAsk();
    }

    /**
     * Validate a created RDF file.
     *
     * @param rdfFile A string pointing to an on-disk RDF file.
     * @throws IOException When the file to validate does not exist.
     */
    public static void validateRDF(String rdfFile) {
        // Get the correct parser
        File rdfFileToValidate = new File(rdfFile);
        // Get the extension
        RDFParser parser = Rio.createParser(org.eclipse.rdf4j.rio.RDFFormat.TURTLE);
        if (rdfFileToValidate.getName().endsWith(".ttl")) {
            parser = Rio.createParser(org.eclipse.rdf4j.rio.RDFFormat.TURTLE);
        } else if (rdfFileToValidate.getName().endsWith(".n3")) {
            parser = Rio.createParser(org.eclipse.rdf4j.rio.RDFFormat.N3);
        } else if (rdfFileToValidate.getName().endsWith(".nt")) {
            parser = Rio.createParser(org.eclipse.rdf4j.rio.RDFFormat.NTRIPLES);
        } else if (rdfFileToValidate.getName().endsWith(".jsonld")) {
            parser = Rio.createParser(org.eclipse.rdf4j.rio.RDFFormat.JSONLD);
        } else if (rdfFileToValidate.getName().endsWith(".rdf")) {
            parser = Rio.createParser(org.eclipse.rdf4j.rio.RDFFormat.RDFXML);
        } else {
            logger.warn("Unknown RDF file format, defaulting to TURTLE");
        }

        // Set up a StatementCollector to collect parsed statements
        StatementCollector collector = new StatementCollector();
        parser.setRDFHandler(null);

        // Parse the file
        try {
            FileInputStream fis = new FileInputStream(rdfFile);
            // Parse the file and collect the statements
            parser.parse(fis, "");

            // Get the collected statements as a model
            Collection<Statement> statements = collector.getStatements();

            logger.info("RDF file passed validation");
        } catch (IOException e) {
            // Handle the exception (e.g., log an error, show a message to the user, etc.)
            logger.error("The RDF file to validate cannot be found");
        }
    }


    /**
     * Obtain the most likely package name.
     *
     * @param subjectName Is the main class of experiment. For instance, Investigation or study &c.
     * @param packageName The subclass of the main class. This is what comes after the `-` in the sheet name.
     * @return
     */
    private String getlikelyPackage(String subjectName, String packageName) {
        // If the likely package is in `the list' that name will be returned
        String likelyPackage = null;
        try {
            ArrayList<Metadata> metadataSet = getMetadataSet().get(subjectName);
            for (Metadata metadata : metadataSet) {
                if (metadata.getPackageName().toLowerCase().startsWith(packageName.toLowerCase())) {
                    if (likelyPackage == null) {
                        likelyPackage = metadata.getPackageName();
                    }
                    if (metadata.getPackageName().length() < likelyPackage.length()) {
                        likelyPackage = metadata.getPackageName();
                    }
                }
            }
        } catch (Exception e) {
            throw new RuntimeException("Key subject: " + subjectName +
                    " likely not within set. \n" + "The following " + getMetadataSet().keySet() + " are " +
                    "availible.");
        }

        // When a package is not in `the list' the error will be logged here.
        if (likelyPackage == null) {
            logger.error("No metadata package was identified for: " + packageName + " will be using default instead.");
            // Here, `subjectName` is the name of the Excel sheet, for example: Assay.
            HashSet<String> packageNames = new HashSet<>();
            for (Metadata metadata : getMetadataSet().get(subjectName)) {
                packageNames.add(metadata.getPackageName());
            }
            logger.error("Available packages are: " + StringUtils.join(packageNames, ", "));
            packageName = "default";
        } else {
            packageName = likelyPackage;
        }
        return packageName;
    }


    /**
     * Get the packages that belong to a specific sheet.
     *
     * @param sheetName The name of the sheet of the spreadsheet.
     * @return The list of metadata fields that belong to the package.
     * @throws Exception When the `sheetName` is not within the accepted
     *                   list of packages.
     */
    private ArrayList<Metadata> getPackage(String sheetName) throws Exception {
        String subjectName;
        String packageNameNotChecked;
        if (sheetName.toLowerCase().contains(" - ")) {
            subjectName = sheetName.split(" - ", 2)[0];
            packageNameNotChecked = sheetName.split(" - ", 2)[1];
        } else {
            subjectName = sheetName;
            packageNameNotChecked = "default";
        }

        AtomicBoolean packageNameMatched = new AtomicBoolean(false);
        ArrayList<Metadata> metadataArrayList = new ArrayList<>();

        String packageName = getlikelyPackage(subjectName, packageNameNotChecked);

        for (Metadata entry : getMetadataSet().get(subjectName)) {
            // Add elements from package name
            if (entry.getPackageName().equalsIgnoreCase(packageName)) {
                metadataArrayList.add(entry);
                packageNameMatched.set(true);
            }
            // Add core elements if package name was not core
            if (!packageName.equalsIgnoreCase("default") && entry.getPackageName().equalsIgnoreCase("default")) {
                metadataArrayList.add(entry);
            }
        }

        if (!packageNameMatched.get()) {
            HashSet<String> packages = new HashSet<>();
            for (Metadata entry : getMetadataSet().get(subjectName)) {
                packages.add(entry.getPackageName());
            }
            logging("Sample package name '" + packageName + "' is not one of the accepted package.\n" +
                    "Please check the accepted packages below...\n" +
                    StringUtils.join(packages, "\n"));

            throw new Exception("Package name not found");
        }

        return metadataArrayList;
    }

    private void dataToSampleCoupling() throws Exception {
        Iterable<ResultLine> resultLines = domain.getRDFSimpleCon().runQuery("getSamplesAndData.txt", true);
        for (ResultLine resultLine : resultLines) {
            String sampleIRI = resultLine.getIRI("sample");
            Sample sample = domain.make(Sample.class, sampleIRI);

            // Sample type same as ISA source
            domain.getRDFSimpleCon().add(sample.getClassTypeIri(), "owl:equivalentClass", "http://www.wikidata.org/wiki/Q53617407-Sample");

            String data = resultLine.getIRI("data");
            Data_sample dataSample = domain.make(Data_sample.class, data);
            domain.getRDFSimpleCon().add(dataSample.getClassTypeIri(), "owl:equivalentClass", "http://www.wikidata.org/wiki/Q5227290-DataFile");

            String isa_process = "http://isa-tools.org/process/" + sample.getIdentifier() + "/" + dataSample.getIdentifier();
            domain.getRDFSimpleCon().add(isa_process, "rdf:type", "http://www.wikidata.org/wiki/Q3249551-Process");
            domain.getRDFSimpleCon().add(isa_process, "http://www.wikidata.org/wiki/Property:P2283-INPUT", sample.getResource().getURI());
            domain.getRDFSimpleCon().add(isa_process, "http://www.wikidata.org/wiki/Property:P1056-OUTPUT", dataSample.getResource().getURI());
        }
    }

    /**
     * Setup the folders for the upload in iRODS.
     *
     * @param fileName    Name of the excel file that is being uploaded.
     * @param workbook    The excel file itself.
     * @param credentials The person who is uploaded the excel sheet to FAIRDS.
     * @throws Exception When there are no studies in the worksheet.
     *  This methods makes the file paths for storing studies on iRODS system. The orininal excel file, and
     * the converted ttl file are written to irods.
     */
    private void irodsSetup(String fileName, Workbook workbook, Credentials credentials) throws Exception {
        // Acquire the first investigation identifier used throughout the parsing
        HashSet<String> identifiers = new HashSet<>();
        workbook.sheetIterator().forEachRemaining(sheet -> {
            if (sheet.getSheetName().toLowerCase().startsWith("investigation")) {
                identifiers.addAll(getInvestigationIdentifiers(workbook.getSheet(sheet.getSheetName())));
            }
        });

        // Create raw data folder / <project> / <investigation>
        for (String identifier : identifiers) {
            Investigation investigation = getInvestigation(identifier);

            if (investigation.getAllHasPart().isEmpty()) {
                throw new Exception("No studies found");
            }

            ArrayList<String> investigationIDs = new ArrayList<>();
            ArrayList<String> studyIDs = new ArrayList<>();

            identifier = investigation.getIdentifier().toLowerCase();
            String baseInvestigationPath;
            if (credentials.getZone().equalsIgnoreCase("unlock") && !identifier.startsWith("wur.")) {
                baseInvestigationPath = "/" + credentials.getIrodsAccount().getZone() + "/home/wur." + identifier.substring(0, Math.min(identifier.length(), 16));
            } else {
                baseInvestigationPath = "/" + credentials.getIrodsAccount().getZone() + "/home/" + identifier;
            }

            // Get investigation identifier
            investigationIDs.add(investigation.getIdentifier());
            // Get study identifier
            investigation.getAllHasPart().forEach(study -> studyIDs.add(study.getIdentifier()));
            // String landingZoneInvestigationPath = landingZoneProjectPath + "/INV_" + investigation.getIdentifier();
            logger.debug("Checking if investigation exists: " + baseInvestigationPath);
            // Check if investigation exists
            if (!credentials.getFileFactory().instanceIRODSFile(baseInvestigationPath).exists()) {
                throw new JargonException("Investigation path does not exist: " + baseInvestigationPath);
            }
        }

        logging("\n\nSuccessfully parsed the excel sheet");

        // Only perform the rest when user is logged in?
        logger.debug("USERNAME BOT: " + credentials.getIrodsAccount().getUserName());

        // When successful, upload to a designated location in irods
        // First get the name of the file, which is used to make the directory.
        fileName = fileName.replaceAll(".xlsx$", "");
        File xlsxFile = new File(Application.commandOptions.storage + "/validation/" + fileName + ".xlsx");
        File turtleFile = new File(Application.commandOptions.storage + "/validation/" + fileName + ".ttl");
        File remotexlsxFile = new File(fileName.replaceAll(" +", "_") + ".xlsx");
        File remoteturtleFile = new File(fileName.replaceAll(" +", "_") + ".ttl");

        // Write to temp file
        OutputStream outputStream = new FileOutputStream(xlsxFile.getAbsolutePath());
        workbook.write(outputStream);
        outputStream.close();

        // Upload to all investigations
        // Create a DataObjectAO object to stream the data to
        DataObjectAO dataObjectAO = IRODSFileSystem.instance().getIRODSAccessObjectFactory().getDataObjectAO(credentials.getIrodsAccount());

        // TODO refactor this into a method.
        // It upload the initial excel file to iRODS, and
        for (String identifier : identifiers) {
            Investigation investigation = getInvestigation(identifier);
            identifier = investigation.getIdentifier().toLowerCase();
            File baseInvestigationPath;
            if (credentials.getZone().equalsIgnoreCase("unlock") && !identifier.startsWith("wur.")) {
                baseInvestigationPath = new File("/" + credentials.getIrodsAccount().getZone() + "/home/wur." + identifier.substring(0, Math.min(identifier.length(), 16)));
            } else {
                baseInvestigationPath = new File("/" + credentials.getIrodsAccount().getZone() + "/home/" + identifier);
            }
            logging("\n\nUploading excel file to the data management system");
            // Create metadata folder
            boolean success = credentials.getFileFactory().instanceIRODSFile(baseInvestigationPath + "/metadata").mkdirs();
            if (!success) {
                if (!credentials.getFileFactory().instanceIRODSFile(baseInvestigationPath + "/metadata").exists())
                    throw new JargonException("Unable to create metadata folder");
            }

            // Stream to server to stream to irods
            FileInputStream xlsxFileInputStream = new FileInputStream(xlsxFile);
            String destination = baseInvestigationPath + "/metadata/" + xlsxFile.getName().replaceAll(" +", "_");
            uploadIrodsFile(credentials.getIrodsAccount(), xlsxFile, new File(destination));

//            logger.info("Uploading to " + destination);
//            IRODSFile irodsExcelFile = credentials.getFileFactory().instanceIRODSFile(destination);
//            if (irodsExcelFile.exists())
//                irodsExcelFile.delete();
//            logger.info("Uploading to " + irodsExcelFile);

//            IRODSFileOutputStream xlsxOutputStream = dataObjectAO.getIRODSFileFactory().instanceIRODSFileOutputStream(irodsExcelFile);
//            IOUtils.copy(xlsxFileInputStream, xlsxOutputStream);
//            xlsxFileInputStream.close();
//            xlsxOutputStream.close();

            // Stream to server to stream to irods
//            FileInputStream turtleFileInputStream = new FileInputStream(turtleFile);
            destination = baseInvestigationPath + "/metadata/" + turtleFile.getName().replaceAll(" +", "_");
            uploadIrodsFile(credentials.getIrodsAccount(), turtleFile, new File(destination));
//            IRODSFile irodsTurtleFile = credentials.getFileFactory().instanceIRODSFile(destination);
//            if (irodsTurtleFile.exists() && irodsTurtleFile.canWrite()) {
//                irodsTurtleFile.delete();
//            } else if (irodsTurtleFile.exists()) {
//                logging("Unable to override the file, check permission settings");
//            }
//
//            logger.info("Uploading to " + irodsTurtleFile);
//            IRODSFileOutputStream turtleFileOutputStream = dataObjectAO.getIRODSFileFactory().instanceIRODSFileOutputStream(irodsTurtleFile);
//            IOUtils.copy(turtleFileInputStream, turtleFileOutputStream);
//            turtleFileInputStream.close();
//            turtleFileOutputStream.close();

            // Data.uploadIrodsFile(credentials.getIrodsAccount(), xlsxFile, new File(baseInvestigationPath + "/" + remotexlsxFile.getName()));
            logging("\n\nUploading database file to the data management system");
            // Data.uploadIrodsFile(credentials.getIrodsAccount(), turtleFile, new File(baseInvestigationPath + "/" + remoteturtleFile.getName()));
            logging("\n\nFiles uploaded to: " + baseInvestigationPath + "/");
            logging("\nExcel file: " + baseInvestigationPath + "/metadata/" + remotexlsxFile.getName());
            logging("\nTurtle file: " + baseInvestigationPath + "/metadata/" + remoteturtleFile.getName());

            // Write and Read excel and turtle file for entire group
            logger.info("Setting permissions for the metadata files");
            logger.info(credentials.getIrodsAccount().getZone() + "\t" + baseInvestigationPath + "/metadata/" + remoteturtleFile.getName()+ "\t" + baseInvestigationPath.getName());
            dataObjectAO.setAccessPermissionOwn(credentials.getIrodsAccount().getZone(), baseInvestigationPath + "/metadata/" + remoteturtleFile.getName(), baseInvestigationPath.getName());
            dataObjectAO.setAccessPermissionOwn(credentials.getIrodsAccount().getZone(), baseInvestigationPath + "/metadata/" + remotexlsxFile.getName(), baseInvestigationPath.getName());
            // Create collectionAO object
            CollectionAO collectionAO = credentials.getAccessObjectFactory().getCollectionAO(credentials.getIrodsAccount());
            // Create data directory under groups folder
            credentials.getFileFactory().instanceIRODSFile(baseInvestigationPath + "/data").mkdirs();
            // Enable inheritance for group metadata folder
            collectionAO.setAccessPermissionInherit(credentials.getIrodsAccount().getZone(), baseInvestigationPath + "/metadata", true);
            // Set own permission for metadata folder
            collectionAO.setAccessPermissionOwn(credentials.getIrodsAccount().getZone(), baseInvestigationPath + "/metadata", baseInvestigationPath.getName(), true);
            // Enable inheritance for group data folder
            collectionAO.setAccessPermissionInherit(credentials.getIrodsAccount().getZone(), baseInvestigationPath + "/data", true);
            // Set own permission for data folder
            collectionAO.setAccessPermissionOwn(credentials.getIrodsAccount().getZone(), baseInvestigationPath + "/data", baseInvestigationPath.getName(), true);
            // Notify user
            logging("\nData folder: " + baseInvestigationPath + "/data");
        }
    }

    /**
     * Wrapper around the logger object
     *
     * @param message The Message to log.
     */
    private void logging(String message) {
        logger.info(message);
        this.message = this.message + "\n" + message;
    }

    /**
     * Void method to set the prefixes from a file to the domain.
     *
     *  Prefixes also known as the name spaces, come from a file stored on disk. These are added to the
     * domain.
     * @implNote The prefixes are stored on disk in the file `queries/prefixes.txt`
     */
    private void setPrefixes() {
        Scanner scanner = new Scanner(WebGeneric.getFileContentFromResources("queries/prefixes.txt"));
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            domain.getRDFSimpleCon().setNsPrefix(line.split(" ")[1]
                            .replaceAll(":", ""),
                    line.split(" ")[2]
                            .replaceAll("[<>]", "")
            );
        }
    }

    /**
     * Check for duplicate id values
     *
     * @return Iterator<ResultLine> The results that have been checked for duplicate ids.
     * @throws Exception
     */
    private Iterator<ResultLine> checkDuplicateIds() throws Exception {
        // Check if duplicate identifiers have been used in the project
        Iterator<ResultLine> resultLineIterator = domain.getRDFSimpleCon().runQuery("getAllIdentifiers.txt",
                        true)
                .iterator();
        boolean failed = false;

        while (resultLineIterator.hasNext()) {
            ResultLine resultLine = resultLineIterator.next();
            String identifier = resultLine.getLitString("identifier");
            // Check for illegal characters for now it is only a "/"
            boolean allowed = identifier.matches("[A-Za-z0-9\\-_.]+");
            if (!allowed) {
                if (identifier.contains(" ")) {
                    logging("Identifier \"" + identifier
                            + "\" contains a space, only alphanumeric and - _ : . are allowed");
                } else {
                    logging("Identifier \"" + identifier
                            + "\" contains an illegal character, only alphanumeric and - _ : . are allowed");
                }
            }

            // Check for identifier counts
            int counts = resultLine.getLitInt("count");
            if (counts > 1) {
                failed = true;
                logging("Warning: Identifier " + identifier + " used in multiple places at least " + counts + " times");
            }
        }


        // Saving conditional on whether multiple identifiers are allowed.
        if (failed && nl.fairbydesign.Application.debug) {
            domain.save(Application.commandOptions.storage + "/validation/debug_multipleidentifiers.ttl");
            throw new Exception();
        }

        if (nl.fairbydesign.Application.debug) {
            domain.save(Application.commandOptions.storage + "/validation/debug.ttl");
        }

        return resultLineIterator;
    }

    /**
     * Validation process helper
     *
     *  This void method helps with the validation by checking the objects made in the `validation` String
     * method. It does the following things: Checks whether there are duplicate identifiers. Determines if all assays
     * have an unique name. Whether DNA barcodes are used only once per library and if they have only two datasets when
     * still being multiplexed. When these checks pass, the data is added to a RDF file. Once the data is added, a final
     * check is made to determine whether all required data is present.
     * @note Validation is an on-going process but the Empusa API should take care of most things.
     * @implNote Validation data are retrieved using sparql queries (`src/main/resources/queries/`).
     */
    private void validateHelper() throws Exception {
        // Validates if all assays have the same name
        Iterator<ResultLine> resultLineIterator = domain.getRDFSimpleCon().runQuery("getAssayTitles.txt", true).iterator();
        while (resultLineIterator.hasNext()) {
            ResultLine resultLine = resultLineIterator.next();
            if (resultLine.getLitInt("count") > 1) {
                resultLine.getLitString("desc");
                resultLine.getLitString("title");
                logging("It is advised that title and description is unique " +
                        "for each assay (e.g. Amplicon data from chicken sampled on day 30 from Farm 1)");
                // throw new Exception("Multiple usage of title and description detected in the assay descriptions");
            }
        }

        // Validate barcodes if they are used only once per library (prevents copy paste errors)
        resultLineIterator = domain.getRDFSimpleCon().runQuery("getBarcodeValidation.txt", true).iterator();
        while (resultLineIterator.hasNext()) {
            ResultLine resultLine = resultLineIterator.next();
            // Only enters this area when the same barcode is used multiple times for the same library
            logging("The same barcode " + resultLine.getLitString("fwd") + " "
                    + resultLine.getLitString("rev") + " used multiple times in the library "
                    + resultLine.getIRI("library"));
            throw new Exception("Multiple usage of the barcode detected");
        }

        // Validate amplicon assays if they have only 2 datasets when they are still multiplexed
        resultLineIterator = domain.getRDFSimpleCon().runQuery("getLibraryCount.txt", true).iterator();
        if (resultLineIterator.hasNext()) {
            ResultLine resultLine = resultLineIterator.next();
            logging("One of the assays in library name "
                    + resultLine.getLitString("libraryName")
                    + " is associated with the wrong library");
            logging("The following library files are involved");
            while (resultLineIterator.hasNext()) {
                resultLine = resultLineIterator.next();
                logging(resultLine.getLitString("filename"));
            }
            // Only enters this area when the same barcode is used multiple times for the same library
            throw new Exception("Wrong library association detected");
        }

        // Final validation through hasPart
        HashSet<String> allParts = new HashSet<>();
        // Obtain all subjects of the selected types
        domain.getRDFSimpleCon().runQuery("getAllParts.txt", true).iterator().
                forEachRemaining(resultLine -> {
                    allParts.add(resultLine.getIRI("subject"));
                });

        // Double check with ontology for properties that are obligatory
        resultLineIterator = domain.getRDFSimpleCon()
                .runQuery("getInvestigations.txt", true).iterator();
        while (resultLineIterator.hasNext()) {
            Investigation investigation = (Investigation) domain.getObject(resultLineIterator.next().getIRI("iri"), Investigation.class);
            allParts.remove(investigation.getResource().getURI());
            logger.debug("Number of parts left: " + allParts.size());
            investigation.getIdentifier();
            investigation.getAllDataset().forEach(data_sample -> {
                allParts.remove(data_sample.getResource().getURI());
                logger.debug("Number of parts left: " + allParts.size());
                data_sample.getIdentifier();
            });
            for (Study study : investigation.getAllHasPart()) {
                allParts.remove(study.getResource().getURI());
                logger.debug("Number of parts left: " + allParts.size());
                study.getIdentifier();
                study.getAllDataset().forEach(data_sample -> {
                    allParts.remove(data_sample.getResource().getURI());
                    logger.debug("Number of parts left: " + allParts.size());
                    data_sample.getIdentifier();
                });
                for (observation_unit observation_unit : study.getAllHasPart()) {
                    allParts.remove(observation_unit.getResource().getURI());
                    logger.debug("Number of parts left: " + allParts.size());
                    observation_unit.getIdentifier();
                    observation_unit.getAllDataset().forEach(data_sample -> {
                        allParts.remove(data_sample.getResource().getURI());
                        logger.debug("Number of parts left: " + allParts.size());
                        data_sample.getIdentifier();
                    });
                    for (Sample sample : observation_unit.getAllHasPart()) {
                        allParts.remove(sample.getResource().getURI());
                        logger.debug("Number of parts left: " + allParts.size());
                        sample.getIdentifier();
                        for (Assay assay : sample.getAllHasPart()) {
                            allParts.remove(assay.getResource().getURI());
                            logger.debug("Number of parts left: " + allParts.size());
                            assay.getIdentifier();
                            for (Data_sample data_sample : assay.getAllDataset()) {
                                allParts.remove(data_sample.getResource().getURI());
                                logger.debug("Number of parts left: " + allParts.size());
                                data_sample.getIdentifier();
                                // Check if there is a description
                                NodeIterator nodeIterator = domain.getRDFSimpleCon()
                                        .getObjects(data_sample.getResource().getURI(),
                                                "http://schema.org/description");
                                AtomicInteger count = new AtomicInteger();
                                // Count the number of descriptions
                                nodeIterator.forEachRemaining(node -> count.set(count.get() + 1));
                                if (count.get() > 1) {
                                    logging("Too many descriptions found for "
                                            + data_sample.getResource().getURI());
                                    throw new Exception("Too many descriptions found for "
                                            + data_sample.getResource().getURI());
                                }
                            }
                        }
                    }
                }
            }
        }
        if (!allParts.isEmpty()) {
            logging("Disconnected URLs detected... please check:");
            for (String allPart : allParts) {
                logging(allPart);
            }
        }
    }

    /**
     * Sample creation module from the sample sheet
     *
     * @param sheetName
     * @param sheet
     * @param metadataArrayList
     * @param logArea
     */
    private void sampleCreation(String sheetName, Sheet sheet, ArrayList<Metadata> metadataArrayList, TextArea logArea) throws Exception {
        logger.info("Analyzing " + sheetName + " sheet");
        boolean headerRow = true;

        Set<String> parsedHeaders = new HashSet<>();
        parsedHeaders.addAll(List.of(OBSERVATION_UNIT_IDENTIFIER));

        // To store the header of the excel sheet in
        ArrayList<String> header = new ArrayList<>();

        for (Row row : sheet) {
            if (row.getRowNum() % 10 == 0) {
                // Here we report the output of the programme.
                if (logArea != null)
                    logArea.getUI().ifPresent(
                            ui -> ui.access(
                                    () -> logArea.setValue("Processing "
                                            + sheetName
                                            + " ("
                                            + row.getRowNum() + ")"
                                    )
                            )
                    );
            }
            // Default row parser...
            // First cell has to be a string...
            if (row.getCell(0) != null && row.getCell(0).getCellType().equals(CellType.NUMERIC)) {
                row.getCell(0).setCellValue(String.valueOf(row.getCell(0).getNumericCellValue()));
            }

            if (row.getCell(0) != null
                    && !row.getCell(0).getStringCellValue().trim().startsWith("#")
                    && row.getLastCellNum() != -1 && checkNotEmpty(row)) {
                if (headerRow) {
                    header = getHeader(row);
                    headerRow = false;
                } else {
                    // Required fields check
                    boolean status = checkObligatoryFields(header, row, metadataArrayList);
                    if (status) {
                        logging("Please complete the missing cell values");
                        throw new Exception("");
                    }

                    // Get the top level objects.
                    Resource obsResource = getHigherLevel(OBSERVATION_UNIT_IDENTIFIER,
                            getStringValueCell(row.getCell(header.indexOf(OBSERVATION_UNIT_IDENTIFIER))).strip(),
                            metadataArrayList, logArea, "http://purl.org/ppeo/PPEO.owl#observation_unit", sheetName);
                    observation_unit observation_unit = domain.make(observation_unit.class,
                            obsResource.getURI());

                    // Set the top level objects to this one.
                    Sample sample = domain.make(Sample.class,
                            PREFIX
                                    + new URL(obsResource.getURI()).getPath()
                                    + "/sam_"
                                    + getStringValueCell(row.getCell(header.indexOf(SAMPLE_IDENTIFIER))).trim());

                    // Get identifier
                    try {
                        if (sample.getIdentifier() != null) {
                            logging("Sample identifier " + sample.getIdentifier() + " already exists");
                            throw new Exception("");
                        }
                    } catch (Exception e) {
                        if (!e.getMessage().startsWith("Cardinality"))
                            // Only throw an exception when it is able to retrieve data and not a cardinality issue
                            throw new Exception("");
                    }

                    // Se the variable columns.
                    otherColumns(header, parsedHeaders, row, sample.getResource().getURI(), metadataArrayList);

                    observation_unit.addHasPart(sample);

                    // Set the Sheet/Package name
                    if (sheetName.contains(" - "))
                        sample.getResource().addLiteral(domain.getRDFSimpleCon()
                                        .getModel()
                                        .createProperty("http://fairbydesign.nl/ontology/packageName"),
                                sheetName.split(" - ")[1]);
                }
            }
        }
        logging("Finished processing " + sheetName + " sheet");
    }

    /**
     * Checks if the obligatory fields are filled in for each row
     *
     * @param header            the specific column / header being requested
     * @param row               the row being parsed
     * @param metadataArrayList is a list of metadata objects
     * @return
     */
    private boolean checkObligatoryFields(ArrayList<String> header,
                                          Row row,
                                          ArrayList<Metadata> metadataArrayList) {
        boolean status = false;

        // String identifier = row.getCell(0).getStringCellValue().trim();
        for (Metadata metadata : metadataArrayList) {
            // Non obligatory elements are skipped
            if (!metadata.getRequirement().equals(MANDATORY)) continue;
            // Obtain name of metadata element
            if (!header.contains(metadata.getLabel())) {
                logging("Internal package name: " + metadata.getPackageName());
                logging("in sheet \"" + row.getSheet().getSheetName() + "\" row " + row.getRowNum() + " does not contain column \"" + metadata.getLabel() + "\" which is obligatory\n" +
                        "The following rows were found instead: " + header);
                status = true;
            } else {
                CellType cellType = row.getCell(header.indexOf(metadata.getLabel()), Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).getCellType();
                if (cellType.equals(CellType.BLANK)) {
                    if (metadata.getTerm().getRegex() != null && !metadata.getTerm().getRegex().pattern().matches("\\.\\*")) {
                        String pattern = metadata.getTerm().getRegex().pattern();
                        if (pattern.length() > 102) {
                            pattern = metadata.getTerm().getRegex().pattern().substring(1, 100) + "....";
                        }
                        logging("in sheet " + row.getSheet().getSheetName() + " row " + row.getRowNum() + " contains an empty cell for column " + metadata.getLabel() + " which is obligatory. \nThis value needs to match the pattern of " + pattern + "\n");
                    } else if (metadata.getTerm().getOntology() != null) {
                        logging("in sheet " + row.getSheet().getSheetName() + " row " + row.getRowNum() + " contains an empty cell for column " + metadata.getLabel() + " which is obligatory (ontology)\nDefinition: " + metadata.getTerm().getDefinition() + "\n");
                    } else {
                        logging("in sheet " + row.getSheet().getSheetName() + " row " + row.getRowNum() + " contains an empty cell for column " + metadata.getLabel() + " which is obligatory (free text)\nDefinition: " + metadata.getTerm().getDefinition() + "\n");
                    }
                    status = true;
                }
            }
        }
        return status;
    }

    /**
     * Removes the spaces from URL-like strings.
     *
     * @param iriPart part of the IRI that needs to be corrected
     * @return corrected IRI
     *  Corrects URLS which could contain a space due to naming conventions, all should
     * eventually be replaced by PID's
     */
    public static String iriCorrector(String iriPart) {
        return iriPart.trim().replaceAll("[ \\(\\)]", "_").replaceAll(" +", "_").replaceAll("_+", "_").replaceAll("_$", "");
    }

    /**
     * @param sheetName
     * @param sheet
     * @param metadataArrayList
     * @param logArea
     * @throws Exception
     */
    private void assayCreation(String sheetName, Sheet sheet, ArrayList<Metadata> metadataArrayList, TextArea logArea) throws Exception {
        logging("Processing " + sheetName + " sheet");
        ArrayList<String> header = new ArrayList<>();

        // Header capture
        boolean headerRow = true;
        // Iterating over each row
        for (Row row : sheet) {
            if (row.getRowNum() % 10 == 0) {
                if (logArea != null)
                    logArea.getUI().ifPresent(ui -> ui.access(() -> logArea.setValue("Processing " + sheetName + " (" + row.getRowNum() + ")")));
            }

            // First cell has to be a string... and due to numeric identifiers sometimes it will be parsed incorrectly
            if (row.getCell(0) != null && row.getCell(0).getCellType().equals(CellType.NUMERIC)) {
                row.getCell(0).setCellValue(String.valueOf(row.getCell(0).getNumericCellValue()));
            }

            // Standard row/cell check
            if (row.getCell(0) != null && !row.getCell(0).getStringCellValue().trim().startsWith("#") && row.getLastCellNum() != -1 && checkNotEmpty(row)) {
                if (headerRow) {
                    header = getHeader(row);
                    headerRow = false;
                } else {
                    boolean status = checkObligatoryFields(header, row, metadataArrayList);

                    if (status) {
                        // Logging from validate obligatory contains information that have been passed to the interface
                        throw new Exception("");
                    }

                    // TODO Reduce list to the core elements
                    Set<String> parsedHeaders = new HashSet<>();

                    // Obtain identifier
                    int index = header.indexOf(ASSAY_IDENTIFIER);
                    String assayIdentifier = row.getCell(index).getStringCellValue().trim();

                    /**
                     * Specific sequence assay information
                     */

                    // Check if assay identifier exists?
                    if (domain.getRDFSimpleCon().runQuery("getAssay.txt", true, assayIdentifier).iterator().hasNext()) {
                        // Duplication detected, check if SameAs is present
                        if (!header.contains(SAME_AS)) {
                            logging("Assay with identifier " + assayIdentifier + " already exists!\n" +
                                    "Check for duplicate row entry in your input file");
                            throw new DuplicateDataException("");
                        } else {
                            // Further, testing if it is of the same type assayType
                            // TODO Sheet types changed as everything is now of type Assay
                            if (row.getCell(header.indexOf(SAME_AS)) == null) {
                                logging("Identifier " + assayIdentifier + " has a duplicate entry but no value for the SameAs column.");
                                throw new Exception("");
                            }
                            String sameAsValue = getStringValueCell(row.getCell(header.indexOf(SAME_AS))).trim();
                            // If identifier is not the same throw an error
                            if (!assayIdentifier.equals(sameAsValue)) {
                                logging("SameAs detected but assay identifier not the same as sameAs identifier " + assayIdentifier + " and " + sameAsValue);
                                throw new DuplicateDataException("SameAs detected but assay identifier not the same as sameAs identifier " + assayIdentifier + " and " + sameAsValue);
                            } else {
                                // Duplicate assay with same identifier detected
                                parsedHeaders.add("Same as");
                            }
                        }
                    }

                    Resource sampleResource = getHigherLevel(SAMPLE_IDENTIFIER, getStringValueCell(row.getCell(header.indexOf(SAMPLE_IDENTIFIER))), metadataArrayList, logArea, "http://jermontology.org/ontology/JERMOntology#Sample", sheetName);
                    Sample sample = domain.make(Sample.class, sampleResource.getURI());

                    Assay assay = domain.make(Assay.class, PREFIX + new URL(sampleResource.getURI()).getPath() + "/asy_" + getStringValueCell(row.getCell(header.indexOf(ASSAY_IDENTIFIER))).trim());
//                    domain.getRDFSimpleCon().add(assay.getClassTypeIri(), "owl:equivalentClass", "http://www.wikidata.org/wiki/Q739897-Assay");

                    /**
                     * Generic Assay information
                     */

                    Cell fileCell = null;

                    // Parse all other columns
                    parsedHeaders.add(SAMPLE_IDENTIFIER);
                    otherColumns(header, parsedHeaders, row, assay.getResource().getURI(), metadataArrayList);

                    // Sheet/Package name
                    if (sheetName.contains(" - "))
                        assay.getResource().addLiteral(domain.getRDFSimpleCon().getModel().createProperty("http://fairbydesign.nl/ontology/packageName"), sheetName.split(" - ")[1]);

                    // Coupling the assay to the sample
                    sample.addHasPart(assay);
                }
            }
        }
        logging("Finished parsing " + sheetName + " sheet");
    }

    private Resource getHigherLevel(String identifier, String value, ArrayList<Metadata> metadataArrayList, TextArea logArea, String classURL, String sheetName) throws Exception {
        // Making sure no trailing spaces are left behind
        value = value.strip();
        String url = getPredicate(identifier, metadataArrayList);
        ResIterator resIterator = domain.getRDFSimpleCon().getModel().listSubjectsWithProperty(domain.getRDFSimpleCon().getModel().createProperty(url), value);
        // Testing multiple hits for the same identifier
        ArrayList<Resource> elements = new ArrayList<>();
        while (resIterator.hasNext()) {
            Resource resource = resIterator.next();
            String classURL2 = domain.getRDFSimpleCon().getObjects(resource.getURI(), "rdf:type").next().toString();
            if (classURL.equals(classURL2)) {
                elements.add(resource);
            }
        }
        String message;
        if (elements.size() > 1) {
            message = "Multiple matches for " + identifier + " with the value '" + value + " found'. Make sure that the " + identifier.replace(" identifier", "") + " is unique in all studies...";
        } else if (elements.size() == 1) {
            return elements.get(0);
        } else {
            message = "No match for " + identifier + " with the value '" + value + "' in sheet '"+sheetName+"'. Did you create the " + identifier.replace(" identifier", "") + " in the appropriate sheet?";
        }

        logging(message);

        domain.save(Application.commandOptions.storage + "/validation/debug_higherlevel.ttl");

        throw new Exception(message);
    }

    private boolean isBlank(Row row, int columnIndex) {
        return row.getCell(columnIndex, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).getCellType().equals(CellType.BLANK);
    }


    private void exception(String exception) throws Exception {
        message = message + "\n" + exception;
        throw new Exception(exception);
    }

    private Investigation getInvestigation(String investigationIdentifier) throws Exception {
        try {
            return domain.make(Investigation.class, domain.getRDFSimpleCon().runQuerySingleRes("getInvestigation.txt", false, investigationIdentifier).getIRI("investigation"));
        } catch (Exception e) {
            // Get all studies
            domain.getRDFSimpleCon().runQuery("getInvestigations.txt", true).iterator().forEachRemaining(resultLine -> logging("Investigations available: " + resultLine.getLitString("investigation")));
            exception("Cannot find a study coupled to this investigation: \"" + investigationIdentifier + "\"");
            return null;
        }
    }

    /**
     * @param sheetName
     * @param sheet     The observation unit sheet
     * @param logArea
     * @throws Exception
     */
    private void observationUnitCreation(String sheetName, Sheet sheet, ArrayList<Metadata> metadataArrayList, TextArea logArea) throws Exception {
        logging("Analyzing observation unit sheet");

        boolean headerRow = true;
        ArrayList<String> header = new ArrayList<>();
        Set<String> parsedHeaders = new HashSet<>();
        parsedHeaders.addAll(List.of(STUDY_IDENTIFIER));

        for (Row row : sheet) {
            if (row.getRowNum() % 10 == 0) {
                if (logArea != null)
                    logArea.getUI().ifPresent(ui -> ui.access(() -> logArea.setValue("Processing " + sheetName + " (" + row.getRowNum() + ")")));
            }

            // Default row parser... First cell has to be a string...
            if (row.getCell(0) != null && row.getCell(0).getCellType().equals(CellType.NUMERIC)) {
                row.getCell(0).setCellValue(String.valueOf(row.getCell(0).getNumericCellValue()));
            }

            if (row.getCell(0) != null && !row.getCell(0).getStringCellValue().trim().startsWith("#") && row.getLastCellNum() != -1 && checkNotEmpty(row)) {
                if (headerRow) {
                    header = getHeader(row);
                    headerRow = false;
                } else {
                    boolean status = checkObligatoryFields(header, row, metadataArrayList);
                    if (status) {
                        logging("Please complete the missing cell values");
                        throw new Exception("");
                    }

                    String url = getPredicate(STUDY_IDENTIFIER, metadataArrayList);
                    Resource studyResource = getHigherLevel(STUDY_IDENTIFIER, row.getCell(header.indexOf(STUDY_IDENTIFIER)).getStringCellValue(), metadataArrayList, logArea, "http://jermontology.org/ontology/JERMOntology#Study", sheetName);
                    Study study = domain.make(Study.class, studyResource.getURI());

                    observation_unit observationUnit = domain.make(observation_unit.class, PREFIX + new URL(studyResource.getURI()).getPath() + "/obs_" + row.getCell(header.indexOf(OBSERVATION_UNIT_IDENTIFIER)).getStringCellValue().trim());

                    // Get identifier
                    try {
                        if (observationUnit.getIdentifier() != null) {
                            logging("Observation unit identifier " + observationUnit.getIdentifier() + " already exists");
                            throw new Exception("");
                        }
                    } catch (Exception e) {
                        if (!e.getMessage().startsWith("Cardinality"))
                            // Only throw an exception when it is able to retrieve data and not a cardinality issue
                            throw new Exception("");
                    }


                    otherColumns(header, parsedHeaders, row, observationUnit.getResource().getURI(), metadataArrayList);

                    // Sheet/Package name
                    if (sheetName.contains(" - "))
                        observationUnit.getResource().addLiteral(domain
                                        .getRDFSimpleCon()
                                        .getModel()
                                        .createProperty("http://fairbydesign.nl/ontology/packageName"),
                                sheetName.split(" - ")[1]
                        );
                    study.addHasPart(observationUnit);
                }
            }
        }
        logger.info("Observation unit analysis finished");
    }


    private void studyCreation(Sheet sheet, ArrayList<Metadata> metadataArrayList, TextArea logArea) throws Exception {
        logging("Analysing study information");

        boolean headerRow = true;
        ArrayList<String> header = new ArrayList<>();

        Set<String> parsedHeaders = new HashSet<>();
        parsedHeaders.addAll(List.of(INVESTIGATION_IDENTIFIER));

        for (Row row : sheet) {
            // Default row parser...
            // First cell has to be a string...
            if (row.getCell(0) != null && row.getCell(0).getCellType().equals(CellType.NUMERIC)) {
                row.getCell(0).setCellValue(String.valueOf(row.getCell(0).getNumericCellValue()));
            }

            if (row.getCell(0) != null && !row.getCell(0).getStringCellValue().trim().startsWith("#") && row.getLastCellNum() != -1 && checkNotEmpty(row)) {
                if (headerRow) {
                    header = getHeader(row);
                    headerRow = false;
                } else {
                    boolean status = checkObligatoryFields(header, row, metadataArrayList);
                    if (status) {
                        logging("Please complete the missing cell values");
                        throw new Exception("");
                    }

                    // Init investigation
                    // String url = getPredicate(INVESTIGATION_IDENTIFIER, metadataArrayList);
                    Resource investigationResource = getHigherLevel(INVESTIGATION_IDENTIFIER, row.getCell(header.indexOf(INVESTIGATION_IDENTIFIER)).getStringCellValue(), metadataArrayList, logArea, "http://jermontology.org/ontology/JERMOntology#Investigation", sheet.getSheetName());
                    Investigation investigation = domain.make(Investigation.class, investigationResource.getURI());

                    // Obtaining study from Observation Unit
                    Study study = domain.make(Study.class, PREFIX + new URL(investigationResource.getURI()).getPath() + "/stu_" + row.getCell(header.indexOf(STUDY_IDENTIFIER)).getStringCellValue().trim());
//                    domain.getRDFSimpleCon().add(study.getClassTypeIri(), "owl:equivalentClass", "http://www.wikidata.org/wiki/Q101965-Study");

                    otherColumns(header, parsedHeaders, row, study.getResource().getURI(), metadataArrayList);

                    investigation.addHasPart(study);
                }
            }
        }
        logger.info("Study analysis finished");
    }

    private boolean checkNotEmpty(Row row) {
        return WebGeneric.checkNotEmpty(row);
    }

    private HashSet<String> getInvestigationIdentifiers(Sheet sheet) {
        logging("Get investigation identifier");
        boolean headerRow = true;
        ArrayList<String> header = new ArrayList<>();
        HashSet<String> identifiers = new HashSet<>();
        for (Row row : sheet) {
            // Default row parser...
            // First cell has to be a string...
            if (row.getCell(0) != null && row.getCell(0).getCellType().equals(CellType.NUMERIC)) {
                row.getCell(0).setCellValue(String.valueOf(row.getCell(0).getNumericCellValue()));
            }

            if (row.getCell(0) != null && !row.getCell(0).getStringCellValue().trim().startsWith("#") && row.getLastCellNum() != -1 && checkNotEmpty(row)) {
                if (headerRow) {
                    header = getHeader(row);
                    headerRow = false;
                } else {
                    // Investigation identifier
                    int index = header.indexOf(INVESTIGATION_IDENTIFIER);
                    identifiers.add(iriCorrector(row.getCell(index).getStringCellValue().trim().trim()));
                }
            }
        }
        return identifiers;
    }

    /**
     * @param sheet
     * @param logArea
     * @throws Exception
     */
    private void investigationCreation(Sheet sheet, ArrayList<Metadata> metadataArrayList, TextArea logArea) throws Exception {
        logging("Analysing investigation information");

        boolean headerRow = true;
        ArrayList<String> header = new ArrayList<>();

        Set<String> parsedHeaders = new HashSet<>();
        parsedHeaders.addAll(Arrays.asList("First name", "Last name", "lastname", "firstname", EMAIL, ORCID, ORGANIZATION, DEPARTMENT));

        for (Row row : sheet) {
            // Default row parser... First cell has to be a string...
            if (row.getCell(0) != null && row.getCell(0).getCellType().equals(CellType.NUMERIC)) {
                row.getCell(0).setCellValue(String.valueOf(row.getCell(0).getNumericCellValue()));
            }

            if (row.getCell(0) != null && !row.getCell(0).getStringCellValue().trim().trim().startsWith("#") && row.getLastCellNum() != -1 && checkNotEmpty(row)) {
                if (headerRow) {
                    header = getHeader(row);
                    headerRow = false;
                } else {
                    boolean status = checkObligatoryFields(header, row, metadataArrayList);
                    if (status) {
                        logging("Please complete the missing cell values");
                        throw new Exception("");
                    }

                    // Init investigation
                    String url = PREFIX + "/ontology/inv_" + row.getCell(header.indexOf(INVESTIGATION_IDENTIFIER)).getStringCellValue().trim();
                    Investigation investigation = domain.make(Investigation.class, url);

//                    domain.getRDFSimpleCon().add(investigation.getClassTypeIri(), "owl:equivalentClass", "http://www.wikidata.org/wiki/Q170584-Investigation");
                    if (investigation == null) {
                        throw new Exception("Investigation not matching to study, reason could be that there is no study with this investigation: " + row.getCell(header.indexOf(INVESTIGATION_IDENTIFIER)).getStringCellValue().trim().trim());
                    }

                    // FirstName	LastName	Email
                    // TODO remove this from the final file due to person information?
                    Person person = domain.make(Person.class, investigation.getResource().getURI() + "/person/" + DigestUtils.md5Hex(row.getCell(header.indexOf(EMAIL)).getStringCellValue().trim().trim()));
                    // Add same as for person to Q215627
//                    domain.getRDFSimpleCon().add(person.getClassTypeIri(), "owl:equivalentClass", "http://www.wikidata.org/wiki/Q215627-Person");
                    // Other information
                    person.setGivenName(row.getCell(header.indexOf(FIRSTNAME)).getStringCellValue().trim().trim());
                    person.setFamilyName(row.getCell(header.indexOf(LASTNAME)).getStringCellValue().trim().trim());
                    person.setEmail("mailto:" + row.getCell(header.indexOf(EMAIL)).getStringCellValue().trim().trim());
                    person.setGivenName(person.getGivenName() + " " + person.getFamilyName());
                    person.setDepartment(row.getCell(header.indexOf(DEPARTMENT)).getStringCellValue().trim().trim());

                    Organization organization = domain.make(Organization.class, "http://m-unlock.nl/ontology/organization/" + row.getCell(header.indexOf(ORGANIZATION)).getStringCellValue().trim().trim().replaceAll(" ", "_"));
                    organization.setLegalName(row.getCell(header.indexOf(ORGANIZATION)).getStringCellValue().trim().trim());

                    person.addMemberOf(organization);

                    // TODO clean this up
                    parsedHeaders.add("familyName");
                    parsedHeaders.add("givenName");
                    // If orcid is available, to be removed and generalised
                    if (header.contains(ORCID) && row.getCell(header.indexOf(ORCID)) != null) {
                        String value = getStringValueCell(row.getCell(header.indexOf(ORCID))).trim().trim();
                        if (value.length() > 1)
                            person.setOrcid("https://orcid.org/" + value);
                    }

                    // TODO Check if contribution works
                    investigation.getResource().addProperty(domain.getRDFSimpleCon().getModel().createProperty("http://schema.org/contributor"), person.getResource());

                    // Check for other arguments
                    otherColumns(header, parsedHeaders, row, investigation.getResource().getURI(), metadataArrayList);
                }
            }
        }
    }

    private String getPredicate(String label, ArrayList<Metadata> metadataArrayList) {
        for (Metadata metadata : metadataArrayList) {
            if (metadata.getLabel().equalsIgnoreCase(label)) {
                return metadata.getTerm().getURL();
            }
        }
        return null;
    }

    /**
     * Check any ontology terms entered by the user
     *
     * @param metadata The metadata object to be used during the check.
     * @param value    The value of the cell to be checked.
     * @throws Exception Message when the user input is incorrect.
     */
    public void handleOntology(Metadata metadata, String value) throws Exception {
        if (metadata.getRequirement().equals(MANDATORY) && value.isEmpty()) {
            logging("There is no value for the obligatory field " + metadata.getLabel() +
                    " and should match a label in the ontology " + metadata.getTerm().getOntology() +
                    " such as in example " + metadata.getTerm().getExample());
            throw new IllegalFormatException(metadata.getTerm().getSyntax() + "\n" + value);
        }
        if (!ontologies.containsKey(metadata.getTerm().getOntology())) {
            // Load the directory as a domain
            this.ontology.loadOntology(metadata.getTerm().getOntology());
        }

        // Does the label exist in the ontology?
        boolean correctLabel = this.ontology.checkTerm(metadata.getTerm().getOntology(), value, "");
        if (!correctLabel) {
            // This label value is not part of the ontology
            if (!ontology_warning) {
                ontology_warning = true;
                logging("Warning: The value '" + value + "' does not match any labels of the ontology " + metadata.getTerm().getSyntax() +
                        "\n for example \"" + metadata.getTerm().getExample() + "\" could be used.\n" +
                        metadata.getTerm().getDefinition() + "\n" +
                        "Possible terms similar to your statement are:\n");
                Set<String> closesTerms = this.ontology.findClosestTermsUsingSparql(metadata.getTerm().getOntology(), value, "");
                StringBuilder termList = new StringBuilder();

                for (String term : closesTerms) {
                    termList.append(term).append("\n");
                }
                logging(termList.toString());
            }
        }
    }

    /**
     * Validate all non hardcoded columns.
     *
     * @param header            the header of the sheet
     * @param ignoredHeaders    headers that have already been parsed and can be ignored
     * @param row               the row that is being processed
     * @param iri               the base url used?
     * @param metadataArrayList The metadata objects for this particular class type e.g. assay, sample, investigation.
     *                          Each meta data object is basically a column from the excel file.
     */
    private void otherColumns(ArrayList<String> header,
                              Set<String> ignoredHeaders,
                              Row row,
                              String iri,
                              ArrayList<Metadata> metadataArrayList) throws Exception {
        // TODO make sure to refactor this method to be more use full.

        // Transform an array into hashmap
        HashMap<String, Metadata> metadataLookup = new HashMap<>();
        for (Metadata metadata : metadataArrayList) {
            metadataLookup.put(metadata.getLabel(), metadata);
        }

        // Parsing all the headers
        for (String headerItemOriginal : header) {
            if (headerItemOriginal == null) continue;

            // Skip processed headers and ignore casing
            boolean containsSearchStr = ignoredHeaders.stream().anyMatch(headerItemOriginal::equalsIgnoreCase);
            if (!containsSearchStr) {
                // Predicate element
                String predicate = null;

                // Obtain the cell and if empty / null skip it
                Cell cell = row.getCell(header.indexOf(headerItemOriginal));
                if (cell == null) continue;

                // If the metadata key in the lookup list process accordingly, otherwise process dynamically
                Metadata metadata = null;
                if (metadataLookup.containsKey(headerItemOriginal)) {
                    metadata = metadataLookup.get(headerItemOriginal);
                } else if (Application.termLookup.containsKey(headerItemOriginal.toLowerCase())) {
                    Term term = Application.termLookup.get(headerItemOriginal.toLowerCase());
                    metadata = new Metadata();
                    metadata.setLabel(term.getLabel());
                    metadata.setRequirement(OPTIONAL.name());
                }

                if (metadata != null) {
                    String value = getStringValueCell(cell);

                    // Check if we are dealing with an ontology.
                    if (metadata.getTerm().getOntology() != null) {
                        // Check the ontology here.
                        handleOntology(metadata, value);
                    } else {
                        // Check the regular expressions
                        Pattern pattern = metadata.getTerm().getRegex();
                        if (pattern == null)
                            logger.debug("No pattern for " + metadata.getLabel());
                        // Clear out multi line strings
                        value = value.replaceAll("[\n\r]", " ");
                        // ignoring empty cells
                        if (metadata.getRequirement().equals(MANDATORY) && value.isEmpty()) {
                            logging("There is no value in row " + row.getRowNum() + " in " + row.getSheet() + " for the obligatory field " + metadata.getLabel()
                                    + " and should match pattern " + metadata.getTerm().getSyntax()
                                    + " such as in example:\n\n " + metadata.getTerm().getExample());
                            throw new IllegalFormatException(metadata.getTerm().getSyntax() + "\n" + value);
                        }

                        // Ignore empty cells that are not obligatory
                        if (value.isEmpty()) continue;

                        Matcher m = pattern.matcher(value);
                        // Check if it passes the regex validation
                        if (m.matches()) {
                            predicate = metadata.getTerm().getURL();
                        } else {
                            // Message for normal field and obligatory field
                            String additionalLine = " does not match the pattern of " +
                                    metadata.getTerm().getSyntax() +
                                    " regex " + metadata.getTerm().getRegex().pattern() +
                                    " such as in example \"" + metadata.getTerm().getExample() + "\"";
                            String sheetName = metadata.getSheetName();
                            if (sheetName == null) sheetName = "default";
                            String prefixLine = "The value \"" + value + "\" of \"" + metadata.getLabel() + "\" in the \"" + sheetName + "\" sheet";
                            if (metadata.getRequirement().equals(MANDATORY)) {
                                logging(prefixLine + " which is obligatory " + additionalLine);
                            } else {
                                logging(prefixLine + additionalLine);
                            }

                            // Minimum length
                            if (pattern.pattern().matches(".*\\{[0-9]+,}.*")) {
                                logging("A minimum length (" + pattern.pattern().replaceAll(".*?\\{([0-9]+),}.*", "$1") + ") is set for " + metadata.getLabel());
                            } else
                                // Exact length
                                if (pattern.pattern().matches(".*\\{[0-9]+}.*")) {
                                    logging("An exact length (" + pattern.pattern().replaceAll(".*?\\{([0-9]+)}.*", "$1") + ") is set for " + metadata.getLabel());
                                } else
                                    // Range length
                                    if (pattern.pattern().matches(".*\\{[0-9]+,[0-9]+}.*")) {
                                        logging("A range length (" + pattern.pattern().replaceAll(".*?\\{([0-9]+,[0-9]+)}.*", "$1") + ") is set for " + metadata.getLabel());
                                    }
                            throw new IllegalFormatException(additionalLine + "\n" + metadata.getTerm().getSyntax() + "\n" + value);
                        }
                    }
                }

                // If no predicate was available from the excel sheet due to self created columns in the metadata sheet
                if (predicate != null && predicate.trim().isEmpty()) {
                    // Check terms overview
                    predicate = null;
                }

                if (predicate == null && Application.termLookup.containsKey(headerItemOriginal.toLowerCase())) {
                    predicate = Application.termLookup.get(headerItemOriginal).getURL();
                }

                if (predicate == null) {
                    // Parsing elements from the standardisation and also optionally added by the user not in the metadata excel sheet
                    // Processing the header name as it can contain non-acceptable chars
                    String headerItem = iriCorrector(headerItemOriginal);

                    // Replace non-ascii characters by closed character
                    headerItem = Normalizer.normalize(headerItem, Normalizer.Form.NFD);
                    headerItem = headerItem.replaceAll("[^\\x00-\\x7F]", "");
                    headerItem = headerItem.replaceAll("/", "_");

                    // It is null if the user created this column by itself
                    // logger.debug("No predicate defined for " + headerItemOriginal);
                    predicate = PREFIX + "/ontology/" + headerItem.toLowerCase();
                }

                // logger.debug("Predicate value: " + predicate + " ");

                // If cell is not null / empty see if we can map to the appropriate triple type
                switch (cell.getCellType()) {
                    case BOOLEAN:
                        domain.getRDFSimpleCon().add(iri, predicate, cell.getBooleanCellValue());
                        break;
                    case NUMERIC:
                        if (DateUtil.isCellDateFormatted(cell)) {
                            Date date = cell.getDateCellValue();
                            LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                            domain.getRDFSimpleCon().add(iri, predicate, localDate);
                        } else {
                            if (cell.getNumericCellValue() % 1 == 0) {
                                domain.getRDFSimpleCon().add(iri, predicate, (int) cell.getNumericCellValue());
                            } else {
                                domain.getRDFSimpleCon().add(iri, predicate, cell.getNumericCellValue());
                            }
                        }
                        break;
                    case BLANK:
                    case ERROR:
                    case _NONE:
                    case FORMULA:
                        String objectValue = getStringValueCell(cell).trim().trim();
                        if (!objectValue.isEmpty())
                            domain.getRDFSimpleCon().addLit(iri, predicate, getStringValueCell(cell).trim().trim());
                    case STRING:
                        if (getStringValueCell(cell).trim().trim().isEmpty()) break;
                        // Check if it is a normal string or a file object
                        boolean isFile = false;
                        if (metadata == null || !metadata.getTerm().isFile()) {
                            objectValue = getStringValueCell(cell).trim().trim();
                            // Check if objectValue is a URL
                            if (!Generic.isValidURL(objectValue)) {
                                domain.getRDFSimpleCon().addLit(iri, predicate, objectValue);
                            }
                            // Disabled as URL's were treated as files and this causes conflicts on management level
//                            else {
                                // isFile = true; // Can be turned true if it is a uri...
//                            }
                        } else {
                            isFile = true;
                        }

                        if (isFile) {
                            // For file objects
                            String filePath = getStringValueCell(cell).trim().trim();
                            Data_sample data_sample;
                            // TODO check if URL / FILE / or name
                            if (Generic.isValidURL(filePath)) {
                                // Store as URL
                                URI uri;
                                try {
                                    uri = URI.create(filePath);
                                } catch (IllegalArgumentException e) {
                                    // Encode the URL path
                                    String filePathEncoded = filePath.replaceAll(" ", "%20");
                                    uri = URI.create(filePathEncoded);
                                }
                                data_sample = domain.make(Data_sample.class, uri.toString());
                                data_sample.setContentUrl(uri.toString());
                            } else if (!new File(filePath).getName().equalsIgnoreCase(filePath)) {
                                // For unsupported protocols e.g. webdav:
                                // Making sure root / is removed
                                filePath = filePath.replace("^/", "");
                                if (filePath.matches("^[a-z]+:.*")) {
                                    data_sample = domain.make(Data_sample.class, filePath);
                                    data_sample.setContentUrl(filePath);
                                } else if (filePath.contains("/")) {
                                    // If it is a path due to / content
                                    data_sample = domain.make(Data_sample.class, "file://" + filePath.replaceAll("/+", "/"));
                                    data_sample.setContentUrl("file://" + filePath.replaceAll("/+", "/"));
                                } else {
                                    data_sample = domain.make(Data_sample.class, ("file://" + UUID.randomUUID() + "/" + filePath).replaceAll("/+", "/"));
                                    data_sample.setContentUrl("file:/" + filePath);
                                }
                            } else {
                                data_sample = domain.make(Data_sample.class, PREFIX.replaceAll("/$", "") + "/data_sample/" + new File(filePath).getName().replaceAll("/+", "/"));
                            }
                            data_sample.setIdentifier(new File(filePath).getName().trim());
                            data_sample.getResource().addLiteral(domain.getRDFSimpleCon().getModel().createProperty("http://schema.org/description"), headerItemOriginal);
                            // Any level can have files attached now.
                            domain.getRDFSimpleCon().add(iri, "schema:dataset", data_sample.getResource());
                        } else {
                            String value = getStringValueCell(cell).trim().trim();
                            if (Generic.isValidURL(value)) {
                                // Store as URL
                                URI uri;
                                try {
                                    uri = URI.create(value);
                                } catch (IllegalArgumentException e) {
                                    // Encode the URL path
                                    String filePathEncoded = value.replaceAll(" ", "%20");
                                    uri = URI.create(filePathEncoded);
                                }
                                domain.getRDFSimpleCon().add(iri, predicate, domain.getRDFSimpleCon().getModel().createResource(uri.toString()));
                            } else {
                                // Normal string
                                domain.getRDFSimpleCon().addLit(iri, predicate, getStringValueCell(cell).trim().trim());
                            }
                        }
                        break;
                    default:
                        logging("Unknown cell type, please contact admin to improve the code : " + cell.getCellType().name());
                        domain.getRDFSimpleCon().addLit(iri, predicate, getStringValueCell(cell).trim().trim());
                }

                // Check if predicate already has a type property and
                // if not create the predicate with rdf type and label
                NodeIterator nodeIterator = domain.getRDFSimpleCon().getObjects(predicate, "rdf:type");
                if (!nodeIterator.hasNext()) {
                    // Add predicate RDFS statement
                    domain.getRDFSimpleCon().add(predicate, "rdf:type", "rdf:Property");
                    if (metadata != null) {
                        logger.debug("Standardised label: " + metadata.getLabel());
                        domain.getRDFSimpleCon().addLit(predicate, "rdfs:label", metadata.getLabel());
                        // Add definition if available
                        if (metadata.getTerm().getDefinition() != null) {
                            domain.getRDFSimpleCon().addLit(predicate, "http://schema.org/description", metadata.getTerm().getDefinition());
                        }
                        // Add other elements such as unit or regex?
                        if (metadata.getTerm().getRegex() != null)
                            domain.getRDFSimpleCon().addLit(predicate, "http://schema.org/valuePattern", metadata.getTerm().getRegex().pattern());
                        else
                            domain.getRDFSimpleCon().addLit(predicate, "http://schema.org/valuePattern", metadata.getTerm().getOntology().toString());
                        domain.getRDFSimpleCon().add(predicate, "http://schema.org/valueRequired", metadata.getRequirement().equals(MANDATORY));
                        // TODO unit can be a list of units? or a regex?
                        if (metadata.getTerm().getPreferredUnit() != null && metadata.getTerm().getPreferredUnit().length() > 0)
                            domain.getRDFSimpleCon().addLit(predicate, "http://schema.org/unitCode", metadata.getTerm().getPreferredUnit());
                    } else {
                        // Self invented element by researcher, still important to capture!
                        logger.debug("Self made label: " + headerItemOriginal);
                        // String value = StringUtils.join(splitCamelCase(predicate.replaceAll(".*/", "")), " ").toLowerCase();
                        domain.getRDFSimpleCon().addLit(predicate, "rdfs:label", headerItemOriginal);
                        domain.getRDFSimpleCon().addLit(predicate, "http://schema.org/valuePattern", ".+");
                    }
                }
            }
        }
    }


    /**
     * @param row Creates a column index lookup based on naming scheme
     * @return
     */
    private static ArrayList<String> getHeader(Row row) {
        ArrayList<String> headerLookup = new ArrayList<>();
        for (int j = 0; j < row.getLastCellNum(); j++) {
            Cell cell = row.getCell(j);
            try {
                headerLookup.add(cell.getStringCellValue().trim().trim().toLowerCase());
            } catch (NullPointerException e) {
                headerLookup.add(null);
            }
        }
        return headerLookup;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}