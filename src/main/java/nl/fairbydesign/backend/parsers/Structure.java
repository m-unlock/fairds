package nl.fairbydesign.backend.parsers;

import nl.fairbydesign.Application;
import nl.fairbydesign.backend.credentials.Credentials;
import nl.wur.ssb.RDFSimpleCon.RDFFormat;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.irods.jargon.core.pub.io.IRODSFile;
import org.jermontology.ontology.JERMOntology.domain.Assay;
import org.jermontology.ontology.JERMOntology.domain.Data_sample;
import org.jermontology.ontology.JERMOntology.domain.Investigation;
import org.jermontology.ontology.JERMOntology.domain.Sample;
import org.jermontology.ontology.JERMOntology.domain.Study;
import org.purl.ppeo.PPEO.owl.domain.observation_unit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.HashMap;

import static nl.fairbydesign.backend.irods.Browser.getFilesRecursively;

public class Structure {
    private static final Logger logger = LoggerFactory.getLogger(Structure.class);

    public static void create(Credentials credentials, String rdfFile, String investigationName) {
        // Get project and iterated from there?
        try {
            logger.info("Loading RDF file " + rdfFile + " with investigation " + investigationName);
            Domain domain = new Domain("file://" + rdfFile);
            Iterable<ResultLine> resultLines = domain.getRDFSimpleCon().runQuery("getInvestigation.txt", true, investigationName);
            String root = "/" + credentials.getZone() + "/home/" + investigationName + "/";
            for (ResultLine resultLine : resultLines) {
                Investigation investigation = domain.make(Investigation.class, resultLine.getIRI("investigation"));
                // Create data folder in investigation
                IRODSFile dataFolder = credentials.getFileFactory().instanceIRODSFile(new File(root + "data").getAbsolutePath());
                if (!credentials.getFileFactory().instanceIRODSFile(dataFolder.getAbsolutePath()).exists()) {
                    credentials.getFileFactory().instanceIRODSFile(dataFolder.getAbsolutePath()).mkdirs();
                    logger.info("Creating data folder at " + root + "data");
                    logger.info("Please upload the data files recorded in the metadata document to this folder.");
                } else {
                    // Obtain the files in the fairds-data folder
                    HashMap<String, String> fairdsDataFiles = getFilesRecursively(credentials, dataFolder);
                    logger.info("Detected " + fairdsDataFiles.size() + " files in the fairds-data folder.");
                    if (fairdsDataFiles.isEmpty()) {
                        logger.warn("No data files detected in the fairds-data folder. Please upload the data files recorded in the metadata document to this folder.");
                        return;
                    }
                    // Create study folder // TODO at what level should we start creating sub-folders?
                    for (Study study : investigation.getAllHasPart()) {
                        File studyPath = new File(root + "/" + study.getIdentifier());
                        credentials.getFileFactory().instanceIRODSFile(studyPath.getAbsolutePath()).mkdirs();
                        for (observation_unit observationUnit : study.getAllHasPart()) {
                            File obsUnitPath = new File(studyPath + "/" + observationUnit.getIdentifier());
                            for (Sample sample : observationUnit.getAllHasPart()) {
                                File samplePath = new File(obsUnitPath + "/" + sample.getIdentifier());
                                for (Assay assay : sample.getAllHasPart()) {
                                    File assayPath = new File(samplePath + "/" + assay.getIdentifier());
                                    for (Data_sample dataSample : assay.getAllDataset()) {
                                        // Move the data file to the assay folder if it is present
                                        if (fairdsDataFiles.containsKey(dataSample.getIdentifier())) {
                                            IRODSFile dataCollection = credentials.getFileFactory().instanceIRODSFile(new File(assayPath + "/data/").getAbsolutePath());
                                            // Create data folder in assay sub-sub-level...
                                            dataCollection.mkdirs();
                                            IRODSFile irodsFile = credentials.getFileFactory().instanceIRODSFile(fairdsDataFiles.get(dataSample.getIdentifier()));
                                            String destination = dataCollection.getAbsolutePath() + "/" + irodsFile.getName();
                                            logger.info("Moving " + irodsFile.getAbsolutePath() + " to " + destination);
                                            irodsFile.renameTo(credentials.getFileFactory().instanceIRODSFile(destination));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String assaySnippets(Domain domain, Assay assay) throws Exception {
        logger.debug("Creating assay snippet for " + assay.getIdentifier());
        // Making the RDF snippet for the assay set as long as it is not amplicon library data
        if (!assay.getClassTypeIri().endsWith("AmpliconLibraryAssay")) {
            logger.warn("Nothing is an amplicon library assay anymore right?...");
            // Get the sample corresponding to this assay as well
            ResultLine resultLine = domain.getRDFSimpleCon().runQuerySingleRes("getSampleFromAssay.txt", true, assay.getResource().getURI());
            Sample sample = domain.make(Sample.class, resultLine.getIRI("sample"));

            // Making a Assay specific RDF database with a snapshot of the data
            Domain assayDomain = new Domain("");
            assayDomain.getRDFSimpleCon().setNsPrefix("schema", "http://schema.org/");
            assayDomain.getRDFSimpleCon().setNsPrefix("unlock", "http://m-unlock.nl/ontology/");
            assayDomain.getRDFSimpleCon().setNsPrefix("jerm", "http://jermontology.org/ontology/JERMOntology#");

            Resource resource = domain.getRDFSimpleCon().getModel().getResource(sample.getResource().getURI());
            StmtIterator stmtIterator = resource.listProperties();
            while (stmtIterator.hasNext()) {
                Statement statement = stmtIterator.next();
                // A skip step for assay predicate if not matching the original assay
                if (statement.getPredicate().getURI().contains("http://m-unlock.nl/ontology/assay")) {
                    // Check if iri matches
                    // System.err.println(statement.getObject().toString());
                    if (statement.getObject().toString().contains(assay.getResource().getURI())) {
                        assayDomain.getRDFSimpleCon().getModel().add(statement);
                    }
                } else {
                    assayDomain.getRDFSimpleCon().getModel().add(statement);
                }
            }

            resource = domain.getRDFSimpleCon().getModel().getResource(assay.getResource().getURI());
            stmtIterator = resource.listProperties();
            while (stmtIterator.hasNext()) {
                Statement statement = stmtIterator.next();
                assayDomain.getRDFSimpleCon().getModel().add(statement);
            }

            for (Data_sample data_sample : assay.getAllDataset()) {
                resource = domain.getRDFSimpleCon().getModel().getResource(data_sample.getResource().getURI());

                stmtIterator = resource.listProperties();
                while (stmtIterator.hasNext()) {
                    Statement statement = stmtIterator.next();
                    assayDomain.getRDFSimpleCon().getModel().add(statement);
                }
            }

            String assayFile = assay.getIdentifier() + ".ttl";
            assayDomain.save(Application.commandOptions.storage + "/validation/" + assayFile, RDFFormat.TURTLE);
            assayDomain.close();

            return assayFile;
        }
        return null;
    }
}
