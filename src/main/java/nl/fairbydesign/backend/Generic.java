package nl.fairbydesign.backend;

import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.NodeIterator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.HashSet;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class Generic {
    public static final Logger logger = LogManager.getLogger(Generic.class);

    public static boolean isValidURL(String urlString) {
        try {
            URL url = new URL(urlString);
            url.toURI();
            return true;
        } catch (Exception e) {
            // Check if it is an invalid url but still uses a protocol
            if (urlString.matches("^[a-z]+:.*")) {
                return true;
            }
            logger.debug("Normal string: " + e.getMessage());
            return false;
        }
    }

    public static boolean isInteger(String str) {
        if (str == null) {
            return false;
        }
        int length = str.length();
        if (length == 0) {
            return false;
        }
        int i = 0;
        if (str.charAt(0) == '-') {
            if (length == 1) {
                return false;
            }
            i = 1;
        }
        for (; i < length; i++) {
            char c = str.charAt(i);
            if (c < '0' || c > '9') {
                return false;
            }
        }
        return true;
    }

    public static void createZipArchive(ZipOutputStream zos, File fileToZip, String parrentDirectoryName) throws Exception {
        if (fileToZip == null || !fileToZip.exists()) {
            return;
        }

        String zipEntryName = fileToZip.getName();
        if (parrentDirectoryName!=null && !parrentDirectoryName.isEmpty()) {
            zipEntryName = parrentDirectoryName + "/" + fileToZip.getName();
        }

        if (fileToZip.isDirectory()) {
            System.out.println("+" + zipEntryName);
            for (File file : fileToZip.listFiles()) {
                createZipArchive(zos, file, zipEntryName);
            }
        } else {
            System.out.println("   " + zipEntryName);
            byte[] buffer = new byte[1024];
            FileInputStream fis = new FileInputStream(fileToZip);
            zos.putNextEntry(new ZipEntry(zipEntryName));
            int length;
            while ((length = fis.read(buffer)) > 0) {
                zos.write(buffer, 0, length);
            }
            zos.closeEntry();
            fis.close();
        }
    }

    public static File createENAZipArchive(File directory) throws IOException {

        // create a ZipOutputStream object
        FileOutputStream fos = new FileOutputStream(directory + "/ena_submission.zip");
        ZipOutputStream zos = new ZipOutputStream(fos);

        File[] filesToBeWritten = directory.listFiles();
        for (int i = 0; i < filesToBeWritten.length; i++) {

            File srcFile = filesToBeWritten[i];
            FileInputStream fis = new FileInputStream(srcFile);

            // Start writing a new file entry
            zos.putNextEntry(new ZipEntry(srcFile.getName()));

            int length;
            // create byte buffer
            byte[] buffer = new byte[1024];

            // read and write the content of the file
            while ((length = fis.read(buffer)) > 0) {
                zos.write(buffer, 0, length);
            }
            // current file entry is written and current zip entry is closed
            zos.closeEntry();

            // close the InputStream of the file
            fis.close();
        }

        // close the ZipOutputStream
        zos.close();
        return new File(directory + "/ena_submission.zip");
    }

    // Get value of predicate when available otherwise return null
    public static HashSet<String> getObjectsOfPredicate(Domain domain, String subject, String predicate) {
        try {
            HashSet<String> uris = new HashSet<>();
            NodeIterator nodeIterator = domain.getRDFSimpleCon().getObjects(subject, predicate);
            while (nodeIterator.hasNext()) {
                uris.add(nodeIterator.next().toString());
            }
            return uris;
        } catch (Exception e) {
            logger.error("No results for " + predicate + " from " + subject);
            return null;
        }
    }

    public static String getValueOfPredicate(Domain domain, String subject, String predicate) {
        try {
            return domain.getRDFSimpleCon().getObjects(subject, predicate).next().toString();
        } catch (Exception e) {
            logger.error("No results for " + predicate + " from " + subject);
            return null;
        }
    }

    public static FolderStats calculateFolderSize(Path folder) {
        final long[] size = {0};
        final long[] files = {0};
        try {
            Files.walkFileTree(folder, new SimpleFileVisitor<>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
                    size[0] += attrs.size();
                    files[0]++;
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException e) {
            logger.error("Obtaining file size failed");
        }

        return new FolderStats(size[0], files[0]);
    }

    public static class FolderStats {
        private final long size;
        private final long count;

        public FolderStats(long size, long count) {
            this.size = size;
            this.count = count;
        }

        public long getSize() {
            return size;
        }

        public String getFormatSize() {
            return formatSize(size);
        }

        public long getCount() {
            return count;
        }
    }

    public static String formatSize(long size) {
        String[] units = {"bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"};
        int i = 0;
        while (size >= 1024 && i < units.length - 1) {
            size /= 1024;
            i++;
        }
        return String.format("%d %s", size, units[i]);
    }

    public static String humanReadableByteCountBin(long bytes) {
        long absB = bytes == Long.MIN_VALUE ? Long.MAX_VALUE : Math.abs(bytes);
        if (absB < 1024) {
            return bytes + " B";
        }
        long value = absB;
        CharacterIterator ci = new StringCharacterIterator("KMGTPE");
        for (int i = 40; i >= 0 && absB > 0xfffccccccccccccL >> i; i -= 10) {
            value >>= 10;
            ci.next();
        }
        value *= Long.signum(bytes);
        return String.format("%.1f %ciB", value / 1024.0, ci.current());
    }


    public static void zipFiles(HashSet<File> filesToAdd, File outputZipPath) {
            try {
                // Making sure the location to store the zip file exists
                if (!outputZipPath.getParentFile().exists()) {
                    outputZipPath.getParentFile().mkdirs();
                }
                // Create a FileOutputStream to write the zip file to
                FileOutputStream fos = new FileOutputStream(outputZipPath);
                ZipOutputStream zipOut = new ZipOutputStream(fos);
                // For each file in the list, add it to the zip file
                for (File file : filesToAdd) {
                    if (file == null) {
                        // EBI.logging("File is null", true);
                        continue;
                    }
                    // EBI.logging("Adding file to zip: " + file.getName(), true);
                    addFileToZip(file, zipOut);
                }
                // Close the ZipOutputStream
                zipOut.close();
                fos.close();

                // TODO implement logMessage
                logger.info("Files successfully zipped with directory structure.");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        private static void addFileToZip(File file, ZipOutputStream zipOut) throws IOException {
            if (file.isDirectory()) {
                File[] files = file.listFiles();
                for (File subFile : files) {
                    addFileToZip(subFile, zipOut);
                }
            } else {
                FileInputStream fis = new FileInputStream(file);
                String entryName = file.getPath().replace("\\", "/");

                ZipEntry zipEntry = new ZipEntry(entryName);
                zipOut.putNextEntry(zipEntry);

                byte[] buffer = new byte[1024];
                int length;
                while ((length = fis.read(buffer)) > 0) {
                    zipOut.write(buffer, 0, length);
                }

                fis.close();
            }
        }

}
