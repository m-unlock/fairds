package nl.fairbydesign.backend.wrappers.io;

import org.apache.commons.io.FileExistsException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.util.Date;


/**
 * File download wrapper.
 * <p>
 * A nice facade around various classes to easily download files.
 * Instantiate the constructor, then use the desired download method.
 * </p>
 * <p>
 *     There are methods for dealing with common errors stemming from IO, such as missing
 *     directories, and files already being made.
 * </p>
 */
public class FileDownload {
    private final URL target;
    public static final Logger logger = LogManager.getLogger(FileDownload.class);

    /**
     * Construct a FileDownload object from an URL.
     * @param desination An URL.
     */
    public FileDownload(URL desination) {
        logger.info("Preparing download from " + desination);
        this.target = desination; }

    /**
     * Construct a FileDownload object from a string url.
     * @param desination The string pointing to the download URL.
     * @throws MalformedURLException When the download location does not point to a valid url.
     */
    public FileDownload(String desination) throws MalformedURLException {
        logger.info("Preparing download from " + desination);
        this.target = new URL(desination);
    }

    /**
     * Set the private conn field.
     *
     * @return
     * @throws IOException
     */
    private HttpURLConnection getConnection() throws IOException {
        HttpURLConnection conn = (HttpURLConnection) this.target.openConnection();
        return conn;
    }

    /**
     * Check whether a connection is usable.
     * @throws Exception
     */
    private void checkConnection(HttpURLConnection conn) throws Exception {
        int responseCode = conn.getResponseCode();
        if (responseCode != HttpURLConnection.HTTP_OK) {
            throw new Exception("Cannot open connection "
                    + this.target.toString() + " since the connection is not oke."
                    + " actual status is " + responseCode);
        }
    }

    /**
     * Determine the filename without downloading
     * @return The filename.
     */
    public File askFileName() throws Exception {
        HttpURLConnection conn = null;
        try {
            conn = getConnection();
            checkConnection(conn);
            return getFileName(conn);
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
    }

    /**
     * Determine the last modified date without downloading
     * @return The date on which the file was last modified.
     * @throws Exception
     */
    public Date askLastModified() throws Exception {
        HttpURLConnection conn = null;
        try {
            conn = getConnection();
            checkConnection(conn);
            return getLastModified(conn);
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
    }

    /**
     * Determine when the connection was last changed.
     * @param conn
     * @return Date when the connection was last changed.
     */
    private Date getLastModified(HttpURLConnection conn) {
        return new Date(conn.getLastModified());
    }

    private File getFileName(HttpURLConnection conn) {
        String fileName = "";
        String disposition = conn.getHeaderField("Content-Disposition");

        if (disposition != null) {
            int index = disposition.indexOf("filename=");
            if (index > 0) {
                fileName = disposition.substring(index + 10, disposition.length() - 1);
            }
        } else {
            fileName = this.target.toString().substring(this.target.toString().lastIndexOf("/") + 1);
        }
        return  new File(fileName);
    }

    private void logDownload() {
        logger.info("Downloading " + this.target);
    }

    /**
     * Download a file over the internet taking the filename from the internet
     * @param p The path where the file should be saved
     */
    private void downloader(Path p) throws Exception {
        HttpURLConnection conn = null;
        try {
            conn = getConnection();
            checkConnection(conn);
            logDownload();
            String folder = p.toString();
            File f = new File(folder + "/" + getFileName(conn));
            logger.info("saving to file name " + f);
            FileWrite writer = new FileWrite(f);
            writer.write(conn);
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
    }


    /**
     * Download a file over the internet taking the file name given to the method.
     * @param f File where the downloaded object should be stored.
     */
    private void downloader(File f) throws Exception {
        HttpURLConnection conn = null;
        try {
            conn = getConnection();
            checkConnection(conn);
            logDownload();
            FileWrite writer = new FileWrite(f);
            writer.write(conn);
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
    }



    private void checkOverWrite(File f) throws FileExistsException {
        if (f.exists()) {
            logger.info("The file exists");
            logger.warn("The file " + f + " already exists. Overwrite is not allowed.");
        }
    }

    /**
     * Download a file over the internet, taking the filename supplied to the method
     * @param destination Where the downloaded file should be written.
     * @param overwrite Whether the file should be overwritten.
     * @throws Exception When there is an error, such as when the file already exist and overwrite is set.
     */
    public void download(File destination, boolean overwrite) throws Exception {
        if (!overwrite) {
            checkOverWrite(destination);
            downloader(destination);
        }
        downloader(destination);
    }

    /**
     * Download a file over the internet, taking the internet filename
     * @param p The path to the folder where the file should be placed.
     * @param overwrite Whether the file should be overwritten.
     * @throws Exception When there is an error, such as when the file already exist and overwrite is set.
     */
    public void download(Path p, boolean overwrite) throws Exception {
        File destination = askFileName();
        File destinationFile = p.resolve(String.valueOf(destination)).toFile();
        if (!overwrite) {
            checkOverWrite(destinationFile);
            downloader(p);
        }
        downloader(p);
    }

    /**
     * Download a file regardless whether it exist to a path.
     * @param p The path to the folder where the file should be placed.
     * @throws Exception
     */
    public void download(Path p) throws Exception {
        downloader(p);
    }
}
