package nl.fairbydesign.backend.wrappers.io;

import org.apache.commons.io.FileExistsException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.nio.file.FileSystemException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Wrapper class to safely write files. From various sources.
 *
 * Currently, this writer can handle webconnections. It should be expanded for other connection types when needed.
 */
public class FileWrite {
    public static final Logger logger = LogManager.getLogger(FileWrite.class);
    private final File f;

    public FileWrite(File f) {
        this.f = f;
        createPathIfNotExist();
    }

    private void createPathIfNotExist() {
        try {
            logger.info("Making directory for " + this.f);
            if (!this.f.exists()) {
                boolean sucess = this.f.mkdirs();
                if (!sucess) {
                    throw new FileSystemException("directory of" + this.f + " could not be made");
                }
            } else {
                throw new FileExistsException(this.f + " exists!");
            }
        } catch (IOException e) {
            logger.info("No new directory made as " + this.f + " error: " + e);
        }
    }


    /**
     * Write an HttpURLConnection.
     * @param conn A Web connection.
     * @throws Exception
     */
    public void write(HttpURLConnection conn) throws Exception {
        try (
                InputStream is = conn.getInputStream()) {
            logger.info("Attempting write on " + this.f);
            Files.copy(is, this.f.toPath(),
                    StandardCopyOption.REPLACE_EXISTING);
        }  catch (Exception e) {
            throw new Exception("Error when writing "
                    + conn.toString()
                    + " to file "
                    + this.f.toString()
                    + " cause " + e);
        }
    }
}
