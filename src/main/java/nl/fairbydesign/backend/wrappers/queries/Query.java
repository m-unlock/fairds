package nl.fairbydesign.backend.wrappers.queries;

import com.google.common.io.Resources;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.NodeIterator;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;

enum QueryDataType {
    STRING,
    FILE,
}

/**
 * Running SPARQL queries on in memory graphs
 *
 * @description You should use setQueryData before running the query.
 *
 */
public class Query {

    Model data;
    String QueryDataString;
    QueryExecution exec;

    /**
     * Prepare a sparql query for a domain.
     * @param d The domain for which a query is to be made.
     */
    public Query(Domain d) {
        this.data = d.getRDFSimpleCon().getModel();
    }

    /**
     * Prepare a query from a jena model object.
     * @param m A jena model object.
     */
    public Query(Model m) {
        this.data = m;
    }

    /**
     * Set the query data using a string.
     * @param s A valid sparql query as a String.
     */
    public void setQueryData(String s) {
        this.QueryDataString = s;
    }

    /**
     * Set the query data using a file.
     * @param f A file holding a valid sparql query.
     * @throws IOException If the file cannot be read.
     */
    public void setQueryData(URL f) throws IOException {
        String fs = Resources.toString(f, Charset.defaultCharset());
        setQueryData(fs);
    }

    private void createQuery() {
        this.exec = QueryExecutionFactory.create(this.QueryDataString, this.data);
    }

    /**
     * Run the query loaded in the object
     * <p>
     * The ResultSet must be copied, using the ResultSetFactory.copyResults(result) statement before it can be used in
     * an iterator pattern.
     *
     * @return The result of the selection query.
     */
    public ResultSet runSelect() {
        createQuery();
        return this.exec.execSelect();
    }

    public boolean runAsk() {
        createQuery();
        return this.exec.execAsk();
    }

    /**
     * Run a query to obtain the value of a subject-property.
     * @param model The model to query.
     * @param subj The subject of the property.
     * @param pred The property to query.
     * @return The value of the subject-property pair.
     */
    public static NodeIterator getProperty(Model model, String subj, String pred) {
        Resource subjR = null;
        if (subj != null) {
            subjR = model.createResource(subj);
        }

        Property predR = null;
        if (pred != null) {
            predR = model.createProperty(pred);
        }

        return model.listObjectsOfProperty(subjR, predR);
    }
}
