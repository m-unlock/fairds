package nl.fairbydesign.backend.data.objects.metadata.regex;

/**
 * Representation of a Regex Term.
 */
public class RegexTerm {
    private final String baseRegex = ".+";
    private String short_hand;
    private String long_form;
    private String name;

    private String example;
    private String description ;

    public String getShort_hand() {
        return short_hand;
    }

    public void setShort_hand(String short_hand) {
        this.short_hand = short_hand;
    }

    public String getLong_form() {
        if (long_form == null) {
            long_form = baseRegex;
        }
        return long_form;
    }

    public void setLong_form(String long_form) {
        this.long_form = long_form;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }



    public String getExample() {
        return example;
    }

    public void setExample(String example) {
        this.example = example;
    }

    @Override
    public String toString() {
        return "RegexTerm{" +
                "long_form='" + long_form + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
