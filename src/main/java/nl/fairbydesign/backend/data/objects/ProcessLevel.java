package nl.fairbydesign.backend.data.objects;

public class ProcessLevel {
private int unprocessed;
private String name;
private int observationUnit;
    private int study;
    private int investigation;
    private int assay;
    private int sample;

    public ProcessLevel() {
    }

    public int getObservationUnit() {
        return observationUnit;
    }

    public void setObservationUnit(int observationUnit) {
        this.observationUnit = observationUnit;
    }

    public int getStudy() {
        return study;
    }

    public void setStudy(int study) {
        this.study = study;
    }

    public int getInvestigation() {
        return investigation;
    }

    public void setInvestigation(int investigation) {
        this.investigation = investigation;
    }

    public int getAssay() {
        return assay;
    }

    public void setAssay(int assay) {
        this.assay = assay;
    }

    public int getSample() {
        return sample;
    }

    public void setSample(int sample) {
        this.sample = sample;
    }

    public int getUnprocessed() {
        return unprocessed;
    }

    public void setUnprocessed(int unprocessed) {
        this.unprocessed = unprocessed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
