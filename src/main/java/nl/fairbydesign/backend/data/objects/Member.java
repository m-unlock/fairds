package nl.fairbydesign.backend.data.objects;

import java.util.UUID;

public class Member {
    private String email;
    private String firstName;
    private String lastName;
    private String organization;
    private String department;
    private String identifier;
    private String role;
    private String ORCID;

    public Member() {
        this.identifier = UUID.randomUUID().toString();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() { return firstName; }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getORCID() { return ORCID; }

    public void setORCID(String orcid) {
        this.ORCID = orcid;
    }
}
