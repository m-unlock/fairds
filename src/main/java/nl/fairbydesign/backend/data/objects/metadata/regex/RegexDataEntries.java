package nl.fairbydesign.backend.data.objects.metadata.regex;

/**
 * This enum describes the table of the excel file.
 *
 * The column names can be altered by changing the enum default variant `prettyName`. Indeces can be changed by altering
 * the `toIndex` method.
 */
public enum RegexDataEntries {
    SHORT_HAND("short hand form"),
    LONG_FORM("long form"),
    EXAMPLE("example"),
    NAME("name"),
    DESCRIPTION("description"),
    ;

    private final String prettyName;
    RegexDataEntries(String s) {
        this.prettyName = s;
    }

    @Override
    public String toString() {
        return prettyName;
    }

    public int toIndex() {
        switch (this) {
            case SHORT_HAND:
                return 0;
            case LONG_FORM:
                return 1;
            case EXAMPLE:
                return 2;
            case NAME:
                return 3;
            case DESCRIPTION:
                return 4;
        }
        return 0;
    }
}
