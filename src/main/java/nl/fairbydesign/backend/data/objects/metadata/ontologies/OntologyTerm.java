package nl.fairbydesign.backend.data.objects.metadata.ontologies;


import java.io.File;

/**
 * An ontology term
 */
public class OntologyTerm {
    // TODO list
    // 1) I must find out a way to make a dynamic sparql query to the ontology in question.

    private final File ontologyFile;
    private final String term;
    private final String constraint;
    private final Ontology ontologies;

    /**
     * Instantiate a new ontology term
     *
     * @param ontologyFile Ontology to which the term belongs, for example
     *                     `./fairds_storage/environmentontology/envo/master/envo.owl.dir`.
     * @param term Term to be checked, for example `field`.
     * @param constraint The condition the term must forfill.
     * @param ontologies The ontologies to be searched for the term.
     */
    public OntologyTerm(File ontologyFile, String term, String constraint, Ontology ontologies) {
        this.ontologyFile = ontologyFile;
        this.term = term;
        this.constraint = constraint;
        this.ontologies = ontologies;
    }

    /**
     * Check the ontology term.
     *
     * @return `true` if the ontology term is allowed, `false` otherwise.
     */
    public boolean checkTerm() {
        return false;
    }

    public String getIRI() {
        return "NOTHING";
    }

}
