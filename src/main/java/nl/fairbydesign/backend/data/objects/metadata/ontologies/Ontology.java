package nl.fairbydesign.backend.data.objects.metadata.ontologies;

import nl.fairbydesign.backend.wrappers.queries.Query;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * The ontology used by the FAIRDS validation
 *
 *  Here the ontologies loaded in by the current data set are managed.
 */
public class Ontology {
    private final HashMap<File, Domain> ontologies;
    public static final Logger logger = LogManager.getLogger(Ontology.class);
    private String message = "";

    public Ontology() {
        this.ontologies = new HashMap<File, Domain>();
    }

    /**
     * Load an extra ontology into the current set of ontologies.
     *
     * @param name The path of the ontology on disk.
     * @exception Exception When the `name` does not point to a valid ontology.
     */
    public void loadOntology(File name) throws Exception {
        if (this.ontologies.containsKey(name)) {
            return;
        }
        logging(name.toString()  + " Ontology is being added to the set.");
        Domain ontologyDomain = new Domain("file://" + name);
        this.ontologies.put(name, ontologyDomain);
    }

    /**
     * Get a specific ontology
     *
     * @param name The name of the ontology as a valid file.
     * @return The ontology requested.
     */
    public Domain get(File name) {
        Domain requestedOntology = this.ontologies.get(name);
        if (requestedOntology == null) {
            logging("This ontology has not yet been loaded!");
        }
        return requestedOntology;
    }


    /**
     * Check whether a term exists within an ontology
     * @param ontology Ontology to be searched.
     * @param term term to be searched.
     * @param term_type Type or property of the term. This is an object that is connected to the term.
     * @implNote By default, if `term_type` is left blank, rdfs:label will be used as the type of the term.
     * @throws Error When the ontology that is referred to in `ontology` has not yet been loaded.
     * @return `true` if the term exists, `false` if the term does not exist.
     */
    public boolean checkTerm(File ontology, String term, String term_type) throws Error {
        if (term_type.isEmpty()) {
            term_type = "http://www.w3.org/2000/01/rdf-schema#label";
        }

        checkKey(ontology);

        Property rdfslabel = this.ontologies.get(ontology).getRDFSimpleCon().getModel().createProperty(term_type);
        ResIterator resIterator = this.ontologies.get(ontology).getRDFSimpleCon().getModel().listSubjectsWithProperty(rdfslabel, term);
        if (resIterator.hasNext()) {
            return true;
        } else {
            try {
                Iterable<ResultLine> resultLines = this.ontologies.get(ontology).getRDFSimpleCon().runQuery("getTerm.txt", true, term);
                if (resultLines.iterator().hasNext()) {
                    logging("Term found in the ontology");
                    return true;
                }
            } catch (Exception e) {
                logging("Error in running the query");
            }
        }
        return false;
    }


    /**
     * Check which terms in the ontology are closest to a given term.
     * @param ontology Ontology to be searched.
     * @param term term to be searched.
     * @param term_type Type or property of the term. This is an object that is connected to the term.
     * @implNote By default, if `term_type` is left blank, rdfs:label will be used as the type of the term.
     * @return A set of terms that are close to the term that is searched.
     * @throws Error When the ontology that is referred to in `ontology` has not yet been loaded.
     */
    public Set<String> findClosestTermsUsingSparql(File ontology, String term, String term_type) throws  Error {
        String lowercaseTerm = term.toLowerCase();
        if (term_type.isEmpty()) {
            term_type = "http://www.w3.org/2000/01/rdf-schema#label";
        }

        checkKey(ontology);

        String query =  "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \n"
                + "prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> \n"
                + "prefix owl: <http://www.w3.org/2002/07/owl#> "
                + "SELECT * WHERE {?s "
                + "<"
                + term_type
                +  ">"
                + "?o}";
        ResultSet result = sparqlQuery(ontology, query);
        Set<String> alikeStrings = new HashSet<String>();
        while ( result.hasNext() ) {
            QuerySolution qs =  result.next();
            String currentSubject = String.valueOf(qs.get("?s"));
            String currentObject = String.valueOf(qs.get("?o"));
            if (currentObject.toLowerCase().contains(lowercaseTerm)) {
                alikeStrings.add(currentObject);
            }
        }
        return alikeStrings;
    }

    public ResultSet sparqlQuery(File ontology, String query) throws Error {
        checkKey(ontology);
        Query sparql = new Query(this.ontologies.get(ontology));
        sparql.setQueryData(query);
        return sparql.runSelect();
    }

    /**
     * Find the uri of a term
     *
     * @param ontology Ontology  to be searched.
     * @param term Term for which the uri is to be retrieved.
     * @param term_type What connections need to be searched. rdf:label by default.
     * @return The uri as an url if availible, an empty string if the term is not
     * in the ontology.
     * @throws Error If the term is not in the ontology.
     */
    public String findUri(File ontology, String term, String term_type) throws Error {
        String lowercaseTerm = term.toLowerCase();
        if (term_type.isEmpty()) {
            term_type = "http://www.w3.org/2000/01/rdf-schema#label";
        }
        checkKey(ontology);

        if (checkTerm(ontology, term, term_type)) {

            String query =  "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \n"
                    + "prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> \n"
                    + "prefix owl: <http://www.w3.org/2002/07/owl#> "
                    + "SELECT * WHERE {?s "
                    + "<"
                    + term_type
                    +  ">"
                    + "?o}";
            ResultSet result = sparqlQuery(ontology, query);
            Set<String> alikeURIs = new HashSet<String>();
            while ( result.hasNext() ) {
                QuerySolution qs =  result.next();
                String currentSubject = String.valueOf(qs.get("?s"));
                String currentObject = String.valueOf(qs.get("?o"));
                if (currentObject.toLowerCase().equals(lowercaseTerm)) {
                    alikeURIs.add(currentSubject);
                }
            }
            return alikeURIs.iterator().next();
        } else {
            throw new Error("Term " + term + " not found in " + ontology.toString());
        }
    }

    /**
     * Check whether a given ontology is already loaded.
     *
     * @param ontology The ontology to be checked.
     * @throws Error
     */
    private void checkKey(File ontology) throws Error {
        if (!this.ontologies.containsKey(ontology)) {
            throw new Error("Ontology " + ontology.toString() + " has not yet been loaded!");
        }
    }


    @Override
    public String toString() {
        int size = this.ontologies.size();
        return String.format("Number of ontologies is " + size);
    }

    /**
     * Wrapper around the logger object
     *
     * @param message The Message to log.
     */
    private void logging(String message) {
        logger.info(message);
        this.message = this.message + "\n" + message;
    }
}
