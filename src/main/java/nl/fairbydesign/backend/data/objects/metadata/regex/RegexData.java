package nl.fairbydesign.backend.data.objects.metadata.regex;

import nl.fairbydesign.backend.metadata.MetadataParser;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A data structure to store information about the regular expressions.
 *
 * This class parses data from Excel into a dataset, and handles using this data to make long forms out of a shorthand
 * expression. For example "{text}{10,}" -> ".{10,}".
 */
public class RegexData {
    public static final Logger logger = LogManager.getLogger(RegexData.class);
    private final String sheetName = "regex";
    private Sheet sheet;
    private final HashMap<String, RegexTerm> lookupMap = new HashMap<>();

    /**
     * Setting up the regular expression data from an input stream.
     * @param inputStream The input to be used.
     * @throws Exception
     */
    private void setUp(InputStream inputStream) throws Exception {
        setWorkBook(inputStream);
        processData();
        replaceShortHandFormsInLongForm();
    }

    /**
     * Instantiate a regex data from system environment variable, if present, otherwise use the available excel file.
     */
    public RegexData() throws Exception {
        String path = System.getProperty("metadata_path");
        InputStream inputStream = null;
        if (path != null) {
            File inputFile = new File(path);
            inputStream = new FileInputStream(inputFile);
        } {
            inputStream = MetadataParser.obtainMetadataExcel();
        }
        setUp(inputStream);
    }

    /**
     * Construct a regex data object from a file.
     * @param inputFile The file with a valid excel fileinputstream.
     * @throws Exception
     */
    public RegexData(File inputFile) throws Exception {
        logger.info("Opening " + inputFile + " to parse.");
        InputStream inputStream = new FileInputStream(inputFile);
        setUp(inputStream);
    }

    /**
     * Checking if the workbook is valid; then setting it to the class.
     * @param inputStream Workbook to be set.
     */
    private void setWorkBook(InputStream inputStream) throws Exception {
        Workbook workbook = null;
        try {
            workbook = WorkbookFactory.create(inputStream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        List<String> sheetNames = new ArrayList<String>();
        for (int i=0; i<workbook.getNumberOfSheets(); i++) {
            sheetNames.add( workbook.getSheetName(i) );
        }
        if (workbook.getSheet(this.sheetName) == null) {
            throw new RuntimeException("The expected sheet name '"+ this.sheetName
                    +"' was not found in the workbook that was given.\n" +
                    "Sheets are " + sheetNames);
        }

        this.sheet = workbook.getSheet(this.sheetName);
    }

    /**
     * Load the table in the datastructures.
     *
     * Does not deal with typos in the heading values at all.
     */
    private void processData() {
        ArrayList<String> header = new ArrayList<>();
        this.sheet.iterator().forEachRemaining(row -> {
            if (row.getRowNum() % 10 == 0) {
                logger.debug("Processing row " + row.getRowNum() + " of " + this.sheet.getLastRowNum());
            }
            // Get the row and check if the first cell is part of the header
            if (row.getCell(0).getStringCellValue().equalsIgnoreCase(RegexDataEntries.SHORT_HAND.toString())) {
                row.cellIterator().forEachRemaining(cell -> header.add(cell.getStringCellValue().toLowerCase()));
                // If so, skip this row
            } else {
                // Turn it into an array
                String[] rowArray = new String[header.size()];
                for (int j = 0; j < header.size(); j++) {
                    if (row.getCell(j) != null) {
                        Cell cell = row.getCell(j);
                        if (cell.getCellType().equals(CellType.STRING))
                            rowArray[j] = row.getCell(j).getStringCellValue();
                        else if (cell.getCellType().equals(CellType.NUMERIC)) {
                            // Obtain number, remove .0 and if scientific notation turns into integer
                            String value = String.valueOf(cell.getNumericCellValue()).trim().replaceAll("\\.0$",
                                    "");
                            int val = new BigDecimal(value).intValue();
                            rowArray[j] = String.valueOf(val);
                        } else if (cell.getCellType().equals(CellType.BLANK)) {
                            // Do nothing
                        } else {
                            rowArray[j] = "";
                            logger.warn("Cell type not recognized: "
                                    + cell.getCellType()
                                    + " "
                                    + row.getRowNum()
                                    + " "
                                    + j
                                    + " "
                                    + StringUtils.join(rowArray, ", "));
                        }
                    }
                }
                // Check of rowArray[0] is empty
                if (rowArray[0] == null || rowArray[0].isEmpty()) return;
                // Process this into a term object
                RegexTerm term = new RegexTerm();
                term.setName(rowArray[RegexDataEntries.NAME.toIndex()]);
                term.setShort_hand(rowArray[RegexDataEntries.SHORT_HAND.toIndex()]);
                term.setLong_form(rowArray[RegexDataEntries.LONG_FORM.toIndex()]);
                term.setExample(rowArray[RegexDataEntries.EXAMPLE.toIndex()]);
                term.setDescription(rowArray[RegexDataEntries.DESCRIPTION.toIndex()]);

                this.lookupMap.put(term.getShort_hand(), term);

            }
        });
    }

    public String toString() {
        return this.lookupMap.toString();
    }

    public RegexTerm getRegex(String shortForm) throws Exception {
        keyCheck(shortForm);
        return this.lookupMap.get(shortForm);
    }


    /**
     * Check if the short from is available in the data structure.
     * @param shortForm Key to be checked.
     * @throws Exception If the key is not present.
     */
    private void keyCheck(String shortForm) throws Exception {
        if (!this.lookupMap.containsKey(shortForm)) {
            throw new Exception("The shortform " + shortForm + " does not exist.");
        }
    }

    /**
     * Break a String containing shorthand forms into a list of regular expressions of those shorthand forms.
     * @param input The shorthand forms.
     * @return the list of regular expressions for each shorthand forms.
     */
    private List<RegexTerm> extractShortHandForms(String input) throws Exception {
        List<RegexTerm> elements = new ArrayList<>();
        Pattern pattern = Pattern.compile("\\{([^|\\d]*?)\\}");
        Matcher matcher = pattern.matcher(input);
        while (matcher.find()) {
            String shortHandForm = matcher.group();
            try {
                elements.add(getRegex(shortHandForm));
            } catch (Exception e) {
                throw new RuntimeException("Error '" + e + "' for regular expression " + input );
            }
        }
        return elements;
    }

    private void replaceShortHandFormsInLongForm() throws Exception {
        for (RegexTerm term : this.lookupMap.values() ) {
            term.setLong_form(toLongForm(term.getLong_form()));
        }
    }


    /**
     * Get A string containing shorthand forms, and parse it to a string with longhand forms.
     * @param syntax The string with shorthand forms.
     * @return the string with the long form only.
     * @throws Exception If there are unknown shorhand forms
     */
    public String toLongForm(String syntax) throws Exception {
        List<RegexTerm> terms = extractShortHandForms(syntax);
        if (terms.isEmpty()) return syntax;
        for (RegexTerm term : terms) {
            syntax = syntax.replace(term.getShort_hand(), term.getLong_form());
        }
        if (syntax.contains(".+{")) {
            syntax = syntax.replace(".+{", ".{");
        }
        return syntax;
    }
}
