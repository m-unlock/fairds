package nl.fairbydesign.backend.data.objects;

import nl.fairbydesign.backend.Generic;

public class DiskUsage {
    private String group = "";

    private String resource = "";
    private long storageSize = 0L;

    public void setGroup(String group) {
        this.group = group;
    }

    public String getGroup() {
        return group;
    }


    public long getStorageSize() {
        return storageSize;
    }

    public void setStorageSize(long storageSize) {
        this.storageSize = storageSize;
    }

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    public String getStorageSizeFormat() {
        return Generic.formatSize(this.storageSize);
    }
}
