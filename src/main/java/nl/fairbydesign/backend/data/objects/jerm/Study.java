package nl.fairbydesign.backend.data.objects.jerm;

public class Study {
    private String uid;
    private String identifier;
    private String title;
    private String description;
    private org.jermontology.ontology.JERMOntology.domain.Study RDFObject;

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUid() { return uid; }

    public void setUid(String uid) { this.uid = uid;  }

    public void setRDFObject(org.jermontology.ontology.JERMOntology.domain.Study rdfObject) {
        this.RDFObject = rdfObject;
    }

    public org.jermontology.ontology.JERMOntology.domain.Study getRDFObject() {
        return RDFObject;
    }
}