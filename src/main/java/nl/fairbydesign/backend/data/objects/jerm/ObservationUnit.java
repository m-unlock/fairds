package nl.fairbydesign.backend.data.objects.jerm;

public class ObservationUnit {
    private String uid;
    private String identifier;
    private String title;
    private String description;
    private String newParam;
    private double observationUnits;

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getObservationUnits() {
        return observationUnits;
    }

    public void setObservationUnits(double observationUnits) {
        this.observationUnits = observationUnits;
    }

    public String getNewParam() { return newParam;  }

    public void setNewParam(String addParam) { this.newParam = newParam;  }

    public String getUid() { return uid; }

    public void setUid(String uid) { this.uid = uid;  }
}