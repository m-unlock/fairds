package nl.fairbydesign.backend.data.objects;

import java.util.Date;

public class File {
    private String path;
    private long size;
    private String storage = ";";
    private String resource;
    private String modificationDate;

    public File(String path, int i, String date) {
        this.path = path;
        this.size = i;
        this.modificationDate = date;
    }

    public File() {

    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getName() {
        return new java.io.File(path).getName();
    }
    public String getPath() {
        return path;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public long getSize() {
        return size;
    }

    public void setStorage(String storage) {
        this.storage += storage + ";";
    }

    public String getStorage() {
        return storage.replaceAll("^;|;$", "");
    }

    public void setModificationDate(String modificationDate) {
        this.modificationDate = modificationDate;
    }

    public String getModificationDate() {
        return modificationDate;
    }

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    public String getCalendarDate() {
        // Convert this.modificationDate string to a epoch datetime string
        long epoch = Long.parseLong(this.modificationDate) * 1000;
        Date date = new Date(epoch);
        return date.toString();
    }
}
