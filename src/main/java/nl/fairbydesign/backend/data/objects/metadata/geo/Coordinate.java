package nl.fairbydesign.backend.data.objects.metadata.geo;

/**
 * A class to manage the conversion of lat long to SPARQL coordinates.
 */
public class Coordinate {

    private final double latitude;
    private final double longitude;
    private final String geo_prefix = "http://www.opengis.net/ont/geosparql";


    /**
     * Instantiate a new coordinate object.
     *
     * @param longitude The longitude in decimal format.
     * @param latitude The latitude in decimal format.
     * @throws IllegalArgumentException When the latitude or longitude is out of bounds.
     */
    public Coordinate(double longitude, double latitude) throws IllegalArgumentException {
        if (latitude < -90.0 || latitude > 90.0) {
            throw new IllegalArgumentException("Input latitude is " +
                    latitude +
                    " where as it should be between -90 and +90.");
        }

        if (longitude < -180.0 || longitude > 180.0) {
            throw new IllegalArgumentException("Input longitude is " +
                    latitude +
                    " where as it should be between -180 and +180.");
        }

        this.latitude = latitude;
        this.longitude = longitude;
    }

    private String make_geosparql_point(String coordinate_system) {
        // Convertion according to the specsheet: https://opengeospatial.github.io/ogc-geosparql/geosparql11/spec.html.
        if (coordinate_system == "") {
            // No coordinate system specified
            return "Point(" + this.longitude+ " " +  this.latitude + ")";
        } else {
            // A coordinate system is specified.
            // TODO check if the input `coordinate_system` is a valid uri.
            return "<" + coordinate_system + "> " +
                    "Point(" + this.longitude+ " " +  this.latitude + ")";
        }
    }

    /** Get the well-known text literal version of the coordinate.
     *
     * @return The well-known text literal of the coordinate. Without the unit.
     */
    public String get_geosparql_point() {
        // Convertion according to the specsheet: https://opengeospatial.github.io/ogc-geosparql/geosparql11/spec.html.
        String coordinate_system = "";
        return make_geosparql_point(coordinate_system);
    }

    @Override
    public String toString() {
        return "Coordinate{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }
}
