package nl.fairbydesign.backend.data.objects.metadata.regex;

import nl.fairbydesign.Application;
import nl.fairbydesign.backend.metadata.Term;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.bind.ValidationException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import static org.apache.commons.lang3.StringUtils.containsIgnoreCase;

/**
 * This class can convert the regular expression values of metadata terms.
 *
 * As a side effect, the data type (file, date, &c) is also set.
 */
public class Regex {
    public static final Logger logger = LogManager.getLogger(Regex.class);

//    private final RegexData regexData = Application.regexData;

    /**
     * A metadata term
     * @param term A metadata term.
     */
    public static Term setRegex(Term term) {
        // logger.debug("Regex " + term.getLabel() + " being processed.");
        String baseRegex = ".*";

        String value = null;
        if (term.getSyntax() != null) {
            value = term.getSyntax();
        }

        String syntax = baseRegex;
        if (term.getSyntax() != null && !term.getSyntax().isEmpty()) {
            syntax = value;
        } else {
            logger.debug("No regex for " + " " + term.getLabel());
            return validateRegex(term, syntax);
        }

        // Special data objects...
        if (containsIgnoreCase(value, "{file}")) {
            syntax = syntax.replace("{file}", baseRegex);
            term.setFile(true);
        }
        if (containsIgnoreCase(value,"{date}")) {
            term.setDate(true);
            syntax = ".+";
        }
        if (containsIgnoreCase(value,"{datetime}")) {
            term.setDateTime(true);
            syntax = ".+";
        }

        if (syntax.contains("{")) {
            // Fix the syntax using the regex lookup
            for (String key : Application.termLookup.keySet()) {
                if (key.startsWith("{") && key.endsWith("}")) {
                    if (syntax.contains(key)) {
                        syntax = syntax.replace(key, Application.termLookup.get(key).getSyntax());
                    }
                }
            }
        }


        // Add units to regex if available
        if (term.getPreferredUnit() != null && !term.getPreferredUnit().isEmpty()) {
            syntax += " ?(" + term.getPreferredUnit() + ")";
        }

        return validateRegex(term, syntax);
    }


    /**
     * Validate the converted metadata object by checking if the example is correct.
     * @param term The metadata object to which the syntax belongs.
     * @param syntax The converted syntax.
     * @return The validated metadta object.
     */
    private static Term validateRegex(Term term, String syntax) {
        String example = term.getExample();
        // Validate
        try {
            Pattern r = Pattern.compile(syntax);
            term.setRegex(r);
            if (example != null) {
                Matcher m = r.matcher(example);
                if (!m.matches()) {
                    throw new ValidationException("Regex does not match '" + example + "' with regex '" + syntax + "'");
                }
                return term;
            } else {
                return term;
            }
        } catch (NullPointerException | PatternSyntaxException | ValidationException e) {
            if (example != null && !example.isEmpty()) {
                logger.error("Entity: " + term.getLabel() + " not parsable yet " + syntax + " with example: " + example);
                logger.error(e.getMessage());
                throw new RuntimeException(e);
            }
            return term;
        }
    }
}
