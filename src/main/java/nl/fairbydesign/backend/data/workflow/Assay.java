package nl.fairbydesign.backend.data.workflow;

public class Assay {
    private String description;
    private String identifier;
    private String platform;
    private String selection;
    private String source;
    private String strategy;
    private String type;
    private String aPackage;

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setPlatform(String platform) {

        this.platform = platform;
    }

    public String getPlatform() {
        return platform;
    }

    public void setSelection(String selection) {

        this.selection = selection;
    }

    public String getSelection() {
        return selection;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSource() {
        return source;
    }

    public void setStrategy(String strategy) {

        this.strategy = strategy;
    }

    public String getStrategy() {
        return strategy;
    }

    public void setType(String type) {

        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setaPackage(String aPackage) {
        this.aPackage = aPackage;
    }

    public String getaPackage() {
        return aPackage;
    }
}
