package nl.fairbydesign.backend.data.workflow;

import java.io.File;
import java.util.ArrayList;

public class IlluminaQuality {
    private String identifier;

    private int threads;
    final private int memory = 4000;
    private final ArrayList<File> forward_reads = new ArrayList<>();
    private final ArrayList<File> reverse_reads = new ArrayList<>();
    private final boolean skip_fastqc_before = true;
    private final boolean filter_rrna = true;
    private final ArrayList<File> filter_references = new ArrayList<>();
    private final boolean deduplicate = false;
    final private float kraken2_confidence = 0.0F;
    private final ArrayList<File> kraken2_database = new ArrayList<>();
    private boolean keep_reference_mapped_reads;
    private boolean prepare_reference;
    final private int step = 1;

    public IlluminaQuality() {

    }

    public int getThreads() {
        return threads;
    }

    public void setThreads(int threads) {
        this.threads = threads;
    }
}
