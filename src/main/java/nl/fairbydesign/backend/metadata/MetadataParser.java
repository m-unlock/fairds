package nl.fairbydesign.backend.metadata;

import nl.fairbydesign.Application;
import nl.fairbydesign.backend.data.objects.metadata.regex.Regex;
import nl.fairbydesign.backend.wrappers.io.FileDownload;
import nl.wur.ssb.RDFSimpleCon.Util;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.commons.io.FileExistsException;
import org.apache.commons.lang.StringUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.*;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.regex.Pattern;


/**
 * Parser to transform the excel file into objects including regex information based on metadata types.'
 * <p>
 *     Use this class by using the main method.
 * </p>
 */
public class MetadataParser extends Metadata {
    // TODO add documentation.
    public static final Logger logger = LogManager.getLogger(MetadataParser.class);
    static final String STORAGE_LOCATION = Application.commandOptions.storage;
    static File xlsxFile = new File(Application.commandOptions.storage +"/metadata.xlsx");
    static URL metadataOnlineLocation;

    static HashSet<String> ontologies = new HashSet<>();

    static {
        try {
            metadataOnlineLocation = new URL("http://download.systemsbiology.nl/unlock/metadata.xlsx");
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }


    /**
     * Generate the full regular expression from the shorthand notation.
     *
     * @param term The term to be converted.
     * @return The Term object with the shorthand replaced by actual regular expressions.
     */
    public static Term generateRegex(Term term) {
        if(term.getLabel() == null) {
            throw new RuntimeException("The metadata object does not have a label!\n");
        }

        // logger.debug("validating '" + term.getLabel() + "'");

        try {
            ontologyCheck(term);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }

        return Regex.setRegex(term);
    }

    /**
     * Check the ontology for a specific metadata term.
     *
     * @description As a side effect, the ontologies are attached to the terms.
     *
     * @param term
     * 
     * @return Term for which the metadata references are verified.
     */
    private static Term ontologyCheck(Term term) throws MalformedURLException {
        // Set regex ontology
        /*
        * In this method, the ontology gets added, if the URL is not malformed.
         */

        try {
            if (term.getSyntax() == null || term.getSyntax().isEmpty()) {
                return term;
            }
            java.net.URL urlString = new URL(term.getSyntax());
            File destination = getOntologyFile(urlString);
            writeOntology(destination);
            term.setOntology(new File(destination + ".dir"));
            term.setRegex(Pattern.compile("Use the labels defined in the ontology"));
        } catch (MalformedURLException e) {
            return term;
        } catch (FileExistsException e) {
            if (!ontologies.contains(term.getSyntax())) {
                logger.info("No metadata downloaded as " + term.getSyntax() + " already exists locally.");
                ontologies.add(term.getSyntax());
            }
            File destination = new File(Application.commandOptions.storage +"/" + new URL(term.getSyntax()).getFile().toLowerCase());
            term.setOntology(new File(destination + ".dir"));
            term.setRegex(Pattern.compile("Use the labels defined in the ontology"));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return term;
    }

    /**
     * A ontology to be written to disk as a model.
     *
     * @param ontologyFile The name of the ontology
     */
    public static void writeOntology(File ontologyFile) throws Exception {
        File dir = new File(ontologyFile + ".dir");
        if (!dir.exists()) {
            dir.mkdirs();
            // TEST
            // Create an empty Jena model
            Model model = ModelFactory.createDefaultModel();
            // Use FileManager to read RDF file directly into the model
            // FileManager.get().readModel(model, ontologyFile.getAbsolutePath());
            Lang lang = RDFDataMgr.determineLang(ontologyFile.getName(), null, null);
            RDFDataMgr.read(model, new FileInputStream(ontologyFile), lang);

            Domain domain = new Domain("file://" + ontologyFile + ".dir");
            // Load file in memory and add to disk store
            // Domain tempDomain = new Domain("file://" + ontologyFile);
            domain.getRDFSimpleCon().getModel().add(model);
            // tempDomain.close();
            model.close();
        }
    }

    @NotNull
    public static File getOntologyFile(URL urlString) throws Exception {
        URL url = new URL(urlString.toString());
        File destination = new File(Application.commandOptions.storage +"/" + url.getFile().toLowerCase());
        Path p = Paths.get(STORAGE_LOCATION + Paths.get(url.getPath()).getParent().toString().toLowerCase());
        FileDownload fd = new FileDownload(url);
        fd.download(p, false);
        return destination;
    }


    /**
     * Create a hashmap of metadata objects.
     *
     * @return objects per sheet in the metadata excel file
     */
    public static void createMetaDataHashMap() {
        excelMetadataValidator(obtainMetadataExcel());
    }

    /**
     * Check where the most recent version of the metadata file can be found.
     * @return The input stream of the metadata excel sheet.
     */
    public static InputStream obtainMetadataExcel() {
        // Metadata has a default path but can be changed using boot arguments
        InputStream is;
        xlsxFile = new File(Application.commandOptions.metadata);

        logger.info("Checking if " + xlsxFile.getAbsolutePath() + " exists");
        // Check if it still exists and otherwise ensure the file is placed there
        if (xlsxFile.exists()) {
            // Check the data
            logger.info("Using local excel file: " + xlsxFile.getAbsolutePath());
            try {
                is = new FileInputStream(xlsxFile);
            } catch (FileNotFoundException e) {
                logger.info("Using internal excel file");
                is = MetadataParser.class.getClassLoader().getResourceAsStream(xlsxFile.getName());
            }
        } else {
            // else load xlsx from jar file and copy to local
            try {
                is = Util.getResourceFile(xlsxFile.getName());
                if (is != null) {
                    // Copy the resource Excel file to the fairds storage location.
                    logger.info("Using internal excel file " + xlsxFile.getAbsolutePath());
                    xlsxFile.getParentFile().mkdirs();
                    Files.copy(is, xlsxFile.toPath());
                    is.close();
                }
                is = new FileInputStream(xlsxFile);
            } catch (IOException e) {
                logger.error(e);
                throw new RuntimeException(e);
            }
        }
        return is;
    }

    public static void excelMetadataValidator(InputStream is) {
        Workbook workbook;
        try {
            workbook = WorkbookFactory.create(is);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        Application.termLookup.clear();
        // Process the regex sheet
        logger.debug("Processing regex sheet");

        processRegexSheet(workbook.getSheet("regex"));

        // Get the sheet with terms
        logger.debug("Processing terms sheet");
        termSheetProcessor(workbook);

        // Process all other sheets
        sheetProcessor(workbook);

    }

    private static void termSheetProcessor(Workbook workbook) {
        Sheet termSheet = workbook.getSheet("terms");
        ArrayList<String> header = new ArrayList<>();
        termSheet.rowIterator().forEachRemaining(row -> {
            if (row.getRowNum() % 10 == 0)
                logger.debug("Processing row " + row.getRowNum() + " of " + termSheet.getLastRowNum());
            // Get the row and check if the first cell is part of the header
            if (row.getCell(0) == null) {
                logger.warn("Empty row " + row.getRowNum());
            }
            // Get the row and check if the first cell is part of the header
            if (row.getCell(0).getStringCellValue().equalsIgnoreCase("Item label")) {
                row.cellIterator().forEachRemaining(cell -> header.add(cell.getStringCellValue().toLowerCase()));
                // If so, skip this row
            } else {
                // Turn it into an array
                String[] rowArray = new String[header.size()];
                for (int j = 0; j < header.size(); j++) {
                    if (row.getCell(j) != null) {
                        Cell cell = row.getCell(j);
                        if (cell.getCellType().equals(CellType.STRING))
                            rowArray[j] = row.getCell(j).getStringCellValue();
                        else if (cell.getCellType().equals(CellType.NUMERIC)) {
                            // Obtain number, remove .0 and if scientific notation turns into integer
                            String value = String.valueOf(cell.getNumericCellValue()).trim().replaceAll("\\.0$", "");
                            int val = new BigDecimal(value).intValue();
                            // Check for percentage
                            String dataFormatString = cell.getCellStyle().getDataFormatString();
                            if (dataFormatString.contains("%")) {
                                double percentageValue = cell.getNumericCellValue() * 100;
                                System.out.println("Percentage Value: " + percentageValue + "%");
                                rowArray[j] = String.valueOf(percentageValue) + "%";
                            } else {
                                rowArray[j] = String.valueOf(val);
                            }
                        } else if (cell.getCellType().equals(CellType.BLANK)) {
                            // Do nothing
                        } else {
                            rowArray[j] = "";
                            logger.warn("Cell type not recognized: " + cell.getCellType() +" " + row.getRowNum() + " " + j + " " + StringUtils.join(rowArray, ", "));
                        }
                    }
                }
                // Check of rowArray[0] is empty
                if (rowArray[0] == null || rowArray[0].isEmpty()) return;

                // Process this into a term object
                Term term = new Term();
                // Item label	Requirement	Value syntax	Example	Preferred unit	URL	Definition
                term.setLabel(rowArray[header.indexOf("item label")].toLowerCase().strip());
                term.setRequirement(Requirement.OPTIONAL.name()); // Default value
                term.setSyntax(rowArray[header.indexOf("value syntax")]);
                term.setExample(rowArray[header.indexOf("example")]);
                term.setPreferredUnit(rowArray[header.indexOf("preferred unit")]);
                term.setURL(rowArray[header.indexOf("url")]);
                term.setDefinition(rowArray[header.indexOf("definition")]);
                term = generateRegex(term);
                // Validate the term
                term.validate();
                if (Application.termLookup.containsKey(term.getLabel())) {
                    throw new RuntimeException("Term already exists: " + term.getLabel());
                }
                Application.termLookup.put(term.getLabel(), term);
            }
        });
    }

    private static void sheetProcessor(Workbook workbook) {
        workbook.sheetIterator().forEachRemaining(sheet -> {
            // Skip the terms sheet
            if (sheet.getSheetName().equalsIgnoreCase("terms")) return;
            if (sheet.getSheetName().equalsIgnoreCase("regex")) return;
            logger.info("processing sheet name " + sheet.getSheetName());
            // Obtain the header from the first row
            ArrayList<String> headerRow = new ArrayList<>();
            sheet.getRow(0).cellIterator().forEachRemaining(cell -> headerRow.add(cell.getStringCellValue().toLowerCase()));
            // Check if header row contains the required fields
            if (!headerRow.contains("# level")) {
                throw new RuntimeException("Sheet " + sheet.getSheetName() + " does not contain the required field # level");
            }
            if (!headerRow.contains("package name")) {
                throw new RuntimeException("Sheet " + sheet.getSheetName() + " does not contain the required field package name");
            }
            if (!headerRow.contains("item label")) {
                throw new RuntimeException("Sheet " + sheet.getSheetName() + " does not contain the required field item label");
            }
            if (!headerRow.contains("requirement")) {
                throw new RuntimeException("Sheet " + sheet.getSheetName() + " does not contain the required field requirement");
            }
            // Iterate over all rows
            sheet.rowIterator().forEachRemaining(row -> {
                try {
                    // If the row is empty skip it
                    if (row.getCell(0) == null) return;

                    // # Level	Package name	Item label	Requirement
                    Metadata metadata = new Metadata();
                    // Check if all the cells in the row are not empty

                    String level = row.getCell(headerRow.indexOf("# level")).getStringCellValue();
                    if (level.startsWith("#")) return;
                    Application.metadataSet.putIfAbsent(level, new ArrayList<>());
                    metadata.setPackageName(row.getCell(headerRow.indexOf("package name")).getStringCellValue());
                    metadata.setLabel(row.getCell(headerRow.indexOf("item label")).getStringCellValue().toLowerCase().strip());
                    String requirement = row.getCell(headerRow.indexOf("requirement")).getStringCellValue();
                    // A simple in between header check
                    if (requirement.equalsIgnoreCase("requirement")) return;
                    metadata.setRequirement(requirement);
                    // Check if the metadata label is not empty
                    if (metadata.getLabel().isEmpty()) return;

                    // Copy all over from term
                    if (Application.termLookup.containsKey(metadata.getLabel())) {
                        Application.termLookup.get(metadata.getLabel());
                    } else {
                        throw new RuntimeException("Term not found in the terms sheet: '" + metadata.getLabel() + "'");
                    }
                    Application.metadataSet.get(level).add(metadata);
                } catch (Exception e) {
                    logger.error("Error in sheet " + sheet.getSheetName() + " row " + row.getRowNum() + " " + e.getMessage());
                    // Print line number of exception
                    throw new RuntimeException(e);
                }
            });
        });
        // Check application metadataSet
        if (Application.metadataSet.isEmpty()) {
            throw new RuntimeException("No metadata found in the metadata excel sheet");
        }
        // Done ... all sheet objects should be as before
        Application.metadataSet.forEach((key, value) -> {
            logger.info("Sheet " + key + " has " + value.size() + " entries");
        });
    }

    /**
     * Process the regex sheet and add the regex to the term lookup.
     * @param regexSheet The sheet with the regex information.
     */
    public static void processRegexSheet(Sheet regexSheet) {
        regexSheet.rowIterator().forEachRemaining(row -> {
            // Skip the first row
            if (row.getRowNum() == 0) {
                return;
            }
            if (row.getCell(0) == null) {
                return;
            }
            // Get the row and check if the first cell is part of the header
            String shortHandForm = row.getCell(0).getStringCellValue();
            String regex = row.getCell(1).getStringCellValue();
            String example = "";
            logger.debug("Processing regex " + shortHandForm + " " + regex);
            if (row.getCell(2) == null) {
                // No validation
            } else if (row.getCell(2).getCellType().equals(CellType.STRING)) {
                example = row.getCell(2).getStringCellValue();
            } else if (row.getCell(2).getCellType().equals(CellType.BOOLEAN)) {
                example = String.valueOf(row.getCell(2).getBooleanCellValue());
            } else if (row.getCell(2).getCellType().equals(CellType.NUMERIC)) {
                // Check if the cell is a date
                if (DateUtil.isCellDateFormatted(row.getCell(2))) {
                    // Create a SimpleDateFormat object with the desired format
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                    // Format the Date object into a string
                    example = dateFormat.format(DateUtil.getJavaDate(row.getCell(2).getNumericCellValue()));
                    if (example.endsWith("T00:00:00")) {
                        example = example.replace("T00:00:00", "");
                    }
                } else {
                    // Check if the cell is a percentage
                    CellStyle style = row.getCell(2).getCellStyle();
                    if (style != null && style.getDataFormatString().endsWith("%")) {
                        example = row.getCell(2).getNumericCellValue() * 100 + "%";
                    } else {
                        // Obtain number, remove .0 and if scientific notation turns into integer
                        example = String.valueOf(row.getCell(2).getNumericCellValue());
                        // Strip .0 from the end
                        example = example.replaceAll("\\.0$", "");
                    }
                }
            } else {
                throw new RuntimeException("Cell type not recognized: " + row.getCell(2).getCellType());
            }

            // Check if the shorthand form is not empty
            if (shortHandForm.isEmpty()) return;
            // Check if the regex form is not empty
            if (regex.isEmpty()) return;
            // Transform {elements} into regex using previously parsed regex
            for (String key : Application.termLookup.keySet()) {
                if (regex.contains(key)) {
                    regex = regex.replace(key, Application.termLookup.get(key).getSyntax());
                }
            }
            // check if the example is not empty
            if (!example.isEmpty()) {
                // Check if the example matches the regex
                if (!Pattern.compile(regex).matcher(example).matches()) {
                    throw new RuntimeException("Regex does not match example: " + regex + " '" + example + "' from " + shortHandForm);
                }
            }
            // Create the regex object and add it to the metadata set as a regex lookup element
            Term term = new Term();
            term.setLabel(shortHandForm);
            term.setRegex(Pattern.compile(regex));
            term.setSyntax(regex);
            term.setRequirement(Requirement.IGNORE.name());
            Application.termLookup.put(shortHandForm, term);
        });
    }
}
