package nl.fairbydesign.backend.metadata;

import nl.fairbydesign.backend.parsers.ExcelValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.Normalizer;
import java.util.regex.Pattern;

import static nl.fairbydesign.backend.parsers.ExcelValidator.iriCorrector;

public class Term {
    public static final Logger logger = LogManager.getLogger(ExcelValidator.class);


    private String label;
    private String requirement;
    private String syntax;
    private String example;
    private String preferredUnit;
    private String URL;
    private String definition;
    private File ontology;
    private Pattern regex;
    private boolean file;
    private boolean date;
    private boolean dateTime;

    public void setLabel(String label) {
        this.label = label.toLowerCase();
    }

    public String getLabel() {
        return label;
    }

    public void setRequirement(String requirement) {
        this.requirement = requirement;
    }

    public String getRequirement() {
        return requirement;
    }

    public void setSyntax(String syntax) {
        if (syntax != null)
            this.syntax = syntax;
    }

    public String getSyntax() {
        return syntax;
    }

    public void setExample(String example) {

        this.example = example;
    }

    public String getExample() {
        return example;
    }

    public void setPreferredUnit(String preferredUnit) {
        this.preferredUnit = preferredUnit;
    }

    public String getPreferredUnit() {
        return preferredUnit;
    }

    public void setURL(String url) {
        if (url == null || url.isBlank()) {
            // Parsing elements from the standardisation and also optionally added by the user not in the metadata excel sheet
            // Processing the header name as it can contain non-acceptable chars
            String headerItem = iriCorrector(this.label);

            // Replace non ascii characters by closed character
            headerItem = Normalizer.normalize(headerItem, Normalizer.Form.NFD);
            headerItem = headerItem.replaceAll("[^\\x00-\\x7F]", "");
            headerItem = headerItem.replaceAll("/", "_");

            // It is null if the user created this column by itself
            url = "http://fairbydesign.nl/ontology/" + headerItem.toLowerCase();
            logger.debug("No predicate defined for '" + this.label + "' using " + url);
        } else {
            try {
                new URL(url);
                logger.debug("Using '" + url + "' for '" + this.label + "'");
            } catch (MalformedURLException e) {
                logger.error("URL is malformed: " + url);
                throw new RuntimeException(e);
            }
        }
        this.URL = url;
    }

    public String getURL() {
        return URL;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }

    public String getDefinition() {
        return definition;
    }

    public void setOntology(File ontology) {
        this.ontology = ontology;
    }

    public File getOntology() {
        return ontology;
    }

    public void setRegex(Pattern regex) {
        this.regex = regex;
    }

    public Pattern getRegex() {
        return regex;
    }

    public void setFile(boolean file) {
        this.file = file;
    }

    public boolean isFile() {
        return file;
    }

    public void setDate(boolean date) {
        this.date = date;
    }

    public boolean isDate() {
        return date;
    }

    public void setDateTime(boolean dateTime) {
        this.dateTime = dateTime;
    }

    public boolean isDateTime() {
        return dateTime;
    }

    public void validate() {
        if (this.example != null && !this.example.isEmpty() && this.regex != null) {
            boolean exampleCorrect = this.regex.matcher(this.example).matches();
            if (!exampleCorrect) {
                throw  new RuntimeException("The example for term '" + this.label + "' is not correct!\n"+
                        this.regex + " was the regular expression while the example is '" + this.example +"'");
            }
        }
    }
}
