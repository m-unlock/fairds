package nl.fairbydesign.backend.metadata;

import nl.fairbydesign.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Serializable;

/**
 * Metadata object based on MIXS (excel) file adapted to ISA structure.
 *
 *  In this class, the column names of each row of the metadata.xlsx sheet are stored. There are setters
 * and getters for each field.
 */
public class Metadata implements Serializable {

    public static final Logger logger = LogManager.getLogger(Metadata.class);

    public Term getTerm() {
        return Application.termLookup.get(this.label);
    }

    public enum Requirement {
        MANDATORY, RECOMMENDED, OPTIONAL, IGNORE, PREFERRED, REQUIRED
    }

    private static final long serialVersionUID = 7208928741154500357L;

    private String sheetName;
    private String packageName;
    private Requirement requirement;
    private String label;
    private String sessionID = "no_session";
    private Term term;

    public Metadata(Metadata m) {
        // to make a copy object of metadata without the link to the original object
        this.sheetName = m.sheetName;
        this.packageName = m.packageName;
        this.requirement = m.requirement;
        this.label = m.label;
        this.sessionID = m.sessionID;
    }

    public Metadata() {

    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String stringCellValue) {
        this.packageName = stringCellValue;
    }

    public void setRequirement(String requirement) {
        if (requirement != null && !requirement.isEmpty())
            this.requirement = Requirement.valueOf(requirement.toUpperCase());
        else {
            if (!this.label.isBlank()) {
                logger.warn("No requirement found for " + this.label + " setting to optional");
            }
            this.requirement = Requirement.OPTIONAL;
        }
    }

    public Requirement getRequirement() {
        return requirement;
    }

    public void setLabel(String label) {
        this.label = label.toLowerCase();
    }

    /**
     * @return The label represents the human readable version of the field.
     */
    public String getLabel() {
        return label;
    }

    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }

    public String getSessionID() {
        return sessionID;
    }

    public String getSheetName() {
        return sheetName;
    }

    public void setSheetName(String sheetName) {
        this.sheetName = sheetName;
    }

    @Override
    public String toString() {
        return "Metadata{" +
                "sheetName='" + sheetName + '\'' +
                ", packageName='" + packageName + '\'' +
                ", requirement=" + requirement +
                ", label='" + label + '\'' +
                ", sessionID='" + sessionID + '\'' +
                ", term=" + term +
                '}';
    }
}
