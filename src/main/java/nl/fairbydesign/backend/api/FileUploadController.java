package nl.fairbydesign.backend.api;

import nl.fairbydesign.Application;
import nl.fairbydesign.backend.parsers.ExcelValidator;
import org.apache.poi.openxml4j.exceptions.NotOfficeXmlFileException;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

@RestController
@RequestMapping("/api")
public class FileUploadController {

    private static final Logger logger = LoggerFactory.getLogger(FileUploadController.class);

    @PostMapping("/upload")
    public ResponseEntity<byte[]> uploadFile(@RequestParam("file") MultipartFile file) {
        if (!Application.config.getMenus().isApi()) {
            logger.info("API is disabled");
            return new ResponseEntity<>("API is disabled".getBytes(StandardCharsets.UTF_8), HttpStatus.FORBIDDEN);
        }

        logger.info("Received file: {}", file.getOriginalFilename());
        if (file.isEmpty()) {
            logger.error("Uploaded file is empty");
             return new ResponseEntity<>("File is empty".getBytes(StandardCharsets.UTF_8), HttpStatus.BAD_REQUEST);
        }

        try (InputStream inputStream = file.getInputStream()) {
            XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
            logger.info("File received and processed");
            // Perform validation logic
            String content = validateWorkbook(workbook, file.getName());
            // Content if it is a file path it passed validation
            if (new File(content).exists()) {
                logger.info("File is valid");
                // Sent a text message back in a .txt file for testing
                // Read the content of the file
                byte[] content_bytes;
                try {
                    content_bytes = Files.readAllBytes(new File(content).toPath());
                } catch (IOException e) {
                    logger.error("Error reading file content", e);
                     return new ResponseEntity<>("Error reading file content".getBytes(StandardCharsets.UTF_8), HttpStatus.INTERNAL_SERVER_ERROR);
                }
                logger.info("File content: {}", content_bytes.length + " bytes from " + file.getOriginalFilename() + " received");
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.TEXT_PLAIN);
                headers.setContentDispositionFormData("attachment", "results.ttl");
                return new ResponseEntity<>(content_bytes, headers, HttpStatus.OK);
            } else {
                logger.error("File content is invalid");
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.TEXT_PLAIN);
                headers.setContentDispositionFormData("attachment", "failure.txt");
                return new ResponseEntity<>(content.getBytes(StandardCharsets.UTF_8), headers, HttpStatus.CONFLICT);
            }
        } catch (NotOfficeXmlFileException e) {
            logger.error("Uploaded file is not an Office Open XML file", e);
            return new ResponseEntity<>("Uploaded file is not an Office Open XML file".getBytes(StandardCharsets.UTF_8), HttpStatus.BAD_REQUEST);
        } catch (IOException e) {
            logger.error("Error processing file", e);
             return new ResponseEntity<>("Error processing file".getBytes(StandardCharsets.UTF_8), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private String validateWorkbook(XSSFWorkbook workbook, String fileName) {
        // Implement your validation logic here
        // For example, check if the workbook has the required sheets, columns, etc.
        logger.info("Validating workbook");
        ExcelValidator validator = new ExcelValidator();
        try {
            String rdfPath = validator.validate(null, workbook, null, fileName, "turtle");
            logger.info("Validation successful");
            return rdfPath;
        } catch (Exception e) {
            return e.getMessage();
        }
    }
}