package nl.fairbydesign.backend;

import com.vaadin.flow.component.Html;
import nl.fairbydesign.Application;
import nl.wur.ssb.RDFSimpleCon.Util;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Scanner;

import static org.apache.commons.lang3.math.NumberUtils.isCreatable;

public class WebGeneric {
    private static final Logger logger = LoggerFactory.getLogger(WebGeneric.class);

    public static Html getTextFromResource(String filePath) {
        String helpText = new Scanner(getFileContentFromResources(filePath)).useDelimiter("\\Z").next();
        // Set version in help text
        if (helpText.contains("VERSION") && Application.version != null)
            helpText = helpText.replaceAll("VERSION", Application.version);
        Html text = new Html("<span>" + helpText + "</span>");
        return text;
    }

    /**
     * Reads file from local internal resource
     *
     * @param fileName
     * @return
     * @throws IOException
     */
    public static String getFileContentFromResources(String fileName) {
        logger.debug("Parsing " + fileName);
        StringWriter writer = new StringWriter();
        try (InputStream inputStream = Util.getResourceFile(fileName)) {
            IOUtils.copy(inputStream, writer, "UTF-8");
            String theString = writer.toString();
            return theString;
        } catch (IOException e) {
            // Handle the exception
            logger.debug(e.getMessage());
            return "Reading failed for " + fileName;
        }
    }


    public static String getEdamFormat(String fileName) {
        // Always remove compression end when gz
        fileName = fileName.replaceAll(".gz$", "");

        String extension = FilenameUtils.getExtension(fileName);

        logger.info("File extension " + extension + " detected");
        if (fileName.startsWith("http")) {
            return fileName;
        } else if (extension.matches("bam")) {
            return "http://edamontology.org/format_2572";
        } else if (extension.matches("(fq|fastq)")) {
            return "http://edamontology.org/format_1930";
        } else if (extension.matches("(fa|fasta)")) {
            return "http://edamontology.org/format_1929";
        } else {
            logger.error("Unknown file format " + extension);
            return null;
        }
    }

    public static boolean checkNotEmpty(Row row) {
        Iterator<Cell> rowIter = row.iterator();
        while (rowIter.hasNext()) {
            Cell cell = rowIter.next();
            if (!cell.getCellType().equals(CellType.BLANK))
                return true;
        }
        return false;
    }

    /**
     * @param cell
     * @return
     */
    public static String getStringValueCell(Cell cell) {
        if (cell == null) {
            logger.error("Cell is empty");
            return "";
        }
        CellType type = cell.getCellType();
        String value = "";
        if (type.equals(CellType.FORMULA)) {
            if(cell.getCellType() == CellType.FORMULA) {
                logger.debug("Formula is " + cell.getCellFormula());
                switch(cell.getCachedFormulaResultType()) {
                    case NUMERIC:
                        logger.debug("Last evaluated as: " + cell.getNumericCellValue());
                        // System.("NUMERIC VALUE: " + cell.getNumericCellValue());
                        value = String.valueOf(cell.getNumericCellValue()).trim();
                        break;
                    case STRING:
                        logger.debug("Last evaluated as \"" + cell.getRichStringCellValue() + "\"");
                        value = cell.getStringCellValue().trim();
                        break;
                }
            }
        } else if (type.equals(CellType.NUMERIC)) {
            // Transforms all values ending with .0 to a full integer
            if (DateUtil.isCellDateFormatted(cell)) {
                Date date = DateUtil.getJavaDate(cell.getNumericCellValue());
                String pattern = "yyyy-MM-dd";
                DateFormat df = new SimpleDateFormat(pattern);
                value = df.format(date);
            } else {
                value = String.valueOf(cell.getNumericCellValue()).trim().replaceAll("\\.0$", "");
                if (value.contains("E")) {
                    long doubleValue = Double.valueOf(value).longValue();
                    value = String.valueOf(doubleValue);
                }
            }
        } else if (type.equals(CellType.STRING)) {
            value = cell.getStringCellValue().trim();
            // Check if value is not sneakily still a numeric value
            boolean numeric = isCreatable(value);
            if (numeric)
                value = value.trim().replaceAll("\\.0$", "");
        } else if (type.equals(CellType.BLANK)) {
            value = "";
        } else if (type.equals(CellType.BOOLEAN)) {
            boolean cellValue = cell.getBooleanCellValue();
            value = "false";
            if (cellValue)
                value = "true";
        } else {
            logger.info("Celltype not captured " + type);
        }
        return value.trim();
    }
}
