package nl.fairbydesign.backend.manager;

import com.vaadin.flow.component.textfield.TextArea;
import nl.fairbydesign.Application;
import nl.fairbydesign.backend.credentials.Credentials;
import nl.fairbydesign.backend.data.objects.File;
import nl.fairbydesign.backend.irods.Data;
import nl.fairbydesign.backend.metadata.Metadata;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.rdf.model.NodeIterator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.pub.CollectionAO;
import org.irods.jargon.core.pub.domain.AvuData;
import org.irods.jargon.core.pub.io.IRODSFile;
import org.irods.jargon.core.query.GenQueryBuilderException;
import org.jermontology.ontology.JERMOntology.domain.*;
import org.purl.ppeo.PPEO.owl.domain.observation_unit;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import static nl.fairbydesign.backend.wrappers.queries.Query.getProperty;

public class Manager {
    private Credentials credentials;
    private final Logger logger = LogManager.getLogger(Metadata.class);
    private String message = "";
    private TextArea logArea;


    public void process(Credentials credentials, File file, TextArea logAreaX) throws Exception {
        this.logArea = logAreaX;
        this.credentials = credentials;
        // Create a temp file
        java.io.File tempFile = new java.io.File(Application.commandOptions.storage + "/manager/" + new java.io.File(file.getPath()).getName());

        if (logArea != null) logArea.getUI().ifPresent(ui -> ui.access(() -> logArea.setValue("Obtaining information from the metadata file")));

        logging("Downloading metadata file " + file.getPath() + " to " + tempFile.getPath());
        Data.downloadFile(this.credentials, file.getPath(), tempFile.getPath());
        // Process the file
        Domain domain = new Domain("file://" + tempFile);
        // Get the investigations
        Iterable<ResultLine> investigationResult = domain.getRDFSimpleCon().runQuery("getInvestigations.txt", false);
        HashSet<String> investigationIRIs = new HashSet<>();
        // To prevent a java.util.ConcurrentModificationException
        for (ResultLine resultLine : investigationResult) {
            // Get the investigation IRI
            String investigationIRI = resultLine.getIRI("iri");
            investigationIRIs.add(investigationIRI);
        }

        HashMap<String, String> createPaths = new HashMap<>();
        HashMap<String, String> moveFiles = new HashMap<>();
        for (String investigationIRI : investigationIRIs) {
            if (logArea != null) {
                logArea.getUI().ifPresent(ui -> ui.access(() -> logArea.setValue("Processing investigation " + investigationIRI)));
                logging("Processing investigation " + investigationIRI);
            }
            // Get the investigation
            Investigation investigation = domain.make(Investigation.class, investigationIRI);

            // Check if the investigation identifier exists
            ArrayList<String> investigationIdentifiers = new ArrayList<>();
            investigationIdentifiers.add(investigation.getIdentifier());
            investigationIdentifiers.add(investigation.getIdentifier().toLowerCase());
            investigationIdentifiers.add("wur."+investigation.getIdentifier().toLowerCase());
            investigationIdentifiers.add(investigation.getIdentifier().toUpperCase());
            String investigationIdentifier = null;
            while (!investigationIdentifiers.isEmpty()) {
                investigationIdentifier = investigationIdentifiers.remove(0);
                if (credentials.getZone().equalsIgnoreCase("unlock") && !investigationIdentifier.startsWith("wur.")) {
                    investigationIdentifier = "wur." + investigationIdentifier.substring(0, Math.min(investigationIdentifier.length(), 16));
                    logger.info("Investigation identifier renamed to " + investigationIdentifier);
                }

                String investigationPath = constructPath(credentials.getZone(), "home", investigationIdentifier);
                if (credentials.getFileFactory().instanceIRODSFile(investigationPath).exists()) {
                    break;
                } else {
                    investigationIdentifier = null;
                }
            }

            if (investigationIdentifier == null) {
                logging("Unable to find investigation " + investigation.getIdentifier());
                throw new RuntimeException("Unable to find investigation folder " + investigation.getIdentifier());
            }

            // Test to find all files
            HashMap<String, ArrayList<java.io.File>> files = Data.SearchFile(credentials, "%", constructPath(credentials.getZone(), "home", investigationIdentifier));

            // Process each study
            for (Study study : investigation.getAllHasPart()) {
                boolean studyContentURL = false;
                logging("Preparing study " + study.getIdentifier());
                String studyPath = constructPath(credentials.getZone(), "home", investigationIdentifier, "stu_" + study.getIdentifier());
                createPaths.put(studyPath, "study");

                // Process data samples for the study
                for (Data_sample dataSample : study.getAllDataset()) {
                    logging("Processing data sample " + dataSample.getIdentifier() + " in study " + study.getIdentifier());
                    // if the data sample is not in the moveFiles
                    addToMoveFiles(moveFiles, dataSample, studyPath);
                    if (!studyContentURL)
                        study.setContentUrl("https://" + credentials.getHost() + "/" + studyPath);
                    studyContentURL = true;
                }
                // Process observation units within the study
                for (observation_unit observationUnit : study.getAllHasPart()) {
                    boolean observationUnitContentURL = false;
                    String observationUnitPath = constructPath(studyPath, "obs_"  + observationUnit.getIdentifier());
                    createPaths.put(observationUnitPath, "Observation unit");
                    for (Data_sample dataSample : observationUnit.getAllDataset()) {
                        logging("Processing data sample " + dataSample.getIdentifier() + " in observation unit " + observationUnit.getIdentifier());
                        // Use observationUnitPath here for data sample processing
                        addToMoveFiles(moveFiles, dataSample, observationUnitPath);
                        study.setContentUrl("https://" + credentials.getHost() + "/" + studyPath);
                        if (!observationUnitContentURL)
                            observationUnit.setContentUrl("https://" + credentials.getHost() + "/" + observationUnitPath);
                        observationUnitContentURL = true;
                    }
                    for (Sample sample : observationUnit.getAllHasPart()) {
                        boolean sampleContentURL = false;
                        String samplePath = constructPath(observationUnitPath, "sam_" + sample.getIdentifier());
                        createPaths.put(samplePath, "Sample");
                        for (Assay assay : sample.getAllHasPart()) {
                            boolean assayContentURL = false;
                            // Obtain library source when available?
                            NodeIterator nodeIteratorSource = getProperty(assay.getResource().getModel(), assay.getResource().getURI(), "http://fairbydesign.nl/ontology/library_source");
                            NodeIterator nodeIteratorStrategy = getProperty(assay.getResource().getModel(), assay.getResource().getURI(), "http://fairbydesign.nl/ontology/library_strategy");
                            NodeIterator nodeIteratorPlatform = getProperty(assay.getResource().getModel(), assay.getResource().getURI(), "http://fairbydesign.nl/ontology/platform");

                            StringBuilder builder = new StringBuilder();
                            if (nodeIteratorSource.hasNext()) {
                                builder.append(nodeIteratorSource.next().asLiteral().toString().toLowerCase());
                            }
                            if (nodeIteratorStrategy.hasNext()) {
                                builder.append("_").append(nodeIteratorStrategy.next().asLiteral().toString().toLowerCase());
                            }
                            if (nodeIteratorPlatform.hasNext()) {
                                builder.append("_").append(nodeIteratorPlatform.next().asLiteral().toString().toLowerCase());
                            }

                            String assayPath = constructPath(samplePath, builder.toString(), "asy_" + assay.getIdentifier());

                            createPaths.put(assayPath, "Assay");
                            for (Data_sample dataSample : assay.getAllDataset()) {
                                // logging("Processing data sample " + dataSample.getIdentifier() + " in assay " + assay.getIdentifier());
                                if (files.containsKey(dataSample.getIdentifier())) {
                                    // Reset contentURL?
                                    dataSample.setContentUrl("");
                                    addToMoveFiles(moveFiles, dataSample, assayPath);
                                    // Set the content URLs for the different levels
                                    if (!studyContentURL) {
                                        study.setContentUrl("https://" + credentials.getHost() + "/" + studyPath);
                                        studyContentURL = true;
                                    }
                                    if (!observationUnitContentURL) {
                                        observationUnit.setContentUrl("https://" + credentials.getHost() + "/" + observationUnitPath);
                                        observationUnitContentURL = true;
                                    }
                                    if (!sampleContentURL) {
                                        sample.setContentUrl("https://" + credentials.getHost() + "/" + samplePath);
                                        sampleContentURL = true;
                                    }
                                    if (!assayContentURL) {
                                        assay.setContentUrl("https://" + credentials.getHost() + "/" + assayPath);
                                        assayContentURL = true;
                                    }
                                } else {
                                    logging("Data sample " + dataSample.getIdentifier() + " not found in the project folder");
                                }
                            }
                        }
                    }
                }
            }
            // createDirectories(createPaths);
            moveDataFiles(files, moveFiles, constructPath(credentials.getZone(), "home","%" + investigationIdentifier));
            // Save the metadata
            String location = Application.commandOptions.storage + "/manager/" + file.getName();
            logging("Saving metadata file " + location);
            domain.save(location);
            // Upload the metadata file to the original location
            logging("Uploading metadata file to " + file.getPath());
            Data.uploadIrodsFile(this.credentials.getIrodsAccount(), new java.io.File(location), new java.io.File(file.getPath()), true);
            logging("Finished organising the metadata");
        }
    }

    private void moveDataFiles(HashMap<String, ArrayList<java.io.File>> lookup, HashMap<String, String> moveFiles, String searchRoot) throws JargonException, GenQueryBuilderException {
        int counter = 0;

        for (String dataFileName : moveFiles.keySet().stream().sorted().toList()) {
            ArrayList<java.io.File> files = new ArrayList<>();
            if (lookup.containsKey(dataFileName)) {
                files = lookup.get(dataFileName);
            }
            counter++;
            if (counter % 100 == 0) {
                logging("Processing data file " + counter + " of " + moveFiles.size());
            }

            if (moveFiles.get(dataFileName) == null) {
                logging(dataFileName + " has multiple entries and will not be moved");
                continue;
            }
            // Find the data file in the groups home folder as lookup failed?...
            if (files.isEmpty()) {
                HashMap<String, ArrayList<java.io.File>> found_files = Data.SearchFile(credentials, dataFileName, searchRoot);
                files = found_files.get(dataFileName);
            }
            if (files.isEmpty()) {
                logging("Unable to find data for " + dataFileName + " at " + searchRoot);
            } else if (files.size() > 1) {
                logging("Multiple files found for " + dataFileName + " at " + searchRoot);
                for (java.io.File file : files) {
                    logging("File: " + file.getAbsolutePath());
                }
            } else {
                IRODSFile location = credentials.getFileFactory().instanceIRODSFile(moveFiles.get(dataFileName) + "/" + files.get(0).getName());
                if (files.get(0).getAbsolutePath().equals(location.getAbsolutePath())) {
                    // logging("Data file " + dataFileName + " already exists at " + location.getAbsolutePath());
                    continue;
                }
                logging(files.get(0).getAbsolutePath() + " moving to " + moveFiles.get(dataFileName));
                // Create the directory if it does not exist
                credentials.getFileFactory().instanceIRODSFile(location.getParentFile().getAbsolutePath()).mkdirs();
                // Move the file to the new location
                try {
                    credentials.getFileFactory().instanceIRODSFile(files.get(0).getAbsolutePath()).renameTo(location);
                } catch (JargonException e) {
                    // Need to work on HIERARCHY_ERROR / LOCKED_DATA_OBJECT_ACCESS
                    logging("Failed to move " + files.get(0).getAbsolutePath() + " to " + location.getAbsolutePath());
                } catch (Exception e) {
                    // did not catch this
                    System.err.println("Failed catch..." + e.getMessage());
                }
            }
        }
    }

    private void addToMoveFiles(HashMap<String, String> moveFiles, Data_sample dataSample, String parentPath) throws JargonException {
        parentPath = parentPath.replaceAll("/+", "/").replaceAll("/$", "");
        if (!moveFiles.containsKey(dataSample.getIdentifier())) {
            moveFiles.put(dataSample.getIdentifier(), parentPath + "/data");
            String fileURL = credentials.getFileFactory().instanceIRODSFile(parentPath + "/data/" + dataSample.getIdentifier()).toString();
            String contentURL = "https://" + fileURL.replaceAll(".*?@","").replaceFirst(":"+credentials.getPort(), "");
            dataSample.setContentUrl(contentURL);
        } else {
            logger.info("Data sample " + dataSample.getIdentifier() + " already exists in the moveFiles and will not be modified");
            moveFiles.put(dataSample.getIdentifier(), null);
        }
    }

//    private void createDirectories(HashMap<String, String> createPaths) throws JargonException, MalformedURLException {
//        // Sort the array by length in descending order
//        ArrayList<String> pathList = new ArrayList<>(createPaths.keySet());
//        pathList.sort((o1, o2) -> Integer.compare(o2.length(), o1.length()));
//
//        HashSet<String> createdPaths = new HashSet<>();
//        for (String path : pathList) {
//            path = path.replaceAll("/+", "/").replaceAll("/$", "");
//
//            if (createdPaths.contains(path)) {
//                continue;
//            }
//
//            java.io.File folderPath = new java.io.File(path);
//            // Create folder in iRODS
//            IRODSFile irodsFile = credentials.getFileFactory().instanceIRODSFile(path);
//            if (!irodsFile.exists()) {
//                irodsFile.mkdirs();
//            }
//
//            if (!credentials.getHost().contains("scomp")) {
//                addMetadataCollection(credentials, "type", createPaths.get(path), "", path);
//            }
//
//
//            // Add all parent folders for the lookup to reduce folder creation
//            while (folderPath.getParentFile() != null) {
//                folderPath = folderPath.getParentFile();
//                createdPaths.add(folderPath.getPath());
//            }
//        }
//    }

    // Method to construct paths dynamically
    private String constructPath (String...parts){
        String path = ("/" + String.join("/", parts) + "/").toLowerCase();
        path = path.replaceAll("/+", "/").replaceAll("/$", "");
        return path;
    }


    private  String validate(String base) {
        String characters = "!\"#$&'()*,;<=>?[\\]^`{|}~";
        for (char c : characters.toCharArray()) {
            base = base.replace(String.valueOf(c), "");
        }
        return base;
    }

    /**
     * Wrapper around the logger object
     *
     * @param message The Message to log.
     */
    private void logging(String message) {
        logger.info(message);
        this.message = this.message + "\n" + message;
        if (logArea != null) logArea.getUI().ifPresent(ui -> ui.access(() -> logArea.setValue(this.message)));
    }

    public static void addMetadataCollection(Credentials credentials, String name, String value, String unit, String collection) throws MalformedURLException {
        System.err.println("Adding metadata to " + collection + " with " + name + " " + value + " " + unit);
        try {
            if (!collection.startsWith("/"))
                collection = new URL(collection).getPath();
            AvuData avuData = AvuData.instance(name, value, unit);
            CollectionAO collectionAO = credentials.getAccessObjectFactory().getCollectionAO(credentials.getIrodsAccount());
            collectionAO.deleteAVUMetadata(collection, avuData);
            collectionAO.addAVUMetadata(collection, avuData);
        } catch (JargonException e) {
            System.err.println("Failed to add metadata to " + collection);
            // throw new RuntimeException();
        }
    }
}
