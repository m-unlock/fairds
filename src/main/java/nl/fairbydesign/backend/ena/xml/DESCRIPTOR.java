package nl.fairbydesign.backend.ena.xml;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class DESCRIPTOR {
    @JsonProperty("FIELD_GROUP")
    public ArrayList<FIELDGROUP> getFIELD_GROUP() {
        return this.fIELD_GROUP;
    }

    public void setFIELD_GROUP(ArrayList<FIELDGROUP> fIELD_GROUP) {
        this.fIELD_GROUP = fIELD_GROUP;
    }

    ArrayList<FIELDGROUP> fIELD_GROUP;

    @JsonProperty("DESCRIPTION")
    public String getDESCRIPTION() {
        return this.dESCRIPTION;
    }

    public void setDESCRIPTION(String dESCRIPTION) {
        this.dESCRIPTION = dESCRIPTION;
    }

    String dESCRIPTION;

    @JsonProperty("LABEL")
    public String getLABEL() {
        return this.lABEL;
    }

    public void setLABEL(String lABEL) {
        this.lABEL = lABEL;
    }

    String lABEL;

    @JsonProperty("NAME")
    public String getNAME() {
        return this.nAME;
    }

    public void setNAME(String nAME) {
        this.nAME = nAME;
    }

    String nAME;

    @JsonProperty("AUTHORITY")
    public String getAUTHORITY() {
        return this.aUTHORITY;
    }

    public void setAUTHORITY(String aUTHORITY) {
        this.aUTHORITY = aUTHORITY;
    }

    String aUTHORITY;
}
