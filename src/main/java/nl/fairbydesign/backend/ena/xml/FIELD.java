package nl.fairbydesign.backend.ena.xml;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

// import com.fasterxml.jackson.databind.ObjectMapper; // version 2.11.1
// import com.fasterxml.jackson.annotation.JsonProperty; // version 2.11.1
/* ObjectMapper om = new ObjectMapper();
Root root = om.readValue(myJsonString, Root.class); */
public class FIELD {
    @JsonProperty("LABEL")
    public String getLABEL() {
        return this.lABEL;
    }

    public void setLABEL(String lABEL) {
        this.lABEL = lABEL;
    }

    String lABEL;

    @JsonProperty("NAME")
    public String getNAME() {
        return this.nAME;
    }

    public void setNAME(String nAME) {
        this.nAME = nAME;
    }

    String nAME;

    @JsonProperty("SYNONYM")
    public String getSYNONYM() {
        return this.sYNONYM;
    }

    public void setSYNONYM(String sYNONYM) {
        this.sYNONYM = sYNONYM;
    }

    String sYNONYM;

    @JsonProperty("DESCRIPTION")
    public String getDESCRIPTION() {
        return this.dESCRIPTION;
    }

    public void setDESCRIPTION(String dESCRIPTION) {
        this.dESCRIPTION = dESCRIPTION;
    }

    String dESCRIPTION;

    @JsonProperty("FIELD_TYPE")
    public ArrayList<FIELDTYPE> getFIELD_TYPE() {
        return this.fIELD_TYPE;
    }

    public void setFIELD_TYPE(ArrayList<FIELDTYPE> fIELD_TYPE) {
        this.fIELD_TYPE = fIELD_TYPE;
    }


    ArrayList<FIELDTYPE> fIELD_TYPE;

    @JsonProperty("MANDATORY")
    public String getMANDATORY() {
        return this.mANDATORY;
    }

    public void setMANDATORY(String mANDATORY) {
        this.mANDATORY = mANDATORY;
    }

    String mANDATORY;

    @JsonProperty("MULTIPLICITY")
    public String getMULTIPLICITY() {
        return this.mULTIPLICITY;
    }

    public void setMULTIPLICITY(String mULTIPLICITY) {
        this.mULTIPLICITY = mULTIPLICITY;
    }

    String mULTIPLICITY;

    @JsonProperty("UNITS")
    public UNITS getUNITS() {
        return this.uNITS;
    }

    public void setUNITS(UNITS uNITS) {
        this.uNITS = uNITS;
    }

    UNITS uNITS;
}
