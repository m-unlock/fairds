package nl.fairbydesign.backend.ena.xml;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.LinkedHashMap;

public class FIELDTYPE {
    @JsonProperty("TEXT_FIELD")
    public void setTEXT_FIELD(Object tEXT_FIELD) {
        if (tEXT_FIELD.getClass() == LinkedHashMap.class) {
            LinkedHashMap<String, String> test = (LinkedHashMap<String, String>) tEXT_FIELD;
            this.tEXT_FIELD = new TEXTFIELD();
            this.tEXT_FIELD.setREGEX_VALUE(test.get("REGEX_VALUE"));
        }
    }

    public TEXTFIELD getTEXT_FIELD() {
        return tEXT_FIELD;
    }

    @JsonProperty("TEXT_AREA_FIELD")
    public void setTEST_AREA_FIELD(String testAreaField) {
        // Property ignored
    }

    @JsonProperty("DATE_FIELD")
    public void setDATE_FIELD(String testAreaField) {
        // Property ignored
    }

    @JsonProperty("ONTOLOGY_FIELD")
    public ONTOLOGYFIELD getONTOLOGY_FIELD() {
        return this.oNTOLOGY_FIELD; }
    public void setONTOLOGY_FIELD(ONTOLOGYFIELD oNTOLOGY_FIELD) {
        this.oNTOLOGY_FIELD = oNTOLOGY_FIELD; }
    ONTOLOGYFIELD oNTOLOGY_FIELD;

    TEXTFIELD tEXT_FIELD;

    @JsonProperty("TEXT_CHOICE_FIELD")
    public TEXTCHOICEFIELD getTEXT_CHOICE_FIELD() {
        return this.tEXT_CHOICE_FIELD;
    }

    public void setTEXT_CHOICE_FIELD(TEXTCHOICEFIELD tEXT_CHOICE_FIELD) {
        this.tEXT_CHOICE_FIELD = tEXT_CHOICE_FIELD;
    }

    TEXTCHOICEFIELD tEXT_CHOICE_FIELD;

        @JsonProperty("TAXON_FIELD")
        public TAXONFIELD getTAXON_FIELD() {
            return this.tAXON_FIELD; }
        public void setTAXON_FIELD(TAXONFIELD tAXON_FIELD) {
            this.tAXON_FIELD = tAXON_FIELD; }
        TAXONFIELD tAXON_FIELD;
}
