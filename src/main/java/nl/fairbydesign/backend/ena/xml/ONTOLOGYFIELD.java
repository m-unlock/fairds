package nl.fairbydesign.backend.ena.xml;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ONTOLOGYFIELD{
    @JsonProperty("ONTOLOGY_ID")
    public String getONTOLOGY_ID() {
        return this.oNTOLOGY_ID; }
    public void setONTOLOGY_ID(String oNTOLOGY_ID) {
        this.oNTOLOGY_ID = oNTOLOGY_ID; }
    String oNTOLOGY_ID;
}