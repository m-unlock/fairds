package nl.fairbydesign.backend.ena.xml;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TAXONFIELD{
    @JsonProperty("_restrictionType")
    public String get_restrictionType() {
        return this.restrictionType; }
    public void set_restrictionType(String _restrictionType) {
        this.restrictionType = _restrictionType; }
    String restrictionType;

    @JsonProperty("restrictionType")
    public void setRestrictionType(String restrictionType) {
        this.restrictionType = restrictionType;
    }
}