package nl.fairbydesign.backend.ena.xml;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CHECKLISTSET {
    @JsonProperty("CHECKLIST")
    public CHECKLIST getCHECKLIST() {
        return this.cHECKLIST;
    }
    public void setCHECKLIST(CHECKLIST cHECKLIST) {
        this.cHECKLIST = cHECKLIST;
    }
    CHECKLIST cHECKLIST;


}
