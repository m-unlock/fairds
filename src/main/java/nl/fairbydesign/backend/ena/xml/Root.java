package nl.fairbydesign.backend.ena.xml;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Root {
    @JsonProperty("CHECKLIST_SET")
    public CHECKLISTSET getCHECKLIST_SET() {
        return this.cHECKLIST_SET;
    }

    public void setCHECKLIST_SET(CHECKLISTSET cHECKLIST_SET) {
        this.cHECKLIST_SET = cHECKLIST_SET;
    }

    CHECKLISTSET cHECKLIST_SET;
}
