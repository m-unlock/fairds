package nl.fairbydesign.backend.ena.xml;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class FIELDGROUP {
    @JsonProperty("restrictionType")
    public String getRestrictionType() {
        return this.restrictionType;
    }

    public void setRestrictionType(String restrictionType) {
        this.restrictionType = restrictionType;
    }

    String restrictionType;

    @JsonProperty("DESCRIPTION")
    public String getDESCRIPTION() {
        return this.dESCRIPTION;
    }

    public void setDESCRIPTION(String dESCRIPTION) {
        this.dESCRIPTION = dESCRIPTION;
    }

    String dESCRIPTION;

    @JsonProperty("NAME")
    public String getNAME() {
        return this.nAME; }
    public void setNAME(String nAME) {
        this.nAME = nAME; }
    String nAME;
    @JsonProperty("FIELD")
    public ArrayList<FIELD> getFIELD() {
        return this.fIELD; }
    public void setFIELD(ArrayList<FIELD> fIELD) {
        this.fIELD = fIELD; }
    ArrayList<FIELD> fIELD;

    @JsonProperty("_restrictionType")
    public String get_restrictionType() {
        return this._restrictionType; }
    public void set_restrictionType(String _restrictionType) {
        this._restrictionType = _restrictionType; }
    String _restrictionType;
}




