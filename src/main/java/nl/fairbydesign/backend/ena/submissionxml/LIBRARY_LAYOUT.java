package nl.fairbydesign.backend.ena.submissionxml;

public class LIBRARY_LAYOUT {
    public PAIRED getPAIRED() {
        return this.PAIRED;
    }

    public void setPAIRED(PAIRED PAIRED) {
        this.PAIRED = PAIRED;
    }

    PAIRED PAIRED;

    SINGLE SINGLE;

    public void setSINGLE(SINGLE single) {
        this.SINGLE = single;
    }

    public SINGLE getSINGLE() {
        return SINGLE;
    }
}
