package nl.fairbydesign.backend.ena.submissionxml;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;

@XmlRootElement(name = "RUN_SET")
public class RUN_SET {
    public ArrayList<RUN> getRUN() {
        return this.RUN;
    }

    public void setRUN(ArrayList<RUN> RUN) {
        this.RUN = RUN;
    }

    public void addRUN(RUN RUN) {
        this.RUN.add(RUN);
    }

    ArrayList<RUN> RUN = new ArrayList<>();
}
