package nl.fairbydesign.backend.ena.submissionxml;

public class EXPERIMENT_ATTRIBUTE {
    public String getTAG() {
        return this.TAG;
    }

    public void setTAG(String TAG) {
        this.TAG = TAG;
    }

    String TAG;

    public String getVALUE() {
        return this.VALUE;
    }

    public void setVALUE(String VALUE) {
        this.VALUE = VALUE;
    }

    String VALUE;
}
