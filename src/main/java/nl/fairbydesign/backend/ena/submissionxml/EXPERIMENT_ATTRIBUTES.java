package nl.fairbydesign.backend.ena.submissionxml;

import java.util.ArrayList;

public class EXPERIMENT_ATTRIBUTES {
    public EXPERIMENT_ATTRIBUTES() {
        nl.fairbydesign.backend.ena.submissionxml.EXPERIMENT_ATTRIBUTE experiment_attribute = new EXPERIMENT_ATTRIBUTE();
        experiment_attribute.setTAG("Metadata submission software");
        experiment_attribute.setVALUE("FAIR Data Station");
        this.addEXPERIMENT_ATTRIBUTE(experiment_attribute);
    }
    public ArrayList<EXPERIMENT_ATTRIBUTE> getEXPERIMENT_ATTRIBUTE() {
        return this.EXPERIMENT_ATTRIBUTE;
    }

    public void setEXPERIMENT_ATTRIBUTE(ArrayList<EXPERIMENT_ATTRIBUTE> EXPERIMENT_ATTRIBUTE) {
        this.EXPERIMENT_ATTRIBUTE = EXPERIMENT_ATTRIBUTE;
    }
    public void addEXPERIMENT_ATTRIBUTE(EXPERIMENT_ATTRIBUTE EXPERIMENT_ATTRIBUTE) {
        this.EXPERIMENT_ATTRIBUTE.add(EXPERIMENT_ATTRIBUTE);
    }

    ArrayList<EXPERIMENT_ATTRIBUTE> EXPERIMENT_ATTRIBUTE = new ArrayList<>();
}
