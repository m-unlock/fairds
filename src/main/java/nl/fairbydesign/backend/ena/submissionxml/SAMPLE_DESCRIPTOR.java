package nl.fairbydesign.backend.ena.submissionxml;

import javax.xml.bind.annotation.XmlAttribute;

public class SAMPLE_DESCRIPTOR {
    public String getaccession() {
        return this.accession;
    }

    @XmlAttribute(name = "refname")
    public void setaccession(String accession) {
        this.accession = accession;
    }

    String accession;
}
