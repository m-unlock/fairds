package nl.fairbydesign.backend.ena.submissionxml;

import javax.xml.bind.annotation.XmlAttribute;

public class FILE {
    public String getfilename() {
        return this.filename;
    }

    @XmlAttribute(name = "filename")
    public void setfilename(String filename) {
        this.filename = filename;
    }

    String filename;

    public String getfiletype() {
        return this.filetype;
    }

    @XmlAttribute(name = "filetype")
    public void setfiletype(String filetype) {
        this.filetype = filetype;
    }

    String filetype;

    public String getchecksum_method() {
        return this.checksum_method;
    }

    @XmlAttribute(name = "checksum_method")
    public void setchecksum_method(String checksum_method) {
        this.checksum_method = checksum_method;
    }

    String checksum_method;

    public String getchecksum() {
        return this.checksum;
    }

    @XmlAttribute(name = "checksum")
    public void setchecksum(String checksum) {
        this.checksum = checksum;
    }

    String checksum;
}
