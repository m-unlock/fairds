package nl.fairbydesign.backend.ena.submissionxml;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;

@XmlRootElement(name = "SAMPLE_SET")
public class SAMPLE_SET {
    public ArrayList<SAMPLE> getSAMPLE() {
        return this.SAMPLE;
    }

    public void setSAMPLE(ArrayList<SAMPLE> SAMPLE) {
        this.SAMPLE = SAMPLE;
    }
    public void addSAMPLE(SAMPLE SAMPLE) {
        this.SAMPLE.add(SAMPLE);
    }

    ArrayList<SAMPLE> SAMPLE = new ArrayList<>();

//    public void setBla(String bla) {
//        this.bla = bla;
//    }
//
//    public String getBla() {
//        return this.bla;
//    }
//
//    String bla = "blabla";
}
