package nl.fairbydesign.backend.ena.submissionxml;

import javax.xml.bind.annotation.XmlAttribute;

public class EXPERIMENT_REF {
    public String getrefname() {
        return this.refname;
    }
    @XmlAttribute(name = "refname")
    public void setrefname(String refname) {
        this.refname = refname;
    }

    String refname;
}
