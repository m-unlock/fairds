package nl.fairbydesign.backend.ena.submissionxml;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "PROJECT_SET")
public class PROJECT_SET {
    public PROJECT getPROJECT() {
        return this.PROJECT;
    }

    public void setPROJECT(PROJECT PROJECT) {
        this.PROJECT = PROJECT;
    }

    PROJECT PROJECT = new PROJECT();
}
