package nl.fairbydesign.backend.config;

import nl.fairbydesign.Application;
import nl.wur.ssb.RDFSimpleCon.Util;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.List;

public class Config {
    private Menus menus;
    private List<Irods> irods;
    private View view;

    public Config loadConfig() {
        // Load the configuration file
        File irodsConfigFile = new File(Application.commandOptions.storage + "/config.yaml");
        if (!irodsConfigFile.exists()) {
            Application.logger.error("Loading internal config file");
            InputStream inputStream = null;
            try {
                inputStream = Util.getResourceFile("config.yaml");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            irodsConfigFile.getParentFile().mkdirs();
            // Copy to local config path
            try {
                Files.copy(inputStream, irodsConfigFile.toPath());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        // Load the configuration file
        Application.logger.info("Loading config file: " + irodsConfigFile.getAbsolutePath());
        Yaml yaml = new Yaml(new Constructor(Config.class));
        Config config = null;
        try {
            config = yaml.load(Files.newBufferedReader(irodsConfigFile.toPath()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        Application.logger.info("Config loaded");
        return config;
    }


    // Getters and setters
    public Menus getMenus() {
        return menus;
    }

    public void setMenus(Menus menus) {
        this.menus = menus;
    }

    public List<Irods> getIrods() {
        return irods;
    }

    public void setIrods(List<Irods> irods) {
        this.irods = irods;
    }


    public View getView() {
        return view;
    }

    public void setView(View view) {
        this.view = view;
    }
}