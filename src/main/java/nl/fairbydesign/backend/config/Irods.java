package nl.fairbydesign.backend.config;

public class Irods {
    private String label;
    private String host;
    private int port;
    private String zone;
    private String authscheme;
    private String ssl;

    // Getters and setters
    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getAuthscheme() {
        return authscheme;
    }

    public void setAuthscheme(String authscheme) {
        this.authscheme = authscheme;
    }

    public String getSsl() {
        return ssl;
    }

    public void setSsl(String ssl) {
        this.ssl = ssl;
    }
}