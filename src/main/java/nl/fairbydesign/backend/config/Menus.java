package nl.fairbydesign.backend.config;

public class Menus {
    private boolean about;
    private boolean configurator;
    private boolean validator;
    private boolean bioprojects;
    private boolean ena;
    private boolean terms;
    private boolean irods;
    private boolean api;

    // Getters and setters
    public boolean isAbout() {
        return about;
    }

    public void setAbout(boolean about) {
        this.about = about;
    }

    public boolean isConfigurator() {
        return configurator;
    }

    public void setConfigurator(boolean configurator) {
        this.configurator = configurator;
    }

    public boolean isValidator() {
        return validator;
    }

    public void setValidator(boolean validator) {
        this.validator = validator;
    }

    public boolean isBioprojects() {
        return bioprojects;
    }

    public void setBioprojects(boolean bioprojects) {
        this.bioprojects = bioprojects;
    }

    public boolean isEna() {
        return ena;
    }

    public void setEna(boolean ena) {
        this.ena = ena;
    }

    public boolean isTerms() {
        return terms;
    }

    public void setTerms(boolean terms) {
        this.terms = terms;
    }

    public boolean isIrods() {
        return irods;
    }

    public void setIrods(boolean irods) {
        this.irods = irods;
    }

    public boolean isApi() {
        return api;
    }

    public void setApi(boolean api) {
        this.api = api;
    }
}