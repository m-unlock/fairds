package nl.fairbydesign.backend.config;

public class View {
    private Validation validation;

    // Getters and setters
    public Validation getValidation() {
        return validation;
    }

    public void setValidation(Validation validation) {
        this.validation = validation;
    }

    public static class Validation {
        private String investigation_identifier;
        private String investigation_title;
        private String investigation_description;
        private String study_identifier;
        private String study_title;
        private String study_description;

        // Getters and setters
        public String getInvestigation_identifier() {
            return investigation_identifier;
        }

        public void setInvestigation_identifier(String investigation_identifier) {
            this.investigation_identifier = investigation_identifier;
        }

        public String getInvestigation_title() {
            return investigation_title;
        }

        public void setInvestigation_title(String investigation_title) {
            this.investigation_title = investigation_title;
        }

        public String getInvestigation_description() {
            return investigation_description;
        }

        public void setInvestigation_description(String investigation_description) {
            this.investigation_description = investigation_description;
        }

        public String getStudy_identifier() {
            return study_identifier;
        }

        public void setStudy_identifier(String study_identifier) {
            this.study_identifier = study_identifier;
        }

        public String getStudy_title() {
            return study_title;
        }

        public void setStudy_title(String study_title) {
            this.study_title = study_title;
        }

        public String getStudy_description() {
            return study_description;
        }

        public void setStudy_description(String study_description) {
            this.study_description = study_description;
        }
    }
}