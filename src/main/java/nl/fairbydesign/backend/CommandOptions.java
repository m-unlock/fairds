package nl.fairbydesign.backend;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


@Parameters(commandDescription = "Available options: ")

public class CommandOptions {
    public static final Logger logger = LogManager.getLogger(CommandOptions.class);

    @Parameter(names = {"-debug"}, description = "Debug mode")
    public boolean debug;

    @Parameter(names = {"-h","-help"}, description = "Help overview")
    public boolean help;

    @Parameter(names = {"-store"}, description = "FAIR Data Station storage location")
    public String storage = "./fairds_storage/";

    @Parameter(names = {"-metadata"}, description = "Metadata template file")
    public String metadata = this.storage + "/metadata.xlsx";

    @Parameter(names = {"-port"}, description = "FAIR Data Station port")
    public int port = 8083;

    @Parameter(names = {"-metadata_check"}, description = "Checks metadata file only (for validation purposes)")
    public boolean metadata_check;

    public CommandOptions(String[] args) {
        logger.info("For startup options use -help");

        JCommander jc = JCommander.newBuilder().addObject(this).build();
        jc.parse(args);

        if (this.help) {
            jc.usage();
            System.out.println("  * required parameter");
            System.exit(0);
        }
    }

    public CommandOptions() {
    }
}
