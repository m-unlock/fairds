package nl.fairbydesign.backend.credentials;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.irods.jargon.core.connection.*;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.protovalues.ChecksumEncodingEnum;
import org.irods.jargon.core.pub.IRODSAccessObjectFactory;
import org.irods.jargon.core.pub.IRODSFileSystem;
import org.irods.jargon.core.pub.UserGroupAO;
import org.irods.jargon.core.pub.io.IRODSFileFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.util.HashMap;

/**
 * Class for the credentials object for irods authentication with authentication functionality.
 */
public class Credentials {
    private static final Logger logger = LoggerFactory.getLogger(Credentials.class);

    private String username;
    private String password;
    private String host;
    private int port;
    private String zone;
    private ClientServerNegotiationPolicy.SslNegotiationPolicy sslNegotiationPolicy;
    private boolean success = false;
    private IRODSAccount irodsAccount;
    private boolean administrator = false;
    private String message;
    private AuthScheme authenticationScheme;
    private IRODSFileSystem irodsFileSystem;
    private IRODSAccessObjectFactory accessObjectFactory;
    private IRODSFileFactory fileFactory;

    public IRODSFileSystem getIrodsFileSystem() {
        return this.irodsFileSystem;
    }
    public void setUsername(String username) {
        this.username = username;
    }

    public ClientServerNegotiationPolicy.SslNegotiationPolicy getSslNegotiationPolicy() {
        return this.sslNegotiationPolicy;
    }

    public IRODSAccessObjectFactory getAccessObjectFactory() {
        return this.accessObjectFactory;
    }
    public String getUsername() {
        return this.username;
    }

    public String getHost() {
        return this.host;
    }

    public int getPort() {
        return this.port;
    }

    public String getZone() {
        return this.zone;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public void setSslNegotiationPolicy(String policy) {
        this.sslNegotiationPolicy = ClientServerNegotiationPolicy.findSslNegotiationPolicyFromString(policy);
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public void authenticate() throws JargonException {
        // null check
        if (host == null || port == -1 || username == null || password == null || zone == null || sslNegotiationPolicy == null || authenticationScheme == null) {
            this.setSuccess(false);
            this.setMessage("Not all fields are filled in");
            return;
        }

        // Initialize account object
        irodsAccount = IRODSAccount.instance(host, port, username, password, "/" + zone + "/home/", zone, "");

        SettableJargonPropertiesMBean jargonProperties = new SettableJargonProperties();
        jargonProperties.setChecksumEncoding(ChecksumEncodingEnum.SHA256);

        // Sets SSL settings
        ClientServerNegotiationPolicy clientServerNegotiationPolicy = new ClientServerNegotiationPolicy();
        clientServerNegotiationPolicy.setSslNegotiationPolicy(getSslNegotiationPolicy());
        irodsAccount.setClientServerNegotiationPolicy(clientServerNegotiationPolicy);
        irodsAccount.setAuthenticationScheme(getAuthenticationScheme());

        irodsFileSystem = IRODSFileSystem.instance();

        /// CUT
        SettableJargonPropertiesMBean settableJargonProperties = (SettableJargonPropertiesMBean) irodsFileSystem
                .getJargonProperties();
        settableJargonProperties.setNegotiationPolicy(ClientServerNegotiationPolicy.SslNegotiationPolicy.CS_NEG_DONT_CARE);
        irodsFileSystem.getIrodsSession().setJargonProperties(settableJargonProperties);
        TrustAllX509TrustManager manager = new TrustAllX509TrustManager();
        irodsFileSystem.getIrodsSession().setX509TrustManager(manager);
        //// CUT IN
        accessObjectFactory = irodsFileSystem.getIRODSAccessObjectFactory();
        fileFactory = accessObjectFactory.getIRODSFileFactory(irodsAccount);
        setFileFactory(fileFactory);

        if (accessObjectFactory.authenticateIRODSAccount(irodsAccount).isSuccessful()) {
            this.setSuccess(true);
            // TODO look into rodsuser / rodsadmin
            // Get user information when possible
            UserGroupAO userGroupAO = accessObjectFactory.getUserGroupAO(irodsAccount);
            // Group of technicians or administrators in irods
            if (userGroupAO.isUserInGroup(this.username, "technicians") || userGroupAO.isUserInGroup(this.username, "administrators")) {
                this.administrator = true;
            }
        } else {
            this.setSuccess(false);
            logger.error("Authenticate message: " + accessObjectFactory.authenticateIRODSAccount(irodsAccount).getAuthMessage());
        }
    }

    public void setPort(int port) {
        this.port = port;
    }

    public IRODSAccount getIrodsAccount() {
        return irodsAccount;
    }

    public void clear() {
        this.username = null;
        this.password = null;
        this.host = null;
        this.zone = null;
        this.sslNegotiationPolicy = null;
        this.success = false;
        this.port = -1;
        this.irodsAccount = null;
    }

    public boolean isAdministrator() {
        return administrator;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setAuthenticationScheme(String authenticationScheme) {
        this.authenticationScheme = AuthScheme.valueOf(authenticationScheme);
    }

    public AuthScheme getAuthenticationScheme() {
        return authenticationScheme;
    }


    public IRODSFileFactory getFileFactory() {
        return fileFactory;
    }

    public void setFileFactory(IRODSFileFactory fileFactory) {
        this.fileFactory = fileFactory;
    }

    public void setUsingIrodsConnection() {
        if (new File("irods_credentials.json").exists()) {
            // Load a json object into hashmap and use that to connect to irods
            String jsonData;
            try {
                jsonData = new String(Files.readAllBytes(new File("irods_credentials.json").toPath()));

                Gson gson = new Gson();
                Type type = new TypeToken<HashMap<String, Object>>() {}.getType();
                HashMap<String, Object> dataMap = gson.fromJson(jsonData, type);

                HashMap<String, String> environment = new HashMap<>();
                for (String key : dataMap.keySet()) {
                    environment.put(key, (String) dataMap.get(key));
                    logger.debug(key + " " + dataMap.get(key));
                }
                this.host = environment.get("IRODS_HOST");
                this.port = Integer.parseInt(environment.get("IRODS_PORT"));
                this.username = environment.get("IRODS_USER_NAME");
                this.password = environment.get("IRODS_USER_PASSWORD");
                this.zone = environment.get("IRODS_ZONE_NAME");
                this.sslNegotiationPolicy = ClientServerNegotiationPolicy.SslNegotiationPolicy.valueOf(environment.get("IRODS_SSL"));
                if (environment.get("IRODS_AUTHENTICATION_SCHEME").equals("password")) {
                    environment.put("IRODS_AUTHENTICATION_SCHEME", environment.get("IRODS_AUTHENTICATION_SCHEME").replace("password", "STANDARD"));
                    logger.warn("Changed IRODS_AUTHENTICATION_SCHEME to standard " + environment.get("IRODS_AUTHENTICATION_SCHEME"));
                }
                this.authenticationScheme = AuthScheme.valueOf(environment.get("IRODS_AUTHENTICATION_SCHEME"));
            } catch (IOException e) {
                logger.error("Could not read irods_connection.json file");
            }
        } else {
            logger.error("irods_credentials.json file does not exist");
        }
    }
}
