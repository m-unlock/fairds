package nl.fairbydesign;

import com.vaadin.flow.server.ServiceInitEvent;
import com.vaadin.flow.server.VaadinServiceInitListener;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * Startup listener accessed once during startup
 * Sets the config environment and the metadata excel file is parsed + copied outside this jar
 * Excel file is converted into a java disk object for fast access and timestamps are used to identify
 * if it needs to be reloaded.
 */
public class ApplicationServiceInitListener implements VaadinServiceInitListener {
    // Generic logger
    // public static final Logger logger = LogManager.getLogger(ApplicationServiceInitListener.class);


    @Override
    public void serviceInit(ServiceInitEvent event) {
        event.getSource().addSessionInitListener( initEvent -> {
            // Set credentials object
            initEvent.getSession().setAttribute("credentials", null);
        });
    }
}