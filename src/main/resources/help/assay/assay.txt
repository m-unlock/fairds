Assay metadata derived from a processed sample obtained from an observation unit.
<br>
For example the metadata derived from a sequence run.
<br>
Additional columns can be adding using this form when such information is available.
<br>