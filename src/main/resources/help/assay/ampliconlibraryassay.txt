<b>An assay corresponds to the data (for example a sequence run) that was performed on a sample obtained from an observation unit.</b>
<br>
Additional columns can be adding using this form when such information is available.
<br>