PREFIX jerm: <http://jermontology.org/ontology/JERMOntology#>
PREFIX ppeo: <http://purl.org/ppeo/PPEO.owl#>
SELECT ?subject
WHERE {
    VALUES ?object { jerm:Investigation jerm:Study ppeo:observation_unit jerm:Sample jerm:Assay jerm:Data_sample }
	?subject a ?object .
}