SELECT DISTINCT ?study ?iri
WHERE {
	?iri a jerm:Study .
	?iri schema:identifier ?study .
}