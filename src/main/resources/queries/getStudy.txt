SELECT DISTINCT ?study
WHERE {
	?study a jerm:Study .
	?study schema:identifier "%1$s" .
}