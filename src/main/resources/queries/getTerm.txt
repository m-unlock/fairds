select *
where {
    ?iri <http://www.w3.org/2000/01/rdf-schema#label> ?label .
    FILTER (str(?label) = "%1$s")
}