package nl.fairbydesign.testutils;
import java.io.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 *  Class to check whether a file contains a specific string.
 */
public class FileContentChecker {

    /**
     * Check whether specific lines exist in a file
     * @param filePath File to be checked.
     * @param pattern Lines to be searched for.
     * @return `true` if the `expectedLines` are in the `filePath` contents, `false` otherwise.
     */
    public static boolean check(File filePath, String pattern) throws FileNotFoundException {
        Pattern regularExpression = Pattern.compile(pattern);
        Matcher matcher = regularExpression.matcher("");

        try (
            BufferedReader reader = new BufferedReader(new FileReader(filePath))
        ) {
            String currentLine = null;
            while ((currentLine = reader.readLine()) != null) {
                matcher.reset(currentLine);
                if (matcher.find()) {
                    return true;
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return false;
    }
}


