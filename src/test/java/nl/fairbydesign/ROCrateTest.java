package nl.fairbydesign;

import edu.kit.datamanager.ro_crate.RoCrate;
import edu.kit.datamanager.ro_crate.entities.data.FileEntity;
import edu.kit.datamanager.ro_crate.writer.FolderWriter;
import edu.kit.datamanager.ro_crate.writer.RoCrateWriter;
import org.junit.jupiter.api.Test;

public class ROCrateTest {

    @Test
    public void testCrate() {
        // Create a new RoCrate
        RoCrate.RoCrateBuilder roCrateBuilder = new RoCrate.RoCrateBuilder("name", "description")
                .addValuePairToContext("Station", "www.station.com")
                .addUrlToContext("http://example.com");
//                .setPreview(new AutomaticPreview());

        // Add a file entity
        FileEntity.FileEntityBuilder fileEntity = new FileEntity.FileEntityBuilder();
        fileEntity.setId("survey-responses-2019.csv");
        fileEntity.addProperty("name", "Survey responses");
        fileEntity.addProperty("contentSize", "26452");
        fileEntity.addProperty("encodingFormat", "text/csv");

        // Add the file entity to the RoCrate
        roCrateBuilder.addDataEntity(fileEntity.build());
        RoCrate roCrate = roCrateBuilder.build();

        RoCrateWriter folderRoCrateWriter = new RoCrateWriter(new FolderWriter());
        folderRoCrateWriter.save(roCrate, Application.commandOptions.storage + "/ro-crate/");
    }
}
