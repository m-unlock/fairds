package nl.fairbydesign.backend.data.objects.metadata.regex;

import nl.fairbydesign.Application;
import nl.fairbydesign.backend.metadata.Metadata;
import nl.fairbydesign.backend.metadata.Term;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.helger.commons.mock.CommonsAssert.assertEquals;
import static nl.fairbydesign.backend.metadata.MetadataParser.generateRegex;
import static nl.fairbydesign.backend.metadata.MetadataParser.processRegexSheet;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RegexTest {
    public static final Logger logger = Logger.getLogger(Metadata.class);

    /**
     * Test if the regex sheet can be read from the metadata file
     * Addition
     * 
     * @throws IOException if the file cannot be read
     */
    @Test
    public void testMainMetadataXLSX() throws IOException {
        // Load workbook from file
        File metadataFile = new File("./src/main/resources/metadata.xlsx");
        // Clear the term lookup from any parallel tests
        Application.termLookup.clear();
        assertTrue(Application.termLookup.keySet().isEmpty());
        try (Workbook workbook = WorkbookFactory.create(metadataFile)) {
            // Process the regex sheet
            processRegexSheet(workbook.getSheet("regex"));
        }
        Assertions.assertFalse(Application.termLookup.keySet().isEmpty());
        // TODO test if the regexes are correct?.. using other tests...
        testRegex();
    }

    @Test
    public void testFaultyExcelFormat() {
        try {
            File invalidMetaDataFile = new File("./src/test/resources/meta/invalidFileNoRegex.xlsx");
            new RegexData(invalidMetaDataFile);
        } catch (Exception e) {
            assertEquals(
                    e.toString(),
                    "java.lang.RuntimeException: The expected sheet name 'regex' was not found in the workbook that was given.\n"
                            +
                            "Sheets are [terms, Investigation, Study, ObservationUnit, Sample, Assay]");
        }
    }

    @Test
    public void testCorrectRegexFormat() throws Exception {
        File metadataFile = new File("./src/test/resources/meta/metadata.xlsx");
        new RegexData(metadataFile);
    }

    // @Test
    private void testRegex() {
        // Test extension from the testMainMetadataXLSX method as it needs the regex
        // sheet to be loaded

        // System.setProperty("metadata_path",
        // "./src/test/resources/meta/metadata.xlsx");

        // Longer match for a new term
        Term term = new Term();
        term.setLabel("A test label");
        term.setExample("example_with_more_than_10_chars");
        term.setSyntax("{text}{10,}");
        // {text} is not known...
        term = generateRegex(term);
        logger.info(term.getRegex().pattern());
        logger.info(term.getRegex().matcher(term.getExample()).matches());
        assertTrue(term.getRegex().matcher(term.getExample()).matches());

        // Exact match
        term.setExample("example_10");
        term.setSyntax("{text}{10}");
        term = generateRegex(term);
        logger.info(term.getRegex().pattern());
        logger.info(term.getRegex().matcher(term.getExample()).matches());
        assertTrue(term.getRegex().matcher(term.getExample()).matches());

        // Short match
        term.setExample("short_10");
        term.setSyntax("{text}{1,10}");
        logger.info(term.getRegex().pattern());
        logger.info(term.getRegex().matcher(term.getExample()).matches());
        assertTrue(term.getRegex().matcher(term.getExample()).matches());

    }

    public void testFalseTerm(String input) {
        String falseString = "{" + input + "}";
        System.setProperty("metadata_path", "./src/test/resources/meta/metadata.xlsx");
        // Longer match
        Term term = new Term();
        // Short match
        term.setExample("short_10");
        term.setLabel("short_10");

        term.setSyntax(falseString);
        try {
            generateRegex(term);
        } catch (Exception e) {
            logger.info(e.getMessage());
            assertEquals(e.toString(),
                    "java.lang.RuntimeException: Regex conversion failed 'java.lang.RuntimeException: Error 'java.lang.Exception: The short form "
                            + falseString + " does not exist.' for regular expression " + falseString + "'.");
        }
    }

    @Test
    public void falseRegularExpression() {
        // here we test a regular expression that'll never exist.
        testFalseTerm("bla");
        testFalseTerm("|");
        testFalseTerm("Some thing with spaces|something and...");
    }

    @Test
    public void simpleTest() {
        String syntax = "{text}{10}";
        syntax = syntax.replace("-", "\\-");
        boolean match = syntax.matches("\\{text}[0-9]+\\\\-");
        logger.info(match);
    }

    @Test
    public void countTest() {
        String regex = "(PRJ[DEN][A-Z][0-9]+)";
        Pattern r = Pattern.compile(regex);
        Matcher m = r.matcher(
                "PRJNA248299PRJNA248299PRJNA248299PRJNA248299PRJNA248299PRJNA248299PRJNA248299PRJNA248299PRJNA248299");
        // Count the number of matches
        int count = 0;
        while (m.find()) {
            count++;
        }
        logger.info("Detected " + count + " projects");
        Assertions.assertEquals(9, count);
    }
}
