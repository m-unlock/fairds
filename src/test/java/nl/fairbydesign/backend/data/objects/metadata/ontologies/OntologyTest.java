package nl.fairbydesign.backend.data.objects.metadata.ontologies;

import nl.fairbydesign.backend.metadata.MetadataParser;
import org.apache.jena.base.Sys;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

class OntologyTest {
    @Test
    public void testPreloadOntology() throws Exception {
        URL ontologyPath = new URL("https://raw.githubusercontent.com/EnvironmentOntology/envo/master/envo.owl");
        @NotNull File destination = MetadataParser.getOntologyFile(ontologyPath);
        MetadataParser.writeOntology(destination);
    }
    @Test
    public void testOntologyLoad() throws Exception {
        Ontology ont = new Ontology();
        String Envo = "./fairds_storage/environmentontology/envo/master/envo.owl.dir";
        File envoFile = new File(Envo);
        ont.loadOntology(envoFile);

        assertEquals("failure - strings are not equal",
                "Number of ontologies is 1",
                ont.toString());
    }

    @Test
    public void testOntologyValidate() throws Exception {
        Ontology ont = new Ontology();
        String Envo = "./fairds_storage/environmentontology/envo/master/envo.owl.dir";
        File envoFile = new File(Envo);
        ont.loadOntology(envoFile);
        boolean field_check  = ont.checkTerm(envoFile, "bottle", "");
        assertTrue(field_check);
    }

    @Test
    public void testOntologyValidateNegative() throws Exception {
        Ontology ont = new Ontology();
        String Envo = "./fairds_storage/environmentontology/envo/master/envo.owl.dir";
        File envoFile = new File(Envo);
        ont.loadOntology(envoFile);
        boolean garden  = ont.checkTerm(envoFile, "garden chair", "");
        assertFalse(garden);
    }

    @Test
    public void testFindClosestTerm() throws Exception {
        // Expected
        Set<String> stringSet = new HashSet<>();

        // Add the strings to the set
        stringSet.add("garden soil");
        stringSet.add("allotment garden soil");
        stringSet.add("allotment garden");
        stringSet.add("botanical garden");
        stringSet.add("domestic garden");
        stringSet.add("garden");
        stringSet.add("zoological garden");
        stringSet.add("vegetable garden soil");

        Ontology ont = new Ontology();
        String Envo = "./fairds_storage/environmentontology/envo/master/envo.owl.dir";
        File envoFile = new File(Envo);
        ont.loadOntology(envoFile);
        Set<String> res = ont.findClosestTermsUsingSparql(envoFile, "garden", "");
        boolean setsAreTheSame = stringSet.equals(res);
        assert setsAreTheSame;
    }

    @Test
    public void findIriTest() throws Exception {
        Ontology ont = new Ontology();
        String Envo = "./fairds_storage/environmentontology/envo/master/envo.owl.dir";
        File envoFile = new File(Envo);
        ont.loadOntology(envoFile);
        String uri = ont.findUri(envoFile, "garden", "");
        assertEquals("http://purl.obolibrary.org/obo/ENVO_00000011", uri);
    }
}