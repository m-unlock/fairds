package nl.fairbydesign.backend.data.objects.metadata.geo;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CoordinateTest {

    @Test
    void get_geosparql_point() {
        // test location of amsterdam
        Coordinate amsterdam_coord = new Coordinate(  4.893611, 52.372778 );
        String well_known_text_literal = amsterdam_coord.get_geosparql_point();
        assertEquals(well_known_text_literal,
                "Point(4.893611 52.372778)");
    }

    @Test
    void fail_coordinate() {
        // Invalid coordinates
        try {
            Coordinate invalid = new Coordinate(-200, 200);
        } catch (IllegalArgumentException error) {
            assertEquals(error.toString(),
                    "java.lang.IllegalArgumentException: Input latitude is 200.0 where as it should be between -90 and +90.");
        }
    }
}