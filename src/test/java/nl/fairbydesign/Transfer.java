package nl.fairbydesign;

import nl.fairbydesign.backend.credentials.Credentials;
import nl.fairbydesign.backend.irods.StatusCallbackListener;
import nl.fairbydesign.backend.wrappers.io.FileDownload;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.packinstr.TransferOptions;
import org.irods.jargon.core.pub.DataTransferOperations;
import org.irods.jargon.core.pub.IRODSFileSystem;
import org.irods.jargon.core.transfer.DefaultTransferControlBlock;
import org.irods.jargon.core.transfer.TransferControlBlock;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.irods.jargon.core.packinstr.TransferOptions.ForceOption.USE_FORCE;

public class Transfer {

    @Test
    @Disabled
    public void testFolderTransfer() throws JargonException {
            try {
                Credentials credentials = new Credentials();
                credentials.setUsingIrodsConnection();
                credentials.authenticate();
                DataTransferOperations dataTransferOperationsAO = IRODSFileSystem.instance().getIRODSAccessObjectFactory().getDataTransferOperations(credentials.getIrodsAccount());
                StatusCallbackListener statusCallbackListener = new StatusCallbackListener();
                TransferControlBlock defaultTransferControlBlock = DefaultTransferControlBlock.instance();
                TransferOptions transferOptions = dataTransferOperationsAO.buildTransferOptionsBasedOnJargonProperties();
                transferOptions.setIntraFileStatusCallbacks(true);
                transferOptions.setUseParallelTransfer(true);
                transferOptions.setForceOption(USE_FORCE);
                defaultTransferControlBlock.setTransferOptions(transferOptions);
                // transferOptions.setMaxThreads(0);
                dataTransferOperationsAO.putOperation(new File("./src/test/resources"), credentials.getFileFactory().instanceIRODSFile("/unlock/home/koehorst/"), statusCallbackListener, defaultTransferControlBlock);
            } catch (org.irods.jargon.core.exception.InvalidUserException invalidUserException) {
                System.err.println("Authentication failed, cant perform test");
            }
    }

    @Test
    public void testDownloadFromURL() {
        String fileUrl = "http://download.systemsbiology.nl/unlock/metadata-dev.xlsx";
//        String saveDir = Application.commandOptions.storage +;
        try {
            FileDownload fd = new FileDownload(fileUrl);
            Path p = Paths.get(Application.commandOptions.storage);
            fd.download(p);
        } catch (IOException ex) {
            System.err.println("Unable to obtain a metadata.xlsx file please contact the developers");
            throw new RuntimeException(ex);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        Assertions.assertTrue(new File(Application.commandOptions.storage +"/metadata-dev.xlsx").exists());
    }
}
