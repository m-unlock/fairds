package nl.fairbydesign.validation;

import nl.fairbydesign.backend.metadata.Metadata;
import nl.fairbydesign.backend.metadata.MetadataParser;
import nl.fairbydesign.backend.parsers.ExcelValidator;
import nl.fairbydesign.backend.wrappers.queries.Query;
import nl.fairbydesign.testutils.FileContentChecker;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ExcelTest {
    public static final Logger logger = LogManager.getLogger(Metadata.class);

    /**
     * Processes the metadata file to see if all is well
     *
     * @throws Exception when the metadata file is not found
     */
    @Test
    public void testSheetParsing() throws Exception {
        System.setProperty("metadata_path", "./src/test/resources/meta/metadata.xlsx");
        // If metadata file exists locally
        InputStream is;
        logger.info("Checking if " + new File("./fairds_storage/metadata.xlsx").getAbsolutePath() + " exists");
        if (new File("./fairds_storage/metadata.xlsx").exists()) {
            logger.info("Using local excel file");
            is = new FileInputStream("./fairds_storage/metadata.xlsx");
        } else {
            // else load xlsx from jar file
            logger.info("Using internal excel file");
            is = MetadataParser.class.getClassLoader().getResourceAsStream("metadata.xlsx");
        }

        if (is == null) {
            throw new Exception("Metadata file not found");
        }

        Workbook workbook = WorkbookFactory.create(is);

        for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
            Sheet sheet = workbook.getSheetAt(i);
            logger.debug("Processing " + sheet.getSheetName() + " with " + sheet.getLastRowNum() + " entries");
        }
    }

    /**
     * @throws Exception when the Excel file is not valid, according to the metadata
     *                   validator file
     */
    @Test
    public void validateFailed() throws Exception {
        System.setProperty("metadata_path", "./src/test/resources/meta/metadata.xlsx");
        ArrayList<File> excelFiles = new ArrayList<>();
        excelFiles.add(new File("./src/test/resources/meta/ValidationDemo.xlsx"));
        for (File excelFile : excelFiles) {
            if (excelFile.exists()) {
                try (FileInputStream inputStream = new FileInputStream(excelFile)) {
                    ExcelValidator excelValidator = new ExcelValidator();
                    // TODO capture the message and assert that as well?...
                    assertThrows(RuntimeException.class, () -> excelValidator.validate(excelFile.getName(), inputStream, null, null, "TURTLE"));
                }
            } else {
                logger.error("Test file does not exists");
            }
        }
    }

    @Test
    public void validateASPAR_KR_dataset() throws Exception {
        System.setProperty("metadata_path", "./src/test/resources/meta/metadata.xlsx");
        ArrayList<File> excelFiles = new ArrayList<>();
        excelFiles.add(new File("./src/test/resources/asper/AF-monitor.xlsx"));

        for (File excelFile : excelFiles) {
            if (excelFile.exists()) {
                FileInputStream inputStream = new FileInputStream(excelFile);
                ExcelValidator excelValidator = new ExcelValidator();
                // No credentials = no upload
                excelValidator.validate(excelFile.getName(), inputStream, null, null, "TURTLE");
                // Check if RDF file exists
                File rdfFile = new File("./fairds_storage/validation/" + excelFile.getName().replace(".xlsx", ".ttl"));
                // has the rdf file been made?
                assert rdfFile.exists();
                // Are the contents correct?
                assert FileContentChecker.check(rdfFile, " geo:hasGeometry ");
                assert FileContentChecker.check(rdfFile, "geo:asWKT");
            } else {
                logger.error("Test file does not exists");
            }
        }
    }

    @Test
    public void validateAsparTerms() throws Exception {
        System.setProperty("metadata_path", "./src/test/resources/meta/metadata.xlsx");
        ArrayList<File> excelFiles = new ArrayList<>();
        excelFiles.add(new File("./src/test/resources/asper/AF-monitor_closest_terms_test.xlsx"));

        for (File excelFile : excelFiles) {
            if (excelFile.exists()) {
                FileInputStream inputStream = new FileInputStream(excelFile);
                ExcelValidator excelValidator = new ExcelValidator();
                // No credentials = no upload
                excelValidator.validate(excelFile.getName(), inputStream, null, null, "TURTLE");
                // Check if RDF file exists
                File rdfFile = new File("./fairds_storage/validation/" + excelFile.getName().replace(".xlsx", ".ttl"));
                // has the rdf file been made?
                assert rdfFile.exists();
                // Are the contents correct?
                assert FileContentChecker.check(rdfFile, " geo:hasGeometry ");
                assert FileContentChecker.check(rdfFile, "geo:asWKT");
            } else {
                logger.error("Test file does not exists");
            }
        }
    }


    /**
     * Test whether the `derives_from` field works as it should.
     */
    @Test
    public void validateTermsWithDerivePositive() throws Exception {
        System.setProperty("metadata_path", "./src/test/resources/meta/metadata.xlsx");
        ArrayList<File> excelFiles = new ArrayList<>();
        excelFiles.add(new File("./src/test/resources/derives_from.xlsx"));

        for (File excelFile : excelFiles) {
            if (excelFile.exists()) {
                FileInputStream inputStream = new FileInputStream(excelFile);
                ExcelValidator excelValidator = new ExcelValidator();
                // No credentials = no upload
                excelValidator.validate(excelFile.getName(), inputStream, null, null, "TURTLE");
                // Check if RDF file exists
                File rdfFile = new File("./fairds_storage/validation/" + excelFile.getName().replace(".xlsx", ".ttl"));
                // has the rdf file been made?
                assert rdfFile.exists();

                // check the contents of the RDF
                Model model = RDFDataMgr.loadModel(rdfFile.toString());
                Query q = new Query(model);
                String doTwoObjectsShareOU =  "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \n"
                        + "prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> \n"
                        + "prefix ppeo:     <http://purl.org/ppeo/PPEO.owl#> \n"
                        + "prefix owl: <http://www.w3.org/2002/07/owl#> \n"
                        + "prefix fair:     <http://fairbydesign.nl/ontology/> \n"
                        + "prefix jerm:     <http://jermontology.org/ontology/JERMOntology#> \n"
                        + "ASK"
                        + "WHERE {"
                        + "?any <http://fairbydesign.nl/ontology/derives> ?obj ."
                        + "?obs1 jerm:hasPart ?any ."
                        + "?obs2 jerm:hasPart ?obj ."
                        + "FILTER(?obs1 != ?obs2) "
                        + "}";
                q.setQueryData(doTwoObjectsShareOU);
                assert !q.runAsk();
            } else {
                logger.error("Test file does not exists");
            }
        }
    }

    /**
     * Test whether the `derives_from` field works as it should.
     */
    @Test
    public void validateTermsWithDeriveNegative() throws Exception {
        System.setProperty("metadata_path", "./src/test/resources/meta/metadata.xlsx");
        String expectedError ="java.lang.Exception: There are samples where the derivation cannot be matched with the observational unit! These are:\n" +
                "java.lang.Exception: Object http://fairbydesign.nl/ontology/inv_Afmonitor_farm_1/stu_compost_measurements/obs_DN3_heap/sam_Sample_DN3_a_8_8 was used to derive http://fairbydesign.nl/ontology/inv_Afmonitor_farm_1/stu_compost_measurements/obs_D1_heap/sam_Sample_D1a8_8_result_mm but they share no observational unit";
        ArrayList<File> excelFiles = new ArrayList<>();
        excelFiles.add(new File("./src/test/resources/derives_from_false.xlsx"));
        try {
            for (File excelFile : excelFiles) {
                if (excelFile.exists()) {
                    FileInputStream inputStream = new FileInputStream(excelFile);
                    ExcelValidator excelValidator = new ExcelValidator();
                    // No credentials = no upload
                    excelValidator.validate(excelFile.getName(), inputStream, null, null, "TURTLE");
                    // Check if RDF file exists
                    File rdfFile = new File("./fairds_storage/validation/" + excelFile.getName().replace(".xlsx", ".ttl"));
                    // has the rdf file been made?
                    assert rdfFile.exists();
                    // check the contents of the RDF
                    Model model = RDFDataMgr.loadModel(rdfFile.toString());
                    Query q = new Query(model);
                    String doTwoObjectsShareOU = "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> \n"
                            + "prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> \n"
                            + "prefix ppeo:     <http://purl.org/ppeo/PPEO.owl#> \n"
                            + "prefix owl: <http://www.w3.org/2002/07/owl#> \n"
                            + "prefix fair:     <http://fairbydesign.nl/ontology/> \n"
                            + "prefix jerm:     <http://jermontology.org/ontology/JERMOntology#> \n"
                            + "ASK"
                            + "WHERE {"
                            + "?any <http://fairbydesign.nl/ontology/derives> ?obj ."
                            + "?obs1 jerm:hasPart ?any ."
                            + "?obs2 jerm:hasPart ?obj ."
                            + "FILTER(?obs1 != ?obs2) "
                            + "}";
                    q.setQueryData(doTwoObjectsShareOU);
                    assert !q.runAsk();
                } else {
                    logger.error("Test file does not exists");
                }
            }
        } catch(Exception e) {
            assertEquals(e.toString(), expectedError);
        }
    }
    /**
     *  This test determines whether invalid coordinates are correctly
     *              rejected, also whether there is an
     *              informative error message.
     * @throws Exception When the file to be tested does not exist.
     */
    @Test
    public void validateASPAR_KR_dataset_coordinate_error() throws Exception {
        ArrayList<File> excelFiles = new ArrayList<>();
        excelFiles.add(new File("./src/test/resources/asper/AF-monitor_coordinate_error.xlsx"));
        System.setProperty("metadata_path", "./src/test/resources/meta/metadata.xlsx");
        String expectedError = "org.rdfhdt.hdt.exceptions.IllegalFormatException: Invalid coordinate: java.lang.IllegalArgumentException: Input latitude is 200.0 where as it should be between -90 and +90. for object \n"
                +
                "http://fairbydesign.nl/ontology/inv_Afmonitor_farm_1/stu_compost_measurements/obs_HN7_heap/sam_Sample_HN7_b_8_8";
        for (File excelFile : excelFiles) {
            try {
                if (excelFile.exists()) {
                    FileInputStream inputStream = new FileInputStream(excelFile);
                    ExcelValidator excelValidator = new ExcelValidator();
                    // No credentials = no upload
                    excelValidator.validate(excelFile.getName(), inputStream, null, null, "TURTLE");
                    // Check if RDF file exists
                    File rdfFile = new File("./fairds_storage/validation/" + excelFile.getName().replace(".xlsx", ".ttl"));
                    assert rdfFile.exists();
                } else {
                    logger.error("Test file does not exists");
                }
            } catch (Exception e) {
                assertEquals(expectedError, e.toString());
            }
        }
    }

    // Write a test to see if the BMOCK12 file works
    @Test
    public void validateBMOCK12() throws Exception {
        System.setProperty("metadata_path", "./src/test/resources/meta/metadata.xlsx");
        ArrayList<File> excelFiles = new ArrayList<>();
        excelFiles.add(new File("./src/test/resources/meta/BMOCK12.xlsx"));

        for (File excelFile : excelFiles) {
            if (excelFile.exists()) {
                FileInputStream inputStream = new FileInputStream(excelFile);
                ExcelValidator excelValidator = new ExcelValidator();
                // No credentials = no upload
                excelValidator.validate(excelFile.getName(), inputStream, null, null, "TURTLE");
                // Check if RDF file exists
                File rdfFile = new File("./fairds_storage/validation/" + excelFile.getName().replace(".xlsx", ".ttl"));
                // has the rdf file been made?
                assert rdfFile.exists();
                // Load the file
                Domain domain = new Domain("file://" + rdfFile);
                ResultSet resultSet = domain.getRDFSimpleCon().runQueryDirect("SELECT * WHERE {?obs a <http://purl.org/ppeo/PPEO.owl#observation_unit> . ?obs <http://schema.org/dataset> ?dataset .}");
                assert resultSet.hasNext();
                while (resultSet.hasNext()) {
                    QuerySolution querySolution = resultSet.next();
                    System.err.println(querySolution);
                }
            } else {
                logger.error("Test file does not exists");
            }
        }
    }

    @Test
    public void testMIXS() throws Exception {
        System.setProperty("metadata_path", "./src/test/resources/meta/metadata.xlsx");
        ArrayList<File> excelFiles = new ArrayList<>();
        excelFiles.add(new File("./src/test/resources/meta/ValidationDemo2.xlsx"));

        for (File excelFile : excelFiles) {
            if (excelFile.exists()) {
                FileInputStream inputStream = new FileInputStream(excelFile);
                ExcelValidator excelValidator = new ExcelValidator();
                // No credentials = no upload
                excelValidator.validate(excelFile.getName(), inputStream, null, null, "TURTLE");
                // Check if RDF file exists
                File rdfFile = new File("./fairds_storage/validation/" + excelFile.getName().replace(".xlsx", ".ttl"));
                // has the rdf file been made?
                assert rdfFile.exists();
                // Load the file
                Domain domain = new Domain("file://" + rdfFile);
                ResultSet resultSet = domain.getRDFSimpleCon().runQueryDirect("SELECT * WHERE {?obs a <http://purl.org/ppeo/PPEO.owl#observation_unit> . ?obs <http://fairbydesign.nl/ontology/birthdate> ?birthdate . ?obs ?x 'female'}");
                assert resultSet.hasNext();
                while (resultSet.hasNext()) {
                    QuerySolution querySolution = resultSet.next();
                    String url = querySolution.get("x").asResource().getURI();
                    // Check if valid url
                    logger.info("Analyzing " + url);
                    assert url.startsWith("http");
                    URL url1 = new URL(url);
                    url1.toURI();
                }
            } else {
                logger.error("Test file does not exists");
            }
        }
    }
}
