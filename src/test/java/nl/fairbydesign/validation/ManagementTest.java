package nl.fairbydesign.validation;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import nl.fairbydesign.backend.credentials.Credentials;
import nl.fairbydesign.backend.manager.Manager;
import org.irods.jargon.core.connection.AuthScheme;
import org.irods.jargon.core.connection.ClientServerNegotiationPolicy;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.util.HashMap;

public class ManagementTest {
    @Test
    public void test() throws Exception {
        String jsonData = new String(Files.readAllBytes(new File("irods_credentials.json").toPath()));
        Gson gson = new Gson();Type type = new TypeToken<HashMap<String, Object>>() {}.getType();
        HashMap<String, String> dataMap = gson.fromJson(jsonData, type);

        Credentials credentials = new Credentials();
        credentials.setAuthenticationScheme(AuthScheme.STANDARD.getTextValue());
        credentials.setPort(Integer.parseInt(dataMap.get("IRODS_PORT")));
        credentials.setSslNegotiationPolicy(ClientServerNegotiationPolicy.SslNegotiationPolicy.CS_NEG_REQUIRE.toString());

        credentials.setHost(dataMap.get("IRODS_HOST"));
        credentials.setUsername(dataMap.get("IRODS_USER_NAME"));
        credentials.setZone(dataMap.get("IRODS_ZONE_NAME"));
        credentials.setPassword(dataMap.get("IRODS_USER_PASSWORD"));

        credentials.authenticate();

        Manager manager = new Manager();
        nl.fairbydesign.backend.data.objects.File file = new nl.fairbydesign.backend.data.objects.File();
        file.setPath("/unlock/home/wur.gutbrain_axis/metadata/RUMC_Psychiatry_v2.ttl");
        manager.process(credentials, file, null);
    }
}
