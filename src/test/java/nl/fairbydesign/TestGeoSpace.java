package nl.fairbydesign;

import io.github.galbiston.geosparql_jena.implementation.vocabulary.Geo;
import nl.fairbydesign.backend.data.objects.metadata.geo.Coordinate;
import nl.wur.ssb.RDFSimpleCon.RDFSimpleCon;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.impl.PropertyImpl;
import org.apache.jena.rdf.model.impl.ResourceImpl;
import org.apache.jena.vocabulary.RDF;
import org.junit.jupiter.api.Test;

class TestGeoSpace {
    @Test
    public void testGeo() throws Exception {
        // Create a Jena model
        RDFSimpleCon con = new RDFSimpleCon("");

        // Create a resource for your point
        Resource pointResource = new ResourceImpl("http://example.org/points/1");

        // Create a property for the GeoSPARQL geometry
        PropertyImpl asWKT = new PropertyImpl(Geo.WKT);

        // Create a GeoSPARQL point literal
        String pointWKT = "<http://www.opengis.net/def/crs/OGC/1.3/CRS84> POINT(2.0 1.0)";

        Literal pointLiteral = con.getModel().createTypedLiteral(pointWKT, "http://www.opengis.net/ont/geosparql#wktLiteral");

        // Add the class type Point
        con.getModel().add(pointResource, RDF.type, con.getModel().createResource("http://www.opengis.net/ont/sf#Point"));

        // Add the point to the model
        con.getModel().add(pointResource, asWKT, pointLiteral);

        // Serialize and print the RDF
        con.getModel().write(System.out, "TURTLE");
    }

    @Test
    public void  testCoordinate() throws  Exception {
        Coordinate coord = new Coordinate(5.2, 0.0);
        // Assert that the coordinate is not null
        assert coord.get_geosparql_point().equals("Point(5.2 0.0)");
    }
}


