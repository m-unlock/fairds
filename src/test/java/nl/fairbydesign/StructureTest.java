package nl.fairbydesign;

import nl.fairbydesign.backend.credentials.Credentials;
import nl.fairbydesign.backend.irods.Data;
import nl.fairbydesign.backend.metadata.Metadata;
import org.apache.log4j.Logger;
import org.irods.jargon.core.exception.JargonException;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

public class StructureTest {
    public static final Logger logger = Logger.getLogger(Metadata.class);

    @Test
    public void testStructure() throws JargonException, IOException {
        Credentials credentials = new Credentials();
        credentials.authenticate();
        if (credentials.isSuccess()) {
            File rdfFile = new File("/wur/home/research-ssb-projects-example/metadata/890a7914-7df3-49ca-94d8-ca5171203a4e.ttl");
            Data.downloadFile(credentials, rdfFile.getAbsolutePath(), "./research-ssb-projects-example.ttl");
            nl.fairbydesign.backend.parsers.Structure.create(credentials, "research-ssb-projects-example.ttl", "research-ssb-projects-example");
        }
    }
}
