package nl.fairbydesign.io;

import nl.fairbydesign.backend.metadata.MetadataParser;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;

public class MetadataTest {

    /**
     * Validates the metadata template structure
     *
     * @throws IOException when the metadata file is not found
     */
    @Test
    public void excelMetadataValidatorTest() throws IOException {
        // Read new metadata file
        InputStream is = MetadataParser.class.getClassLoader().getResourceAsStream("metadata.xlsx");
        if (is == null) {
            throw new IOException("Metadata file not found");
        }
        // Uses the main validation function to validate the terms / packages input sheets
        MetadataParser.excelMetadataValidator(is);
    }
}
