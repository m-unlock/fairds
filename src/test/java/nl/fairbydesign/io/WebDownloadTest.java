/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package nl.fairbydesign.io;

import nl.fairbydesign.Application;
import nl.fairbydesign.backend.wrappers.io.FileDownload;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Testing file download
 * <p>
 *     We check wether a file can be downloaded at all. And whether we can assert the
 *     name of a file from the internet. For each test, files are downloaded and subsequently
 *     deleted.
 * </p>
 */
public class WebDownloadTest {

    @Test
    public void testOntologyDownload() throws Exception {
        String testAdress = "https://raw.githubusercontent.com/EnvironmentOntology/envo/master/envo.owl";

        File testStorage = new File(Application.commandOptions.storage +"fairds_storage/test/envo.owl");
        FileDownload fd = new FileDownload(testAdress);
        fd.download(testStorage, false);
        assert testStorage.exists();
        testStorage.delete();
        assert !testStorage.exists();
        // clean up
        Files.delete(testStorage.toPath().getParent());
    }

    @Test
    public  void  testWebConnectivity() throws Exception {
        String testAdress = "https://gitlab.com/m-unlock/fairds/-/raw/main/ontology/FAIRDS.ttl?ref_type=heads";
        File testStorage = new File("fairds_storage/test/test_download.zip");
        FileDownload fd = new FileDownload(testAdress);
        fd.download(testStorage, true);
        assert testStorage.exists();
        testStorage.delete();
        assert !testStorage.exists();
        // clean up
        Files.delete(testStorage.toPath().getParent());
    }

    @Test
    public void testAnonymousDownload() throws Exception {
        String testAdress = "http://download.systemsbiology.nl/unlock/metadata.xlsx";
        Path testStorage = Paths.get("fairds_storage/test/");
        File t = new File(testStorage + "/metadata.xlsx");
        FileDownload fd = new FileDownload(testAdress);
        fd.download(testStorage, true);
        System.out.println("Checking if " + t + " exists");
        assert t.exists();
        t.delete();
        assert !t.exists();
        // clean up
        Files.delete(testStorage);
    }

    @Test
    public void testFileOverWrite() throws Exception {
        // We download once but don't delete.
        String testAdress = "http://download.systemsbiology.nl/unlock/metadata.xlsx";
        Path testStorage = Paths.get("fairds_storage/test/");
        File t = new File(testStorage + "/metadata.xlsx");
        FileDownload fd = new FileDownload(testAdress);
        fd.download(testStorage, true);
        System.out.println("Checking if " + t + " exists");
        assert t.exists();

        // Checking second download.
        FileDownload fdTwo = new FileDownload(testAdress);
        try {
            fdTwo.download(testStorage, false);
        } catch (Exception e) {
            // we do nothing...
        }
        // Final clean up
        t.delete();
        assert !t.exists();
        // clean up
        Files.delete(testStorage);

    }
}
