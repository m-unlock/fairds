package nl.fairbydesign;

import nl.fairbydesign.backend.config.Config;
import nl.fairbydesign.backend.credentials.Credentials;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class ConfigTest {
    private static final Logger logger = LoggerFactory.getLogger(Credentials.class);

    @Test
    public void testNewConfig() throws IOException {
        Config config = new Config().loadConfig();
        logger.info("Config created");
        logger.info("Term setting: " + config.getMenus().isTerms());
    }
}