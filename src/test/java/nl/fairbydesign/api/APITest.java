package nl.fairbydesign.api;

import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.commons.lang.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class APITest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testFileUploadWithSuccess() throws Exception {
        String url = "http://localhost:" + port + "/api/upload";

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        File testFile = new File("./src/test/resources/meta/BMOCK12.xlsx");
        if (!testFile.exists())
            throw new RuntimeException("Test file not found");
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", new FileSystemResource(testFile));

        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);

        ResponseEntity<byte[]> response = restTemplate.postForEntity(url, requestEntity, byte[].class);
        byte[] responseBody = response.getBody();
        assertThat(responseBody).isNotNull();
        // Save the response to a file to verify the content
        Path outputPath = Files.createTempFile("processed_file", ".ttl");
        Files.write(outputPath, responseBody);
        // Optionally, you can add more assertions to verify the content of the output file
        assertThat(outputPath.toFile()).exists();
        // Read the content of the file
        String content = Files.readString(outputPath);
        // Validate the content of the file
        Domain domain = new Domain("file://" + outputPath);
        // domain.getRDFSimpleCon().getModel().listStatements().forEachRemaining(System.out::println);
        Assertions.assertEquals(542, domain.getRDFSimpleCon().getModel().size());
    }

    @Test
    public void testFileUploadWithFail() throws IOException {
        String url = "http://localhost:" + port + "/api/upload";

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        File testFile = new File("./src/test/resources/meta/ValidationDemo.xlsx");
        if (!testFile.exists())
            throw new RuntimeException("Test file not found");
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", new FileSystemResource(testFile));

        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);

        ResponseEntity<byte[]> response = restTemplate.postForEntity(url, requestEntity, byte[].class);
        // Checks if the response contains the expected headers with a failure.txt
        Assertions.assertTrue(Objects.requireNonNull(StringUtils.join(response.getHeaders().get("Content-Disposition"), "")).contains("form-data; name=\"attachment\"; filename=\"failure.txt\""));
    }

    @Test
    public void testFileUploadWithNoFile() throws IOException {
        String url = "http://localhost:" + port + "/api/upload";

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        File testFile = new File("./fakefile.txt");
        PrintWriter writer = new PrintWriter(testFile);
        writer.println("");
        writer.close();
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", new FileSystemResource(testFile));

        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);

        ResponseEntity<byte[]> response = restTemplate.postForEntity(url, requestEntity, byte[].class);
        byte[] bodyContent = response.getBody();
        assertThat(bodyContent).isNotNull();
        // Convert the byte array to a string
        String content = new String(bodyContent);
        // Check if the response contains the expected message
        Assertions.assertTrue(content.contains("Uploaded file is not an Office Open XML file"));
    }
}
