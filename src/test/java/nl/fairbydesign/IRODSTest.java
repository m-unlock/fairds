package nl.fairbydesign;

import nl.fairbydesign.backend.Generic;
import nl.fairbydesign.backend.credentials.Credentials;
import nl.fairbydesign.backend.data.objects.Process;
import nl.fairbydesign.backend.data.objects.ProcessLevel;
import nl.fairbydesign.backend.irods.Data;
import nl.fairbydesign.backend.irods.ProgressListener;
import org.apache.commons.codec.digest.DigestUtils;
import org.irods.jargon.core.connection.AuthScheme;
import org.irods.jargon.core.connection.IRODSAccount;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.packinstr.DataObjInp;
import org.irods.jargon.core.pub.IRODSAccessObjectFactory;
import org.irods.jargon.core.pub.IRODSFileSystem;
import org.irods.jargon.core.pub.IRODSGenQueryExecutor;
import org.irods.jargon.core.pub.RuleProcessingAO;
import org.irods.jargon.core.pub.io.IRODSFile;
import org.irods.jargon.core.pub.io.IRODSFileOutputStream;
import org.irods.jargon.core.query.*;
import org.irods.jargon.core.rule.*;
import org.junit.Ignore;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.lang.reflect.Field;
import java.util.*;

import static nl.fairbydesign.backend.irods.Data.getIS;
import static nl.fairbydesign.backend.irods.Data.logger;
import static nl.fairbydesign.backend.irods.StreamCopyUtil.copyWithProgress;
import static nl.fairbydesign.backend.irods.StreamCopyUtil.copyWithProgressCoordinated;
import static org.irods.jargon.core.connection.ClientServerNegotiationPolicy.SslNegotiationPolicy.CS_NEG_REQUIRE;
import static org.irods.jargon.core.query.QueryConditionOperators.*;
import static org.irods.jargon.core.query.RodsGenQueryEnum.*;

public class IRODSTest {

    private static final Logger logger = LoggerFactory.getLogger(IRODSTest.class);

    @Disabled
    @Test
    // Outputstream upload
    public void uploadStreamTest() throws JargonException {
        Credentials credentials = new Credentials();
        credentials.setUsingIrodsConnection();
        try {
            credentials.authenticate();
            IRODSFile homeFolder = credentials.getFileFactory().instanceIRODSFile("/" + credentials.getIrodsAccount().getZone() + "/home/" + credentials.getIrodsAccount().getUserName());
            for (File file : homeFolder.listFiles()) {
                System.err.println(file);
                break;
            }
        } catch (JargonException e) {
            throw new RuntimeException(e);
        }

        // iRODS file object
        String testFileSingle = "/" + credentials.getIrodsAccount().getZone() + "/home/" + credentials.getIrodsAccount().getUserName() + "/" + UUID.randomUUID() + "_single";
        String testFileMultiple = testFileSingle.replace("_single", "_multiple");

        // Stream to server to stream to irods including the magic number obtained

        // Create a x mb file
        File localFile = new File(new File(testFileSingle).getName());
        createFile(localFile, 50 * 1024 * 1024); // MB in bytes

        // Upload the file to the server using multiple threads
        try (FileInputStream fileInputStream = new FileInputStream(localFile)) {
            copyWithProgressCoordinated(credentials, fileInputStream, testFileMultiple, new ProgressListener() {
                @Override
                public void update(long bytesRead) {
                    logger.debug("Read: " + Generic.formatSize(bytesRead) + " from " + localFile);
                }
            });
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        // Upload the file to the server using a single thread
        try (FileInputStream fileInputStream = new FileInputStream(localFile)) {
            IRODSFileOutputStream irodsFileOutputStream = credentials.getFileFactory().instanceIRODSFileOutputStream(testFileSingle, DataObjInp.OpenFlags.WRITE);
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(irodsFileOutputStream);

            copyWithProgress(fileInputStream, bufferedOutputStream, new ProgressListener() {
                @Override
                public void update(long bytesRead) {
                    logger.info("Read: " + Generic.formatSize(bytesRead) + " from " + localFile);
                }
            });
            // iRODS get checksum
            String checksum = credentials.getAccessObjectFactory().getDataObjectAO(credentials.getIrodsAccount()).computeMD5ChecksumOnDataObject(credentials.getFileFactory().instanceIRODSFile(testFileSingle));
            System.err.println("Checksum chunks irods: " + checksum);

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void createFile(File filePath, long fileSize) {
        try (FileOutputStream fos = new FileOutputStream(filePath)) {
            byte[] buffer = new byte[1024]; // Buffer size (1 KB in this case)

            long bytesWritten = 0;
            while (bytesWritten < fileSize) {
                fos.write(buffer);
                bytesWritten += buffer.length;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            String md5Hex = DigestUtils.md5Hex(new FileInputStream(filePath)).toLowerCase();
            System.err.println("Checksum local (MD5): " + md5Hex);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }



    }

    public static void printObject(Object obj) {
        Class<?> clazz = obj.getClass();
        Field[] fields = clazz.getDeclaredFields();

        for (Field field : fields) {
            field.setAccessible(true);
            String fieldName = field.getName();
            Object fieldValue;

            try {
                fieldValue = field.get(obj);
            } catch (IllegalAccessException e) {
                fieldValue = "N/A";
            }

            System.out.println(fieldName + " = " + fieldValue);
        }
    }

    @Disabled
    @Test
    public void isTest() throws JargonException {
        Credentials credentials = new Credentials();
        credentials.setUsingIrodsConnection();
        // Test credentials needs to be set up
        if (credentials.getUsername().equalsIgnoreCase("xxxxx"))
            return;
        credentials.authenticate();
        if (credentials.isSuccess()) {
            logger.info(credentials.getIrodsAccount().getUserName());
            ArrayList<Process> processes = getIS(credentials);
            for (Process process : processes) {
                logger.info(process.getPath());
            }
        } else {
            throw new JargonException("Authentication failed");
        }
    }

    @Disabled
    @Test
    public void yodaWURTest() throws JargonException {
        Credentials credentials;
        try {
            credentials = new Credentials();
            credentials.setHost("yoda.wur.nl");
            credentials.setPassword("data_access_password");
            credentials.setUsername("yoda_email_account");
            credentials.setPort(1247);
            credentials.setZone("wur");
            credentials.setSslNegotiationPolicy(CS_NEG_REQUIRE.toString());
            credentials.setAuthenticationScheme(AuthScheme.PAM.name());
            credentials.authenticate();
        } catch (org.irods.jargon.core.exception.AuthenticationException authenticationException) {
            logger.error("Authentication failed");
            return;
        }

        if (credentials.isSuccess()) {
            logger.info("/" + credentials.getIrodsAccount().getZone() + "/home/");
            for (File file : credentials.getFileFactory().instanceIRODSFile("/" + credentials.getIrodsAccount().getZone() + "/home/").listFiles()) {
                logger.info(file.getAbsolutePath());
            }
        } else {
            throw new JargonException("Authentication failed");
        }

        if (credentials.isSuccess()) {
            logger.info("QUEST");
            // Obtain all projects via metadata
            IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);
            // iquest --no-page "%s %s %s" "SELECT COLL_NAME, META_DATA_ATTR_VALUE, META_DATA_ATTR_UNITS WHERE
            // COLL_NAME NOT LIKE '/unlock/references%'
            // AND
            // COLL_NAME NOT LIKE '/unlock/trash%' AND
            // DATA_NAME LIKE '%.yaml'"
            queryBuilder.addConditionAsGenQueryField(COL_COLL_NAME, EQUAL, "/wur/home/");
//            queryBuilder.addConditionAsGenQueryField(COL_DATA_NAME, LIKE, "%.yaml");

            try {
                queryBuilder.addSelectAsGenQueryValue(COL_COLL_NAME);
                queryBuilder.addSelectAsGenQueryValue(COL_DATA_NAME);
                IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(999999);
                IRODSFileSystem irodsFileSystem = new IRODSFileSystem();
                IRODSGenQueryExecutor irodsGenQueryExecutor = irodsFileSystem.getIRODSAccessObjectFactory().getIRODSGenQueryExecutor(credentials.getIrodsAccount());
                IRODSQueryResultSet irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);
                List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();
                for (IRODSQueryResultRow queryResultRow : irodsQueryResultSetResults) {
                    System.err.println(">>" + queryResultRow.getColumn(0));
                }
            } catch (GenQueryBuilderException | JargonQueryException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Disabled
    @Test
    public void progressTest() throws JargonException, GenQueryBuilderException, JargonQueryException {
        Credentials credentials = new Credentials();
        credentials.setUsingIrodsConnection();
        credentials.authenticate();
        if (credentials.isSuccess()) {
            // Obtain all home folders
            File[] homeFolders = credentials.getFileFactory().instanceIRODSFile("/" + credentials.getIrodsAccount().getZone() + "/home/").listFiles();
            ArrayList<ProcessLevel> processes = new ArrayList<>();
            for (File homeFolder : homeFolders) {
                ProcessLevel processLevel = new ProcessLevel();
                processes.add(processLevel);
                processLevel.setName(homeFolder.getName());
                // Obtain all job files via metadata
                IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);
                queryBuilder.addConditionAsGenQueryField(COL_COLL_NAME, LIKE, homeFolder.getAbsolutePath() + "/%");
                queryBuilder.addConditionAsGenQueryField(COL_DATA_NAME, LIKE, "%.yaml");
                // Change to status in the future once this is properly established
                queryBuilder.addConditionAsGenQueryField(COL_META_DATA_ATTR_NAME, LIKE, "%");
                queryBuilder.addSelectAsGenQueryValue(COL_META_DATA_ATTR_NAME);
                queryBuilder.addSelectAsGenQueryValue(COL_META_DATA_ATTR_VALUE);
                queryBuilder.addSelectAsGenQueryValue(COL_COLL_NAME);
                queryBuilder.addSelectAsGenQueryValue(COL_DATA_NAME);
                IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(999999);
                IRODSFileSystem irodsFileSystem = new IRODSFileSystem();
                IRODSGenQueryExecutor irodsGenQueryExecutor = irodsFileSystem.getIRODSAccessObjectFactory().getIRODSGenQueryExecutor(credentials.getIrodsAccount());
                IRODSQueryResultSet irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);
                List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();
                for (IRODSQueryResultRow queryResultRow : irodsQueryResultSetResults) {
                    // Status retrieval
                    String metadataName = queryResultRow.getColumn(0);
                    String metadataValue = queryResultRow.getColumn(1);
                    String folder = queryResultRow.getColumn(2);
                    String path = queryResultRow.getColumn(2) + "/" + queryResultRow.getColumn(3);
                    // Parent name of the folder
                    String parentType = folder.substring(folder.lastIndexOf("/") + 1).split("_")[0];
                    // Can also use irods for this but that might be much slower to request
                    if (parentType.equalsIgnoreCase("inv"))
                        processLevel.setInvestigation(processLevel.getInvestigation() + 1);
                    else if (parentType.equalsIgnoreCase("stu"))
                        processLevel.setStudy(processLevel.getStudy() + 1);
                    else if (parentType.equalsIgnoreCase("obs"))
                        processLevel.setObservationUnit(processLevel.getObservationUnit() + 1);
                    else if (parentType.equalsIgnoreCase("sam"))
                        processLevel.setSample(processLevel.getSample() + 1);
                    else if (parentType.equalsIgnoreCase("asy"))
                        processLevel.setAssay(processLevel.getAssay() + 1);
                    else if (parentType.equalsIgnoreCase("unprocessed"))
                        processLevel.setUnprocessed(processLevel.getUnprocessed() + 1);
                    else
                        throw new RuntimeException("Unknown parent type: " + parentType);
                }
                // Set to which level the yaml file is located at
                // Do a query to obtain all unprocessed folders or data folders
                queryBuilder = new IRODSGenQueryBuilder(true, null);
                queryBuilder.addConditionAsGenQueryField(COL_COLL_NAME, LIKE, homeFolder.getAbsolutePath() + "/%unprocessed");
                // Change to status in the future once this is properly established
                queryBuilder.addSelectAsGenQueryValue(COL_COLL_NAME);
                query = queryBuilder.exportIRODSQueryFromBuilder(999999);
                irodsFileSystem = new IRODSFileSystem();
                irodsGenQueryExecutor = irodsFileSystem.getIRODSAccessObjectFactory().getIRODSGenQueryExecutor(credentials.getIrodsAccount());
                irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);
                irodsQueryResultSetResults = irodsQueryResultSet.getResults();
                processLevel.setUnprocessed(irodsQueryResultSetResults.size());
                logger.debug(homeFolder.getName());
                printObject(processLevel);
            }
            // Return processes
        }
    }

    @Disabled
    @Test
    public void isTest2() {
        Credentials credentials = new Credentials();
        credentials.setUsingIrodsConnection();
        try {
            credentials.authenticate();
        } catch (JargonException e) {
            throw new RuntimeException(e);
        }
        if (credentials.isSuccess()) {
            ArrayList<Process> processes = Data.getIS(credentials);
        }
    }

    @Test
    public void testArchive() throws JargonException {
        Credentials credentials = new Credentials();
        credentials.setUsingIrodsConnection();
        try {
            credentials.authenticate();
        } catch (JargonException e) {
            throw new RuntimeException(e);
        }
        if (credentials.isSuccess()) {
            // Obtain all files larger than 1 GB
            ArrayList<String> to_archive = new ArrayList<>();
            HashSet<String> already_archived = new HashSet<>();
            try {
                IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);
                queryBuilder.addConditionAsGenQueryField(COL_DATA_SIZE, QueryConditionOperators.GREATER_THAN, "1073741824");
                queryBuilder.addConditionAsGenQueryField(COL_COLL_NAME, LIKE, "%wur.gutbrain_axis/stu_%");
                queryBuilder.addConditionAsGenQueryField(COL_COLL_NAME, NOT_LIKE, "%stu_mind-set%");
                queryBuilder.addConditionAsGenQueryField(COL_META_DATA_ATTR_NAME, EQUAL, "archive_status");
                queryBuilder.addSelectAsGenQueryValue(COL_DATA_NAME);
                queryBuilder.addSelectAsGenQueryValue(COL_COLL_NAME);
                IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(999999);
                IRODSFileSystem irodsFileSystem = new IRODSFileSystem();
                IRODSGenQueryExecutor irodsGenQueryExecutor = irodsFileSystem.getIRODSAccessObjectFactory().getIRODSGenQueryExecutor(credentials.getIrodsAccount());
                IRODSQueryResultSet irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);
                List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();
                for (IRODSQueryResultRow irodsQueryResultSetResult : irodsQueryResultSetResults) {
                    String path = irodsQueryResultSetResult.getColumn(1) + "/" + irodsQueryResultSetResult.getColumn(0);
                    already_archived.add(path);
                }

                queryBuilder = new IRODSGenQueryBuilder(true, null);
                queryBuilder.addConditionAsGenQueryField(COL_DATA_SIZE, QueryConditionOperators.GREATER_THAN, "1073741824");
                queryBuilder.addConditionAsGenQueryField(COL_COLL_NAME, LIKE, "%wur.gutbrain_axis/stu_%");
                queryBuilder.addConditionAsGenQueryField(COL_COLL_NAME, NOT_LIKE, "%stu_mind-set%");
                queryBuilder.addSelectAsGenQueryValue(COL_DATA_NAME);
                queryBuilder.addSelectAsGenQueryValue(COL_COLL_NAME);
                query = queryBuilder.exportIRODSQueryFromBuilder(999999);
                irodsFileSystem = new IRODSFileSystem();
                irodsGenQueryExecutor = irodsFileSystem.getIRODSAccessObjectFactory().getIRODSGenQueryExecutor(credentials.getIrodsAccount());
                irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);
                irodsQueryResultSetResults = irodsQueryResultSet.getResults();
                for (IRODSQueryResultRow irodsQueryResultSetResult : irodsQueryResultSetResults) {
                    String path = irodsQueryResultSetResult.getColumn(1) + "/" + irodsQueryResultSetResult.getColumn(0);
                    to_archive.add(path);
                }
            } catch (GenQueryBuilderException | JargonQueryException e) {
                throw new RuntimeException(e);
            }

            for (int i = 0; i < to_archive.size(); i++) {
                logger.info("Archiving: " + i + " of " + to_archive.size());
                String filePath = to_archive.get(i);
                if (already_archived.contains(filePath)) {
                    logger.info("Already archived: " + filePath);
                    continue;
                }
                // irule -r irods_rule_engine_plugin-irods_rule_language-instance rdm_archive_this '*file_or_collection=/RDMtest/home/user/somecollection' ruleExecOut
                IRODSAccount acct = credentials.getIrodsAccount();

                IRODSFileSystem fsys = IRODSFileSystem.instance();
                IRODSAccessObjectFactory aof = fsys.getIRODSAccessObjectFactory();

                StringBuilder sb = new StringBuilder();
                sb.append("main { rdm_archive_this(); }\n");
                sb.append("INPUT *file_or_collection=\n");
                sb.append("OUTPUT ruleExecOut");

                List<IRODSRuleParameter> params = new ArrayList<>();

                params.add(new IRODSRuleParameter("*file_or_collection", filePath));

                RuleProcessingAO rpao = aof.getRuleProcessingAO(acct);
                RuleInvocationConfiguration ctx = new RuleInvocationConfiguration();
                ctx.setIrodsRuleInvocationTypeEnum(IrodsRuleInvocationTypeEnum.OTHER);
                ctx.setRuleEngineSpecifier("irods_rule_engine_plugin-irods_rule_language-instance");

                try {
                    rpao.executeRule(sb.toString(), params, ctx);
                } catch (JargonException e) {
                    if (e.getMessage().contains(filePath + " does not have a checksum")) {
                        System.err.println("File does not have a checksum");
                        // Calculate checksum
                        String checksum = credentials.getAccessObjectFactory().getDataObjectAO(credentials.getIrodsAccount()).computeMD5ChecksumOnDataObject(credentials.getFileFactory().instanceIRODSFile(filePath));
                        System.err.println("Checksum: " + checksum);
                        rpao.executeRule(sb.toString(), params, ctx);
                    } else if (e.getMessage().contains(filePath + " already has an archive tag")) {
                        System.err.println("File already has an archive tag");
                    } else {
                        throw new RuntimeException(e);
                    }
                } finally {
                    aof.closeSessionAndEatExceptions();
                    fsys.closeAndEatExceptions();
                }
            }
        }
    }
}
