package nl.fairbydesign.rdf;

import nl.fairbydesign.backend.Generic;
import nl.wur.ssb.RDFSimpleCon.RDFFormat;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import nl.wur.ssb.RDFSimpleCon.api.OWLThing;
import org.jermontology.ontology.JERMOntology.domain.Data_sample;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.UUID;

import static nl.fairbydesign.backend.parsers.ExcelValidator.validateRDF;

public class RDFTest {

    @Test
    public void test() {
        ArrayList<String> formats = new ArrayList<>();
        for (RDFFormat value : RDFFormat.values()) {
            formats.add(value.toString());
        }
        formats.forEach(System.out::println);
    }
    
    @Test
    public void urlStringCheck() throws Exception {
        String PREFIX = "http://fairbydesign.nl";
        Domain domain = new Domain("");
        String filePath = "https://data.yoda.wur.nl/research-bioindustry/Use cases/Legacy datasets/Pichia Pastoris/UAB/Set Data Fermentations Pichia Pastoris UAB/FBHPX12_UAB.xlsx";
        Data_sample data_sample;
        if (Generic.isValidURL(filePath)) {
            // Store as URL
            URI uri;
            try {
                uri = URI.create(filePath);
            } catch (IllegalArgumentException e) {
                // Encode the URL path
                String filePathEncoded = filePath.replaceAll(" ", "%20");
                uri = URI.create(filePathEncoded);
            }
            data_sample = domain.make(Data_sample.class, uri.toString());
            data_sample.setContentUrl(uri.toString());
        } else if (!new File(filePath).getName().equalsIgnoreCase(filePath)) {
            // For unsupported protocols e.g. webdav:
            // Making sure root / is removed
            filePath = filePath.replace("^/", "");
            if (filePath.matches("^[a-z]+:.*")) {
                data_sample = domain.make(Data_sample.class, filePath);
                data_sample.setContentUrl(filePath);
            } else if (filePath.contains("/")) {
                // If it is a path due to / content
                data_sample = domain.make(Data_sample.class, "file://" + filePath.replaceAll("/+", "/"));
                data_sample.setContentUrl("file://" + filePath.replaceAll("/+", "/"));
            } else {
                data_sample = domain.make(Data_sample.class, ("file://" + UUID.randomUUID().toString() + "/" + filePath).replaceAll("/+", "/"));
                data_sample.setContentUrl("file:/" + filePath);
            }
        } else {
            data_sample = domain.make(Data_sample.class, PREFIX.replaceAll("/$", "") + "/data_sample/" + new File(filePath).getName().replaceAll("/+", "/"));
        }
        domain.getRDFSimpleCon().getModel().listStatements().forEachRemaining(System.out::println);
        domain.save("test.ttl");
        validateRDF("test.ttl");
    }
}
