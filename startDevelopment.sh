#!/bin/bash
#============================================================================
#title          :Unlock Interface Project
#description    :Project web and gui interface combined with a library
#author         :Jasper Koehorst
#date           :2021
#version        :0.0.1
#============================================================================
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

git -C $DIR pull

# First remove the node modules as this could be from dev...
rm -rf web/node_modules/

# Build in production mode
./gradlew build

# Start
java -jar -Dserver.port=8082 build/libs/data-interface-1.0.1.jar